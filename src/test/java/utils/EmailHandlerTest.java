package utils;

import com.domain.myenum.DeliveryTypeEnum;
import com.domain.myenum.ProductUnitEnum;
import com.dto.OrderExtDto;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.handler.email.Email;
import com.handler.email.EmailHandler;
import com.service.advice.AdviceService;
import com.service.order.OrderService;
import com.utils.DateUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

/**
 * Created by Huoyunren on 2016/2/15.
 */
public class EmailHandlerTest extends service.BaseTest {
  private static final Logger LOG = LoggerFactory.getLogger(EmailHandlerTest.class);

  @Autowired
  private EmailHandler emailHandler;
  @Autowired
  private OrderService orderService;
  @Autowired
  private AdviceService adviceService;


  @Test
  public void auction() throws Exception {
    Email email = new Email();
    String[] to = new String[1];
    to[0] = "mrliufox@foxmail.com";
    email.setTo(to);
    email.setSubject("拍卖通知");
    email.setTemplate("auction.ftl");
    Map<String, Object> map = Maps.newHashMap();
    map.put("dealPrice", "1.50元");
    map.put("dealPhone", "17608013786");
    map.put("dealEmail", "mrliufox@foxmail.com");
    map.put("dealName", "刘员外");
    email.setMap(map);
    email.setEnableHtml(Boolean.TRUE);
    emailHandler.asyncSendMail(email);
    try {
      Thread.sleep(100000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void reward() throws Exception {
    String rewardEmail = "834989221@qq.com";
    String dealName = "刘员外";
    String dealPhone = "17687654323";
    String dealTime = DateUtils.currentTime();
    String dealEmail = "mrliufox@foxmail.com";
    adviceService.rewardNotify(rewardEmail, dealName, dealPhone, dealTime, dealEmail);
    try {
      Thread.sleep(100000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void incomeTemplateEmail() {
    orderService.notifyReferralIncomeChangeByEmail("mrliufox@foxmail.com", BigDecimal.ONE, "alisa.wang", "卤肉面", "老马家面馆", DateUtils.currentDate());
    try {
      Thread.sleep(100000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void sendOrderTemplateEmail() {
    String subject = "新订单通知";
    Set<String> emailSet = Sets.newHashSet("mrliufox@foxmail.com");
    OrderExtDto orderExtDto = initOrderExtDto();
    orderExtDto.setDeliveryType(String.valueOf(DeliveryTypeEnum.SendHome.getCode()));
    orderService.orderNotifyEmail(orderExtDto, subject, emailSet);
    try {
      Thread.sleep(100000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private OrderExtDto initOrderExtDto() {
    OrderExtDto orderExtDto = new OrderExtDto();
    orderExtDto.setCompanyName("刘员外");
    orderExtDto.setPhone("17608013786");
    orderExtDto.setProductName("一碗香");
    orderExtDto.setPrice(BigDecimal.TEN);
    orderExtDto.setUnit(String.valueOf(ProductUnitEnum.Piece.getCode()));
    orderExtDto.setCount(1);
    orderExtDto.setShopName("刘员外");
    orderExtDto.setAmount(BigDecimal.TEN);
    orderExtDto.setRemark("");
    orderExtDto.setSendtime(DateUtils.currentDate());
    orderExtDto.setSendAddr("长虹科技大厦");
    orderExtDto.setEmail("834989221@qq.com");
    return orderExtDto;
  }

  @Test
  public void getBySelfTemplateEmail() {
    String subject = "新订单通知";
    Set<String> emailSet = Sets.newHashSet("mrliufox@foxmail.com");
    OrderExtDto orderExtDto = initOrderExtDto();
    orderExtDto.setDeliveryType(String.valueOf(DeliveryTypeEnum.GetBySelf.getCode()));
    orderService.orderNotifyEmail(orderExtDto, subject, emailSet);
    try {
      Thread.sleep(100000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void sendEmail() {
    Email email = new Email();
    String[] to = new String[1];
    to[0] = "mrliufox@foxmail.com";
    email.setTo(to);
    email.setSubject("您有新的收益，请注意查收");
    email.setContent("测试邮件");
    email.setEnableHtml(Boolean.FALSE);
    emailHandler.asyncSendMail(email);
    try {
      Thread.sleep(100000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }


}
