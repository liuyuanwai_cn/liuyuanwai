<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK"/>
    <style>
        * {
            font-size: 14px;
            font-family: "Microsoft YaHei", sans-serif;
        }
    </style>
</head>
<body>
<div align="center" style="background-color: #f5f5f5;padding: 20px 5px;">
    <table style="background-color: #ffffff;width: inherit;border-top: 10px solid #d3d3d3;border-bottom: 10px solid #d3d3d3;padding: 10px">
        <th style="padding-bottom: 10px" align="left">${subject?if_exists}</th>
        <tr>
            <td>
                <hr style="width: 100%;border: 1px solid #f5f5f5">
            </td>
        </tr>
        <tr>
            <td style="">商家：<a href="www.liuyuanwai.cn"style="text-decoration: none">${shopName?if_exists}</a></td>
        </tr>
        <tr>
            <td style="">商品：${goodName?if_exists}</td>
        </tr>
        <tr>
            <td style="">单价：${price?if_exists}</td>
        </tr>
        <tr>
            <td style="padding:0px;">
                <hr style="border:1px solid #f5f5f5;">
            </td>
        </tr>
        <tr>
            <td style="">购买数量：${count?if_exists}</td>
        </tr>
        <tr>
            <td style="">用户姓名：${buyer?if_exists}</td>
        </tr>
        <tr>
            <td style="">联系电话：${mobile?if_exists}</td>
        </tr>
        <tr>
            <td style="">用户留言：${remark?if_exists}</td>
        </tr>
        <tr>
            <td>提货方式：${deliveryType?if_exists}</td>
        </tr>
        <tr style="">
            <td style=";">取货时间：${sendTime?if_exists}</td>
        </tr>
        <tr style="">
            <td style=";">备注：${reason?if_exists}</td>
        </tr>
        <tr>
            <td style="padding:0px;">
                <hr style="border:1px solid #f5f5f5;">
            </td>
        </tr>
        <tr style="">
            <td style=";">总金额：${amount?if_exists}</td>
        </tr>
        <tr>
            <td align="center" style="padding: 15px 5px 0px 0px;">
                <a href="www.liuyuanwai.cn" style="color: darkgray;font-size: 12px">
                    查看详情
                </a>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="width: 100%">
                    <td align="center" style="width: 150px">
                        <p>
                            <a href="www.liuyuanwai.cn">
                                <img src="http://shwxyt-10007160.image.myqcloud.com/7564a46b-3380-43e7-9b9c-738c4049284c"
                                     style="width: 80px">
                            </a>
                        </p>

                        <p style="font-size: 12px">专注互联网生活体验</p>
                    </td>
                    <td align="center" style="width: 150px">
                        <img src="http://shwxyt-10007160.image.myqcloud.com/178bde72-feaf-45ae-8b00-e2884ba7a073"
                             style="width: 80px">
                    </td>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
