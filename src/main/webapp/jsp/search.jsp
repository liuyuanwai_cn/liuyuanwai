<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html lang="zh-CN">
<head>
  <title>搜索 - 刘员外</title>

  <jsp:include page="common/common.jsp"></jsp:include>

  <style type="text/css">
    .circle {
      width: 75px;
      height: 75px;
      border-radius: 100px;
      background-color: #33CCCC;
      color: #ffffff;
    }

    hr {
      margin: 10px 0px;
    }
  </style>

</head>

<body style="background-color: #f4f7ed">
<div class="container-fluid" style="padding: 0px;">
  <div class="row-fluid" style="padding: 0px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
         style="background-color: #e8e8e8;padding: 0px 10px">
      <table style="width: 100%;margin: 10px 0px">
        <tr>
          <td style="width: 100%">
            <input class="searchBox" style="width: 100%;padding: 5px;border-radius: 3px;border: none #ffffff;"
                   type="text" placeholder="输入关键字 ...">
          </td>
          <td style="">
            <a href="<%=basePath%>/" class="btn btn-default"
               style="border: none;color: #009999;background-color: #e8e8e8">取&nbsp;消</a>
          </td>
        </tr>
      </table>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 searchItem" align="center" style="padding: 0px;margin-top: 10%">
      <div class="col-xs-0 col-sm-3 col-md-3 col-lg-3"></div>
      <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2" align="center" style="padding: 0px">
        <p>
        <table class="circle">
          <tr>
            <td align="center"><i class="icon-bookmark-empty icon-2x"></i></td>
          </tr>
        </table>
        </p>
        <p>文章</p>
      </div>
      <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2" align="center" style="padding: 0px">
        <p>
        <table class="circle">
          <tr>
            <td align="center"><i class="icon-tags icon-2x"></i></td>
          </tr>
        </table>
        </p>

        <p>商品</p>
      </div>
      <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2" align="center" style="padding: 0px">
        <p>
        <table class="circle">
          <tr>
            <td align="center"><i class="icon-user icon-2x"></i></td>
          </tr>
        </table>
        </p>

        <p>小伙伴</p>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 searchResult"
         style="padding: 0px;display: none;margin-bottom: 100px">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 storyBox"
           style="background-color: #ffffff;margin-top: 15px;padding: 10px;display: none">
        <p>文章</p>
        <hr>
        <p class="stories"></p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 productBox"
           style="background-color: #ffffff;margin-top: 15px;padding: 10px;display: none">
        <p>商品</p>
        <hr>
        <p class="products"></p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 companyBox"
           style="background-color: #ffffff;margin-top: 15px;padding: 10px;display: none">
        <p>小伙伴</p>
        <hr>
        <p class="companies"></p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center NoResult"
           style="padding: 100px 0px;color: #CCCCCC;display: none">
        <h3>Nothing Found</h3>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="loadModal" role="dialog" style="margin-top: 10%;z-index: 10003;">
  <div class="modal-dialog modal-sm" style="">
    <div class="modal-content" style="">
      <div class="modal-body text-center">
        <img src="<%=basePath%>/img/loading.gif">
      </div>
    </div>
  </div>
</div>


<script type="application/javascript">

  function buildStories(stories) {
    var loginEmail = '<%=email%>';
    if (stories.length > 0) {
      for (var i = 0; i < stories.length; i++) {
        var story = stories[i];
        var img = story.imgUrl;
        var email = story.email;
        if (img == undefined || img == '') {
          img = '<%=basePath%>/img/lyw.jpg';
        } else {
          img += '-360x200'
        }
        var subTitle = story.subTitle;
        if (subTitle == undefined) {
          subTitle = '';
        }
        var temp = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding-top:0px;padding-bottom: 0px">';
        temp += '<a style="text-decoration: none" href="<%=basePath%>/story/info?uuid=' + story.uuid + '">';
        temp += '<div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 "style="padding: 0px" align="center" >';
        temp += '<img src="' + img + '" class="img-responsive">'
        temp += '</div>';
        temp += '<div class="col-xs-9 col-sm-9 col-md-10 col-lg-11 "style="padding: 0px 0px 0px 10px;">';
        temp += '<p style="color: #000000;font-weight: bold;margin: 3px 0px">' + story.title + '</p>';
        temp += '<p style="color: #999999;margin: 3px 0px">' + subTitle + '</p>';
        temp += '<p style="color: #000000;margin: 3px 0px">';
        temp += '<small>阅读：' + story.readCount + '</small>';
        if (loginEmail != undefined && loginEmail != '' && email == loginEmail) {
          temp += '<span style="padding: 5px 10px;"align="right"><a style="color: red;" href="javascript:delStory(' + stories[i].id + ');"><small>删除</small></a></span>';
        }
        temp += '</p>';
        temp += '</div>';
        temp += '</a>';
        temp += '</div>';
        temp += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding: 0px"><hr style="width: 100%;"></div>'
        $(".stories").append(temp);
      }
      $('.storyBox').show();
    }
  }

  function delStory(storyId) {
    var backUrl = '<%=basePath%>/story';
    var url = '<%=basePath%>/story/del';
    var param = {bulletinId: storyId};
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        window.location.href = '<%=basePath%>/search';
      } else {
        aler(data);
      }
    })
  }

  function buildProducts(products) {
    if (products.length > 0) {
      for (var i = 0; i < products.length; i++) {
        var product = products[i];
        var img = product.url;
        if (img == undefined || img == '') {
          img = '<%=basePath%>/img/lyw.jpg';
        } else {
          img += '-360x200'
        }
        var temp = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding-top:0px;padding-bottom: 0px">';
        temp += '<a style="text-decoration: none" href="<%=basePath%>/order/preadd?productId=' + product.id + '">';
        temp += '<div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 "style="padding: 0px" align="center" >';
        temp += '<img src="' + img + '" class="img-responsive">'
        temp += '</div>';
        temp += '<div class="col-xs-9 col-sm-9 col-md-10 col-lg-11 "style="padding: 0px 0px 0px 10px;">';
        temp += '<p>' + product.name + '</p>';
        temp += '<p style="color: red">￥' + product.price + '</p>';
        temp += '</div>';
        temp += '</a>';
        temp += '</div>';
        temp += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding: 0px"><hr style="width: 100%;"></div>'
        $(".products").append(temp);
      }
      $('.productBox').show();
    }
  }
  function buildCompanies(companies) {
    if (companies.length > 0) {
      for (var i = 0; i < companies.length; i++) {
        var company = companies[i];
        var img = company.logo;
        if (img == undefined || img == '') {
          img = '<%=basePath%>/img/user.png';
        } else {
          img += '-120x120'
        }
        var temp = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding-top:0px;padding-bottom: 0px">';
        temp += '<a style="text-decoration: none" href="<%=basePath%>/user/queryinfo?email=' + company.email + '">'
        temp += '<table>';
        temp += '<tr>';
        temp += '<td>';
        temp += '<img src="' + img + '" style="width:40px;height:40px" class="img-responsive img-circle">'
        temp += '</td>';
        temp += '<td style="padding-left: 10px">';
        temp += '<div>' + company.name + '</div>';
        temp += '<div>' + company.email + '</div>';
        temp += '</td>';
        temp += '</tr>';
        temp += '</table>';
        temp += '</a>';
        temp += '</div>';
        temp += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding: 0px"><hr style="width: 100%;"></div>'
        $(".companies").append(temp);
      }
      $('.companyBox').show();
    }
  }

  function hideSearchResult() {
    $('.searchResult').slideUp(function () {
      $('.searchItem').slideDown();
    });
  }

  function showSearchResult() {
    $('.searchItem').slideUp(function () {
      $('.searchResult').slideDown();
    });
  }

  function clearSearchResult() {
    $(".stories").empty();
    $(".products").empty();
    $(".companies").empty();
    $(".storyBox").hide();
    $(".productBox").hide();
    $(".companyBox").hide();
  }


  function search(keyword) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    clearSearchResult();
    var url = '<%=basePath%>/search/search';
    var param = {keyword: keyword};
    $.post(url, param, function (data, status) {
      $("#loadModal").modal('hide');
      showSearchResult();
      var stories = data.stories;
      var products = data.products;
      var companies = data.companies;
      if (stories.length == 0 && products.length == 0 && companies.length == 0) {
        $('.NoResult').slideDown();
      } else {
        $('.NoResult').slideUp(function () {
          buildStories(stories);
          buildProducts(products);
          buildCompanies(companies);
        });
      }
    })
  }

  $(function () {
    $('.searchBox').focus();
    $('.searchBox').change(function () {
      var keyword = $.trim($('.searchBox').val());
      if (keyword != '') {
        search(keyword);
      } else {
        hideSearchResult();
      }
    })
  })
</script>
</body>

</html>
