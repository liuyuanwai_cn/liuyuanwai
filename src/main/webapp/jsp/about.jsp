<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
  <title>关于 - 刘员外</title>
  <jsp:include page="common/common-css.jsp"></jsp:include>
  <jsp:include page="common/common-js.jsp"></jsp:include>

  <%--<link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/extension/simditor-markdown.css"/>--%>
  <%--<link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/extension/marked.js"/>--%>
  <%--<link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/extension/to-markdown.js"/>--%>
  <%--<link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/extension/simditor-mark.js"/>--%>
  <%--<link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/extension/simditor-markdown.css"/>--%>

  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/simditor.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/module.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/hotkeys.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/uploader.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/simditor.min.js"></script>
  <%--<script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/simditor-markdown.js"></script>--%>
  <%--<script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/simditor-marked.js"></script>--%>

</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 20px 0px;">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
    <textarea id="editor" placeholder="simditor" autofocus style="border: 1px solid red"></textarea>
  </div>
</div>
<jsp:include page="../foot.jsp"></jsp:include>
</body>
<script>
  $(function () {
    initStyle();

    var editor = new Simditor({
      textarea: $('#editor'),
      toolbar: [
        'title',
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'fontScale',
        'color',
        'ol',
        'ul',
        'blockquote',
        'code',
        'table',
        'link',
        'image',
        'hr',
        'indent',
        'outdent',
        'alignment'
      ],
      upload: {
        url: "<%=basePath%>/img/upload",
        params: {
          fileElementId: "filePath",//file标签的id,
          dateType: "text"
        },
        fileKey: "filePath",
        connectionCount: 1
      },
      success: function(data) {
        alert(data);
      }
    });

  })
</script>
</html>
