<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<!DOCTYPE HTML>
<html>
<head>
  <title>${story.title} - 刘员外</title>
  <meta name="keywords" content="${story.subTitle}"/>
  <meta name="description" content="${story.subTitle}"/>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>


  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/simditor.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/module.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/hotkeys.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/uploader.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/simditor.min.js"></script>

  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    blockquote {
      background-color: #f9f9f9;
    }
  </style>
</head>
<body style="background-color: #f4f7ed;padding: 0px">
<%--<jsp:include page="../../head.jsp"></jsp:include>--%>
<div class="container-fluid" style="padding: 0px;">
  <div class="col-xs-0 col-sm-0 col-md-2 col-lg-3"></div>
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6 "
       style="background-color: #ffffff;padding: 5px;margin-top: 0px;">
    <table style="width: 100%">
      <tr>
        <td style="padding: 10px;">
          <table style="width: 100%;">
            <tr>
              <td style="width: 60px;height: 60px;padding: 10px">
                <a href="<%=basePath%>/user/queryinfo?email=${story.email}">
                  <c:choose>
                    <c:when test="${story.logo != nul and story.logo != ''}">
                      <img src="${story.logo}-120x120" style="width: 40px;height: 40px"
                           class="img-responsive img-circle">
                    </c:when>
                    <c:otherwise>
                      <img src="<%=basePath%>/img/user.png" style="width: 40px;height: 40px"
                           class="img-responsive img-circle">
                    </c:otherwise>
                  </c:choose>
                </a>
              </td>
              <td style="padding: 10px">
                <div>
                  <a href="<%=basePath%>/user/queryinfo?email=${story.email}">
                    <small><span class="label label-default"
                                 style="background-color: #ffffff;color: #FF6666;border: 1px solid #FF6666">${story.name}</span>
                    </small>
                  </a>
                </div>
                <div style="color: #999999;margin-top: 5px">
                  <small>${story.remark}</small>
                </div>
              </td>
              <td align="right" style="padding: 10px;">
                <a class="btn btn-info btn-sm addAttention" href="javascript:void(0);"
                   onclick="addAttention('${story.email}')">
                  <i class="icon-heart-empty">
                    <small>&nbsp;|&nbsp;关注</small>
                  </i>
                </a>
                <a class="btn btn-danger btn-sm cancelAttention" style="display: none;"
                   onclick="cancelAttention('${story.email}')" href="javascript:void(0);">
                  <i class="icon-minus-sign">
                    <small>&nbsp;|&nbsp;取消关注</small>
                  </i>
                </a>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <hr style="margin: 0px;">
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td style="padding: 10px;">
          <h3>
            ${story.title}
          </h3>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="padding: 10px;color: #999999">
          <small>${story.createTime}&nbsp;&nbsp;阅读：${story.readCount}</small>
        </td>
      </tr>
      <tr>
        <td style="padding: 10px;">
          <blockquote style="padding-left: 5px">
            ${story.subTitle}
          </blockquote>
        </td>
      </tr>
    </table>
    <table style="width: 100%;margin-bottom: 100px">
      <tr>
        <td style="padding: 10px">
          <div class="content">
            ${story.content}
          </div>
        </td>
      </tr>
    </table>
    <hr>
    <%--<p style="padding-left: 10px;margin: 0px;"><i class="icon-money">&nbsp;打赏作者</i></p>--%>
    <%--<hr>--%>
    <%--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" style="padding: 15px">--%>
      <%--<c:choose>--%>
        <%--<c:when test="${story.weixinQrcode == null}">--%>
          <%--<img src="<%=basePath%>/img/weixin.jpg" class="img-responsive img-thumbnail"--%>
               <%--style="padding: 10px;width: 200px;height: 200px">--%>
        <%--</c:when>--%>
        <%--<c:otherwise>--%>
          <%--<img src="${story.weixinQrcode}-120x120" class="img-responsive img-thumbnail"--%>
               <%--style="padding: 10px">--%>
        <%--</c:otherwise>--%>
      <%--</c:choose>--%>
      <%--<small style="color: darkgray;display: block;padding: 10px" class="text-center">微信</small>--%>
    <%--</div>--%>
    <%--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" style="padding: 15px;">--%>
      <%--<c:choose>--%>
        <%--<c:when test="${story.alipayQrcode == null}">--%>
          <%--<img src="<%=basePath%>/img/zhifubao.jpg" class="img-responsive img-thumbnail"--%>
               <%--style="padding: 10px;width: 200px;height: 200px">--%>
        <%--</c:when>--%>
        <%--<c:otherwise>--%>
          <%--<img src="${story.alipayQrcode}-120x120" class="img-responsive img-thumbnail"--%>
               <%--style="padding: 10px">--%>
        <%--</c:otherwise>--%>
      <%--</c:choose>--%>
      <%--<small style="color: darkgray;display: block;padding: 10px" class="text-center">支付宝</small>--%>
    <%--</div>--%>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 2px;margin-top: 15px">
      <div style="background-color: #ffffff;">
        <table style="width: 100%;">
          <tr>
            <td colspan="2" style="padding: 10px">
              <span>精彩评论</span>
          <span style="padding-left: 10px">
                        <a href="javascript:toReply('','')">
                          <i class="icon-edit"> &nbsp;写留言</i>
                        </a>
                    </span>
            </td>
          </tr>
          <tr>
            <td style="width: 50px;border-bottom: 1px solid green">&nbsp;</td>
            <td style="border-bottom: 1px solid #e8e8e8">&nbsp;</td>
          </tr>
          <tr class="commentBox" style="display: none">
            <td colspan="2">
              <table style="width: 100%;margin: 10px 0px">
                <tr>
                  <td style="width: 100%;padding: 0px 5px">
                <textarea rows="3"
                          style="width: 100%;padding: 5px;background-color: #ffffff;resize: none;border: none"
                          id="comment" placeholder="在这儿输入留言"></textarea>
                  </td>
                </tr>
                <tr>
                  <td align="right">
                    <a class="btn btn-info" style="padding: 3px"
                       href="javascript:cancelReply();">&nbsp;取&nbsp;消&nbsp;</a>
                    <a class="btn btn-info" style="padding: 3px;margin-left: 10px"
                       href="javascript:reply('${story.uuid}','${story.email}')">&nbsp;回&nbsp;复&nbsp;</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <hr style="width: 100%;margin: 5px 0px">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <div class="comments" style="padding: 10px 0px;">
          <p style="padding: 20px;color: #cccccc;display: none" class="allComments">Comments will be here!</p>
        </div>
        <p class="text-center commentsArea" style="padding: 40px 0px;color: #666666;">
          <i class="icon-spinner icon-spin"></i>&nbsp;正在加载数据......
        </p>

        <p class="text-center loadMore" style="display: none">
          <a class="btn btn-info" href="javascript:void(0);" style="text-decoration: none;">加载更多</a>
        </p>

        <div style="margin: 50px 0px">&nbsp;</div>
        <table style="width: 100%;">
          <tr>
            <td colspan="2" style="padding: 10px">十佳好文</td>
          </tr>
          <tr>
            <td style="width: 50px;border-bottom: 1px solid green">&nbsp;</td>
            <td style="border-bottom: 1px solid #e8e8e8">&nbsp;</td>
          </tr>
        </table>

        <div class="story" style="padding: 10px 0px">
          <p style="padding: 20px;color: #cccccc;display: none" class="NoStory">Top 10 stories will bu Here !</p>
        </div>
        <p class="text-center storyArea" style="padding: 40px 0px;color: #666666;">
          <i class="icon-spinner icon-spin"></i>&nbsp;正在加载数据......
        </p>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg" id="imgModal" role="dialog" style="padding: 0px;margin: 0px;">
  <div class="modal-dialog modal-lg" style="margin: 0px auto;height: 100%">
    <div class="modal-content"
         style="border: none;box-shadow: none;background-color: transparent;padding: 0px;margin: 0px;height: 100%">
      <table style="width: 100%;height: 100%">
        <tr>
          <td align="center" valign="middle">
            <img src="" class="img-responsive imgShow">
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<input type="hidden" id="toEmail">
<input type="hidden" id="attentionEmail" value="${story.email}">
<jsp:include page="/foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>

<script type="application/javascript">
  var pageNo = 1;
  var pageSize = 10;


  // - 添加关注
  function addAttention(email) {
    $('.btn').blur();
    var loginEmail = '<%=email%>';
    if (!validateEmail(loginEmail)) {
      return;
    }
    if (email == loginEmail) {
      alerErr('不能自己关注自己');
      return;
    }
    var url = '<%=basePath%>/attention/add';
    var param = {attentionEmail: email};
    $.post(url, param, function (data, status) {
      aler(data);
      if (data.code == 200) {
        showCancelAttention();
      }
    })
  }

  // - 取消关注
  function cancelAttention(email) {
    $('.btn').blur();
    var loginEmail = '<%=email%>';
    if (!validateEmail(loginEmail)) {
      return;
    }
    var url = '<%=basePath%>/attention/cancel';
    var param = {attentionEmail: email};
    $.post(url, param, function (data, status) {
      aler(data);
      if (data.code == 200) {
        showAddAttention();
      }
    })
  }

  function showCancelAttention() {
    $('.addAttention').hide();
    $('.cancelAttention').show();
  }
  function showAddAttention() {
    $('.cancelAttention').hide();
    $('.addAttention').show();
  }

  function initAttention() {
    var loginEmail = '<%=email%>';
    if (isNull(loginEmail)) {
      return;
    }
    var attentionEmail = $("#attentionEmail").val();
    var url = '<%=basePath%>/attention/queryAttention';
    var param = {email: loginEmail, attentionEmail: attentionEmail};
    $.post(url, param, function (data, status) {
      var attention = data.data;
      if (attention == undefined) {
        return;
      }
      if (attention.deleted == 1) {
        showCancelAttention();
      }
    })
  }

  function loadStory(pageNo, pageSize) {
    var url = "<%=basePath%>/story/list";
    var param = {pageNo: pageNo, pageSize: pageSize, orderField: "read_count", storyType: 0};
    var item = '';
    var logo = '';
    $.post(url, param, function (data, status) {
      var stories = data;
      $(".storyArea").remove();
      if (stories.length > 0) {
        $(".story").empty();
        for (var i = 0; i < stories.length; i++) {
          logo = '';
          if (stories[i].imgUrl == null || stories[i].imgUrl == '' || stories[i].imgUrl == undefined || stories[i].imgUrl == '<%=basePath%>/img/product.png') {
            logo = '<%=basePath%>/img/product.png';
          } else {
            logo = stories[i].imgUrl + '-360x200';
          }
          item = '';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="background-color: #ffffff;padding: 0px 10px">';
          item += '<a style="text-decoration: none;" href="<%=basePath%>/story/info?uuid=' + stories[i].uuid + '">';
          item += '<div class=" col-xs-4 col-sm-3 col-md-3 col-lg-2 text-center"style="padding: 0px ;">';
          item += '<img class="img-responsive" src="' + logo + '" alt="' + stories[i].name + '">';
          item += '</div>';
          item += '<div class=" col-xs-8 col-sm-9 col-md-9 col-lg-10">';
          item += '<p style="">' + stories[i].title + '<small style="color: #CCCCCC">&nbsp;(阅读：' + stories[i].readCount + ')</small></p>';
          item += '</div>';
          item += '</a>';
          item += '<hr>';
          item += '</div>';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="background-color: #ffffff;padding: 0px"><hr></div>';
          $(".story").append(item);
        }
      } else {
        $(".NoStory").show();
      }
    })
  }

  function incrReadCount() {
    var url = '<%=basePath%>/story/incrReadCount?uuid=${story.uuid}';
    $.post(url, function (data, status) {

    });
  }
  function loadComments(pageNo, pageSize) {
    var uuid = '${story.uuid}';
    var url = '<%=basePath%>/note/list';
    var param = {uuid: uuid, pageNo: pageNo, pageSize: pageSize};
    $.post(url, param, function (data, status) {
      $(".commentsArea").hide();
      if (data != null && data.length > 0) {
        $(".allComments").hide();
        var item = '';
        for (var i = 0; i < data.length; i++) {
          item += parseNote(data[i].email, data[i].logo, data[i].content, data[i].name, data[i].createTime);
        }
        $(".comments").append(item);
      } else {
        if (pageNo == 1 && data.length == 0) {
          $(".allComments").show();
        }
      }
      if (data.length < pageSize) {
        $(".loadMore").hide();
      } else {
        $(".loadMore").show();
      }
    })
  }

  function cancelReply() {
    $(".commentBox").hide();
    $("#comment").val('');
  }

  function toReply(toEmail, toName) {
    $("#toEmail").val(toEmail);
    if (toEmail != '') {
      $("#comment").val('@' + toName + ' ');
    }
    $(".commentBox").show();
    $("#comment").focus();
  }
  function parseNote(email, logo, content, name, createTime) {
    if (logo != null && logo != '' && logo != undefined) {
      logo += "-120x120";
    }
    if (logo == undefined) {
      logo = '<%=basePath%>/img/user.png';
    }
    var url = "<%=basePath%>/user/queryinfo?email=" + email;
    var subItem = '';
    subItem += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr>';
    subItem += '<td style="width: 50px;height: 50px;padding-left: 10px" align="center" >';
    subItem += '<a href="' + url + '"><img class="img-responsive img-circle"  src="' + logo + '" /></a>';
    subItem += '</td>';
    subItem += '<td style="padding: 0px 0px 0px 5px;color: #999999;"><small>' + name + '</small></td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style="padding: 0px">&nbsp;</td>';
    subItem += '<td style="padding: 5px">' + content + '</td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style="padding: 0px">&nbsp;</td>';
    subItem += '<td style="color: #999999;padding: 0px 0px 0px 5px;">';
    subItem += '<small>' + createTime + '&nbsp;&nbsp;<a href="javascript:toReply(\'' + email + '\',\'' + name + '\');"><i class="icon-comments-alt"></i></a></small>';
    subItem += '</td>';
    subItem += '</tr>';
    subItem += '<tr><td>&nbsp;</td><td><hr style="width: 100%;margin: 0px;padding: 0px"></td></tr>';
    subItem += '</table>';
    subItem += '</div>';
    return subItem;
  }
  function reply(uuid, authEmail) {
    var title = '${story.title}';
    $(".btn").blur();
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    var content = $.trim($('#comment').val());
    if (content == '') {
      alerErr("输入留言内容");
      return;
    }
    var url = "<%=basePath%>/note/add";
    var toEmail = $("#toEmail").val();
    if (toEmail != '') {
      authEmail = toEmail;
    }
    var param = {content: content, uuid: uuid, toEmail: authEmail, title: title};
    $.post(url, param, function (data, status) {
      if (status == 'success') {
        $("#toEmail").val('');
        $('#comment').val('');
        $(".commentBox").hide();
        $(".allComments").remove();
        var temp = parseNote(data.data.email, data.data.logo, data.data.content, data.data.name, data.data.createTime);
        $('.comments').append(temp);
      } else {
        aler(data);
      }
    })
  }

  function loadAuther() {
    var temp = '作者：';
    var auther = '${story.name}';
    if (auther == '' || auther == undefined) {
      auther = '${story.email}';
    }
    temp += auther;
    $(".auther").text(temp);
  }
  $(function () {
    initStyle();
    incrReadCount();
    loadAuther();
    initAttention();
    loadComments(pageNo++, pageSize);
    loadStory(0, pageSize);
    $('img').addClass('img-responsive ');
    $('pre').addClass('preNoBorder');

    $('.loadMore').click(function () {
      $(".commentsArea").show();
      $(".loadMore").hide();
      loadComments(pageNo++, pageSize);
    })

    $('.content img').css('cursor', 'crosshair');
    $('.content img').click(function () {
      var imgUrl = $(this).attr('src');
      if (imgUrl.indexOf('800x800') > 0 || imgUrl.indexOf('750x500') > 0 || imgUrl.indexOf('580x350') > 0 || imgUrl.indexOf('120x120') > 0) {
        imgUrl = imgUrl.substr(0, imgUrl.length - 8);
      }
      $('.imgShow').attr('src', imgUrl);
      $('#imgModal').modal();
    })

    $('.imgShow').click(function () {
      $('#imgModal').modal('hide');
    })


  })
</script>

</html>
