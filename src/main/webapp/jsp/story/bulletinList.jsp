<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>公告</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
  </style>
</head>
<script>
  function toPage(uuid) {
    window.location = "<%=basePath%>/story/info?uuid=" + uuid;
  }
  function getDate(temp) {
    var index = temp.indexOf(' ');
    temp = temp.substr(0, index - 1);
    return temp;
  }
  function getTime(temp) {
    var index = temp.indexOf(' ');
    temp = temp.substring(index, temp.length - 2);
    return temp;
  }
  function delBulletin(bulletinId) {
    var url = '<%=basePath%>/story/del?bulletinId=' + bulletinId;
    $.post(url, function (data, status) {
      if (data.code == 200) {
        var backUrl = '<%=basePath%>/bulletin';
        window.location.href = '<%=basePath%>/success?backUrl=' + backUrl;
      } else {
        aler(data);
      }
    })
  }
  function HTMLDeCode(str) {
    var s = "";
    if (str.length == 0)    return "";
    s = str.replace(/&lt;/g, "<");
    s = s.replace(/&gt;/g, ">");
    s = s.replace(/&nbsp;/g, "    ");
    s = s.replace(/'/g, "\'");
    s = s.replace(/&quot;/g, "\"");
    s = s.replace(/ <br>/g, "\n");
    return s;
  }
  function loadStories(pageNo, pageSize) {
    var url = "<%=basePath%>/story/bulletin";
    var param = {pageNo: pageNo, pageSize: pageSize, storyType: 1};
    var item = '';
    var email = '<%=email%>';
    $.post(url, param, function (data, status) {
      $("#storiesArea").hide();
      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          item = '';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px;">';
          item += '<div class=" col-xs-0 col-sm-0 col-md-0 col-lg-3"></div>';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-6"style="padding: 0px">';
          item += '<h4 style="color: #666666;margin-top: 20px">' + data[i].date + '</h4>';
          var stories = data[i].list;
          for (var j = 0; j < stories.length; j++) {
            item += '<div class="shadow" style="background-color: #ffffff;margin-top: 15px;padding: 0px 15px">';
            item += '<p style="padding: 15px 0px;">';
            item += '<table style="width: 100%">';
            item += '<tr>';
            item += '<td style="font-weight: bold;color: #009999"><i class="icon-bullhorn icon-2x">&nbsp;&nbsp;</i>' + stories[j].title + '</td>';
            if (email == stories[j].email) {
              item += '<td align="right">';
              item += '<a href="javascript:delBulletin(' + stories[j].id + ')" style="text-decoration: none"><i class="icon-remove icon-large"></i></a>';
              item += '</td>';
            }
            item += '</tr>';
            item += '</table>';
            item += '</p>';
            item += '<p style="padding: 5px 0px;">' + HTMLDeCode(stories[j].content) + '</p>';
            item += '<p style="padding: 5px 0px;color: #999999"><small><i class="icon-time">' + getTime(stories[j].createTime) + '</i></small></p>';
            item += '</div>'
          }
          console.log(item)
          item += '</div>';
          item += '</div>';
          $("#stories").append(item);
        }
        if (data.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        // - 没有数据
        if (pageNo == 1) {
          var noDataShow = '';
          noDataShow += '<h3 class="text-center" style="color: #cccccc;padding: 100px 0px">All Bulletin will be here</h3>';
          $("#stories").empty();
          $("#stories").append(noDataShow);
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  var pageNo = 1;
  $(function () {
    initStyle();
    $(".load").mousemove(function () {
      $(this).css("cursor", "pointer");
    })
    $(".load").mouseout(function () {
      $(this).css("cursor", "default");
    })
    loadStories(pageNo++, 10);
    $("#loadMore").click(function () {
      loadStories(pageNo++, 10);
    })
  })
</script>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;margin-top: 15px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;margin-top: 15px">
    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 5px;">
      <div id="storiesArea">
        <table style="width: 100%">
          <tr>
            <td align="center" style="color: #d3d3d3;"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载内容......
            </td>
          </tr>
        </table>
      </div>
      <div id="stories"></div>
      <table style="width: 100%;display: none;" id="loadMore">
        <tr>
          <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">加载更多</td>
        </tr>
      </table>
      <table style="width: 100%;display: none;" id="loadOver">
        <tr>
          <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
        </tr>
      </table>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
</html>
