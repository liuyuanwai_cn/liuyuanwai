<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<!DOCTYPE HTML>
<html>
<head>
  <title>发公告</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/simditor.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/module.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/hotkeys.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/uploader.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/simditor.min.js"></script>

</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin: 10px 0px;padding: 20px">
    <p style="margin-top: 10px"><input type="text" id="title" style="border: none;padding: 5px;width: 100%"
                                       maxlength="60" placeholder="公告标题"></p>
    <hr style="width: 100%;margin: 5px 0px">
    <p style="margin: 10px 0px">
      <textarea id="content" class="textarea"
                placeholder="在这里输入公告内容......" rows="15"
                style="resize: none;width: 100%;padding: 5px;border: none"></textarea>
    </p>
    <hr style="width: 100%;margin: 5px 0px">
    <div class="btn-group" role="group" aria-label="..."style="margin-top: 20px">
      <button type="button" class="btn btn-info" onclick="save()">发&nbsp;布</button>
      <button type="button" class="btn btn-default" onclick="history.go(-1)">取&nbsp;消</button>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript" type="text/javascript">


  function validate(title, content) {
    if (title == '') {
      alerErr("输入标题");
      return false;
    }
    if (content == "") {
      alerErr("输入内容");
      return false;
    }
    return true;
  }

  function save() {
    $(".btn").blur();
    if (!validateEmail('<%=email%>')) {
      return;
    }
    if (!validateEmail('<%=email%>')) {
      return;
    }
    var title = $.trim($("#title").val());
    var content = $.trim($("#content").val());
    if (!validate(title, content)) {
      return;
    }
    ;
    var param = {
      content: content,
      title: title,
      email: '<%=email%>',
      storyType: 1
    };
    var url = "<%=basePath%>/story/save";
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        $("#content").val('');
        $("#title").val('');
        var story = data.data;
        var backUrl = '<%=basePath%>/bulletin';
        window.location = '<%=basePath%>/success?backUrl=' + backUrl;
      } else {
        aler(data);
      }
    })
  }

  $(function () {
    initStyle();
    var editor = new Simditor({
      textarea: $('#content'),
      toolbar: [
        'title',
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'fontScale',
        'color',
        'ol',
        'ul',
        'blockquote',
        'code',
        'link',
        'image',
        'hr',
        'indent',
        'outdent',
        'alignment'
      ],
      upload: {
        url: "<%=basePath%>/img/uploadForSimditor",
        params: {
          fileElementId: "filePath",
          dateType: "text"
        },
        fileKey: "filePath",
        leaveConfirm: '正在上传...'
      },
      pasteImage: true
    });
    editor.focus();
    editor.on('valuechanged', function (e, src) {
      $('img').addClass('img-responsive');
    })

    if (!validateEmail('<%=email%>')) {
      return;
    }
  })

</script>
</html>
