<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
  Object role = request.getSession().getAttribute("role");
%>
<html>
<head>
  <title>资讯 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
    .panel .badge{
      background-color: #ffffff;
      color: #000000;
      border: 1px solid #CCCCCC;
    }
  </style>
</head>
<script>
  var pageNo = 1;
  var pageSize = 20;

  function toPage(uuid) {
    window.location = "<%=basePath%>/story/info?uuid=" + uuid;
  }

  function delStory(storyId,authEmail) {
    var backUrl = '<%=basePath%>/story';
    var url = '<%=basePath%>/story/del';
    var param = {storyId: storyId,authEmail:authEmail};
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        window.location.href = '<%=basePath%>/success?backUrl=' + backUrl;
      } else {
        aler(data);
      }
    })
  }
  // - 加载发现
  function loadStories(pageNo, pageSize, storyTag) {
    $("#storiesArea").show();
    if (storyTag == 0) {
      storyTag = null;
    }
    var url = "<%=basePath%>/story/list";
    var param = {pageNo: pageNo, pageSize: pageSize, storyType: 0, tag: storyTag};
    var item = '';
    var logo = '';
    var email = '<%=email%>';
    var role = '<%=role%>';
    $.post(url, param, function (data, status) {
      var stories = data;
      $("#storiesArea").hide();
      if (stories.length > 0) {
        for (var i = 0; i < stories.length; i++) {
          logo = '';
          var auther = stories[i].name;
          if (auther == '' || auther == undefined) {
            auther = stories[i].email;
          }
          if (stories[i].imgUrl == null || stories[i].imgUrl == '' || stories[i].imgUrl == undefined || stories[i].imgUrl == '<%=basePath%>/img/product.png') {
            logo = '<%=basePath%>/img/product.png';
          } else {
            logo = stories[i].imgUrl + '-360x200';
          }
          var subTitle = stories[i].subTitle;
          item = '';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 30px 20px;margin: 5px 0px;background-color: #ffffff">';
          item += '<div class=" col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"style="padding: 0px ;">';
          item += '<a style="text-decoration: none;" href="javascript:void(0);" onclick="javascript:toPage(\'' + stories[i].uuid + '\');">';
          item += '<img class="img-responsive" src="' + logo + '" alt="' + stories[i].name + '">';
          item += '</a>';
          item += '</div>';
          item += '<div class=" col-xs-9 col-sm-9 col-md-9 col-lg-9"style="padding-right: 5px">';
          item += '<table style="width: 100%">';
          item += '<tr>';
          item += '<td>';
          item += '<a style="text-decoration: none;" href="javascript:void(0);" onclick="javascript:toPage(\'' + stories[i].uuid + '\');">';
          item += '<h4 style="margin-top: 2px;font-weight: bold;color: #444444">' + stories[i].title + '</h4>';
          item += '</a>';
          item += '</td>';
          item += '</tr>';
          item += '<tr>';
          item += '<td>';
          item += '<a style="text-decoration: none;" href="javascript:void(0);" onclick="javascript:toPage(\'' + stories[i].uuid + '\');">';
          item += '<p class="hidden-xs">' + subTitle + '</p>';
          item += '</a>';
          item += '</td>';
          item += '</tr>';
          item += '<tr>';
          item += '<td align="right">';
          item += '<small style="color: #999999;padding: 0px;"align="right">作者：' + auther + '&nbsp;|&nbsp;阅读：' + stories[i].readCount + '</small>';
          if (email == stories[i].email || (role != 'null' && (role == 1 || role == 2))) {
            item += '<span style="padding: 5px 10px;"align="right"><a style="color: red;" href="javascript:void(0);" onclick="delStory(' + stories[i].id + ',\''+stories[i].email+'\');"><small>删除</small></a></span>';
          }
          item += '</td>';
          item += '</tr>';
          item += '<tr><td style="padding-top: 10px"><span class="badge" style="background-color: #CC66CC">' + stories[i].tag + '</span></td></tr>';
          item += '</table>';
          item += '</div>';
          item += '</div>';
          $("#stories").append(item);
        }
        if (stories.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        // - 没有数据
        if (pageNo == 1) {
          var noDataShow = '';
          noDataShow += '<table style="width: 100%">';
          noDataShow += '<tr>';
          noDataShow += '<td align="center"style="padding: 100px 0px"><h3 style="color: #999999">No comment Here!</h3></td>';
          noDataShow += '</tr>';
          noDataShow += '</table>';
          $("#stories").empty();
          $("#stories").append(noDataShow);
          $("#loadMore").hide();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }

  function loadTag() {
    var url = '<%=basePath%>/tag/list';
    $.post(url, function (data, status) {
      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          var tagId = 'tag' + data[i].id;
          var temp = '<a style="text-decoration: none" href="javascript:chooseTag(' + data[i].id + ')"><span style="margin: 7px;color:' + getRandomColor() + ';font-size:' + getRandomSize() + '" id="' + tagId + '" class="tag">' + data[i].name + '</span></a>';
          $('.storyTags').append(temp);
        }
      }
    })
  }

  var getRandomColor = function () {
    return '#' + (function (color) {
        return (color += '0123456789abcdef' [Math.floor(Math.random() * 16)])
        && (color.length == 6) ? color : arguments.callee(color);
      })('');
  }
  var getRandomSize = function () {
    var randomSize = parseInt((12 + Math.random() * 8));
    return randomSize;
  }
  function chooseTag(tagId) {
    pageNo = 1;
    $(".tag").removeClass('badge');
    var id = '#tag' + tagId;
    $(id).addClass('badge');
    $("#storyTag").val(tagId);
    $("#stories").empty();
    var tag = $("#storyTag").val();
    if (tagId == 0) {
      tag = null;
    }
    loadStories(pageNo, pageSize, tag);
  }

  $(function () {
    initStyle();
    loadTag();
    loadStories(pageNo++, pageSize, null);
    $("#loadMore").click(function () {
      $("#loadMore").hide();
      loadStories(pageNo++, pageSize, $("#storyTag").val());
    })
  })
</script>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;margin-top: 5px">
    <div class=" col-xs-0 col-sm-0 col-md-1 col-lg-2"></div>
    <div class=" col-xs-0 col-sm-0 col-md-10 col-lg-8"style="padding: 0px">
      <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 5px;">
        <div class="panel panel-default" style="border-radius: 0px;border: none;margin: 0px;box-shadow: none">
          <div class="panel-heading" style="border: none;background-color: #cccccc;border-radius: 0px;">
            <table style="width: 100%">
              <tr>
                <td>标签</td>
                <td align="right">
                  <a href="<%=basePath%>/writeStory" style="text-decoration: none;color: #ffffff">
                    提笔作文&nbsp;<i class="icon-pencil" style=""></i>
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div class="panel-body" style="border: none;border-radius: 0px">
            <div class="storyTags">
              <a href="javascript:chooseTag(0)" style="text-decoration: none">
                <span id="tag0" class="tag badge">所有</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 5px;">
        <div id="stories"></div>
        <div id="storiesArea">
          <table style="width: 100%">
            <tr>
              <td align="center" style="color: #d3d3d3;padding: 40px 0px"><i
                class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载内容......
              </td>
            </tr>
          </table>
        </div>
        <table style="width: 100%;display: none;" id="loadMore">
          <tr>
            <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">
              <a class="btn btn-info" style="text-decoration: none;padding: 7px 40px"
                 href="javascript:void(0);">加载更多</a>
            </td>
          </tr>
        </table>
        <table style="width: 100%;display: none;" id="loadOver">
          <tr>
            <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="storyTag">
<jsp:include page="../../foot.jsp"></jsp:include>
</body>


</html>
