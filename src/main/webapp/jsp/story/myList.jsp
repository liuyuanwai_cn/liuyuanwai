<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getParameter("email");
  Object logo = request.getParameter("logo");
  Object name = request.getParameter("name");
%>
<html>
<head>
  <title>${user.name} 文章</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<script>
  function toPage(uuid) {
    window.location = "<%=basePath%>/story/info?uuid=" + uuid;
  }
  // - 加载发现
  function loadStories(pageNo, pageSize) {
    var url = "<%=basePath%>/story/list?email=<%=email%>";
    var param = {pageNo: pageNo, pageSize: pageSize};
    var item = '';
    var logo = '';
    $.post(url, param, function (data, status) {
      var stories = data;
      $("#storiesArea").hide();
      if (stories.length > 0) {
        for (var i = 0; i < stories.length; i++) {
          logo = '';
          if (stories[i].imgUrl == null || stories[i].imgUrl == '' || stories[i].imgUrl == undefined || stories[i].imgUrl == '<%=basePath%>/img/product.png') {
            logo = '<%=basePath%>/img/product.png';
          } else {
            logo = stories[i].imgUrl + '-360x200';
          }
          item = '';
          item += '<div>';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 30px 20px;margin: 10px 0px;background-color: #ffffff">';
          item += '<a style="text-decoration: none;margin-bottom: 10px" href="javascript:toPage(\'' + stories[i].uuid + '\');">';
          item += '<div class=" col-xs-4 col-sm-4 col-md-4 col-lg-3 text-center"style="padding: 0px ;">';
          item += '<img class="img-responsive" src="' + logo + '" alt="' + stories[i].name + '">';
          item += '</div>';
          item += '<div class=" col-xs-8 col-sm-8 col-md-8 col-lg-9">';
          item += '<h4 style="margin-top: 2px;color: #000000;font-weight: bold">' + stories[i].title + '</h4>';
          item += '<p style="color: gray" class="hidden-xs">' + stories[i].subTitle + '</p>';
          item += '<p style="color: #999999" class="text-right"><small>' + stories[i].name + '&nbsp;&nbsp;阅读：' + stories[i].readCount + '</small></p>';
          item += '<span class="badge" style="background-color: #CC66CC">' + stories[i].tag + '</span>';
          item += '</div>';
          item += '</a>';
          item += '</div>';
          item += '</div>';
          $("#stories").append(item);
        }
        if (stories.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        // - 没有数据
        if (pageNo == 1) {
          var noDataShow = '';
          noDataShow += '<p class="text-center"><h3 style="color: #999999">该用户有0篇文章!</h3></p>';
          $("#stories").empty();
          $("#stories").append(noDataShow);
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  function writeStory() {
    var url = '<%=basePath%>/writeStory';
    if (!validateEmail('<%=email%>')) {
      return;
    }
    window.location = url;
  }
  var pageNo = 1;
  $(function () {
    initStyle();
    $(".load").mousemove(function () {
      $(this).css("cursor", "pointer");
    })
    $(".load").mouseout(function () {
      $(this).css("cursor", "default");
    })
    loadStories(pageNo++, 10);
    $("#loadMore").click(function () {
      loadStories(pageNo++, 10);
    })
  })
</script>
<body style="background-color: #f9f9f9">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;margin-top: 15px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;">
    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;">
      <div id="storiesArea">
        <table style="width: 100%">
          <tr>
            <td align="center" style="color: #d3d3d3;"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载内容......
            </td>
          </tr>
        </table>
      </div>
      <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">
        <div class=" col-xs-0 col-sm-0 col-md-0 col-lg-1"></div>
        <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-7" id="stories" style="padding: 20px 0px 0px;"></div>
        <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-4" style="padding: 30px 0px">
          <a href="<%=basePath%>/user/queryinfo?email=${user.email}">
            <table align="center">
              <tr>
                <td>
                  <p>
                    <c:choose>
                      <c:when test="${user.logo == null or user.logo == ''}">
                        <img src="<%=basePath%>/img/user.png" style="width: 90px;height: 90px"class="img-responsive img-circle">
                      </c:when>
                      <c:otherwise>
                        <img src="${user.logo}-120x120" style="width: 90px;height: 90px"class="img-responsive img-circle">
                      </c:otherwise>
                    </c:choose>
                  </p>
                </td>
              </tr>
              <tr>
                <td style="padding: 10px 15px" align="center">
                  <span class="label label-danger" style="margin: 0px">${user.name}</span>
                </td>
              </tr>
            </table>
          </a>

          <p class="text-center" style="color: slategray;margin: 10px 0px">作者简介：${user.remark} </p>

          <p class="text-center">
            <a href="<%=basePath%>/story"><i class="icon-eye-open">&nbsp;发现更多美好</i></a>
            &nbsp;&nbsp;
            <a href="<%=basePath%>/writeStory"><i class="icon-facetime-video">&nbsp;写文章,做原创</i></a>
          </p>
        </div>
      </div>
      <table style="width: 100%;display: none;" id="loadMore">
        <tr>
          <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">加载更多</td>
        </tr>
      </table>
      <table style="width: 100%;display: none;" id="loadOver">
        <tr>
          <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
        </tr>
      </table>
    </div>
  </div>
</div>
<jsp:include page="/foot.jsp"></jsp:include>
</body>
</html>
