<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<!DOCTYPE HTML>
<html>
<head>
  <meta name="keywords" content="刘员外,专注小而美的生活体验"/>
  <meta name="description" content="刘员外,专注小而美的生活体验"/>
  <title>写资讯</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/simditor.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/module.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/hotkeys.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/uploader.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/simditor.min.js"></script>

  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/simditor-mark.js"></script>
  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/extension/simditor-markdown.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/marked.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/to-markdown.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/simditor-markdown.js"></script>


  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/extension/simditor-fullscreen.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/simditor-fullscreen.js"></script>

  <style>
    input {
      border: none;
    }

    ul {
      margin: 0px;
      padding: 0px;
    }

    .pubtag {
      background-color: #ffffff;
      color: #000000;
      border: 1px solid #CCCCCC;
    }

    input,textarea{outline: none}
  </style>
</head>
<body style="background-color: #f4f7ed">
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-0 col-md-1 col-lg-2"></div>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8"
       style="margin: 0px;padding: 0px;background-color: #ffffff">
    <table style="width: 100%">
      <tr>
        <th align="center" style="padding: 0px;border-bottom: 1px dashed #e8e8e8">
          <h3 style="color: #cccccc">
            <input type="text" id="title" style="border: none;width: 100%;padding: 5px" maxlength="60"
                   class="text-center"
                   placeholder="在这里输入标题">
          </h3>
        </th>
      </tr>
      <tr>
        <td style="padding: 5px;border-bottom: 0px dashed #e8e8e8">
          <input type="text" id="subTitle" style="border: none;width: 100%;padding: 5px" maxlength="60"
                 placeholder="在这里输入副标题" class="text-center">
        </td>
      </tr>
    </table>
    <hr style="margin-top: 0px">
    <p class="storyTags" style="padding-left: 10px">
      <a href="javascript:void(0);" onclick="javascript:toAddTag()" style="text-decoration: none">
        <span class="tag badge" style="font-weight: lighter">＋|&nbsp;添加分类</span>
      </a>
      <a href="javascript:void(0)" onclick="javascript:chooseTag(0)">
        <span class="badge pubtag" id="tag0" style="margin:5px;padding: 5px 10px">未分类</span>
      </a>
    </p>
    <hr style="margin-bottom: 0px">
    <textarea id="content" class="textarea"
              placeholder=""
              style="resize: none;width: 100%;border: none;height: 100%">
    </textarea>
    <hr style="width: 100%;margin: 5px 0px">
    <div style="background-color: #ffffff;" class="chooseImg">
      <table style="width: 100%;">
        <tr>
          <td style="padding: 10px 8px;color: darkgrey" id="imgPath">
            上传封面
          </td>
          <td align="right" style="padding-right: 15px;width: 20px;color: darkgray">
            <%--&gt;--%>
          </td>
        </tr>
      </table>
    </div>
    <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath" style="display: none">

    <hr style="width: 100%;margin: 5px 0px">
    <div class="btn-group" role="group" aria-label="..." style="padding: 40px;">
      <button onclick="save();" type="button" class="btn btn-info">发&nbsp;布</button>
      <button type="button" class="btn btn-default" onclick="history.go(-1)">取&nbsp;消</button>
    </div>
  </div>
</div>
<form id="form" action="<%=basePath%>/previewStory" method="post" style="display: none">
  <input name="name" class="name">
  <input name="title" class="title">
  <input name="subTitle" class="subTitle">
  <input name="content" class="content">
  <input name="logo" class="logo">
  <input name="chooseTag" class="chooseTag">
</form>
<input type="hidden" id="img">
<input type="hidden" id="chooseTag" value="0">
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="tagModal" role="dialog" style="margin-top: 10%">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" align="center">
        添加标签
      </div>
      <div class="modal-body" style="margin: 0px;padding: 0px">
        <input type="text" maxlength="8" style="width: 100%;padding: 10px" id="tagName" placeholder="输入标签名">
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" onclick="addTag();">添&nbsp;&nbsp;加</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" class="isHomeImg" value="1">
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript" type="text/javascript">

//  function noBorder(){
//    $('input').css("border",'none');
//  }


  $(function () {
    initStyle();
    loadTag();

    var editor = new Simditor({
      textarea: $('#content'),
      toolbar: [
        'title',
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'fontScale',
        'color',
        'ol',
        'ul',
        'blockquote',
        'code',
        'link',
        'image',
        'hr',
        'indent',
        'outdent',
        'alignment',
        'mark',
        'markdown',
        'fullscreen'
      ],
      toolbarFloat: true,
      upload: {
        url: "<%=basePath%>/img/uploadForSimditor",
        params: {
          fileElementId: "filePath",
          dateType: "text"
        },
        fileKey: "filePath",
        leaveConfirm: '正在上传...'
      },
      pasteImage: true
    });
    editor.focus();
    editor.on('valuechanged', function (e, src) {
      $('img').addClass('img-responsive');
    })
  })

  function toInsertImg() {
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    $(".isHomeImg").val(0);
    $("#filePath").click();
  }
  var getRandomColor = function () {
    return '#' + (function (color) {
        return (color += '0123456789abcdef' [Math.floor(Math.random() * 16)])
        && (color.length == 6) ? color : arguments.callee(color);
      })('');
  }
  var getRandomSize = function () {
    var randomSize = parseInt((12 + Math.random() * 8)) + 'px';
    return randomSize;
  }

  // - 上传图片
  function uploadFile(obj) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/img/upload";
    var isHomeImage = $(".isHomeImg").val();
    $.ajaxFileUpload({
      url: url,
      fileElementId: "filePath",//file标签的id,
      dateType: "text",
      success: function (data, status) {
        // - ajaxFileUpload这儿必须得转换以下
        data = jQuery.parseJSON(jQuery(data).text());
        if (data.code == 200) {
          var temp = data.data;
          if (isHomeImage == 0) {
            // - 不是上传封面
            insertImg(temp.downloadUrl + '-800x800');
          } else if (isHomeImage == 1) {
            // - 上传封面
            $("#img").val(temp.downloadUrl);
            $("#imgPath").text('  ' + temp.downloadUrl);
          }
        } else {
          alerErr(data.msg);
        }
        $("#loadModal").modal('hide');
      },
      error: function (data, status, e) {
        $("#loadModal").modal('hide');
        $("#filePath").replaceWith($("#upload").clone(true));
        alerErr(e);
      }
    });
  }
  // - 触发上传图片
  $(".chooseImg").mousemove(function () {
    $(this).css("cursor", "pointer");
  })
  $(".chooseImg").mouseout(function () {
    $(this).css("cursor", "default");
  })
  $(".chooseImg").click(function () {
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    $(".isHomeImg").val(1);
    $("#filePath").click();
  })

  function validate(title, subTitle, content, img, chooseTag) {
    if (title == '') {
      alerErr("输入标题");
      return false;
    }
    if (subTitle == '') {
      alerErr("输入副标题");
      return false;
    }
    if (content == "") {
      alerErr("输入内容");
      return false;
    }
    if (img == '') {
      alerErr("选择图片");
      return false;
    }
    if (chooseTag == '0') {
      alerErr("亲，选个标签吧");
      return false;
    }
    return true;
  }

  function chooseTag(tagId) {
    $(".tag").removeClass('badge');
    var id = '#tag' + tagId;
    $(id).addClass('badge');
    $("#chooseTag").val(tagId);
  }

  function toAddTag() {
    if (!validateEmail('<%=email%>')) {
      return;
    }
    $("#tagModal").modal();
  }


  function addTag() {
    $(".btn").blur();
    var tagName = $.trim($("#tagName").val());
    if (tagName == '') {
      alerErr("输入标签名称!");
      return;
    }
    var url = '<%=basePath%>/tag/add';
    var param = {name: tagName};
    $.post(url, param, function (data, status) {
      $("#tagName").val('');
      $("#tagModal").modal('hide');
      if (data.code == 200) {
        var tag = data.data;
        var temp = '<a style="text-decoration: none" href="javascript:chooseTag(' + tag.id + ')"><span id="tag' + tag.id + '" class="tag"style="color:' + getRandomColor() + ';font-size:' + getRandomSize() + '">' + tag.name + '</span></a>';
        $('.storyTags').append(temp);
      } else {
        aler(data);
      }
    })
  }

  function loadTag() {
    var url = '<%=basePath%>/tag/list';
    $.post(url, function (data, status) {
      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          var tagId = 'tag' + data[i].id;
          var temp = '<a style="text-decoration: none" href="javascript:chooseTag(' + data[i].id + ')"><span style="margin:7px;color:' + getRandomColor() + ';font-size:' + getRandomSize() + '" id="' + tagId + '" class="tag" >' + data[i].name + '</span></a>';
          $('.storyTags').append(temp);
        }
      }
    })
  }

  function save() {
    $(".btn").blur();
    if (!validateEmail('<%=email%>')) {
      return;
    }
    var title = $.trim($("#title").val());
    var subTitle = $.trim($("#subTitle").val());
    var content = $.trim($("#content").val());
    var img = $("#img").val();
    var chooseTag = $("#chooseTag").val();
    if (!validate(title, subTitle, content, img, chooseTag)) {
      return;
    }
    var param = {
      simContent: content,
      title: title,
      subTitle: subTitle,
      img: img,
      email: '<%=email%>',
      chooseTag: chooseTag
    };
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/story/save";
    $.post(url, param, function (data, status) {
      $("#loadModal").modal('hide');
      if (data.code == 200) {
        $("#content").val('');
        $("#title").val('');
        $("#subTitle").val('');
        $("#img").val('');
        $("#filePath").val('');
        var story = data.data;
        var backUrl = '<%=basePath%>/story/info?uuid=' + story.uuid;
        window.location = '<%=basePath%>/success?backUrl=' + backUrl;
      } else {
        aler(data);
      }
    })
  }


</script>
</html>
