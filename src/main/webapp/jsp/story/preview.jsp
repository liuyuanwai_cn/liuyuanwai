<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
  <title>${title}</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
</head>
<body style="background-color: #f9f9f9;padding: 0px">
<div class="container-fluid" style="padding: 0px;margin: 0px">
  <div class="col-xs-0 col-sm-0 col-md-0 col-lg-3"></div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5" style="padding-bottom: 100px;background-color: #ffffff;">
    <p style="">
    <table style="width: 100%">
      <tr>
        <td><a class="btn btn-default" href="javascript:void(0);" onclick="history.go(-1)"><i class="icon-double-angle-left">&nbsp;&nbsp;返&nbsp;回</i></a></td>
        <td align="right"><span class="label label-success">预览</span></td>
      </tr>
    </table>
    </p>
    <hr style="width: 100%;margin-top: 0px">
    <table style="width: 100%">
      <tr>
        <td style="padding: 10px;">
          <h3>
            ${title}
          </h3>
          <hr style="width: 100%">
        </td>
      </tr>
      <tr>
        <td style="padding: 10px;color: #cccccc">
          <blockquote style="font-size: small;padding-left: 5px">
            ${subTitle}
          </blockquote>
        </td>
      </tr>
      <tr>
        <td style="padding: 10px;color: #cccccc">
          <span class="badge" style="background-color: #CC66CC">${chooseTag}</span>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="padding: 10px;color: #999999"><small>作者：${name}&nbsp;&nbsp;&nbsp;阅读&nbsp;0</small></td>
      </tr>
    </table>
    <table style="width: 100%">
      <tr>
        <td align="center" style="padding: 10px">
          <img src="${logo}-750x500" class="img-responsive">
        </td>
      </tr>
      <tr>
        <td style="padding: 10px">
          <p id="content">
          </p>
        </td>
      </tr>
    </table>
    <hr style="width: 100%">
    <p style="">
    <table style="width: 100%">
      <tr>
        <td><a class="btn btn-default" href="javascript:void(0);" onclick="history.go(-1)"><i class="icon-double-angle-left">&nbsp;&nbsp;返&nbsp;回</i></a></td>
        <td align="right"><span class="label label-success">预览</span></td>
      </tr>
    </table>
    </p>
  </div>
</div>
<input type="hidden" id="hiddenContent" value="${content}">
<jsp:include page="../../foot.jsp"></jsp:include>
</body>

<script type="application/javascript">
  function HTMLDeCode(str) {
    var s = "";
    if (str.length == 0)    return "";
    s = str.replace(/&lt;/g, "<");
    s = s.replace(/&gt;/g, ">");
    s = s.replace(/&nbsp;/g, "    ");
    s = s.replace(/'/g, "\'");
    s = s.replace(/&quot;/g, "\"");
    s = s.replace(/ <br>/g, "\n");
    return s;
  }

  $(function () {
    $('img').addClass('img-responsive');
    $("#content").html(HTMLDeCode($("#hiddenContent").val()));
  })
</script>

</html>
