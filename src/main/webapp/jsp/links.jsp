<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
  Object role = request.getSession().getAttribute("role");
%>
<!DOCTYPE html>
<html>
<head>
  <title>加入我们 - 刘员外</title>
  <jsp:include page="common/common-css.jsp"></jsp:include>
  <jsp:include page="common/common-js.jsp"></jsp:include>

  <style type="text/css">

  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff;margin-top: 10px">
    <div class="col-xs-0 col-sm-0 col-md-2 col-lg-3"></div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6" style="padding: 0px 0px 40px;">
      <p style="font-weight: bold;padding-top: 40px;color: #666666">关于我们</p>
      <hr>
      <div class="jumbotron"style="padding: 10px">
        <h2>劉員外</h2>

        <p>传播优质内容，分享美好'商品'。专注小而美的生活体验</p>

      </div>
      <p style="font-weight: bold;padding-top: 40px;color: #666666">加入我们</p>
      <hr>
      <p>我们致力于优质内容的传播，美好'商品'的分享。讲究生活品质，做一个有格调的小资。在追求美学的道路上，我们邀你一起：</p>
      <ul>
        <li>5名Java主编</li>
        <li>5名PHP主编</li>
        <li>3名网站内容编辑</li>
        <li>10名商品美学鉴赏主编</li>
      </ul>
      <p>Java主编</p>
      <ul>
        <li>原创Java技术类文章分享</li>
        <li>优质Java技术类文章推荐</li>
      </ul>
      <p>PHP主编</p>
      <ul>
        <li>原创PHP技术类文章分享</li>
        <li>优质PHP技术类文章推荐</li>
      </ul>
      <p>网站内容编辑</p>
      <ul>
        <li>网站内容浏览，评论</li>
        <li>活跃于龙门阵</li>
        <li>网站内容产出</li>
        <li>网站用户意见收集</li>
      </ul>
      <p>10名商品美学鉴赏主编</p>
      <ul>
        <li>小而美商品发掘与推荐</li>
        <li>将商品品鉴文章分到各大社区</li>
      </ul>
      <blockquote style="font-size: x-small">
        <i>我们是非营利组织，故而没法给你报酬，但我们承若：基于网站未来想象空间，我们将视你们为网站团队一员，把你们的需求放在第一位。</i>
        <p>&nbsp;</p>
        <p>如果你有兴趣，请发邮件至market@liuyuanwai.cn。注明职位</p>
      </blockquote>
      <%--<ul>--%>
      <%--<li><p>资讯服务</p></li>--%>
      <%--<li><p>售卖服务</p></li>--%>
      <%--<li><p>广告服务</p></li>--%>
      <%--<li><p>分红福利</p></li>--%>
      <%--</ul>--%>
      <table style="width: 100%;margin-top: 40px">
        <tr>
          <td style="font-weight: bold;color: #666666">友情链接</td>
          <td align="right"><a href="javascript:toAddLink()">添加友链</a></td>
        </tr>
      </table>
      <hr>
      <div class="lindArea text-center" style="padding: 100px 0px">
        <i class="icon-spinner icon-spin"></i>&nbsp;正在加载数据...
      </div>
      <ul class="linkList"></ul>
      <p style="font-weight: bold;padding-top: 40px;color: #666666">交换链接</p>
      <hr>
      <blockquote style="margin-left: 10px;padding: 5px">
        <p>友情链接不仅仅是一种相互推广的方式，更是一种交友方式。我们非常欢迎，在上面添加你的网站链接!</p>

        <p>寻梦？撑一支长篙，向青草更青处漫溯&nbsp;...</p>
      </blockquote>
      <p style="font-weight: bold;padding-top: 40px;color: #666666">关注我们</p>
      <hr>
      <table style="width: 100%">
        <tr>
          <td style="width: 50%" align="center">
            <img src="<%=basePath%>/img/qqQrcode.png" class="img-responsive img-thumbnail" style="width: 50%">

            <p style="padding-top: 5px">劉員外官方群</p>
          </td>
          <td style="width: 50%" align="center">
            <img src="<%=basePath%>/img/qrcode.jpg" class="img-responsive img-thumbnail" style="width: 50%">

            <p style="padding-top: 5px">微信公众号</p>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="linkModal" role="dialog" style="margin-top: 10%">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" align="center">
        添加友链
      </div>
      <div class="modal-body" style="padding: 0px">
        <input type="text" maxlength="16"
               style="width: 100%;padding: 10px;border: none" id="linkName"
               placeholder="网站名称">
        <hr style="margin: 0px">
        <input type="text" maxlength="128"
               style="width: 100%;padding: 10px;border: none" id="linkUrl"
               placeholder="网站地址[http://开头，否则跳转失败]">
        <hr style="margin: 0px">
        <input type="text" maxlength="1024"
               style="width: 100%;padding: 10px;border: none" id="linkDesc"
               placeholder="网站描述">
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" onclick="addLink();">保&nbsp;存</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="linkId">
<jsp:include page="../foot.jsp"></jsp:include>
<jsp:include page="common/common-html.jsp"></jsp:include>
</body>
<script>

  function addLink() {
    $("#btn").blur();
    var email = '<%=email%>';
    if (email == 'null') {
      alerErr("系统检测到你没有登录!");
      return;
    }
    var linkName = $.trim($("#linkName").val());
    var linkUrl = $.trim($("#linkUrl").val());
    var linkDesc = $.trim($("#linkDesc").val());
    var linkId = $("#linkId").val();
    if (linkName == '') {
      alerErr("输入网站名称!");
      return;
    }
    if (linkUrl == '') {
      alerErr("输入网站地址!");
      return;
    }
    if (linkDesc == '') {
      alerErr("输入网站描述!");
      return;
    }
    var param = {linkName: linkName, linkUrl: linkUrl, linkDesc: linkDesc, linkId: linkId};
    var url = '<%=basePath%>/link/saveOrUpdate';
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    $.post(url, param, function (data, status) {
      $("#loadModal").modal('hide');
      if (data.code == 200) {
        $("#linkName").val('');
        $("#linkUrl").val('');
        $("#linkDesc").val('');
        $("#linkId").val('');
        $('#linkModal').modal('hide');
        loadLink();
      } else {
        aler(data);
      }
    })
  }
  function toAddLink() {
    $("#linkModal").modal();
  }

  // - 日期解析
  function formatDate(strTime) {
    if (strTime == null || strTime == '') {
      return "";
    }
    var date = new Date(strTime);
    return date.getFullYear() + "-" + addZero(date.getMonth() + 1) + "-" + addZero(date.getDate()) + " " + addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
  }
  function addZero(val) {
    return val < 10 ? '0' + val : val;
  }

  function loadLink() {
    var url = '<%=basePath%>/link/list';
    var loginEmail = '<%=email%>';
    var role = '<%=role%>';
    $.post(url, function (data, status) {
      $(".lindArea").remove();
      if (data.code == 200) {
        $(".linkList").empty();
        var list = data.data;
        if (list.length > 0) {
          for (var i = 0; i < list.length; i++) {
            var link = list[i];
            var email = link.email;
            var temp = '<li>';
            temp += '<div>';
            temp += '<a target="_blank" style="color: blueviolet" href="' + link.linkUrl + '"onclick="toLink(\'' + link.linkUrl + '\',\'' + link.id + '\',\'' + link.clickCount + '\')">' + link.linkName + '</a>';
            if (email == loginEmail) {
              temp += '<a href="javascript:editLink(\'' + link.id + '\',\'' + link.linkName + '\',\'' + link.linkUrl + '\',\'' + link.linkDesc + '\');"><small style="padding-left: 10px;color: #cccccc">编辑</small></a>';
            }
            if (role == 1) {
              temp += '<a href="javascript:delLink(\'' + link.id + '\');"><small style="padding-left: 10px;color: #cccccc">删除</small></a>';
            }
            temp += '</div>';
            temp += '<div style="color: #CCCCCC;"><small>' + formatDate(link.createTime) + '&nbsp;|&nbsp;点击：</small><span style="color: #666666;" id="clickCount' + link.id + '">' + link.clickCount + '</span></div>';
            temp += '<div style="color: #999999"><small>' + link.linkDesc + '</small></div>';
            temp += '</li>';
            $(".linkList").prepend(temp);
          }
        }
      } else {
        aler(data);
      }
    })
  }

  function toLink(linkUrl, linkId, clickCount) {
    var url = '<%=basePath%>/link/incrClickCount';
    $.post(url, {linkId: linkId}, function (data, status) {
      var clazz = '#clickCount' + linkId;
      $(clazz).text(parseInt(clickCount) + 1);
//      window.open(linkUrl);
//      $("<a target='_blank' href='" + linkUrl + "' style='display:none'>test</a>").appendTo($("body"))[0].click();
    })
  }

  function delLink(linkId) {
    var url = '<%=basePath%>/link/del';
    var param = {linkId: linkId};
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        loadLink();
      } else {
        aler(data);
      }
    })
  }

  function editLink(id, linkName, linkUrl, linkDesc) {
    $("#linkId").val(id);
    $("#linkName").val(linkName);
    $("#linkUrl").val(linkUrl);
    $("#linkDesc").val(linkDesc);
    $("#linkModal").modal();
  }

  $(function () {
    initStyle();

    loadLink();
  })
</script>
</html>
