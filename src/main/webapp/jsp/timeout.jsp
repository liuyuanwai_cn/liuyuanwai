<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <title>登录超时</title>

    <jsp:include page="common/common-css.jsp"></jsp:include>

    <style type="text/css">
        td{padding: 5px}
        li{list-style: disc}
    </style>
</head>

<body style="background-color: #f4f7ed;">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-12 ">
            <table style="margin-left: auto;margin-right: auto;">
                <tr>
                    <td align="center"><h1><i class="icon-spinner icon-3x" style="color: #4acaa8"></i></h1></td>
                </tr>
                <tr>
                    <td>
                        <h3>提示：登录超时</h3>
                        <p>尝试如下操作：</p>
                        <ul style="padding-left: 20px">
                            <li style="list-style: none">您可以操作：<a href="<%=basePath%>/" target="_top">登录</a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>

</html>
