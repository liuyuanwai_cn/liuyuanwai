<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>劉員外,关于梦想,从不含糊</title>
  <link rel="stylesheet" href="<%=basePath%>/css/home.css"/>
  <jsp:include page="common/common-css.jsp"></jsp:include>
  <jsp:include page="common/common-js.jsp"></jsp:include>
  <style type="text/css">
    .IN {
      width: 80px;
      height: 80px;
      border-radius: 100px;
      font-size: 12px;
      background-color: #FFFFFF;
      color: #33CCFF;
      z-index: 10004;
      position: absolute;
      top: 50%;
      left: 50%;
      display: none;
      margin-top: -40px;
      margin-left: -40px;
    }
  </style>
</head>
<script language="JavaScript">

  function syncLogin() {
    var email = "<%=email%>";
    if (email == null || email == "null" || email == "") {
      email = getCookie("email");
      if (email == null || email == "null" || email == "") {
        return;
      }
      var login = getCookie("login");
      if (login == null || login == "null" || login == "") {
        return;
      }
      var url = "<%=basePath%>/user/fakeLogin";
      var param = {email: email, login: login};
      $.post(url, param, function (data, status) {
        data = JSON.parse(data);
        if (data.code == 200) {
          // - 异步登录成功设置email隐藏域的值
          $("#email").val(email);
          // - 同步更新inCount
          setCookie("inCount", data.data.inCount);
        }
      })
    }
  }

  $(function () {
    $(".bg-img").height($(window).height() - 50);
    $(".welcome-bg").height($(window).height());
    // - 动画执行完监听
    $(".welcome-bg").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
      // - 从cookie去帐号信息做异步登录
      var email = "<%=email%>";
      // - 如果没有从session取到登录信息，则从cookie取登录信息做异步登录
      syncLogin(email);
      $(".IN").fadeIn();
    });
    $(".IN").click(function () {
      $(".IN").fadeOut(function () {
        $(".welcome-bg").slideUp(function () {
          window.location = "<%=basePath%>/shop";
        });
      })
    })

  })
</script>
<body style="padding: 0px;margin: 0px;">

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 welcome-bg text-center">
  <img class="img-responsive bg-img bg-img-0" src=""/>

  <div class="the-sun the-sun-0"></div>
</div>
<%--IN--%>
<table class="IN">
  <tr>
    <td align="center" style="font-size:xx-large;" valign="bottom">In</td>
  </tr>
  <tr>
    <td align="center" valign="top" id="inCount"></td>
  </tr>
</table>
<!-- Wrapper -->
<div id="wrapper" style="overflow-y: auto">
  <div id="main">
    <section>
      <div id="container">

      </div>
    </section>
  </div>
</div>
<input id="email" type="hidden">
</body>
</html>
