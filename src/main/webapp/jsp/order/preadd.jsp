<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>${orderExtDto.productName} - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <script type="application/javascript"
          src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>
  <script type="application/javascript"
          src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.zh-CN.js"></script>
  <link rel="stylesheet" href="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.css">
  <style type="text/css">
    .preDesc {
      width: 100px;
    }

    .valDesc {
      color: darkgray;
    }

    td {
      padding: 10px 0px;
    }
  </style>
</head>
<body style="margin: 0px;padding: 0px;background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 10px;padding: 0px">
    <div class="col-xs-0 col-sm-0 col-md-1 col-lg-2"></div>
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8" style="padding: 0px">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 content" style="padding: 0px;background-color: #ffffff">
        <div style="padding: 10px">
          <p style="font-weight: bold;padding-top: 10px">${orderExtDto.productName}</p>

          <p style="color: red;">￥<span
            style="font-size: large">${orderExtDto.price}</span>/${orderExtDto.unit}</p>
          <hr>
          <p class="goodRemark" style="">${orderExtDto.remark}</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0px 1px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-md hidden-lg"><p>&nbsp;</p></div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
          <div style="background-color: #ffffff;padding: 10px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
              <table style="width: 100%">
                <tr>
                  <td align="center">
                    <a href="<%=basePath%>/user/queryinfo?email=${orderExtDto.shopEmail}">
                      <img src="${orderExtDto.shopUrl}-120x120" class="img-responsive img-circle">
                    </a>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
              <p><a style="color: #CC9900"
                    href="<%=basePath%>/user/queryinfo?email=${orderExtDto.shopEmail}">${orderExtDto.shopName}</a>
              </p>

              <p><i class="icon-mobile-phone icon-large" style="color: #999999">
                &nbsp;${orderExtDto.shopPhone}</i></p>

              <p><i class="icon-map-marker" style="color: #999999">&nbsp;${orderExtDto.shopAddr}</i></p>
            </div>
            <hr style="width: 100%;margin: 0px">
            <table style="width: 100%;">
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td colspan="2">
                  <input type="text" id="count" style="border: none;width: 100%;padding-left: 0px"
                         onkeyup="this.value=this.value.replace(/\D/g,'')"
                         onafterpaste="this.value=this.value.replace(/\D/g,'')"
                         placeholder="输入购买数量【数字】">
                </td>
              </tr>
              <tr style="border-bottom: 1px solid #ebebeb;" class="sendHome">
                <td colspan="2">
                  <textarea type="text" style="width: 100%;border: none;resize: none;padding: 0px" rows="3"
                            id="sendAddr"
                            placeholder="输入送货地址">${orderExtDto.addr}</textarea>
                </td>
              </tr>
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="preDesc" id="timeDesc">送货时间</td>
                <td align="right">
                  <div class="input-group">
                    <input type="text" style="width: 100%;border: none" class="form_datetime"
                           readonly id="sendtime">
                                        <span class="input-group-btn">
                  <a type="button" rel="popover">
                    &nbsp;<i class="icon-calendar text-default-color"></i>&nbsp;
                  </a>
                </span>
                  </div>
                </td>
              </tr>
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td colspan="2">
                  <table style="width: 100%">
                    <tr>
                      <td style="padding: 0px">
                        <a type="text" href="javascript:void(0);" class="deliveryTypeText"
                           style="display: block;text-decoration: none" onclick="chooseDeliveryType()">送货上门</a>
                      </td>
                      <td align="center" style="width: 8px;padding: 0px">
                        <i class=" icon-angle-right"></i>
                        <input type="hidden" id="deliveryType" value="2">
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="preDesc">总金额</td>
                <td align="right" class="valDesc">
                  <b id="amountDesc" style="font-weight: bold;display: inline"></b>
                </td>
              </tr>
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="valDesc" colspan="2">
              <textarea rows="3" placeholder="给商家留言" maxlength="512"
                        style="resize: none;width: 100%;border: none;padding: 0px"
                        id="remark"></textarea>
                </td>
              </tr>
              <tr>
                <td>
                  <a href="javascript:calIncome(${orderExtDto.productId});"
                     style="margin-right: 12px;margin-top: 10px">
                    计算返现
                  </a>
                </td>
                <td align="right">
                  <c:choose>
                    <c:when test="${orderExtDto.inventory > 0}">
                      <button id="save" class="btn btn-info"
                              style="">立即购买
                      </button>
                    </c:when>
                    <c:otherwise>
                      <button id="save" class="btn" disabled
                              style="margin-left: 12px;margin-bottom: 20px;margin-top: 10px;background-color: #999999;color: #ffffff">
                        &nbsp;已卖完&nbsp;</button>
                    </c:otherwise>
                  </c:choose>
                </td>
              </tr>
            </table>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
               style="background-color: #ffffff;margin-top: 15px;padding: 10px">
            <div style="padding: 15px 10px"><i class="icon-bookmark">&nbsp;</i>关于支付：通过微信/支付宝扫码转账以节省成本</div>

            <hr style="margin: 0px">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" style="padding: 30px">
              <c:choose>
                <c:when test="${orderExtDto.weixinQrcode == null}">
                  <img src="<%=basePath%>/img/weixin.jpg" class="img-responsive img-thumbnail"
                       style="padding: 10px">
                </c:when>
                <c:otherwise>
                  <img src="${orderExtDto.weixinQrcode}-120x120" class="img-responsive img-thumbnail"
                       style="padding: 10px">
                </c:otherwise>
              </c:choose>
              <small style="color: darkgray;display: block;padding: 10px" class="text-center">微信支付</small>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" style="padding: 30px">
              <c:choose>
                <c:when test="${orderExtDto.alipayQrcode == null}">
                  <img src="<%=basePath%>/img/zhifubao.jpg" class="img-responsive img-thumbnail"
                       style="padding: 10px">
                </c:when>
                <c:otherwise>
                  <img src="${orderExtDto.alipayQrcode}-120x120" class="img-responsive img-thumbnail"
                       style="padding: 10px">
                </c:otherwise>
              </c:choose>
              <small style="color: darkgray;display: block;padding: 10px" class="text-center">支付宝支付
              </small>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
               style="background-color: #ffffff;margin-top: 15px;padding: 10px">
            <a href="javascript:preReply();"
               style="text-decoration: none;padding: 15px 10px;display: block;">
              <i class="icon-edit" style="font-weight: bold">&nbsp;写留言</i>
            </a>
            <hr style="margin: 0px">
            <table style="width: 100%;display: none" class="commentBox">
              <tr>
                <td style="padding: 3px">
                  <textarea id="note" type="text" rows="3" style="width: 100%;padding: 5px;border: none;resize: none"
                            placeholder="输入留言"></textarea>
                </td>
              </tr>
              <tr>
                <td align="right" style="padding:3px">
                  <a class="btn btn-info" style="padding: 2px"
                     href="javascript:cancelReply();">&nbsp;取&nbsp;消&nbsp;</a>
                  <a class="btn btn-info" style="padding: 2px;margin-left: 10px"
                     href="javascript:reply('${orderExtDto.uuid}','${orderExtDto.shopEmail}','${orderExtDto.productName}');">
                    &nbsp;回&nbsp;复&nbsp;</a>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px">
                  <hr style="margin: 0px">
                </td>
              </tr>
            </table>
            <div style="padding: 100px 0px;display: none;color: #CCCCCC" align="center" class="noNotes">
              No Comments Here !
            </div>
            <div class="notes" style="padding: 10px 0px"></div>
            <div style="padding: 100px" align="center" class="loadingNotes">
              <i class="icon- icon-spinner icon-spin text-default-color"></i>&nbsp;加载留言...
            </div>
            <div class="loadMoreNotes text-center" style="display: none;padding: 40px 0px"><a
              style="text-decoration: none" href="javascript:void(0);">加载更多</a></div>
            <div class="loadNotesOver text-center" style="display: none;padding: 40px 0px"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 deliveryTypeBox"
     style="position: fixed;left: 0px;bottom: 0px;z-index: 999;background-color: #ffffff;display: none;padding: 0px">
  <table style="width: 100%;">
    <tr>
      <td style="background-color: #e5e5e5;" colspan="2" align="center">选择购买方式</td>
    </tr>
    <tr>
      <td style="padding: 10px"><a href="javascript:void(0);" style="text-decoration: none;display: block"
                                   onclick="sendToHome()">送货上门</a></td>
      <td style="width: 10px;padding: 10px" class="SendToHome"><i class="icon-ok"></i></td>
    </tr>
    <tr>
      <td colspan="2" style="padding: 0px">
        <hr style="margin: 0px;">
      </td>
    </tr>
    <tr>
      <td style="padding: 10px"><a href="javascript:void(0);" style="text-decoration: none;display: block"
                                   onclick="getBySelf()">预约到店</a></td>
      <td align="right" style="width: 10px;padding: 10px" class="GetBySelf"></td>
    </tr>
  </table>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="incomeModal" role="dialog" style="margin-top: 10%;">
  <div class="modal-dialog modal-sm" style="">
    <div class="modal-content" style="">
      <div class="modal-header" align="center">
        收益明细
      </div>
      <div class="modal-body">
        <p>购买返现：￥<span id="part1" style="font-size: x-large;" class="text-default-color"></span></p>

        <p>邀请购买提成：￥<span id="part2" style="font-size: x-large;" class="text-default-color"></span></p>
      </div>
      <div class="modal-footer">
        <p>
          <a class="btn btn-default" data-dismiss="modal">了解</a>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" id="imgModal" role="dialog" style="padding: 0px;margin: 0px;">
  <div class="modal-dialog modal-lg" style="margin: 0px auto;height: 100%">
    <div class="modal-content"
         style="border: none;box-shadow: none;background-color: transparent;padding: 0px;margin: 0px;height: 100%">
      <table style="width: 100%;height: 100%">
        <tr>
          <td align="center" valign="middle">
            <img src="" class="img-responsive imgShow">
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<input type="hidden" id="productId" value="${orderExtDto.productId}">
<input type="hidden" id="amount">
<input type="hidden" id="originalCost">
<input type="hidden" id="toEmail">
<jsp:include page="../common/common-html.jsp"></jsp:include>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>


<script language="JavaScript">
  var notePageNo = 1;
  var notePageSize = 10;

  function preReply() {
    $('.commentBox').show();
  }
  function cancelReply() {
    $("#note").val('');
    $('.commentBox').hide();
  }


  function toReply(toEmail, toName) {
    $('.commentBox').show();
    $("#toEmail").val(toEmail);
    if (toEmail != '') {
      $("#note").val('@' + toName + ' ');
    }
    $("#note").focus();
  }

  function reply(uuid, shopEmail, title) {
    $(".btn").blur();
    if (uuid == null || uuid == '' || uuid == 'null') {
      return;
    }
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    var content = $.trim($('#note').val());
    if (content == '') {
      alerErr("输入留言内容");
      return;
    }
    var url = "<%=basePath%>/note/add";
    var toEmail = $("#toEmail").val();
    if (toEmail != '') {
      shopEmail = toEmail;
    }
    var param = {content: content, uuid: uuid, toEmail: shopEmail, title: title};
    $.post(url, param, function (data, status) {
      cancelReply();
      if (status == 'success') {
        $("#toEmail").val('');
        $('#note').val('');
        $(".noNotes").hide();
        var temp = parseNote(data.data.email, data.data.logo, data.data.content, data.data.name, data.data.createTime);
        $('.notes').prepend(temp);
      } else {
        aler(data);
      }
    })
  }
  function parseNote(email, logo, content, name, createTime) {
    if (logo != null && logo != '' && logo != undefined) {
      logo += "-120x120";
    }
    if (logo == undefined) {
      logo = '<%=basePath%>/img/user.png';
    }
    var url = "<%=basePath%>/user/queryinfo?email=" + email;
    var subItem = '';
    subItem += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px;">';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr>';
    subItem += '<td style="width: 50px;height: 50px;padding: 5px" align="center" valign="top">';
    subItem += '<a href="' + url + '"><img class="img-responsive img-circle"src="' + logo + '" /></a>';
    subItem += '</td>';
    subItem += '<td style="color: #999999;padding: 0px 0px 0px 5px;"><small>' + name + '</small></td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style="padding: 0px">&nbsp;</td>';
    subItem += '<td style="padding: 0px 0px 0px 5px;">' + content + '</td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style="padding: 0px">&nbsp;</td>';
    subItem += '<td style="color: #999999;padding: 0px 0px 0px 5px;">';
    subItem += '<small>' + createTime + '&nbsp;&nbsp;<a href="javascript:toReply(\'' + email + '\',\'' + name + '\');"><i class="icon-comments-alt"></i></a></small>';
    subItem += '</td>';
    subItem += '</tr>';
    subItem += '<tr><td style="padding: 0px">&nbsp;</td><td style="padding: 0px"><hr style="margin: 0px;padding: 0px"></td></tr>';
    subItem += '</table>';
    subItem += '</div>';
    return subItem;
  }
  function loadNotes(notePageNo, notePageSize) {
    var uuid = '${orderExtDto.uuid}';
    var url = '<%=basePath%>/note/list';
    var param = {uuid: uuid, pageNo: notePageNo, pageSize: notePageSize};
    $.post(url, param, function (data, status) {
      if (data != null && data.length > 0) {
        $(".loadingNotes").hide();
        $(".noNotes").hide();
        var item = '';
        for (var i = 0; i < data.length; i++) {
          item += parseNote(data[i].email, data[i].logo, data[i].content, data[i].name, data[i].createTime);
        }
        $(".notes").prepend(item);
        if (data.length == notePageSize) {
          $(".loadMoreNotes").show();
        } else {
          $(".loadNotesOver").show();
        }
      } else {
        $(".loadingNotes").hide();
        $(".loadMoreNotes").hide();
        if (notePageNo == 1) {
          $(".noNotes").show();
        } else {
          $(".loadNotesOver").show();
        }
      }
    })
  }
  $(".loadMoreNotes").click(function () {
    $(".loadingNotes").show();
    $(".loadMoreNotes").hide();
    loadNotes(notePageNo++, notePageSize);
  })

  function isGetBySelf(deliveryType) {
    if (deliveryType == 1) {
      return true;
    } else {
      return false;
    }
  }

  // - 下单
  function save() {
    $(".btn").blur();
    var email = "<%=email%>";
    if (!validateEmail(email)) {
      return;
    }
    var count = $.trim($("#count").val());
    var sendtime = $.trim($("#sendtime").val());
    var deliveryType = $("#deliveryType").val();
    var remark = $.trim($("#remark").val());
    var sendAddr = $.trim($("#sendAddr").val());
    var productId = $("#productId").val();
    var amount = $("#amount").val();
    var originalCost = $("#originalCost").val();
    if (count == '') {
      alerErr("输入购买数量!");
      return;
    }
    if (sendtime == "") {
      if (isGetBySelf(deliveryType)) {
        alerErr("选择取货时间!");
      } else {
        alerErr("选择送货时间!");
      }
      return;
    }
    if (!isGetBySelf(deliveryType)) {
      // - 送货上门
      if (sendAddr == '') {
        alerErr("填写收获地址!");
        return;
      }
    }
    if (count > ${orderExtDto.inventory}) {
      alerErr("库存不足!");
      return;
    }
    var param = {
      productId: productId,
      email: email,
      count: count,
      sendtime: sendtime,
      amount: amount,
      originalCost: originalCost,
      deliveryType: deliveryType,
      sendAddr: sendAddr,
      remark: remark
    };
    var url = "<%=basePath%>/order/add";
    $.post(url, param, function (data, status) {
      aler(data);
      if (data.code == 200) {
        var backUrl = '<%=basePath%>/order/info?orderNo=' + data.data;
        window.location = "<%=basePath%>/success?backUrl=" + backUrl;
      }
    })
  }

  // - 计算总金额和成本
  function calAmount() {
    var price = ${orderExtDto.price};
    var originalPrice = ${orderExtDto.originalPrice};
    var count = $("#count").val();
    var amount = price * count;
    var originalCost = originalPrice * count;
    $("#amount").val(amount);
    $("#originalCost").val(originalCost);
    $("#amountDesc").text('￥' + amount + " 元");
  }

  function calIncome(productId) {
    var url = '<%=basePath%>/product/calIncome?productId=' + productId;
    $.post(url, function (data, status) {
      if (status == 'success') {
        $("#part1").text(data.part1);
        $("#part2").text(data.part2);
        $("#incomeModal").modal();
      } else {
        alerErr('计算失败');
      }
    })
  }

  function incrReadCount() {
    var url = '<%=basePath%>/product/incrReadCount';
    var param = {productId: '${orderExtDto.productId}'};
    $.post(url, param, function (data, status) {

    })
  }
  var temp = '<i class="icon-ok"></i>';
  function chooseDeliveryType() {
    $('.deliveryTypeBox').slideToggle();
  }
  function sendToHome() {
    $("#deliveryType").val(2);
    $('.GetBySelf').empty();
    $('.SendToHome').empty();
    $('.SendToHome').append(temp);
    $('.deliveryTypeBox').slideToggle();
    $('.deliveryTypeText').text('送货上门');
    $(".sendHome").show();
    $("#timeDesc").text("送货时间");
  }
  function getBySelf() {
    $("#deliveryType").val(1);
    $('.SendToHome').empty();
    $('.GetBySelf').empty();
    // - 到店自提
    $('.GetBySelf').append(temp);
    $('.deliveryTypeBox').slideToggle();
    $('.deliveryTypeText').text('预约到店');
    $(".sendHome").hide();
    $("#timeDesc").text("到店时间");
  }


  $(function () {
    initStyle();
    $('img').addClass('img-responsive');
    $('.goodRemark img').css('margin', '10px 0px');
    incrReadCount();
    loadNotes(notePageNo++, notePageSize);
    // - 日期选择器
    $("#sendtime").datetimepicker({
      format: "yyyy-mm-dd hh:ii",
      autoclose: true,
      todayBtn: true,
      todayHighlight: true,
      minView: "hour",
      language: "zh-CN",
      todayHighlight: true
    });

    $("#count").change(function () {
      // - 计算总金额
      calAmount();
    })

    // - 下单
    $("#save").click(function () {
      save();
    })

    $('.chooseDeliveryType').click(function () {
      chooseDeliveryType();
    })


    $('.content img').css('cursor', 'crosshair');
    $('.content img').click(function () {
      var imgUrl = $(this).attr('src');
      if (imgUrl.indexOf('800x800') > 0 || imgUrl.indexOf('750x500') > 0 || imgUrl.indexOf('580x350') > 0 || imgUrl.indexOf('120x120') > 0) {
        imgUrl = imgUrl.substr(0, imgUrl.length - 8);
      }
      $('.imgShow').attr('src', imgUrl);
      $('#imgModal').modal();
    })

    $('.imgShow').click(function () {
      $('#imgModal').modal('hide');
    })
  })

</script>
</html>
