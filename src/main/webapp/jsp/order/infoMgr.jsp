<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
    String basePath = request.getContextPath();
    Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
    <title>订单详情</title>
    <jsp:include page="../common/common-css.jsp"></jsp:include>
    <jsp:include page="../common/common-js.jsp"></jsp:include>
    <script type="application/javascript"
            src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="application/javascript"
            src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.zh-CN.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.css">

    <style type="text/css">
        .circle {
            width: 45px;
            height: 45px;
            border-radius: 100px;
            font-size: 12px;
            background-color: #d3d3d3;
            color: #FFFFFF;
        }

        .preDesc {
            color: darkgrey;
            padding: 12px;
        }

        .contentDesc {
            padding: 12px;
        }
    </style>
</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed;">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">
        <div class="col-xs-0 col-sm-0 col-md-1 col-lg-2"></div>
        <div class="col-xs-12 col-sm-8 col-md-5 col-lg-4" style="background-color: #ffffff;margin: 15px 0px;padding: 0px">
            <table style="margin: 10px auto;">
                <tr>
                    <td>--</td>
                    <td align="center" class="circle status1">待确认</td>
                    <td class="">--</td>
                    <td align="center" class="circle status2">已接单</td>
                    <c:if test="${orderExtDto.deliveryType == 2}">
                        <td class="">--</td>
                        <td align="center" class="circle status3">派送中</td>
                    </c:if>
                    <td class="">--</td>
                    <td align="center" class="circle status4">已完成</td>
                    <td align="center" class="circle status5" style="display: none">已拒绝</td>
                    <td align="center" class="circle status6" style="display: none">已撤单</td>
                    <td align="center" class="circle status7" style="display: none">超时</td>
                    <td>--</td>
                </tr>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0px;padding: 0px;"
                 align="center">
                <img src="${orderExtDto.url}-750x500" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
                <div class="form-group" style="width: 100%">
                    <table style="width: 100%">
                        <tr style="border-bottom: 1px solid #ebebeb;">
                            <td class="contentDesc" style="width: 120px">商品</td>
                            <td class="preDesc" class="contentDesc" align="right">${orderExtDto.productName}</td>
                        </tr>
                        <tr style="border-bottom: 1px solid #ebebeb;">
                            <td class="contentDesc">单价</td>
                            <td class="preDesc" class="contentDesc"
                                align="right">${orderExtDto.price}&nbsp;元/${orderExtDto.unit}</td>
                        </tr>
                        <tr style="border-bottom: 1px solid #ebebeb;">
                            <td class="contentDesc">购买数量</td>
                            <td class="preDesc" class="contentDesc"
                                align="right">${orderExtDto.count}${orderExtDto.unit}</td>
                        </tr>
                        <c:if test="${orderExtDto.deliveryType == '1'}">
                            <tr style="border-bottom: 1px solid #ebebeb;">
                                <td class="contentDesc">购买方式</td>
                                <td class="preDesc contentDesc" align="right">预约到店</td>
                            </tr>
                        </c:if>
                        <c:if test="${orderExtDto.deliveryType == '2'}">
                            <tr style="border-bottom: 1px solid #ebebeb;">
                                <td class="contentDesc">购买方式</td>
                                <td class="preDesc contentDesc" align="right">送货上门</td>
                            </tr>
                        </c:if>
                        <c:if test="${orderExtDto.deliveryType == '1'}">
                            <tr style="border-bottom: 1px solid #ebebeb;">
                                <td class="contentDesc">到店时间</td>
                                <td class="preDesc contentDesc" align="right"><fmt:formatDate
                                        value='${orderExtDto.sendtime}'
                                        pattern='yyyy-MM-dd HH:mm:ss'/></td>
                            </tr>
                        </c:if>
                        <c:if test="${orderExtDto.deliveryType == '2'}">
                            <tr style="border-bottom: 1px solid #ebebeb;">
                                <td class="contentDesc">送货时间</td>
                                <td class="preDesc contentDesc" align="right"><fmt:formatDate
                                        value='${orderExtDto.sendtime}'
                                        pattern='yyyy-MM-dd HH:mm:ss'/></td>
                            </tr>
                        </c:if>
                        <c:if test="${orderExtDto.deliveryType == '2'}">
                            <tr style="border-bottom: 1px solid #ebebeb;">
                                <td class="contentDesc" valign="top">送货地址</td>
                                <td class="preDesc contentDesc" align="right">${orderExtDto.sendAddr}</td>
                            </tr>
                        </c:if>
                        <tr style="border-bottom: 1px solid #ebebeb;">
                            <td class="contentDesc">联系人</td>
                            <td class="preDesc contentDesc" align="right">${orderExtDto.companyName}</td>
                        </tr>
                        <tr style="border-bottom: 1px solid #ebebeb;">
                            <td class="contentDesc">联系电话</td>
                            <td class="preDesc" align="right">${orderExtDto.phone}</td>
                        </tr>
                        <tr style="border-bottom: 1px solid #ebebeb;">
                            <td class="contentDesc">总金额</td>
                            <td class="preDesc contentDesc" align="right">${orderExtDto.amount}元</td>
                        </tr>
                        <tr>
                            <td class="contentDesc">用户留言</td>
                            <td class="preDesc contentDesc" align="right">${orderExtDto.orderRemark}</td>
                        </tr>
                        <c:if test="${orderExtDto.status == 5}">
                            <tr style="border-top: 1px solid #ebebeb;">
                                <td class="contentDesc">拒绝原因</td>
                                <td class="preDesc contentDesc" align="right">${orderExtDto.reason}</td>
                            </tr>
                        </c:if>
                        <c:if test="${orderExtDto.status == 6}">
                            <tr style="border-top: 1px solid #ebebeb;">
                                <td class="contentDesc">撤单原因</td>
                                <td class="preDesc contentDesc" align="right">${orderExtDto.reason}</td>
                            </tr>
                        </c:if>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4" align="center" style="padding: 0px;">
            <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3" align="center"
                 style="margin-top: 15px;margin-bottom: 15px;padding: 0px">
                <button style="border: none;padding: 15px;background-color: #CC0099;color: #ffffff"
                        class="btn btn-default" id="acceptOrder">
                    <i class="icon-check icon-3x"><br><span>接单</span></i>
                </button>
            </div>
            <c:if test="${orderExtDto.deliveryType == 2}">
                <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3" align="center"
                     style="margin-top: 15px;margin-bottom: 15px;padding: 0px">
                    <button style="border: none;padding: 15px;background-color: #006699;color: #ffffff"
                            class="btn btn-default" id="sendOrder">
                        <i class="icon-plane icon-3x"><br><span>派送</span></i>
                    </button>
                </div>
            </c:if>
            <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3" align="center"
                 style="margin-top: 15px;margin-bottom: 15px;padding: 0px">
                <button style="border: none;padding: 15px;background-color: #669900;color: #ffffff"
                        class="btn btn-default" id="finishOrder">
                    <i class="icon-circle icon-3x"><br><span>完成</span></i>
                </button>
            </div>
            <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3" align="center"
                 style="margin-top: 15px;margin-bottom: 15px;padding: 0px">
                <button style="border: none;padding: 15px;background-color: #FF0000;color: #ffffff"
                        class="btn btn-default" id="rejectOrder">
                    <i class="icon-remove icon-3x"><br><span>拒绝</span></i>
                </button>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
<!-- 拒绝订单Modal -->
<div class="modal fade bs-example-modal-sm" id="rejectOrderModal" role="dialog" style="margin-top: 100px">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <b>拒绝理由</b>
            </div>
            <div class="modal-body" style="padding: 0px;">
                <div class="list-group" style="border: 0px solid red;margin: 0px">
                    <a href="#" class="list-group-item" style="background-color: #4acaa8">不在送货时间</a>
                    <a href="#" class="list-group-item">不在送货范围</a>
                    <a href="#" class="list-group-item">小店暂未营业</a>
          <textarea id="prereason" class="list-group-item" maxlength="100" style="width: 100%;resize: none"
                    placeholder="其他原因..."></textarea>
                    <input type="hidden" id="reason" value="不在送货时间">
                </div>
            </div>
            <div class="modal-footer" align="right">
                <button class="btn btn-info" id="reasonOk">确定</button>
                <button class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
</body>


<script language="JavaScript">
    function initOrderStatus() {
        $(".status1").css("background-color", "#d3d3d3");
        $(".status2").css("background-color", "#d3d3d3");
        $(".status3").css("background-color", "#d3d3d3");
        $(".status4").css("background-color", "#d3d3d3");
        $(".status5").css("background-color", "#d3d3d3");
        $(".status6").css("background-color", "#d3d3d3");
        $(".status7").css("background-color", "#d3d3d3");
    }
    // - 解析订单状态
    function parseOrderStatus(status) {
        initOrderStatus();
        if (status == 1) {
            $(".status1").css("background-color", "#4acaa8");
        } else if (status == 2) {
            $(".status2").css("background-color", "#4acaa8");
        } else if (status == 3) {
            $(".status3").css("background-color", "#4acaa8");
        } else if (status == 4) {
            $(".status4").css("background-color", "#4acaa8");
        } else if (status == 5) {
            // - 订单拒绝
            $(".status5").css("background-color", "#4acaa8");
            $(".status5").show();
            $(".status2").hide();
            $(".status3").hide();
            $(".status4").hide();
        } else if (status == 6) {
            // - 订单撤销
            $(".status6").css("background-color", "#4acaa8");
            $(".status6").show();
            $(".status2").hide();
            $(".status3").hide();
            $(".status4").hide();
        } else if (status == 7) {
            // - 订单超时
            $(".status7").css("background-color", "#4acaa8");
            $(".status7").show();
            $(".status2").hide();
            $(".status3").hide();
            $(".status4").hide();
        }
    }
    // - 批量处理订单
    function dealOrder(orderNo, orderStatus) {
        if (orderNo == null || orderNo == '') {
            alerErr("选择订单");
            return;
        }
        $.ajaxSetup({
            contentType: 'application/x-www-form-urlencoded'
        })
        var reason = '';
        if (orderStatus == 5) {
            // - 拒绝订单
            reason = $("#reason").val();
        } else if (orderStatus == 6) {
            // - 撤单
            reason = $("#cancelReason").val();
        }
        var param = {orderNo: orderNo, orderStatus: orderStatus, reason: reason};
        var url = "<%=basePath%>/order/dealOrder";
        $.post(url, param, function (data, status) {
            aler(data);
            if (data.code == 200) {
                parseOrderStatus(orderStatus);
                setOrderDealButStatus(orderStatus, ${orderExtDto.deliveryType});
            }
        })
    }
    // - 拒绝订单
    $("#rejectOrder").click(function () {
        rejectOrder();
    })

    function rejectOrder() {
        // - 选择拒绝原因
        $("#rejectOrderModal").modal({
            keyboard: false,
            backdrop: 'static'
        });
    }
    // - 选择好拒绝订单原因
    $("#reasonOk").click(function () {
        var prereason = $.trim($("#prereason").val());
        // -
        if (prereason == "" && $("#reason").val() == "") {
            alerErr("选择拒绝原因");
            return;
        }
        if (prereason != "") {
            $("#reason").val(prereason);
        }
        $("#rejectOrderModal").modal("hide");
        dealOrder('${orderExtDto.orderNo}', 5);
    })
    // - 选择拒绝订单原因
    $(".list-group-item").click(function () {
        $(".list-group-item").css("background-color", "#FFFFFF");
        $(this).css("background-color", "#4acaa8");
        $("#reason").val($(this).text());
    })
    // - 将订单处理按钮置为不可用
    function disableOrderDealBut() {
        $("#acceptOrder").attr('disabled', 'disabled');
        $("#sendOrder").attr('disabled', 'disabled');
        $("#finishOrder").attr('disabled', 'disabled');
        $("#rejectOrder").attr('disabled', 'disabled');
    }
    // - 动态设置操作按钮可用状态
    function setOrderDealButStatus(status, deliveryType) {
        disableOrderDealBut();
        if (status == 1) {
            $("#acceptOrder").removeAttr('disabled');
            $("#rejectOrder").removeAttr('disabled');
        } else if (status == 2) {
            if (deliveryType == 1) {
                // - 到店自提
                $("#finishOrder").removeAttr('disabled');
            }
            if (deliveryType == 2) {
                // - 送货上门
                $("#sendOrder").removeAttr('disabled');
            }
        } else if (status == 3) {
            $("#finishOrder").removeAttr('disabled');
        }
    }
    $(function () {
        initStyle();
        setOrderDealButStatus(${orderExtDto.status}, ${orderExtDto.deliveryType});
        // - 解析订单状态
        parseOrderStatus(${orderExtDto.status});
        $("#acceptOrder").click(function () {
            dealOrder('${orderExtDto.orderNo}', 2);
        })
        $("#sendOrder").click(function () {
            dealOrder('${orderExtDto.orderNo}', 3);
        })
        $("#finishOrder").click(function () {
            dealOrder('${orderExtDto.orderNo}', 4);
        })
    })
</script>
</html>
