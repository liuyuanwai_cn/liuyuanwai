<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>我的订单 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff;margin: 15px 0px;padding: 0px">
    <div id="ordersArea" style="padding: 100px 0px;background-color: #f4f7ed" class="text-center">
      <p style="color: #d3d3d3;">
        <i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载订单...
      </p>
    </div>
    <div id="orders"></div>
    <table style="width: 100%;display: none;" id="loadMore">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">加载更多</td>
      </tr>
    </table>
    <table style="width: 100%;display: none;" id="loadOver">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
      </tr>
    </table>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>


<script language="JavaScript">
  // - 日期解析
  function formatDate(strTime) {
    if (strTime == null || strTime == '') {
      return "";
    }
    var date = new Date(strTime);
    return date.getFullYear() + "-" + addZero(date.getMonth() + 1) + "-" + addZero(date.getDate()) + " " + addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
  }
  function addZero(val) {
    return val < 10 ? '0' + val : val;
  }
  // - 订单状态解析
  function parseOrderStatus(status) {
    var temp = "";
    if (status == 5 || status == 6 || status == 7) {
      temp = '<span class="label label-default" >' + parseStatusToStr(status) + '</span>';
    } else if (status == 4) {
      temp = '<span class="label label-success" >' + parseStatusToStr(status) + '</span>';
    } else if (status == 1) {
      temp = '<span class="label label-warning" >' + parseStatusToStr(status) + '</span>';
    } else {
      temp = '<span class="label label-info" >' + parseStatusToStr(status) + '</span>';
    }
    return temp;
  }
  function parseStatusToStr(status) {
    var desc = "";
    if (status == 1) {
      desc = "待确认";
    } else if (status == 2) {
      desc = "已接单";
    } else if (status == 3) {
      desc = "派送中";
    } else if (status == 4) {
      desc = "已完成";
    } else if (status == 5) {
      desc = "商家拒绝";
    } else if (status == 6) {
      desc = "已撤单";
    } else if (status == 7) {
      desc = "超时取消";
    } else {
      desc = "-";
    }
    return desc;
  }
  // - 加载订单
  function loadOrder(pageNo, pageSize) {
    var url = "<%=basePath%>/order/myorder";
    var param = {pageNo: pageNo, pageSize: pageSize};
    $("#ordersArea").hide();
    $.post(url, param, function (data, status) {
      var orders = data.items;
      if (orders.length > 0) {
        var item;
        var createTime;
        var url;
        for (var i = 0; i < orders.length; i++) {
          item = "";
          item += '<a href="<%=basePath%>/order/info?orderNo=' + orders[i].orderNo + '">';
          item += '<table style="width: 100%;margin-top: 5px;margin-bottom: 5px">';
          item += '<tr>';
          item += '<td colspan="2"style="padding: 5px"class="">' + orders[i].productName + '</td>';
          item += '</tr>';
          item += '<tr style="border-bottom: 1px solid #f4f7ed">';
          item += '<td style="font-size: small;color:darkgrey;padding: 5px">' + formatDate(orders[i].orderCreatetime) + '&nbsp;&nbsp;￥' + orders[i].amount + '</td>';
          item += '<td align="right"style=";padding: 5px;">' + parseOrderStatus(orders[i].status) + '</td>';
          item += '</tr>';
          item += '</table>';
          item += '</a>';
          $("#orders").append(item);
        }
        if (orders.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有订单
          var noDataShow = '';
          noDataShow += '<div style="width: 100%;padding: 100px 0px;background-color: #f4f7ed"class="text-center">';
          noDataShow += '<p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p><p style="color: #d3d3d3;">没有订单</p>';
          noDataShow += '</div>';
          $("#orders").empty();
          $("#orders").append(noDataShow);
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  var pageNo = 1;
  $(document).ready(function () {
    initStyle();
    if (!validateEmail('<%=email%>')) {
      return;
    }
    loadOrder(pageNo++, 10);
    $("#loadMore").click(function () {
      loadOrder(pageNo++, 10);
    })
  })
</script>
</html>
