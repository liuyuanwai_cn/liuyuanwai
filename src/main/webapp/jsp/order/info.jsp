<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
  Object role = request.getSession().getAttribute("role");
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>订单详情</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <script type="application/javascript"
          src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>
  <script type="application/javascript"
          src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.zh-CN.js"></script>
  <link rel="stylesheet" href="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.css">

  <style type="text/css">
    .circle {
      width: 45px;
      height: 45px;
      border-radius: 100px;
      font-size: 12px;
      background-color: #d3d3d3;
      color: #FFFFFF;
    }

    .preDesc {
      color: darkgrey;
      padding: 12px;
    }

    .contentDesc {
      padding: 12px;
    }
  </style>
</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed;">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
    <div class="col-xs-0 col-sm-2 col-md-3 col-lg-4"></div>
    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4" style="background-color: #ffffff;margin: 15px 0px;padding: 0px">
      <table style="margin: 10px auto;">
        <tr>
          <td>--</td>
          <td align="center" class="circle status1">待确认</td>
          <td class="">--</td>
          <td align="center" class="circle status2">已接单</td>
          <c:if test="${orderExtDto.deliveryType == 2}">
            <td class="">--</td>
            <td align="center" class="circle status3">派送中</td>
          </c:if>
          <td class="">--</td>
          <td align="center" class="circle status4">已完成</td>
          <td align="center" class="circle status5" style="display: none">已拒绝</td>
          <td align="center" class="circle status6" style="display: none">已撤单</td>
          <td align="center" class="circle status7" style="display: none">超时</td>
          <td>--</td>
        </tr>
      </table>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0px;padding: 0px;"
           align="center">
        <img src="${orderExtDto.url}-750x500" class="img-responsive">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
        <div class="form-group" style="width: 100%">
          <table style="width: 100%">
            <tr style="border-bottom: 1px solid #ebebeb;">
              <td class="contentDesc" style="width: 120px">商品</td>
              <td class="preDesc" class="contentDesc" align="right">
                <a style="text-decoration: none;color: blueviolet" href="<%=basePath%>/order/preadd?productId=${orderExtDto.productId}">${orderExtDto.productName}</a>
              </td>
            </tr>
            <tr style="border-bottom: 1px solid #ebebeb;">
              <td class="contentDesc">单价</td>
              <td class="preDesc" class="contentDesc" align="right">${orderExtDto.price}&nbsp;元/${orderExtDto.unit}</td>
            </tr>
            <tr style="border-bottom: 1px solid #ebebeb;">
              <td class="contentDesc">购买数量</td>
              <td class="preDesc" class="contentDesc" align="right">${orderExtDto.count}${orderExtDto.unit}</td>
            </tr>
            <c:if test="${orderExtDto.deliveryType == '1'}">
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="contentDesc">派送方式</td>
                <td class="preDesc contentDesc" align="right">预约到店</td>
              </tr>
            </c:if>
            <c:if test="${orderExtDto.deliveryType == '2'}">
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="contentDesc">订餐方式</td>
                <td class="preDesc contentDesc" align="right">送货上门</td>
              </tr>
            </c:if>
            <c:if test="${orderExtDto.deliveryType == '1'}">
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="contentDesc">到店时间</td>
                <td class="preDesc contentDesc" align="right"><fmt:formatDate
                  value='${orderExtDto.sendtime}'
                  pattern='yyyy-MM-dd HH:mm:ss'/></td>
              </tr>
            </c:if>
            <c:if test="${orderExtDto.deliveryType == '2'}">
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="contentDesc">送货时间</td>
                <td class="preDesc contentDesc" align="right"><fmt:formatDate
                  value='${orderExtDto.sendtime}'
                  pattern='yyyy-MM-dd HH:mm:ss'/></td>
              </tr>
            </c:if>
            <c:if test="${orderExtDto.deliveryType == '2'}">
              <tr style="border-bottom: 1px solid #ebebeb;">
                <td class="contentDesc" valign="top">送货地址</td>
                <td class="preDesc contentDesc" align="right">${orderExtDto.sendAddr}</td>
              </tr>
            </c:if>
            <tr style="border-bottom: 1px solid #ebebeb;">
              <td class="contentDesc">联系人</td>
              <td class="preDesc contentDesc" align="right">${orderExtDto.companyName}</td>
            </tr>
            <tr style="border-bottom: 1px solid #ebebeb;">
              <td class="contentDesc">联系电话</td>
              <td class="contentDesc" align="right">${orderExtDto.phone}</td>
            </tr>
            <tr style="border-bottom: 1px solid #ebebeb;">
              <td class="contentDesc">总金额</td>
              <td class="preDesc contentDesc" align="right">${orderExtDto.amount}元</td>
            </tr>
            <tr>
              <td class="contentDesc">用户留言</td>
              <td class="preDesc contentDesc" align="right">${orderExtDto.orderRemark}</td>
            </tr>
            <c:if test="${orderExtDto.status == 5}">
              <tr style="border-top: 1px solid #ebebeb;">
                <td class="contentDesc">拒绝原因</td>
                <td class="preDesc contentDesc" align="right">${orderExtDto.reason}</td>
              </tr>
            </c:if>
            <c:if test="${orderExtDto.status == 6}">
              <tr style="border-top: 1px solid #ebebeb;">
                <td class="contentDesc">撤单原因</td>
                <td class="preDesc contentDesc" align="right">${orderExtDto.reason}</td>
              </tr>
            </c:if>
          </table>
        </div>
        <div style="padding: 10px;margin: 40px 0px">
          <button id="cancelOrder" class="btn btn-disabled-color btn-block"
                  style="padding: 9px;color: #ffffff;display: none">
            取消订单
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
<!-- 取消订单Modal -->
<div class="modal fade bs-example-modal-sm" id="cancelOrderModal" role="dialog" style="margin-top: 100px">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" align="center">
        <b>撤单原因</b>
      </div>
      <div class="modal-body" style="padding: 0px;">
        <div class="list-group" style="border: 0px solid red;margin: 0px">
          <a href="#" class="list-group-item" style="background-color: #4acaa8">不想买了</a>
          <textarea id="preCancelReason" class="list-group-item" maxlength="100" style="width: 100%;resize: none"
                    placeholder="其他原因..."></textarea>
          <input type="hidden" id="cancelReason" value="不想买了">
        </div>
      </div>
      <div class="modal-footer" align="right">
        <button class="btn btn-info" id="cancelReasonOk">确定</button>
        <button class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>
</body>


<script language="JavaScript">
  function initOrderStatus() {
    $(".status1").css("background-color", "#d3d3d3");
    $(".status2").css("background-color", "#d3d3d3");
    $(".status3").css("background-color", "#d3d3d3");
    $(".status4").css("background-color", "#d3d3d3");
    $(".status5").css("background-color", "#d3d3d3");
    $(".status6").css("background-color", "#d3d3d3");
    $(".status7").css("background-color", "#d3d3d3");
  }
  // - 解析订单状态
  function parseOrderStatus(status) {
    initOrderStatus();
    if (status == 1) {
      $(".status1").css("background-color", "#4acaa8");
    } else if (status == 2) {
      $(".status2").css("background-color", "#4acaa8");
    } else if (status == 3) {
      $(".status3").css("background-color", "#4acaa8");
    } else if (status == 4) {
      $(".status4").css("background-color", "#4acaa8");
    } else if (status == 5) {
      // - 订单拒绝
      $(".status5").css("background-color", "#4acaa8");
      $(".status5").show();
      $(".status2").hide();
      $(".status3").hide();
      $(".status4").hide();
    } else if (status == 6) {
      // - 订单撤销
      $(".status6").css("background-color", "#4acaa8");
      $(".status6").show();
      $(".status2").hide();
      $(".status3").hide();
      $(".status4").hide();
    } else if (status == 7) {
      // - 订单超时
      $(".status7").css("background-color", "#4acaa8");
      $(".status7").show();
      $(".status2").hide();
      $(".status3").hide();
      $(".status4").hide();
    }
  }
  // - 批量处理订单
  function dealOrder(orderNo, orderStatus) {
    if (orderNo == null || orderNo == '') {
      alerErr("选择订单");
      return;
    }
    $.ajaxSetup({
      contentType: 'application/x-www-form-urlencoded'
    })
    var reason = '';
    if (orderStatus == 5) {
      // - 拒绝订单
      reason = $("#reason").val();
    } else if (orderStatus == 6) {
      // - 撤单
      reason = $("#cancelReason").val();
    }
    var param = {orderNo: orderNo, orderStatus: orderStatus, reason: reason};
    var url = "<%=basePath%>/order/dealOrder";
    $.post(url, param, function (data, status) {
      aler(data);
      if (data.code == 200) {
        parseOrderStatus(orderStatus);
        setOrderDealButStatus(orderStatus, ${orderExtDto.deliveryType});
      }
    })
  }
  // - 取消订单
  $("#cancelOrder").click(function () {
    // - 选择取消原因
    $("#cancelOrderModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
  })
  // - 选择好取消订单原因
  $("#cancelReasonOk").click(function () {
    var prereason = $.trim($("#preCancelReason").val());
    // -
    if (prereason == "" && $("#cancelReason").val() == "") {
      alerErr("选择拒绝原因");
      return;
    }
    if (prereason != "") {
      $("#cancelReason").val(prereason);
    }
    $("#cancelOrderModal").modal("hide");
    dealOrder('${orderExtDto.orderNo}', 6);
  })
  function initCancelOrderBtn() {
    var email = '<%=email%>';
    var buyer = '${orderExtDto.email}';
    var status = ${orderExtDto.status};
    if (buyer == email && status == '1') {
      $("#cancelOrder").show();
    }
  }
  $(function () {
    initStyle();
    initCancelOrderBtn();
    // - 解析订单状态
    parseOrderStatus(${orderExtDto.status});
  })
</script>
</html>
