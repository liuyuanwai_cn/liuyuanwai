<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
    String basePath = request.getContextPath();
%>
<html>
<head>
    <title>新增角色</title>

    <link rel="stylesheet" href="<%=basePath%>/plugin/ztree/css/metroStyle/metroStyle.css">

    <style type="text/css">
        /*去掉树前面的加减号*/
        .ztree li span.button.switch.level0 {
            display: none;
        }
    </style>
    <script type="application/javascript" src="<%=basePath%>/plugin/ztree/js/jquery.ztree.all-3.5.min.js"></script>
</head>
<body  style="padding: 0px;margin: 0px;background-color: #f4f7ed;">
<div class="container-fluid" style="padding: 0px;margin: 0px;">
    <div class="row-fluid" style="padding: 0px;margin: 0px">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <legend style="margin: 5px"><h2>新增角色</h2></legend>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <h4><b>角色名称</b></h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="form-group" style="width: 100%">
                    <p>角色名称</p>
                    <input type="text" class="form-control" id="roleName" placeholder="角色名称" maxlength="8">
                </div>
            </div>
            <hr style="width: 100%">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <h4><b>角色权限</b></h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="form-group" style="width: 100%">
                    <ul id="ztreeMenu" class="ztree" style="border: 0px solid red"></ul>
                </div>
            </div>
            <hr style="width: 100%">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"style="margin-bottom: 20px">
                <button id="save" class="btn btn-info" type="button">&nbsp;保&nbsp;存&nbsp;</button>
            </div>
        </div>
    </div>
</div>
</body>


<script language="JavaScript">

    function onClick(e, treeId, treeNode) {
        var zTree = $.fn.zTree.getZTreeObj("ztreeMenu");
        zTree.expandNode(treeNode);
    }

    var setting = {
        async: {
            enable: true,
            url: "<%=basePath%>/sysmenu/loadMenuTree",
            dataType: "json",
            type: "GET"
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            dblClickExpand: false,
            showTitle: false,
            nameIsHTML: true
        },
        callback: {
            onClick: onClick
        },
        check:{
            enable:true
        }
    };
    // - 保存
    function save(treeObj){
        var roleName = $.trim($("#roleName").val());
        var selNodes = treeObj.getCheckedNodes(true);
        if(roleName == ""){
            alerErr("输入角色名称!");
            return;
        }
        if(selNodes.length == 0){
            alerErr("勾选菜单!");
            return;
        }
        var menuIds = "";
        for(var i=0;i<selNodes.length;i++){
            menuIds = menuIds + selNodes[i].id;
            menuIds = menuIds + ",";
        }
        var param = {roleName:roleName,menuIds:menuIds};
        var url = "<%=basePath%>/role/add";
        $.post(url,param,function(data,status){
            aler(data);
        })
    }

    // - 禁用url
    function disableUrl(treeObj){
        var nodes = treeObj.getNodes();
        alert(nodes[1].url);
        for(var i=0;i<nodes.length;i++){
            nodes[i].url="";
        }
    }

    $(function () {

        $.fn.zTree.init($("#ztreeMenu"), setting);
        var treeObj = $.fn.zTree.getZTreeObj("ztreeMenu");

        // -
//        disableUrl(treeObj)
        $("#save").click(function(){
            save(treeObj);
        })
    })

</script>
</html>
