<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
    String basePath = request.getContextPath();
%>
<html>
<head>
    <title>角色列表</title>
    <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmGrid.css">
    <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmGrid-bootstrap.css">
    <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmPaginator.css">
    <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmPaginator-bootstrap.css">

    <script language="JavaScript" src="<%=basePath%>/plugin/mmgrid/mmGrid.js"></script>
    <script language="JavaScript" src="<%=basePath%>/plugin/mmgrid/mmPaginator.js"></script>

    <style type="text/css">
        table{font-size: small}
    </style>
</head>
<body  style="padding: 0px;margin: 0px">
<div class="container-fluid" style="padding: 0px;margin: 0px;">
    <div class="row-fluid" style="padding: 0px;margin: 0px">
        <div class="col-md-12"style="background-color: white;height: 100%;overflow-y: auto">
            <legend style="margin: 0px"><h2>角色列表</h2></legend>
            <div class="btn-group" role="group" style="margin: 5px; ">
                <a type="button" class="btn btn-info">
                    <i class="icon-list"></i>&nbsp;功能
                </a>
                <a type="button" class="btn btn-default" id="del">
                    <i class="icon-trash"></i>&nbsp;删除
                </a>
            </div>
            <div class="col-md-12" style="padding: 0px">
                <table id="menu"></table>
                <div id="paginator"></div>
            </div>
        </div>
    </div>
</div>
</body>


<script language="JavaScript">

    // - 角色详情链接
    function roleInfo(val,item,rowIndex){
        return "<a href=<%=basePath%>/role/info?roleId="+item.id+">"+item.name+"</a>";
    }

    $(document).ready(function () {
        var title = [
            {title: '序号', name: 'id',width:"50"},
            {title: '名称', name: 'name',width:"150",renderer:roleInfo}
        ];
        var mmGrid = $("#menu").mmGrid({
            url: "<%=basePath%>/role/list",
            method: "post",
            cols: title,
            checkCol: true,
            height: "75%",
            multiSelect: true,
            plugins:[$('#paginator').mmPaginator({})]
        })

        // - 删除选中数据
        $("#del").click(function(){
            var selectedRows = mmGrid.selectedRows();
            if(selectedRows.length == 0){
                alerErr("选择要删除的行");
                return;
            }
            var roleIds = "";
            for(var i=0;i<selectedRows.length;i++){
                roleIds += selectedRows[i].id;
                roleIds += ",";
            }
            $.ajaxSetup({
                contentType : 'application/json'
            });
            var url = "<%=basePath%>/role/del?roleIds="+roleIds;
            $.post(url, function (data,status) {
                aler(data);
                mmGrid.load({
                    url: "<%=basePath%>/role/list"
                });
            })
        })
    })
</script>
</html>
