<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
    String basePath = request.getContextPath();
%>
<html>
<head>
    <title>新增角色</title>

    <link rel="stylesheet" href="<%=basePath%>/plugin/ztree/css/metroStyle/metroStyle.css">

    <style type="text/css">
        /*去掉树前面的加减号*/
        .ztree li span.button.switch.level0 {
            display: none;
        }
    </style>
    <script type="application/javascript" src="<%=basePath%>/plugin/ztree/js/jquery.ztree.all-3.5.min.js"></script>
</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed;">
<div class="container-fluid" style="padding: 0px;margin: 0px;">
    <div class="row-fluid" style="padding: 0px;margin: 0px; ">
        <div class="col-md-6" style="padding: 20px;">
            <legend style="margin: 5px"><h2>角色权限</h2></legend>
            <div class="col-md-4">
                <h4><b>角色名称</b></h4>
            </div>
            <div class="col-md-8">
                <div class="form-group" style="width: 100%">
                    <p>角色名称</p>
                    <input type="hidden" class="form-control" id="roleId" placeholder="角色ID" maxlength="8"
                           value="${roleMenuDto.shRole.id}">
                    <input type="text" class="form-control" id="roleName" placeholder="角色名称" maxlength="8"
                           value="${roleMenuDto.shRole.name}">
                </div>
            </div>
            <hr style="width: 100%">
            <div class="col-md-4">
                <h4><b>角色权限</b></h4>
            </div>
            <div class="col-md-8">
                <div class="form-group" style="width: 100%">
                    <ul id="ztreeMenu" class="ztree" style="border: 0px solid red"></ul>
                </div>
            </div>
            <hr style="width: 100%">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <button id="update" class="btn btn-info" type="button">&nbsp;更&nbsp;新&nbsp;</button>
            </div>
        </div>
    </div>
</div>
</body>


<script language="JavaScript">

    function onClick(e, treeId, treeNode) {
        var zTree = $.fn.zTree.getZTreeObj("ztreeMenu");
        zTree.expandNode(treeNode);
    }

    var setting = {
        async: {
            enable: true,
            url: "<%=basePath%>/sysmenu/loadMenuTree",
            dataType: "json",
            type: "GET"
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            dblClickExpand: false,
            showTitle: false,
            nameIsHTML: true
        },
        callback: {
            onClick: onClick,
            onAsyncSuccess: zTreeOnAsyncSuccess
        },
        check: {
            enable: true
        }
    };
    // - 更新
    function update(treeObj) {
        var roleName = $.trim($("#roleName").val());
        var selNodes = treeObj.getCheckedNodes(true);
        if (roleName == "") {
            alerErr("输入角色名称!");
            return;
        }
        if (selNodes.length == 0) {
            alerErr("勾选菜单!");
            return;
        }
        var menuIds = "";
        for (var i = 0; i < selNodes.length; i++) {
            menuIds = menuIds + selNodes[i].id;
            menuIds = menuIds + ",";
        }
        var roleId = $("#roleId").val();
        var param = {roleName:roleName,menuIds:menuIds,roleId:roleId};
        var url = "<%=basePath%>/role/update";
        $.post(url,param, function (data, status) {
            if (data.code == 200) {
                alerOK(data.msg);
                window.parent.window.reloadTree();
            } else {
                alerErr(data.msg);
            }
        })
    }

    // - 禁用url
    function disableUrl(treeObj) {
        var nodes = treeObj.getNodes();
        alert(nodes[1].url);
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].url = "";
        }
    }


    // - 树加载完后的执行动作
    function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
        // - 勾选角色对应的菜单列表
        var treeObj = $.fn.zTree.getZTreeObj(treeId);
        // - 菜单默认勾选，先全部置为不勾选
        treeObj.checkAllNodes(false);
        // -
        var menuIds = "${roleMenuDto.menuIds}";
        var menuIdArr = menuIds.split(",");
        var nodeMap = {};
        for (var j = 0; j < msg.length; j++) {
            nodeMap[msg[j].id] = treeObj.getNodeByParam("id",msg[j].id);
        }
        for (var i = 0; i < menuIdArr.length; i++) {
            treeObj.checkNode(nodeMap[menuIdArr[i]],true);
        }
    }
    $(function () {

        $.fn.zTree.init($("#ztreeMenu"), setting);
        var treeObj = $.fn.zTree.getZTreeObj("ztreeMenu");

//        disableUrl(treeObj)
        $("#update").click(function () {
            update(treeObj);
        })
    })

</script>
</html>
