<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="zh-CN">
<%
    String basePath = request.getContextPath();
    String orderNo = request.getParameter("orderNo");
%>

<head>
    <title>操作成功</title>

    <jsp:include page="common/common-css.jsp"></jsp:include>
    <jsp:include page="common/common-js.jsp"></jsp:include>
    <style type="text/css">
        td {
            padding: 5px
        }

        li {
            list-style: disc
        }
    </style>
</head>

<body style="background-color: #f4f7ed;">
<jsp:include page="../head.jsp"></jsp:include>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-md-12 ">
        <table style="margin-left: auto;margin-right: auto;">
            <tr>
                <td align="center"><h1><i class=" icon-ok-sign icon-4x text-default-color"></i></h1></td>
            </tr>
            <tr>
                <td>
                    <h3>提示：操作成功</h3>

                    <p><a href="${backUrl}" target="_top">查看详情</a></p>
                </td>
            </tr>
        </table>
    </div>
</div>
<jsp:include page="../foot.jsp"></jsp:include>
<script>
    $(function () {
        initStyle();
    })
</script>
</body>

</html>
