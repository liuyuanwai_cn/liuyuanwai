<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getContextPath();
%>
<html>
<head>
    <title>门店</title>
    <jsp:include page="../common/common-css.jsp"></jsp:include>
    <jsp:include page="../common/common-js.jsp"></jsp:include>

    <style type="text/css">
        .preDesc {
            color: darkgrey;
        }

        td {
            padding: 12px 15px;
        }
    </style>

</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid"style="padding: 0px">
    <div class="col-xs-0 col-sm-2 col-md-3 col-lg-4"></div>
    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4" style="background-color: #ffffff;margin: 30px 0px;padding: 0px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px;margin: 0px" align="center">
            <c:choose>
                <c:when test="${shShop.url == null}">
                    <img src="<%=basePath%>/img/shop.png" id="shopUrl" class="img-responsive">
                </c:when>
                <c:otherwise>
                    <img src="${shShop.url}-750x500" id="shopUrl" class="img-responsive">
                </c:otherwise>
            </c:choose>
            <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath"
                   style="display: none">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
             style="margin: 0px;border-top: 1px solid #eaeaea;padding: 0px">
            <table style="width: 100%;border-bottom: 1px solid #ebebeb;">
                <tr style="border-bottom: 1px solid #ebebeb;">
                    <td style="width: 120px">门店名称</td>
                    <td class="preDesc" align="right">${shShop.name}</td>
                </tr>
                <tr style="border-bottom: 1px solid #ebebeb;">
                    <td>宣传语</td>
                    <td class="preDesc" align="right">${shShop.ad}</td>
                </tr>
                <tr style="border-bottom: 1px solid #ebebeb;">
                    <td>移动电话</td>
                    <td class="preDesc" align="right">${shShop.mobile}</td>
                </tr>
                <tr style="border-bottom: 1px solid #ebebeb;">
                    <td>座机</td>
                    <td class="preDesc" align="right">${shShop.phone}</td>
                </tr>
                <tr style="border-bottom: 1px solid #ebebeb;">
                    <td>门店地址</td>
                    <td class="preDesc" align="right">${shShop.addr}</td>
                </tr>
                <tr style="border-bottom: 1px solid #ebebeb;">
                    <td colspan="2" align="right">
                        <a href="<%=basePath%>/goods">
                            <div>查看所有商品&nbsp;&nbsp;<i class="icon-angle-right"></i></div>
                        </a>
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid #ebebeb;">
                    <td class="preDesc" colspan="2">
                        <textarea style="width: 100%;resize: none;padding: 0px;margin: 0px;border: none;" readonly rows="5">${shShop.remark}</textarea>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
<script>
    $(function () {
        initStyle();
    })
</script>

</html>
