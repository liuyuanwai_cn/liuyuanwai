<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getContextPath();
    Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
    <title>门店管理</title>
    <jsp:include page="../common/common-css.jsp"></jsp:include>
    <jsp:include page="../common/common-js.jsp"></jsp:include>

    <style type="text/css">
        input, textarea {
            color: darkgrey;
        }

        td {
            padding: 12px
        }
    </style>

</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">
        <div class="col-xs-0 col-sm-2 col-md-3 col-lg-4"></div>
        <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4"
             style="background-color: #ffffff;margin: 15px 0px;padding: 10px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0px;padding: 0px;"
                 align="center">
                <c:choose>
                    <c:when test="${shShop.url == null}">
                        <img src="<%=basePath%>/img/shop.png" id="shopUrl" class="img-responsive">
                    </c:when>
                    <c:otherwise>
                        <img src="${shShop.url}-750x500" id="shopUrl" class="img-responsive">
                    </c:otherwise>
                </c:choose>
                <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath"
                       style="display: none">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
                 style="margin: 0px;border-top: 1px solid #eaeaea;padding: 0px">
                <table style="width: 100%;border-bottom: 1px solid #ebebeb;">
                    <tr style="border-bottom: 1px solid #ebebeb;">
                        <td class="preDesc" style="width: 120px;">门店名称</td>
                        <td align="right">
                            <input type="text" class="text-right" id="name" style="width: 100%;border: none"
                                   placeholder="" maxlength="20" value="${shShop.name}">
                        </td>
                    </tr>
                    <tr style="border-bottom: 1px solid #ebebeb;">
                        <td class="preDesc">宣传语</td>
                        <td align="right">
                            <input type="text" class="text-right" id="ad" style="width: 100%;border: none"
                                   placeholder="少于15个字" maxlength="15" value="${shShop.ad}">
                        </td>
                    </tr>
                    <tr style="border-bottom: 1px solid #ebebeb;">
                        <td class="preDesc">移动电话</td>
                        <td align="right">
                            <input type="text" class="text-right NumDecText"
                                   onkeyup="this.value=this.value.replace(/\D/g,'')"
                                   onafterpaste="this.value=this.value.replace(/\D/g,'')"
                                   style="width: 100%;border: none" id="mobile" placeholder="11位数字"
                                   maxlength="11" value="${shShop.mobile}">
                        </td>
                    </tr>
                    <tr style="border-bottom: 1px solid #ebebeb;">
                        <td class="preDesc">座机</td>
                        <td align="right">
                            <input type="text" class="text-right NumDecText"
                                   onkeyup="this.value=this.value.replace(/\D/g,'')"
                                   onafterpaste="this.value=this.value.replace(/\D/g,'')"
                                   style="width: 100%;border: none" id="phone" placeholder=""
                                   maxlength="11" value="${shShop.phone}">
                        </td>
                    </tr>
                    <tr style="border-bottom: 1px solid #ebebeb;">
                        <td class="preDesc">门店地址</td>
                        <td align="right">
                            <input type="text" class="text-right" id="addr" style="width: 100%;border: none"
                                   placeholder="少于30个字" maxlength="30" value="${shShop.addr}">
                        </td>
                    </tr>
                    <tr style="border-bottom: 1px solid #ebebeb;">
                        <td class="preDesc" colspan="2">
                                        <textarea style="resize: none;width: 100%;border: none;padding: 0px"
                                                  maxlength="150" class="" id="remark" rows="5"
                                                  placeholder="门店介绍(少于150字)">${shShop.remark}</textarea>
                        </td>
                    </tr>
                </table>
                <div class="btn-group" role="group" aria-label="..." style="margin: 20px 0px">
                    <button onclick="save()" class="btn btn-info">保&nbsp;存</button>
                    <button type="button" class="btn btn-default" onclick="history.go(-1)">取&nbsp;消</button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="shopLogo" value="${shShop.url}">
<jsp:include page="../common/common-html.jsp"></jsp:include>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>


<script language="JavaScript">
    // - 门店信息维护
    function save() {
        $(".btn").blur();
        var name = $.trim($("#name").val());
        var ad = $.trim($("#ad").val());
        var mobile = $.trim($("#mobile").val());
        var phone = $.trim($("#phone").val());
        var addr = $.trim($("#addr").val());
        var remark = $.trim($("#remark").val());
        var shopUrl = $("#shopLogo").val();
        if (name == "") {
            alerErr("输入门店名称!");
            return;
        }
        if (ad == "") {
            alerErr("输入宣传语!");
            return;
        }
        if (mobile == '' && phone == '') {
            alerErr("移动电话和座机至少填一个!");
            return;
        }
        if (addr == "") {
            alerErr("输入门店地址!");
            return;
        }
        if (shopUrl == '' || shopUrl == '<%=basePath%>/img/shop.png') {
            alerErr("选择门店首页图片!");
            return;
        }
        if (remark == '') {
            alerErr("输入门店介绍!");
            return;
        }
        $.ajaxSetup({
            contentType: 'application/json'
        });
        var param = {
            name: name,
            ad: ad,
            mobile: mobile,
            phone: phone,
            remark: remark,
            addr: addr,
            url: shopUrl
        };
        var url = "<%=basePath%>/shop/mgr";
        $.post(url, JSON.stringify(param), function (data, status) {
            aler(data);
        })
    }

    function updateLogo(logo) {
        var url = '<%=basePath%>/shop/updateLogo';
        var param = {logo: logo};
        $.post(url, param, function (data, status) {
            if (data.code == 200) {
                $("#shopUrl").attr("src", logo+'-750x500');
                $("#shopLogo").val(logo);
                alerOK("更新成功");
            } else {
                aler(data);
            }
        })
    }

    // - 上传图片
    function uploadFile(obj) {
        $("#loadModal").modal({
            keyboard: false,
            backdrop: 'static'
        });
        var url = "<%=basePath%>/img/upload";
        $.ajaxFileUpload({
            url: url,
            fileElementId: "filePath",//file标签的id,
            dateType: "text",
            success: function (data, status) {
                // - ajaxFileUpload这儿必须得转换以下
                data = jQuery.parseJSON(jQuery(data).text());
                if (data.code == 200) {
                    var temp = data.data;
                    var logo = temp.downloadUrl;
                    updateLogo(logo);
                } else {
                    alerErr(data.msg);
                }
                $("#loadModal").modal('hide');
            },
            error: function (data, status, e) {
                $("#loadModal").modal('hide');
                $("#filePath").replaceWith($("#upload").clone(true));
                alerErr(e);
            }
        });
    }
    $(function () {
        if (!validateEmail('<%=email%>')) {
            return;
        }
        initStyle();

        $("button").click(function () {
            $(this).blur()
        });
        $("#shopUrl").mousemove(function () {
            $(this).css("cursor", "pointer");
        })
        $("#shopUrl").mouseout(function () {
            $(this).css("cursor", "default");
        })
        // - 上传图片
        $("#shopUrl").click(function () {
            $("#filePath").click();
        })
    })
</script>
</html>
