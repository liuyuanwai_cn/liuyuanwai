<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
%>

<html>
<head>
  <title>门店</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style>
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    a:default {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-0 col-md-1 col-lg-2" style="margin: 0px;padding: 0px;"></div>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8" style="margin: 0px;padding: 0px;">
    <div id="shopsArea">
      <table style="width: 100%;margin-top: 20px" id="loadingAdvice">
        <tr>
          <td align="center" style="color: #d3d3d3;padding: 100px 0px"><i
            class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载商家数据......
          </td>
        </tr>
      </table>
      <table style="width: 100%;margin-top: 20px;display: none" id="loadingNoShops">
        <td align="center" class="load" style="color: darkgrey;padding: 10px" width="20%"></td>
        <td align="center" class="load" style="color: darkgrey;padding: 100px 0px" width="60%">
          <i class="icon-plane icon-4x text-default-color"></i><span class="text-default-color">&nbsp;&nbsp;商家正在赶来路上&nbsp;.&nbsp;.&nbsp;.</span>
        </td>
        <td align="center" class="load" style="color: darkgrey;padding: 10px" width="20%"></td>
      </table>
    </div>
    <div id="shops" style="margin-top: 10px;"></div>
    <table style="width: 100%;display: none;" id="loadMore">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 10px" width="20%"></td>
        <td align="center" class="load" style="color: darkgrey;padding: 10px" width="60%">加载更多</td>
        <td align="center" class="load" style="color: darkgrey;padding: 10px" width="20%"></td>
      </tr>
    </table>
    <table style="width: 100%;display: none;" id="loadOver">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px" width="20%"></td>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px" width="60%"></td>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px" width="20%"></td>
      </tr>
    </table>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>


<script language="JavaScript">
  function noMoreData() {
    // - 没有更多数据
    $("#loadMore").hide();
    $("#loadOver").show();
  }
  function moreData() {
    // - 还有更多数据
    $("#loadOver").hide();
    $("#loadMore").show();
  }
  var pageNo = 1;
  function processShop(i, shops) {
    if (i >= shops.length) {
      return '';
    }
    var logo = '<%=basePath%>/img/shop.png';
    if (shops[i].url != null && shops[i].url != '' && shops[i].url != undefined) {
      logo = shops[i].url;
    }
    var item = "";
    item += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4"style="padding: 5px;margin: 0px">';
    item += '<div class="panel panel-default shopShow" style="margin-bottom: 10px;background-color: #ffffff">';
    item += '<div class="panel-heading"style="background-color: #ffffff">';
    item += '<a href="<%=basePath%>/shop/shopInfo?email=' + shops[i].email + '">' + shops[i].name + '</a></p>';
    item += '<p style="margin: 0px;">' + shops[i].ad + '</p>';
    item += '</div>';
    item += '<a href="<%=basePath%>/goods?email=' + shops[i].email + '">';
    item += '<div class="panel-body" align="center" style="border: none;padding: 0px;background-color: #ffffff">';
    item += '<img src="' + shops[i].url + '-360x200" class="img-responsive shopUrl">';
    item += '</div>';
    item += '</a>';
    item += '</div>';
    item += '</div>';
    return item;
  }
  function loadShop(pageNo, pageSize) {
    var url = "<%=basePath%>/shop/list";
    var param = {pageNo: pageNo, pageSize: pageSize};
    $.post(url, param, function (data, status) {
      var shops = data;
      if (shops.length == 0) {
        if (pageNo == 1) {
          // - 没有数据
          $("#loadingAdvice").hide();
          $("#loadingNoShops").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        $("#shopsArea").hide();
        for (var i = 0; i < shops.length; i += 4) {
          var item = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="margin: 0px;padding: 0px">';
          item += processShop(i, shops);
          item += processShop(i + 1, shops);
          item += processShop(i + 2, shops);
          item += processShop(i + 3, shops);
          item += '</div>';
          $("#shops").append(item);
        }
        if (shops.length == pageSize) {
          moreData();
        } else {
          noMoreData();
        }
      }
    })
  }
  $(document).ready(function () {
    initStyle();
    loadShop(pageNo++, 10);
    $("#loadMore").click(function () {
      loadShop(pageNo++, 10);
    })
  })
</script>
</html>
