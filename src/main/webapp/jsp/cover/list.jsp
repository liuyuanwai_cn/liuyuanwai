<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
%>

<html>
<head>
  <title>封面申请列表</title>
  <link rel="stylesheet" href="../../plugin/mmgrid/mmGrid.css">
  <link rel="stylesheet" href="../../plugin/mmgrid/mmGrid-bootstrap.css">
  <link rel="stylesheet" href="../../plugin/mmgrid/mmPaginator.css">
  <link rel="stylesheet" href="../../plugin/mmgrid/mmPaginator-bootstrap.css">

  <script language="JavaScript" src="../../plugin/mmgrid/mmGrid.js"></script>
  <script language="JavaScript" src="../../plugin/mmgrid/mmPaginator.js"></script>

  <style type="text/css">
    table {
      font-size: small
    }
  </style>
</head>
<body style="padding:5px;">
<div class="container-fluid" style="padding: 0px;">
  <div class="row-fluid" style="padding: 0px;height: 100%;overflow-y: auto">
    <legend style="margin: 0px"><h2>申请列表</h2></legend>
    <div class="btn-group" role="group" style="margin: 5px; ">
      <a type="button" class="btn btn-info">
        <i class="icon-list"></i>&nbsp;功能
      </a>
      <a type="button" class="btn btn-default" id="checkOK">
        <i class="icon-ok"></i>&nbsp;审核通过
      </a>
      <a type="button" class="btn btn-default" id="checkFailure">
        <i class="icon-remove"></i>&nbsp;审核失败
      </a>
    </div>
    <div class="col-md-12" style="padding: 0px">
      <table id="menu"></table>
      <div id="paginator"></div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="reasonModal" role="dialog" style="">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" align="center">
        <b>审核失败说明</b>
      </div>
      <div class="modal-body" style="padding: 0px;">
        <div class="list-group" style="border: 0px solid red;margin: 0px">
          <a href="#" class="list-group-item" style="background-color: #99CCFF">宣传海报不合规</a>
          <a href="#" class="list-group-item">LOGO不合规</a>
          <a href="#" class="list-group-item">名称不合规</a>
          <a href="#" class="list-group-item">介绍不合规</a>
          <a href="#" class="list-group-item">展示周期排期已满</a>
          <textarea id="prereason" class="list-group-item" maxlength="100" style="width: 100%;resize: none" placeholder="其他原因..."></textarea>
          <input type="hidden" id="reason"value="展示周期排期已满">
        </div>
      </div>
      <div class="modal-footer" align="right">
        <button type="button" class="btn btn-info" id="reasonOk">确定</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>
</body>


<script language="JavaScript">

  var mmGrid = "";


  // - 预览
  function preview(val, item, rowIndex) {
    return "<a title='点击查看预览' target='_blank' href='<%=basePath%>/cover/info?id=" + item.id + "'>预览</a>";
  }

  // - 审核
  function check(status) {
    var selectedRows = mmGrid.selectedRows();
    if (selectedRows.length == 0) {
      alerErr("选择数据");
      return;
    }
    var temp = "";
    for (var i = 0; i < selectedRows.length; i++) {
      temp += selectedRows[i].id;
      temp += ",";
    }
    var reason = $("#reason").val();
    $.ajaxSetup({
      contentType : 'application/x-www-form-urlencoded'
    })
    var param = {ids:temp,status:status,reason:reason};
    var url = "<%=basePath%>/cover/check";
    $.post(url,param, function (data, status) {
      if (data.code == 200) {
        alerOK(data.msg);
        mmGrid.load();
      } else {
        alerErr(data.msg);
      }
    })
  }
  // - 格式化审核状态
  function formatStatus(val, item, rowIndex) {
    if (val == 1) {
      val = '<span class="label label-success" style="padding: 2px">审核通过</span>';
    } else if (val == 2) {
      val = '<span class="label label-danger" style="padding: 2px">审核不通过</span>';
    } else {
      val = '<span class="label label-default" style="padding: 2px">等待审核</span>';
    }
    return val;
  }
  // - 解析时间
  function parseDate(val,item,rowIndex){
    return FormatDate(val);
  }
  function FormatDate (strTime) {
    var date = new Date(strTime);
    return date.getFullYear()+"-"+addZero(date.getMonth()+1)+"-"+addZero(date.getDate());
  }
  function addZero(val){
    return val < 10 ? '0' + val: val;
  }

  var title = [
    {title: '主键', name: 'id', hidden: true},
    {title: '效果预览', name: 'name', width: "80",renderer:preview},
    {title: '公司名称', name: 'name', width: "150"},
    {title: '公司描述', name: 'remark', width: 500},
    {title: '开始时间', name: 'startdate', width: 150,renderer:parseDate},
    {title: '结束时间', name: 'enddate', width: 150,renderer:parseDate},
    {title: '审核状态', name: 'status', width: 100, renderer: formatStatus}
  ];

  $(document).ready(function () {

    mmGrid = $("#menu").mmGrid({
      url: "<%=basePath%>/cover/list",
      method: "post",
      cols: title,
      checkCol: true,
      height: "75%",
      multiSelect: true,
      plugins: [$('#paginator').mmPaginator({})]
    })




    // - 审核通过
    $("#checkOK").click(function () {
      check(1);
    })
    // - 审核不通过
    $("#checkFailure").click(function () {
      var selectedRows = mmGrid.selectedRows();
      if(selectedRows.length == 0){
        alerErr("选择行");
        return;
      }
      // - 选择审核不通过原因
      $("#reasonModal").modal({
        keyboard: false,
        backdrop: 'static'
      });
    })
    // - 选择好审核不通过原因
    $("#reasonOk").click(function () {
      var prereason = $.trim($("#prereason").val());
      // -
      if(prereason == "" && $("#reason").val()==""){
        alerErr("选择拒绝原因");
        return;
      }
      if(prereason != ""){
        $("#reason").val(prereason);
      }
      $("#reasonModal").modal("hide");
      check(2);
    })
    // - 选择审核不通过原因
    $(".list-group-item").click(function () {
      $(".list-group-item").css("background-color","#FFFFFF");
      $(this).css("background-color","#99CCFF");
      $("#reason").val($(this).text());
    })

  })
</script>
</html>
