<%@ page import="java.net.URLDecoder" %>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../../jsp/common/common.jsp"></jsp:include>
<html lang="zh-CN">
<%
  String basePath = request.getContextPath();
%>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>劉員外</title>
  <link rel="stylesheet" href="<%=basePath%>/plugin/fullpage/css/reset.css"/>
  <link rel="stylesheet" href="<%=basePath%>/plugin/fullpage/css/style.css"/>
  <script src="<%=basePath%>/plugin/fullpage/js/jquery.mousewheel.min.js"></script>
  <script src="<%=basePath%>/plugin/fullpage/js/fullpagecode.min.js"></script>

  <style>

    .jumbotron {
      background: #000000;
      filter: alpha(opacity=50);
      -moz-opacity: 0.5;
      -khtml-opacity: 0.5;
      opacity: 0.5;
      padding: 90px;
      color: #ffffff;
    }

    td {
      padding: 10px
    }
  </style>
</head>

<body style="margin: 0px;padding: 0px">
<div class="section-group">
  <section class="sect-01">
    <div style="height: 80%;width: 100%">
      <div style="position:absolute; width:100%; height:80%; z-index:-1;">
        <img style="width: 100%;height: 100%;position: relative;" src="${shCover.url}"/>
      </div>
      <div style="padding: 1px;height: 100%" class="text-center">
        <div class="jumbotron" style="height: 100%;">
          <h1>
            ${shCover.name}
          </h1>

          <p>
          <hr style="width: 100%">
          </p>

          <p>
            ${shCover.remark}
          </p>
        </div>
      </div>
    </div>
    <div class="col-xs-12 text-center" style="height: 20%;">
      <table style="margin-right: auto;margin-left: auto;height: 100%">
        <tr>
          <td>
            <p>
              <img src="<%=basePath%>/img/logo.png" class="" style="width: 90px;">
              <img src="${shCover.logo}" class="" style="width: 80px">
            </p>
          </td>
        </tr>
        <tr>
          <td>
            <p>
              ---&nbsp;关于梦想&nbsp;
              <a type="button" href="<%=basePath%>/login" class="btn btn-info"
                 style="padding-left: 30px;padding-right: 30px">Enter</a>
              &nbsp;从不含糊&nbsp;---
            </p>
          </td>
        </tr>
      </table>
    </div>
  </section>
  <section class="sect-02" style="color: #ffffff">

  </section>
  <section class="sect-03">
    <iframe frameborder="0" src="" style="width: 100%;height: 100%"></iframe>
  </section>
</div>

<script language="javascript">
</script>
</body>

</html>
