<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" import="java.net.URLDecoder" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../../jsp/common/common.jsp"></jsp:include>
<html lang="zh-CN">
<%
    String basePath = request.getContextPath();
    String name = URLDecoder.decode(request.getParameter("name"), "UTF-8");
    String remark = URLDecoder.decode(request.getParameter("remark"), "UTF-8");
%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>劉員外</title>
    <link rel="stylesheet" href="<%=basePath%>/plugin/fullpage/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/plugin/fullpage/css/style.css"/>
    <script src="<%=basePath%>/plugin/fullpage/js/jquery.mousewheel.min.js"></script>
    <script src="<%=basePath%>/plugin/fullpage/js/fullpagecode.min.js"></script>


    <style>

        .jumbotron {
            background: #000000;
            filter: alpha(opacity=50);
            -moz-opacity: 0.5;
            -khtml-opacity: 0.5;
            opacity: 0.5;
            padding: 90px;
            color: #ffffff;
        }
        td{padding: 10px}
    </style>
</head>

<body style="margin: 0px;padding: 0px">
<div class="section-group">
    <section class="sect-01">
        <div style="height: 80%;width: 100%;">
            <div style="position:absolute; width:100%; height:80%; z-index:-1;">
                <img style="width: 100%;height: 100%;position: relative;" src="${param.show}"/>
            </div>
            <div style="padding: 1px;height: 100%" class="text-center">
                <div class="jumbotron text-center" style="height: 100%;">
                    <h1>
                        <c:choose>
                            <c:when test="${param.name==null || param.name==''}">
                                <h1>公司名称</h1>
                            </c:when>
                            <c:otherwise>
                                <%=name%>
                            </c:otherwise>
                        </c:choose>
                    </h1>

                    <p class="text-center">
                    <hr style="width: 100%">
                    </p>

                    <p>
                        <c:choose>
                            <c:when test="${param.remark == null || param.remark == ''}">
                                介绍
                            </c:when>
                            <c:otherwise>
                                <%=remark%>
                            </c:otherwise>
                        </c:choose>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 text-center" style="height: 20%;">
            <table style="margin-right: auto;margin-left: auto;height: 100%;">
                <tr>
                    <td>
                        <p style="">
                            <img src="<%=basePath%>/img/logo.png" class="" style="width: 90px;">
                            <img src="${param.logo}" class="" style="width: 80px;">
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="">
                            ---&nbsp;关于梦想&nbsp;
                            <a type="button" href="<%=basePath%>/login" class="btn btn-info"
                               style="padding-left: 30px;padding-right: 30px">Enter</a>
                            &nbsp;从不含糊&nbsp;---
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </section>
    <section class="sect-02" style="color: #ffffff">
        <div style="position:absolute; z-index:999;height: 100%">
            <img class="img-responsive" style="margin-top: 20px;margin-right: auto;margin-left: auto;height: 100%"
                 src=""/>
        </div>
        <div class="col-xs-12 text-right" style="padding: 20px">
            <table style="float: right;">
                <tr>
                    <td valign="top">
                        <p style="padding-top: 30px;">及<br>时<br>更<br>新<br>鲜</p>
                    </td>
                    <td valign="top">
                        <p style="padding-top: 30px;">小<br>区<br>内<br>送<br>货</p>
                    </td>
                    <td valign="top">
                        <p style="padding-top: 30px;">价<br>格<br>更<br>实<br>惠</p>
                    </td>
                    <td valign="top">
                        <p style="padding-top: 30px;">省<br>掉<br>中<br>间<br>商</p>
                    </td>
                    <td valign="top">
                        <p style="padding-top: 30px;">直<br>接<br>供<br>应<br>农<br>民<br>伯<br>伯<br>的<br>蔬<br>菜</p>
                    </td>
                    <td valign="top">
                        <p style="font-size: x-large;">刘<br>员<br>外</p>
                    </td>
                </tr>
            </table>
        </div>
    </section>
    <section class="sect-03">
        <iframe frameborder="0" src="" style="width: 100%;height: 100%"></iframe>
    </section>
</div>
</body>

</html>
