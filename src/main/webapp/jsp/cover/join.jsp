<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>页面</title>

  <script language="JavaScript" src="<%=basePath%>/plugin/ajaxfileupload/ajaxfileupload.js"></script>

  <script type="application/javascript"
          src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>
  <script type="application/javascript"
          src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.zh-CN.js"></script>
  <link rel="stylesheet" href="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.css">

</head>
<body style="padding: 0px;margin: 0px">
<!-- Wrapper -->
<div id="wrapper" style="overflow-y: auto">
  <div id="main">
    <section>
      <div id="container">
        <div class="col-xs-12 com-sm-12 col-md-6 col-lg-6" style="background-color: #f4f7ed;">
          <legend style=""><h2>我要上封面</h2></legend>
          <div class="col-xs-12 com-sm-12 col-md-4 col-lg-4">
            <h4><b>宣传海报</b></h4>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding-bottom: 20px">
            <img src="${coverDto.url}" border="0" id="coverShow" class="img-responsive img-thumbnail">
            <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath"
                   style="display: none">
          </div>
          <hr style="width: 100%">
          <div class="col-xs-12 com-sm-12 col-md-4 col-lg-4">
            <h4><b>LOGO</b></h4>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding-bottom: 20px">
            <img src="${coverDto.logo}" border="0" class="img-circle img-thumbnail" id="coverLogo"
                 style="width: 110px;height: 110px;">
            <input type="file" onchange="uploadFileLogo(this);" name="filePath_logo" id="filePath_logo"
                   style="display: none">
          </div>
          <hr style="width: 100%">
          <div class="col-xs-12 com-sm-12 col-md-4 col-lg-4">
            <h4><b>宣传信息</b></h4>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="form-group">
              <p><b style="color: red">*</b>名称</p>
              <input type="text" value="${coverDto.name}" class="form-control" id="name" placeholder="名称" maxlength="16">
            </div>
            <div class="form-group">
              <p><b style="color: red">*</b>介绍</p>
          <textarea rows="5" style="resize: none;" maxlength="50" class="form-control" id="remark"
                    placeholder="介绍(50字以内)">${coverDto.remark}</textarea>
            </div>
          </div>
          <div class="col-xs-12 com-sm-12 col-md-4 col-lg-4">
            <h4><b>展示周期</b></h4>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="form-group">
              <p>开始时间</p>

              <div class="input-group" style="width: 240px">
                <input type="text" class="form-control form_datetime" readonly id="startDate"
                       value="${coverDto.startDate}">
                  <span class="input-group-btn">
                    <a class="btn btn-info" type="button" rel="popover">
                      &nbsp;<i class="icon-calendar"></i>&nbsp;
                    </a>
                  </span>
              </div>
            </div>
            <div class="form-group">
              <p>结束时间</p>

              <div class="input-group" style="width: 240px">
                <input type="text" class="form-control form_datetime" readonly id="endDate"
                       value="${coverDto.endDate}">
                  <span class="input-group-btn">
                    <a class="btn btn-info" type="button" rel="popover">
                      &nbsp;<i class="icon-calendar"></i>&nbsp;
                    </a>
                  </span>
              </div>
            </div>
          </div>
          <hr style="width: 100%">
          <div class="col-xs-12 com-sm-12 col-md-4 col-lg-4"><h4><b>审核结果</b></h4></div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding-bottom: 20px">
            <div class="form-group">
              <p>审核结果</p>

              <p>
                <c:choose>
                  <c:when test="${coverDto.status == 0}">
                    <span class="label label-default">等待审核</span>
                  </c:when>
                  <c:when test="${coverDto.status == 1}">
                    <span class="label label-success">审核通过</span>
                  </c:when>
                  <c:when test="${coverDto.status == 2}">
                    <span class="label label-danger">审核不通过</span>
                  </c:when>
                </c:choose>
              </p>
            </div>
            <c:if test="${coverDto.status == 2}">
              <div class="form-group">
                <p>结果说明</p>

                <p style="color: #999999">${coverDto.reason}</p>
                <p>提示</p>

                <p style="color: #999999">您可以根据结果说明做相应修改后重新申请</p>
              </div>
            </c:if>
          </div>
          <hr style="width: 100%">
          <div class="col-xs-12 com-sm-12 col-md-4 col-lg-4">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding-bottom: 20px">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <button id="save" class="btn btn-info" type="button">申请加入</button>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <button id="preview" class="btn btn-info" type="button">效果预览</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</body>


<script language="JavaScript">


  function uploadFile(obj) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/img/upload";
    $.ajaxFileUpload({
      url: url,
      fileElementId: "filePath",//file标签的id,
      dateType: "text",
      success: function (data, status) {
        // - ajaxFileUpload这儿必须得转换以下
        data = jQuery.parseJSON(jQuery(data).text());
        if (data.code == 200) {
          var temp = data.data;
          $("#coverShow").attr("src", temp.downloadUrl);
        } else {
          alerErr(data.msg);
        }
        $("#loadModal").modal('hide');
      },
      error: function (data, status, e) {
        $("#loadModal").modal('hide');
        $("#filePath").replaceWith($("#coverShow").clone(true));
        alerErr(e);
      }
    });
  }

  function uploadFileLogo(obj) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/img/upload2";
    $.ajaxFileUpload({
      url: url,
      fileElementId: "filePath_logo",//file标签的id,
      dateType: "text",
      success: function (data, status) {
        // - ajaxFileUpload这儿必须得转换以下
        data = jQuery.parseJSON(jQuery(data).text());
        if (data.code == 200) {
          var temp = data.data;
          $("#coverLogo").attr("src", temp.downloadUrl);
        } else {
          alerErr(data.msg);
        }
        $("#loadModal").modal('hide');
      },
      error: function (data, status, e) {
        $("#loadModal").modal('hide');
        $("#filePath_logo").replaceWith($("#coverLogo").clone(true));
        alerErr(e);
      }
    });
  }

  // - 预览
  function preview() {
    var show = $("#coverShow").attr("src");
    var logo = $("#coverLogo").attr("src");
    var name = encodeURI(encodeURI($.trim($("#name").val())));
    var remark = encodeURI(encodeURI($.trim($("#remark").val())));
    if (name == "") {
      alerErr("输入名称");
      return;
    }
    if (remark == "") {
      alerErr("输入描述");
      return;
    }
    var url = "<%=basePath%>/jsp/cover/preview.jsp?show=" + show + "&logo=" + logo + "&name=" + name + "&remark=" + remark;
    window.open(url, "_blank");
  }


  function save() {
    var show = $("#coverShow").attr("src");
    var logo = $("#coverLogo").attr("src");
    var name = $.trim($("#name").val());
    var remark = $.trim($("#remark").val());
    var startDate = $.trim($("#startDate").val());
    var endDate = $.trim($("#endDate").val());
    if (show == "") {
      alerErr("上传宣传海报");
      return;
    }
    if (logo == "") {
      alerErr("上传公司LGOG/个人头像");
      return;
    }
    if (name == "") {
      alerErr("输入名称");
      return;
    }
    if (remark == "") {
      alerErr("输入描述");
      return;
    }
    if (startDate == "") {
      alerErr("选择开始日期");
      return;
    }
    if (endDate == "") {
      alerErr("选择结束日期");
      return;
    }

    var param = {show: show, logo: logo, name: name, remark: remark, startDate: startDate, endDate: endDate};
    var url = "<%=basePath%>/cover/save";

//        表单提交
    $.ajaxSetup({
      contentType: 'application/x-www-form-urlencoded'
    })
    $.post(url, param, function (data, status) {
      aler(data);
    })

  }


  $(function () {


    $("img").mousemove(function () {
      $(this).css("cursor", "pointer");
    })
    $("img").mouseout(function () {
      $(this).css("cursor", "default");
    })

    // - 上传海报
    $("#coverShow").click(function () {
      $("#filePath").click();
    })
    // - 上传LOGO
    $("#coverLogo").click(function () {
      $("#filePath_logo").click();
    })

    // - 预览
    $("#preview").click(function () {
      preview();
    })

    // - 保存
    $("#save").click(function () {
      save();
    })


    // - 日期选择器
    $(".form_datetime").datetimepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      minView: "month",
      startView: "month",
      language: "zh-CN"
    });

  })

</script>
</html>
