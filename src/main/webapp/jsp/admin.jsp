<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>集市深处 - 刘员外</title>
  <jsp:include page="common/common-css.jsp"></jsp:include>
  <jsp:include page="common/common-js.jsp"></jsp:include>

  <style type="text/css">
    tr {
      border-bottom: 1px solid #eaeaea;
    }

    td {
      padding: 10px;
    }

    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="margin-top: 15px;">
    <h3>
      集市
      <small>在这里，你可以发布最多10件物品销售。</small>
    </h3>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="margin-top: 5px;padding: 0px;background-color: #ffffff">
    <table style="width: 100%">
      <tr>
        <td><a target='_self' href='<%=basePath%>/userMgr'>
          <div><i class="icon-search"
                  style="margin-right: 10px;color: #CCCC00"></i>小伙伴
          </div>
        </a></td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="margin-top: 5px;padding: 0px;background-color: #ffffff">
    <table style="width: 100%">
      <tr>
        <td><a target='_self' href='<%=basePath%>/orderMgr'>
          <div><i class="icon-tag text-default-color"
                  style="margin-right: 10px;color: red"></i>订单管理
          </div>
        </a></td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <%--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"--%>
       <%--style="margin-top: 15px;padding: 0px;background-color: #ffffff">--%>
    <%--<table style="width: 100%">--%>
      <%--<tr>--%>
        <%--<td><a target='_self' href='<%=basePath%>/shop/info'>--%>
          <%--<div><i class="icon-home text-default-color"--%>
                  <%--style="margin-right: 10px;color: deepskyblue"></i>店铺信息--%>
          <%--</div>--%>
        <%--</a></td>--%>
        <%--<td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>--%>
      <%--</tr>--%>
    <%--</table>--%>
  <%--</div>--%>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="margin-top: 5px;padding: 0px;background-color: #ffffff">
    <table style="width: 100%">
      <tr>
        <td><a target='_self' href='<%=basePath%>/addGood'>
          <div>
            <i class="icon-plus" style="margin-right: 10px;color: #66CC33"></i>新增商品
          </div>
        </a></td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
      <tr>
        <td><a target='_self' href='<%=basePath%>/goodMgr'>
          <div><i class="icon-desktop" style="margin-right: 10px;color: blueviolet"></i>商品管理
          </div>
        </a></td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
      <tr>
        <td><a target='_self' href='<%=basePath%>/productType'>
          <div><i class="icon-th-large" style="margin-right: 10px;color: dodgerblue"></i>商品分类
          </div>
        </a></td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <%--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"--%>
       <%--style="margin-top: 15px;padding: 0px;background-color: #ffffff">--%>
    <%--<table style="width: 100%">--%>
      <%--<tr>--%>
        <%--<td>--%>
          <%--<a target='_self' href='<%=basePath%>/writeBulletin'>--%>
            <%--<div>--%>
              <%--<i class="icon-bullhorn" style="margin-right: 10px;color: #66CC33"></i>发公告--%>
            <%--</div>--%>
          <%--</a>--%>
        <%--</td>--%>
        <%--<td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>--%>
      <%--</tr>--%>
      <%--<tr>--%>
        <%--<td><a target='_self' href='<%=basePath%>/writeStory'>--%>
          <%--<div><i class="icon-edit" style="margin-right: 10px;color: blueviolet"></i>写资讯--%>
          <%--</div>--%>
        <%--</a></td>--%>
        <%--<td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>--%>
      <%--</tr>--%>
    <%--</table>--%>
  <%--</div>--%>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="margin-top: 5px;padding: 0px;background-color: #ffffff">
    <table style="width: 100%;">
      <tr>
        <td><a target='_self' href='<%=basePath%>/platDataStatistic'>
          <div><i class="icon-bar-chart"
                  style="margin-right: 10px;color: #00ccff"></i>销售统计
          </div>
        </a></td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
</div>
</div>
<jsp:include page="../foot.jsp"></jsp:include>
<jsp:include page="common/common-html.jsp"></jsp:include>
</body>
<script>
  $(function () {
    initStyle();
  })
</script>
</html>
