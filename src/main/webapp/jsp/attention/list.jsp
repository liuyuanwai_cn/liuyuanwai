<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>关注列表 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="margin: 15px 0px;padding: 10px;background-color: #ffffff">
    <div id="usersArea">
      <table style="width: 100%;margin-top: 20px">
        <tr>
          <td align="center" style="color: #d3d3d3;"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......
          </td>
        </tr>
      </table>
    </div>
    <div id="users"></div>
    <table style="width: 100%;display: none;" id="loadMore">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">
          <a class="btn btn-info" style="padding: 5px 40px">加载更多</a>
        </td>
      </tr>
    </table>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript">
  var pageNo = 1;
  var pageSize = 10;
  var index = 1;
  function loadUser(pageNo, pageSize) {
    var url = "<%=basePath%>/attention/list";
    var email = '<%=email%>';
    var param = {pageNo: pageNo, pageSize: pageSize, email: email};
    $.post(url, param, function (data, status) {
      var users = data;
      $("#usersArea").hide();
      var item;
      var logo;
      var alias;
      if (users.length > 0) {
        for (var i = 0; i < users.length; i++) {
          logo = "<%=basePath%>/img/user.png"
          if (users[i].logo != null && users[i].logo != '' && users[i].logo != undefined) {
            logo = users[i].logo + "-120x120";
          }
          alias = users[i].name;
          if (users[i].name == 'Your Name' || users[i].name == '') {
            alias = users[i].email;
          }
          item = "";
          item += '<a href="<%=basePath%>/user/queryinfo?email=' + users[i].email + '">';
          item += '<table style="width: 100%;margin-top: 5px;margin-bottom: 5px;" cellpadding="0" cellspacing="0">';
          item += '<tr>';
          item += '<td align="center" style="width: 30px;">' + (index++) + '</td>';
          item += '<td style="width: 55px"align="center"><img style="width: 40px;height: 40px" class="img-responsive img-circle" src="' + logo + '"></td>';
          item += '<td style="">' + alias + '</td>';
          item += '<td style=""align="right"><a class="btn btn-danger" href="javascript:void(0);"onclick="cancelAttention(\''+users[i].email+'\');"><i class="icon-minus-sign">&nbsp;|&nbsp;取消关注</i></a></td>';
          item += '</tr>';
          item += '<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan="2"><hr style="margin: 0px"></td></tr>';
          item += '</table>';
          item += '</a>';
          $("#users").append(item);
        }
        if (users.length == pageSize) {
          // - 还有更多数据
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<table style="width: 100%;margin-top: 20px">';
          noDataShow += '<tr>';
          noDataShow += '<td align="center" style="padding: 100px 0px"><h3 style="color: #cccccc;">all user will be Here</h3></td>';
          noDataShow += '</tr>';
          noDataShow += '</table>';
          $("#users").empty();
          $("#users").append(noDataShow);
        }else{
          $("#loadMore").hide();
        }
      }
    })
  }

  // - 取消关注
  function cancelAttention(email) {
    $('.btn').blur();
    var url = '<%=basePath%>/attention/cancel';
    var param = {attentionEmail: email};
    $.post(url, param, function (data, status) {
      aler(data);
      if (data.code == 200) {
        pageNo = 1;
        index = 1;
        $("#users").empty();
        loadUser(pageNo++, pageSize);
      }
    })
  }

  $(document).ready(function () {
    initStyle();
    loadUser(pageNo++, pageSize);
    $("#loadMore").click(function () {
      loadUser(pageNo++, pageSize);
    })
  })
</script>
</html>
