<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>员外榜 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<body style="padding: 0px;margin: 0px;background-color: #eaeaea">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0px;padding: 0px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;margin: 0px">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="center"
           style="background-color:#996699;margin: 0px;padding: 20px 0px ">
        <a class="championUrl">
          <img src="<%=basePath%>/img/user.png" style="width: 120px;height: 120px"
               class="img-responsive img-circle img-thumbnail logo">
        </a>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <a class="championStoryCount" style="color: #ffff00"><h3 class="storyCount">0</h3></a>
                <a class="championStoryCount" style="color: #ffff00">
                  <small>文章</small>
                </a>
              </td>
            </tr>
          </table>
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <h3 class="storyReadCount">0</h3>
                <small>文章阅读</small>
              </td>
            </tr>
          </table>
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <h3 class="adviceCount">0</h3>
                <small>说说</small>
              </td>
            </tr>
          </table>
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <h3 class="income">0</h3>
                <small>收益</small>
              </td>
            </tr>
          </table>
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <a class="championProductCount" style="color: #ffff00"><h3 class="productCount">0</h3></a>
                <a class="championProductCount" style="color: #ffff00">
                  <small>商品</small>
                </a>
              </td>
            </tr>
          </table>
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <h3 class="productReadCount">0</h3>
                <small>商品浏览</small>
              </td>
            </tr>
          </table>
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <a class="championProductSales" style="color: #ffff00"><h3 class="productSales">0</h3></a>
                <a class="championProductSales" style="color: #ffff00">
                  <small>商品销量</small>
                </a>
              </td>
            </tr>
          </table>
          <table style="margin-top: 10px;display: inline">
            <tr>
              <td align="center" style="color: #ffffff;padding: 10px">
                <h3 class="inviteCount">0</h3>
                <small>邀请用户</small>
              </td>
            </tr>
          </table>
        </div>
        <p style="color: #ffffff">
          <a class="championUrl" ><span class="name" style="padding: 10px;color: #99CCFF;font-size: x-large;font-weight: lighter">xxx</span></a>
          <span>以综合得分</span>
          <span class="totalScore"
                style="padding: 10px;color: #99CCFF;font-size: x-large;font-weight: lighter">xxx</span>
          <span>占领员外榜榜首!</span>
        </p>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff;">
      <div id="usersArea">
        <table style="width: 100%;background-color: #ffffff">
          <tr>
            <td align="center" style="color: #d3d3d3;padding: 100px 0px;"><i
              class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......
            </td>
          </tr>
        </table>
      </div>
      <div id="users" style="padding: 30px 0px;"></div>
      <table style="width: 100%;display: none;" id="loadMore">
        <tr>
          <td align="center" class="load" style="padding: 40px;color: #ffffff">加载更多</td>
        </tr>
      </table>
      <table style="width: 100%;display: none;" id="loadOver">
        <tr>
          <td align="center" class="load" style="padding: 40px 0px"></td>
        </tr>
      </table>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
<script language="JavaScript">
  var pageNo = 1;
  function setChampion(user) {
    var championUrl = '<%=basePath%>/user/queryinfo?email=' + user.email;
    var championStoryCountUrl = '<%=basePath%>/myStory?email=' + user.email;
    var championProductCountUrl = '<%=basePath%>/goodMgr?email=' + user.email;
    var championProductSalesUrl = '<%=basePath%>/platDataStatistic?email=' + user.email;
    $('.championUrl').attr('href', championUrl);
    $('.championStoryCount').attr('href', championStoryCountUrl);
    $('.championProductCount').attr('href', championProductCountUrl);
    $('.championProductSales').attr('href', championProductSalesUrl);
    var logo = user.logo;
    if (logo != null || logo != '' || logo != undefined) {
      $(".logo").attr("src", logo + "-120x120");
    }
    $(".storyCount").text(user.storyCount);
    $(".storyReadCount").text(user.storyReadCount);
    $(".adviceCount").text(user.adviceCount);
    $(".income").text(user.income);
    $(".productCount").text(user.productCount);
    $(".productReadCount").text(user.productReadCount);
    $(".productSales").text(user.productSales);
    $(".inviteCount").text(user.inviteCount);
    $(".name").text(user.name);
    $(".totalScore").text(user.totalScore);
  }

  var getRandomColor = function () {
    return '#' + (function (color) {
        return (color += '0123456789abcdef' [Math.floor(Math.random() * 16)])
        && (color.length == 6) ? color : arguments.callee(color);
      })('');
  }

  function loadUser(pageNo, pageSize) {
    var url = "<%=basePath%>/user/sortList";
    var param = {pageNo: pageNo, pageSize: pageSize};
    $.post(url, param, function (data, status) {
      var users = data.list;
      var user = data.sortDto;
      $("#usersArea").hide();
      var item;
      var subItem;
      var logo;
      var alias;
      if (users != null && users.length > 0) {
        // - 设置最佳硬汉封面
        setChampion(user);
        for (var i = 0; i < users.length; i++) {
          var randomColor = getRandomColor();
          logo = "<%=basePath%>/img/user.png"
          if (users[i].logo != null && users[i].logo != '' && users[i].logo != undefined) {
            logo = users[i].logo + "-120x120";
          }
          alias = users[i].name;
          if (users[i].name == '' || users[i].name == null) {
            alias = users[i].email;
          }
          subItem = "";
          item = "";
          item += '<a href="<%=basePath%>/user/queryinfo?email=' + users[i].email + '">';
          item += '<table style="display: inline;">';
          item += '<tr>';
          item += '<td align="center"style="padding: 5px"><img style="width: 40px;height: 40px" class="img-responsive img-circle" src="' + logo + '"></td>';
          item += '</tr>';
          item += '<tr>';
          item += '<td align="center" style="padding: 5px;"><small style="color: ' + randomColor + '">' + alias + '</small></td>';
          item += '</tr>';
          item += '</table>';
          item += '</a>';
          sort++;
          $("#users").append(item);
        }
        if (users.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<div style="padding: 100px 0px;background-color: #f4f7ed" class="text-center">';
          noDataShow += '<h3 style="color: #cccccc">All user will be Here</h3>';
          noDataShow += '</div>';
          $("#users").empty();
          $("#users").append(noDataShow);
        }
      }
    })
  }
  var sort = 1;
  $(document).ready(function () {
    initStyle();
    loadUser(pageNo++, 100);
    $("#loadMore").click(function () {
      loadUser(pageNo++, 100);
    })
  })
</script>
</html>
