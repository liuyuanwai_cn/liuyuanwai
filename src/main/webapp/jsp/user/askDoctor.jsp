<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getContextPath();
    Object role = request.getParameter("role");
    Object email = request.getSession().getAttribute("email");
    String doctorEmail = request.getParameter("email");
%>
<html>
<head>
    <title>问诊</title>
    <jsp:include page="../common/common-css.jsp"></jsp:include>
    <jsp:include page="../common/common-js.jsp"></jsp:include>
    <style type="text/css">
        a {
            text-decoration: none
        }

        a:hover {
            text-decoration: none
        }

        a:link {
            text-decoration: none
        }

        a:visited {
            text-decoration: none
        }
    </style>
</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
    <div class="col-xs-0 col-sm-1 col-md-3 col-lg-4"></div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-lg-4"
         style="background-color: #ffffff;margin: 15px 0px;padding: 0px 0px 40px;">
        <table style="width: 100%;">
            <tr>
                <td colspan="3" align="center" style="padding: 20px 0px;">
                    <a href="<%=basePath%>/user/queryinfo?email=${doctor.email}">
                        <c:choose>
                            <c:when test="${doctor.logo==nul or doctor.logo == ''}">
                                <img src="<%=basePath%>/img/user.png" class="img-responsive img-circle">
                            </c:when>
                            <c:otherwise>
                                <img src="${doctor.logo}-120x120" class="img-responsive img-circle">
                            </c:otherwise>
                        </c:choose>
                    </a>
                </td>
            </tr>
            <tr>
                <td style="width: 35%;padding: 5px 10px">
                    <hr>
                </td>
                <td style="width: 30%" align="center">
                    <c:choose>
                        <c:when test="${doctor.name==nul or doctor.name == ''}">
                            <i class="icon-star" style="color: #CC0099;"></i>&nbsp;${doctor.email}
                        </c:when>
                        <c:otherwise>
                            <i class="icon-star" style="color: #CC0099;"></i>&nbsp;${doctor.name}
                        </c:otherwise>
                    </c:choose>
                </td>
                <td style="width: 35%;padding: 5px 10px">
                    <hr>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 5px 15px;">
                    <blockquote>${doctor.remark}</blockquote>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 15px 15px;">
                    <table>
                        <tr>
                            <td style="color: darkgray;padding: 5px 0px">
                                <i class="icon-phone">
                                    <c:choose>
                                        <c:when test="${doctor.phone==nul or doctor.phone == ''}">
                                            &nbsp;--
                                        </c:when>
                                        <c:otherwise>
                                            &nbsp;${doctor.phone}
                                        </c:otherwise>
                                    </c:choose>
                                </i>
                            </td>
                            <td style="padding: 5px 10px;color: darkgray">
                                <i class="icon-map-marker">
                                    <c:choose>
                                        <c:when test="${doctor.addr==nul or doctor.addr == ''}">
                                            &nbsp;--
                                        </c:when>
                                        <c:otherwise>
                                            &nbsp;${doctor.addr}
                                        </c:otherwise>
                                    </c:choose>
                                </i>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 5px 15px;">
                    <p><i class="icon-heart" style="color: #FF0000">&nbsp;</i>服务很给力?&nbsp;<a
                            href="<%=basePath%>/user/queryinfo?email=${doctor.email}">打赏TA</a></p>
                </td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr>
                <td style="padding: 5px 15px;">
                    <table style="width: 100%;border-bottom: 1px solid #e8e8e8">
                        <tr>
                            <td style="width: 100%">
                                <input id="content" type="text" maxlength="512"
                                       style="width: 100%;padding: 9px;border-left: 5px solid #ffffff;border: none;"
                                       placeholder="输入问题"/>
                            </td>
                            <td style="padding: 5px;" align="right">
                                <a class="btn btn-info" href="javascript:note()">咨&nbsp;&nbsp;询</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="noteDesc text-center">
            <p style="padding: 100px 0px;color: darkgray">
                <i class="icon-spinner icon-spin"></i>&nbsp;&nbsp;正在加载咨询...
            </p>
        </div>
        <div class="note"></div>
        <div style="margin: 100px;display: none;" class="text-center noNote">
            <p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p>

            <p>没有数据</p>
        </div>
        <div id="loadMore" class="text-center" style="margin: 40px;display: none">加载更多</div>
    </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript">
    var pageNo = 1;
    var pageSize = 10;
    var noteSize = 0;
    function parseContent(url, logo, alias, createtime, content, index, uuid) {
        // - 组装说说
        var item = '';
        item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="background-color: #ffffff;padding: 15px">';
        item += '<table style="width: 100%;">';
        item += '<tr>';
        item += '<td valign="top" style="width: 50px;height: 50px;padding: 0px 5px 5px;">';
        item += '<a href="' + url + '">';
        item += '<img style="width: 40px;height: 40px" class="img-responsive img-circle" src="' + logo + '">';
        item += '</a>';
        item += '</td>';
        item += '<td>';
        item += '<table style="width: 100%;">';
        item += '<tr><td style="padding: 5px;color: #cccccc">' + alias + '</td></tr>';
        item += '<tr><td style="padding: 5px">' + content + '</td></tr>';
        item += '<tr>';
        item += '<td style="padding-left: 5px">';
        item += '<table>';
        item += '<tr>';
        item += '<td>';
        item += '<span style="font-size: smaller;color: darkgrey;">' + formatDate(createtime) + '</span>';
        item += '</td>';
        item += '<td>';
        item += '<span style="padding-left: 10px;font-size: smaller;color: darkgrey;"><a href="javascript:replayIcon(' + index + ',\'' + alias + '\',\'' + alias + '\',' + false + ')"><i class="icon-comments"style="">&nbsp;回复</i></a></span>';
        item += '</td>';
        item += '</tr>';
        item += '</table>';
        item += '</td>';
        item += '</tr>';
        item += '<tr>';
        item += '<td>';
        item += '<table style="width: 100%;margin: 5px;padding: 5px">';
        item += '<tr style="display: none" class="replybox_' + index + '">';
        item += '<td style="width: 100%">';
        item += '<input id="reply' + index + '" type="text"  maxlength="512" style="width: 100%;padding: 5px;border-left: 5px solid #ffffff;border: none;" placeholder="输入内容"/>';
        item += '</td>';
        item += "<td><a class='btn' href='javascript:reply(" + index + ",\"" + uuid + "\")'><i class='icon-reply'></i></a></td>";
        item += '</tr>';
        item += '</table>';
        item += '</td>';
        item += '</tr>';
        item += '</table>';
        item += '</td>';
        item += '</tr>';
        item += '</table>';
        return item;
    }
    function replayIcon(index, email, name, flag) {
        var clazz = '.replybox_' + index;
        $(clazz).show();
        var inputbox = '#reply' + index;
        $(inputbox).focus();
        if (!flag) {
            return;
        }
        var alias = name;
        if (name == '' || name == null || name == undefined) {
            alias = email;
        }
        $(inputbox).val('@' + alias + ' ');
    }
    function parseNote(email, logo, content, name, createTime, index, uuid) {
        if (logo != null && logo != '' && logo != undefined) {
            logo += "-120x120";
        }
        if (logo == undefined) {
            logo = '<%=basePath%>/img/user.png';
        }
        if (name == '' || name == null || name == undefined) {
            name = email;
        }
        var url = "<%=basePath%>/user/queryinfo?email=" + email;
        var subItem = '';
        subItem += '<table style="width: 100%;background-color: #f8f8f8;">';
        subItem += '<tr>';
        subItem += '<td style="width: 50px;background-color: #ffffff" rowspan="2">&nbsp;</td>';
        subItem += '<td rowspan="2" style="padding-top: 10px;padding-bottom: 5px;width: 50px;height: 50px" align="center" valign="top">';
        subItem += '<a href="' + url + '"><img class="img-responsive img-circle" style="width: 30px;height: 30px" src="' + logo + '" /></a>';
        subItem += '</td>';
        subItem += '<td style="padding-top: 10px;padding-bottom: 5px;color: #d3d3d3;">' + name + '</td>';
        subItem += '</tr>';
        subItem += '<tr>';
        subItem += '<td style="padding-bottom: 5px;padding-top: 0px;font-size: smaller;">';
        subItem += '<p>' + content + '</p>';
        subItem += '<table>';
        subItem += '<tr>';
        subItem += '<td align="left" style="padding: 5px 0px;color: #d3d3d3">';
        subItem += '<p style="font-size: smaller;color: darkgrey;margin: 0px">' + createTime + '</p>';
        subItem += '</td>';
        subItem += '<td style="padding: 5px;"valign="bottom">';
        subItem += '<span style="padding: 0px 10px;font-size: smaller;color: darkgrey;"><a href="javascript:replayIcon(' + index + ',\'' + email + '\',\'' + name + '\',' + true + ')"><i class="icon-comments-alt"></i></a></span>';
        subItem += '</td>';
        subItem += '</tr>';
        subItem += '</table>';
        subItem += '</td>';
        subItem += '</tr>';
        subItem += '</table>';
        return subItem;
    }
    function noMoreData() {
        // - 没有更多数据
        $("#loadMore").hide();
        $("#loadOver").show();
    }
    function moreData() {
        // - 还有更多数据
        $("#loadOver").hide();
        $("#loadMore").show();
    }
    // - 日期解析
    function formatDate(strTime) {
        if (strTime == null || strTime == '') {
            return "";
        }
        var date = new Date(strTime);
        return date.getFullYear() + "-" + addZero(date.getMonth() + 1) + "-" + addZero(date.getDate()) + " " + addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
    }
    function addZero(val) {
        return val < 10 ? '0' + val : val;
    }
    function reply(index, uuid) {
        var email = '<%=email%>';
        if (!validateEmail(email)) {
            return;
        }
        var id = '#reply' + index;
        var content = $.trim($(id).val());
        if (content == '') {
            alerErr("输入留言内容");
            return;
        }
        var url = "<%=basePath%>/note/add";
        var param = {content: content, uuid: uuid};
        $.post(url, param, function (data, status) {
            if (data.code == 200) {
                var clazz = '.replybox_' + index;
                $(clazz).hide();
                var noteId = '#note' + index;
                $(id).val('');
                var temp = parseNote(data.data.email, data.data.logo, data.data.content, data.data.name, data.data.createTime);
                $(noteId).prepend(temp);
            } else {
                aler(data);
            }
        })
    }
    function loadNote(pageNo, pageSize) {
        var index_init = (pageNo - 1) * pageSize;
        var index = 0;
        var reply_index = 0;
        var url = "<%=basePath%>/words/list";
        var param = {pageNo: pageNo, pageSize: pageSize, to: '<%=doctorEmail%>'};
        $.post(url, param, function (data, status) {
            var words = data;
            var item;
            var alias;
            var logo = '<%=basePath%>/img/user.png';
            $(".noteDesc").hide();
            if (words.length == 0) {
                if (pageNo == 1) {
                    // - 没有数据
                    $(".noNote").show();
                } else {
                    // - 没有更多数据
                    $("#loadMore").hide();
                }
            } else {
                for (var i = 0; i < words.length; i++) {
                    index = index_init + i;
                    noteSize = index;
                    logo = '<%=basePath%>/img/user.png';
                    alias = words[i].name;
                    if (alias == 'Your Name' || alias == '') {
                        alias = words[i].email;
                    }
                    if (logo != null && logo != '' && logo != undefined) {
                        logo = words[i].logo + "-120x120";
                    }
                    var url = "<%=basePath%>/user/queryinfo?email=" + words[i].email;
                    item = "";
                    item += parseContent(url, logo, alias, words[i].createTime, words[i].content, index, words[i].uuid);
                    item += '<div id="note' + index + '">';
                    var notes = words[i].notes;
                    if (notes != undefined && notes.length > 0) {
                        for (var j = 0; j < notes.length; j++) {
                            var note = notes[j];
                            var replyId = '#reply' + reply_index;
                            item += parseNote(note.email, note.logo, note.content, note.name, note.createTime, index, words[i].uuid);
                        }
                    }
                    item += '</div>';
                    item += '</div>';
                    $(".note").append(item);
                }
                if (words.length == pageSize) {
                    moreData();
                } else {
                    noMoreData();
                }
            }
        })
    }

    function note() {
        $(".btn").blur();
        var email = '<%=email%>';
        if (!validateEmail(email)) {
            return;
        }
        var content = $.trim($("#content").val());
        if (content == "") {
            alerErr("输入内容");
            return;
        }
        var param = {
            from: '<%=email%>',
            to: '<%=doctorEmail%>',
            content: content
        };
        var url = "<%=basePath%>/words/add";
        $.post(url, param, function (data, status) {
            if (data.code == 200) {
                $("#content").val('');
                var note = data.data;
                var url = "<%=basePath%>/user/queryinfo?email=" + note.email;
                var alias = note.name;
                if (alias == null || alias == '' || alias == undefined) {
                    alias = note.email;
                }
                var temp = parseContent(url, note.logo, alias, note.createTime, note.content, noteSize++, note.uuid);
                $(".noteDesc").hide();
                $(".note").prepend(temp);
            } else {
                aler(data);
            }
        })
    }
    $(document).ready(function () {
        initStyle();
        loadNote(pageNo++, pageSize);
    })
</script>
</html>
