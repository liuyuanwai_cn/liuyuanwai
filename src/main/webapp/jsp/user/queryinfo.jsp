<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
  String email = request.getParameter("email");
  Object role = request.getSession().getAttribute("role");
  Object loginEmail = request.getSession().getAttribute("email");
%>

<html>
<head>
  <title>${userExtDto.name} - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <script type="application/javascript" src="<%=basePath%>/plugin/Echarts/echarts.min.js"></script>
  <style type="text/css">
    .titleDesc {
      color: darkgrey;
    }

    td {
      padding: 10px
    }

    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    .statistic td {
      padding: 10px;
    }

    .aliasName {
      position: relative;
    }

    .aliasName:before {
      position: absolute;
      content: "";
      width: 2rem;
      height: 2rem;
      border: none;
      right: -1rem;
      top: 1rem;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      -o-transform: rotate(45deg);
      transform: rotate(45deg);
      background-color: #f4f7ed;
    }

    pre {
      border: none;
      white-space: pre-wrap;
      word-wrap: break-word;
    }

    .circleBox {
      border-radius: 3px;
      border: none;
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3"
       style="padding: 40px 0px;">
    <table align="center" style="width: 100%;">
      <tr>
        <td align="center">
          <c:choose>
            <c:when test="${userExtDto.logo == null or userExtDto.logo == ''}">
              <img src="<%=basePath%>/img/user.png" style="width: 90px;height: 90px"
                   class="img-responsive img-circle">
            </c:when>
            <c:otherwise>
              <img src="${userExtDto.logo}-120x120" style="width: 90px;height: 90px;cursor: crosshair"
                   class="img-responsive img-circle logoImg">
            </c:otherwise>
          </c:choose>
        </td>
      </tr>
      <tr>
        <td style="padding: 10px 15px" align="center">
          <span class="label label-success" style="margin: 0px">${userExtDto.name}</span>
        </td>
      </tr>
      <tr>
        <td align="center">
          &nbsp;${userExtDto.remark}&nbsp;
        </td>
      </tr>
      <tr>
        <td align="center">
          <table style="width: 100%;" class="statistic">
            <tr>
              <td align="center" style="padding-top: 0px">
                <p style="margin: 0px">${userExtDto.sortDto.storyCount}</p>

                <p style="margin: 0px">
                  <small>文章</small>
                </p>
              </td>
              <td align="center" style="padding-top: 0px">
                <p style="margin: 0px">${userExtDto.sortDto.storyReadCount}</p>

                <p style="margin: 0px">
                  <small>文章浏览</small>
                </p>
              </td>
              <td align="center" style="padding-top: 0px">
                <p style="margin: 0px">${userExtDto.sortDto.adviceCount}</p>

                <p style="margin: 0px">
                  <small>说说</small>
                </p>
              </td>
              <td align="center" style="padding-top: 0px">
                <p style="margin: 0px">${userExtDto.sortDto.income}</p>

                <p style="margin: 0px">
                  <small>收益</small>
                </p>
              </td>
              <td align="center" style="padding-top: 0px">
                <p style="margin: 0px">${userExtDto.sortDto.fans}</p>

                <p style="margin: 0px">
                  <small>粉丝</small>
                </p>
              </td>
            </tr>
            <tr>
              <td align="center">
                <p style="margin: 0px">${userExtDto.sortDto.productCount}</p>

                <p style="margin: 0px">
                  <small>商品</small>
                </p>
              </td>
              <td align="center">
                <p style="margin: 0px">${userExtDto.sortDto.productReadCount}</p>

                <p style="margin: 0px">
                  <small>商品浏览</small>
                </p>
              </td>
              <td align="center">
                <p style="margin: 0px">${userExtDto.sortDto.productSales}</p>

                <p style="margin: 0px">
                  <small>销量</small>
                </p>
              </td>
              <td align="center">
                <p style="margin: 0px">${userExtDto.sortDto.inviteCount}</p>

                <p style="margin: 0px">
                  <small>邀请</small>
                </p>
              </td>
              <td align="center">
                <p style="margin: 0px">${userExtDto.sortDto.attentionCount}</p>

                <p style="margin: 0px">
                  <small>关注</small>
                </p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center">
          <table>
            <tr>
              <td>
                <button class="btn btn-info btn-sm addAttention" onclick="addAttention('<%=email%>')"><i
                  class="icon-heart-empty">&nbsp;|&nbsp;关注</i></button>
                <button class="btn btn-danger btn-sm cancelAttention" style="display: none;"
                        onclick="cancelAttention('<%=email%>')"><i class="icon-minus-sign">&nbsp;|&nbsp;取消关注</i>
                </button>
              </td>
              <td>
                <button style="background-color: #CCCCCC" class="btn btn-sm" onclick="" disabled><i
                  class="icon-envelope-alt">&nbsp;|&nbsp;私信</i></button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <% if (role != null && role.toString().equals("1")) { %>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px" align="center">
      <hr>
      <h4>用户角色</h4>
      <select class="shortselect" style="margin-top: 10px">
        <option value="1">超级管理员</option>
        <option value="2">管理员</option>
        <option value="3">普通用户</option>
      </select>
    </div>
    <%}%>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6 content" style="padding: 0px;background-color: #ffffff">
    <div style="padding: 10px 0px;">
      <ul class="nav nav-tabs" role="tablist" style="padding-left: 10px">
        <li role="presentation"><a style="border: none;border-bottom: 2px solid blueviolet" href="javascript:void(0);"
                                   onclick="chooseStory()">文章</a>
        </li>
        <li role="presentation"><a href="javascript:void(0);" onclick="chooseWord()">龙门阵</a></li>
        <li role="presentation"><a href="javascript:void(0);" onclick="chooseGood()">商品</a></li>
        <li role="presentation"><a href="javascript:void(0);" onclick="business()">Business</a></li>
      </ul>
    </div>
    <div class="story"></div>
    <div class="word"></div>
    <div class="good"></div>
    <div class="business" style="display: none;padding: 20px;" id='statisticChart'>
      <p style="color: darkgrey"><i
        class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在计算...
      </p>
    </div>
    <div style="padding: 40px;color: #CCCCCC;background-color: #ffffff"
         class="col-xs-12 col-sm-12 col-md-12 col-lg-12 loading">
      <i class="icon-spinner icon-spin"></i>&nbsp;正在加载&nbsp;...
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 40px 0px;background-color: #ffffff">
      &nbsp;</div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg" id="imgModal" role="dialog" style="padding: 0px;margin: 0px">
  <div class="modal-dialog modal-lg" style="margin: 0px auto;padding: 0px">
    <div class="modal-content"
         style="border: none;box-shadow: none;background-color: transparent;padding: 0px;margin: 0px">
      <table style="width: 100%;height: 100%">
        <tr>
          <td align="center" valign="middle"style="padding: 0px">
            <img src="" class="img-responsive imgShow">
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<input type="hidden" id="story" value="1">
<input type="hidden" id="word" value="0">
<input type="hidden" id="good" value="0">
<input type="hidden" id="business" value="0">
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>


<script type="application/javascript">
  var storyPageNo = 1
  var wordPageNo = 1
  var goodPageNo = 1
  var pageSize = 10;


  // - 添加关注
  function addAttention(email) {
    $('.btn').blur();
    var loginEmail = '<%=loginEmail%>';
    if (!validateEmail(loginEmail)) {
      return;
    }
    if (email == loginEmail) {
      alerErr('不能自己关注自己');
      return;
    }
    var url = '<%=basePath%>/attention/add';
    var param = {attentionEmail: email};
    $.post(url, param, function (data, status) {
      aler(data);
      if (data.code == 200) {
        showCancelAttention();
      }
    })
  }
  // - 取消关注
  function cancelAttention(email) {
    $('.btn').blur();
    var loginEmail = '<%=loginEmail%>';
    if (!validateEmail(loginEmail)) {
      return;
    }
    var url = '<%=basePath%>/attention/cancel';
    var param = {attentionEmail: email};
    $.post(url, param, function (data, status) {
      aler(data);
      if (data.code == 200) {
        showAddAttention();
      }
    })
  }

  function showCancelAttention() {
    $('.addAttention').hide();
    $('.cancelAttention').show();
  }
  function showAddAttention() {
    $('.cancelAttention').hide();
    $('.addAttention').show();
  }

  function initAttention() {
    var loginEmail = '<%=loginEmail%>';
    if (isNull(loginEmail)) {
      return;
    }
    var url = '<%=basePath%>/attention/queryAttention';
    var param = {email: loginEmail, attentionEmail: '<%=email%>'};
    $.post(url, param, function (data, status) {
      var attention = data.data;
      if (attention == undefined) {
        return;
      }
      if (attention.deleted == 1) {
        showCancelAttention();
      }
    })
  }

  function storySelected() {
    $('.word').hide();
    $('.good').hide();
    $('.business').hide();
    $('.story').show();
    $('#story').val(1);
  }
  function wordSelected() {
    $('.good').hide();
    $('.story').hide();
    $('.business').hide();
    $('.word').show();
    $('#word').val(1);
  }
  function goodSelected() {
    $('.story').hide();
    $('.word').hide();
    $('.business').hide();
    $('.good').show();
    $('#good').val(1);
  }
  function businessSelected() {
    $('.story').hide();
    $('.word').hide();
    $('.good').hide();
    $('.business').show();
    $('#business').val(1);
  }

  // - 更新用户角色
  function updateUserRole(email, roleId) {
    var param = {email: email, roleId: roleId};
    var url = "<%=basePath%>/user/updateuserrole";
    $.post(url, param, function (data, status) {
      alerOK(data.msg);
    })
  }
  // -加载用户角色
  function loadRole(roleId) {
    $(".shortselect").val(roleId);
  }


  function statisticChart(xAixs, yAixs) {
    var myChart = echarts.init(document.getElementById('statisticChart'));
    var xName = '';
    var option = {
      title: {
        text: '商品销量统计',
        subtext: '商品名称'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        type: 'value',
        splitLine: {show: false}
      },
      yAxis: {
        type: 'category',
        data: xAixs,
        splitLine: {show: false}
      },
      series: [{
        type: 'bar',
        name: '销量',
        data: yAixs,
        itemStyle: {
          normal: {
            color: function (value) {
              return getRandomColor()
            },
            label: {show: true, position: 'insideRight'}
          }
        }
      }]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
  }

  var getRandomColor = function () {
    var color = '#' + (function (color) {
        return (color += '0123456789abcdef' [Math.floor(Math.random() * 16)])
        && (color.length == 6) ? color : arguments.callee(color);
      })('');
    return color;
  }
  // - 商品销量统计
  function salesStatistic(queryEmail) {
    var url = '<%=basePath%>/order/statisticSales?email=' + queryEmail;
    $.post(url, function (data, status) {
      $('.business').empty();
      var xAixs = data.xAixs;
      var yAixs = data.yAixs;
      if (xAixs == undefined || xAixs.length == 0) {
        $(".business").html('<div class="text-center"style="padding: 100px 0px;color: #999999">No Goods For ' + queryEmail + '</div>');
      } else {
        var h = xAixs.length * 50;
        h += 'px';
        $(".business").height(h);
        statisticChart(xAixs, yAixs);
      }
    })
  }

  function business(){
    var business = $('#business').val();
    businessSelected();
    if (business == 1) {
      return;
    }
    var email = '<%=email%>';
    salesStatistic(email);
  }

  function chooseStory() {
    var story = $('#story').val();
    storySelected();
    if (story == 1) {
      return;
    }
    loadStory(storyPageNo++, pageSize);
  }
  function chooseWord() {
    var word = $('#word').val();
    wordSelected();
    if (word == 1) {
      return;
    }
    loadWord(wordPageNo++, pageSize);
  }
  function chooseGood() {
    var good = $('#good').val();
    goodSelected();
    if (good == 1) {
      return;
    }
    loadGood(goodPageNo++, pageSize);
  }

  function storyDetail(uuid) {
    var url = "<%=basePath%>/story/info?uuid=" + uuid;
    window.location.href = url;
  }

  function goodDetail(productId) {
    window.location = "<%=basePath%>/order/preadd?productId=" + productId;
  }

  function loading() {
    $('.loading').show();
  }
  function loaded() {
    $('.loading').hide();
  }

  function loadStory(pageNo, pageSize) {
    loading();
    var url = "<%=basePath%>/story/list?email=<%=email%>";
    var param = {pageNo: pageNo, pageSize: pageSize};
    var item = '';
    var logo = '';
    $.post(url, param, function (data, status) {
      loaded();
      var stories = data;
      if (stories.length > 0) {
        for (var i = 0; i < stories.length; i++) {
          logo = '';
          if (stories[i].imgUrl == null || stories[i].imgUrl == '' || stories[i].imgUrl == undefined || stories[i].imgUrl == '<%=basePath%>/img/product.png') {
            logo = '<%=basePath%>/img/product.png';
          } else {
            logo = stories[i].imgUrl + '-360x200';
          }
          item = '';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding: 20px 10px;background-color: #ffffff;">';
          item += '<a style="text-decoration: none;margin-bottom: 10px" href="javascript:void(0);" onclick="storyDetail(\'' + stories[i].uuid + '\');">';
          item += '<div class=" col-xs-8 col-sm-9 col-md-9 col-lg-10">';
          item += '<h4 style="margin-top: 2px;color: #000000;font-weight: bold">' + stories[i].title + '</h4>';
          item += '<p style="color: #999999"><small>阅读：' + stories[i].readCount + '</small></p>';
          item += '<span class="badge" style="background-color: #c0a16b">' + stories[i].tag + '</span>';
          item += '</div>';
          item += '<div class=" col-xs-4 col-sm-3 col-md-3 col-lg-2 text-center"style="padding: 0px ;">';
          item += '<img class="img-responsive" src="' + logo + '" alt="' + stories[i].name + '">';
          item += '</div>';
          item += '</a>';
          item += '</div>';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="background-color: #ffffff;padding: 0px 5px"><hr style="margin: 5px"></div>'
          $(".story").append(item);
        }
        if (stories.length == pageSize) {
          item = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 loadMoreStory" align="center" style="background-color: #ffffff;padding: 40px 0px">';
          item += '<button class="btn btn-info" onclick="loadMoreStory()"  style="padding: 7px 40px">加载更多</button>';
          item += '</div>';
          $(".story").append(item);
        }
      } else {
        // - 没有数据
        if (pageNo == 1) {
          var noDataShow = '';
          noDataShow += '<h3 style="color: #CCCCCC;padding: 100px 10px "class="text-center">no article here!</h3>';
          $(".story").empty();
          $(".story").append(noDataShow);
        }
      }
    })
  }
  function loadWord(pageNo, pageSize) {
    loading();
    var index_init = (pageNo - 1) * pageSize;
    var index = 0;
    var url = "<%=basePath%>/advice/list";
    var param = {pageNo: pageNo, pageSize: pageSize, type: 1, status: null, email: '<%=email%>'};
    $.post(url, param, function (data, status) {
      loaded();
      var advices = data;
      var item;
      var alias;
      var logo = '<%=basePath%>/img/user.png';
      if (advices.length == 0) {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<h3 style="color: #CCCCCC;padding: 100px 10px "class="text-center">no note here!</h3>';
          $(".word").empty();
          $(".word").append(noDataShow);
        }
      } else {
        for (var i = 0; i < advices.length; i++) {
          index = index_init + i;
          logo = '<%=basePath%>/img/user.png';
          var msgType = advices[i].msgType;
          alias = advices[i].name;
          var dealName = advices[i].dealName;
          var dealTime = advices[i].dealTime;
          var dealPhone = advices[i].dealPhone;
          if (advices[i].name == 'Your Name') {
            alias = advices[i].email;
          }
          if (advices[i].logo != null && advices[i].logo != '' && advices[i].logo != undefined) {
            logo = advices[i].logo + "-120x120";
          }
          var url = "<%=basePath%>/user/queryinfo?email=" + advices[i].email;
          item = "";
          item += parseContent(url, logo, alias, advices[i].createtime, advices[i].content, index, advices[i].uuid, advices[i].notes, advices[i].email, advices[i].img);
          $(".word").append(item);
          $('.word img').addClass('img-responsive');
        }
        if (advices.length == pageSize) {
          item = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 loadMoreWord" align="center" style="background-color: #ffffff;padding: 40px 0px">';
          item += '<button type="button" class="btn btn-info" onclick="loadMoreWord()"  style="padding: 7px 40px">加载更多</button>';
          item += '</div>';
          $(".word").append(item);
        }
      }
    })
  }

  function parseNote(email, logo, content, name, createTime, index) {
    if (logo != null && logo != '' && logo != undefined) {
      logo += "-120x120";
    }
    if (logo == undefined) {
      logo = '<%=basePath%>/img/user.png';
    }
    var url = "<%=basePath%>/user/queryinfo?email=" + email;
    var subItem = '';
    subItem += '<hr style="margin: 5px 0px">';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr><td style="padding: 0px">';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr>';
    subItem += '<td style="padding: 5px;width: 40px;height: 40px;" align="left" valign="top">';
    subItem += '<a href="' + url + '"><img class="img-responsive img-circle" style="width: 30px;height: 30px" src="' + logo + '" /></a>';
    subItem += '</td>';
    subItem += '<td style="padding: 10px 5px;color: #999999;"valign="middle"><small>' + name + '</small></td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style="padding: 5px"colspan="2">';
    subItem += content;
    subItem += '<table>';
    subItem += '<tr>';
    subItem += '<td style="color: #999999;padding: 0px">';
    subItem += '<small>' + createTime + '</small>';
    subItem += '</td>';
    subItem += '<td style="padding-left: 10px">';
    subItem += '<a href="javascript:replayIcon(' + index + ',\'' + email + '\',\'' + name + '\',' + true + ')"><i class="icon-comments-alt"></i></a>';
    subItem += '</td>';
    subItem += '</tr>';
    subItem += '</table>';
    subItem += '</td>';
    subItem += '</tr>';
    subItem += '</table>';
    subItem += '</td></tr></table>';
    return subItem;
  }

  function formatDate(strTime) {
    if (strTime == null || strTime == '') {
      return "";
    }
    var date = new Date(strTime);
    return date.getFullYear() + "-" + addZero(date.getMonth() + 1) + "-" + addZero(date.getDate()) + " " + addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
  }

  function addZero(val) {
    return val < 10 ? '0' + val : val;
  }

  function HTMLDeCode(str) {
    var s = "";
    if (str.length == 0)    return "";
    s = str.replace(/&lt;/g, "<");
    s = s.replace(/&gt;/g, ">");
    s = s.replace(/&nbsp;/g, "    ");
    s = s.replace(/'/g, "\'");
    s = s.replace(/&quot;/g, "\"");
    s = s.replace(/ <br>/g, "\n");
    return s;
  }

  function parseContent(url, logo, alias, createtime, content, index, uuid, notes, email, img) {
    var loginEmail = '<%=email%>';
    // - 组装说说
    var item = '';
    item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="content' + uuid + '" style="padding: 10px 0px;background-color: #ffffff">';
    item += '<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11"style="padding: 0px">';
    item += '<table style="width: 100%;" >';
    item += '<tr>';
    item += '<td class="hidden-xs hidden-sm aliasName" style="width: 70px;height: 70px;padding: 2px 8px;"valign="top">';
    item += '<img style="width: 40px;height: 40px" class="img-responsive img-circle" src="' + logo + '">';
    item += '</td>';
    item += '<td style="color: darkgrey;background-color: #f4f7ed">';
    item += '<p style="background-color: #f4f7ed;padding: 0px;margin: 0px"class=" hidden-xs"></p>'
    item += '<p style="background-color: #f4f7ed;padding: 0px;" class="circleBox">'
    item += '<table style="width: 100%;background-color: #f4f7ed">';
    item += '<tr>';
    item += '<td style="background-color: #f4f7ed;padding: 0px;">';
    item += '<table>';
    item += '<tr>';
    item += '<td class="visible-xs visible-sm" style="padding-right: 10px;padding-left: 0px">';
    item += '<img style="width: 40px;height: 40px;" class="img-responsive img-circle" src="' + logo + '">';
    item += '</td>';
    item += '<td style="padding-left: 5px">';
    item += alias;
    item += '</td>';
    if (loginEmail != undefined && loginEmail != '' && loginEmail == email) {
      item += '<td>';
      item += '<span style="padding-left: 10px"><a href="javascript:void(0);"onclick="delNote(\'' + uuid + '\')"><small>删除</small></a></span>';
      item += '</td>';
    }
    item += '</tr>';
    item += '</table>';
    item += '</td>';
    item += '</tr>';
    item += '<tr><td style="padding: 10px 5px;background-color: #f4f7ed">' + HTMLDeCode(content) + '</td></tr>';
    item += '<tr>';
    item += '<td style="padding: 0px">';
    item += '<table>';
    item += '<tr>';
    item += '<td align="left" style="padding: 5px;color: #999999;background-color: #f4f7ed"><small>' + formatDate(createtime) + '</small></td>';
    item += '<td align="right" style="padding: 5px;"><a href="javascript:replayIcon(' + index + ',\'' + alias + '\',\'' + alias + '\',' + false + ')"><i class="icon-comments">&nbsp;评论</i></a></td>';
    item += '</tr>';
    item += '</table>';
    item += '<table style="width: 100%;padding:5px 20px;display:none;margin:10px 0px;"class="replyBox_' + index + '">';
    item += '<tr>';
    item += '<td style="padding: 5px">';
    item += '<table style="width: 100%">';
    item += '<tr>';
    item += '<td style="width: 100%;padding: 0px">';
    item += '<textarea rows="3" id="reply' + index + '" type="text"  maxlength="512" style="resize:none;width: 100%;padding: 5px;border-left: 5px solid #ffffff;border: none;" placeholder="输入内容"/>';
    item += '</td>';
    item += '</tr>';
    item += '<tr>';
    item += "<td align='right'style='padding: 0px'>";
    item += "<a style='padding: 3px;margin-right: 10px;margin-top: 10px' class='btn btn-info' href='javascript:cancelReply(" + index + ");'>&nbsp;取&nbsp;消&nbsp;</a>";
    item += "<a style='padding: 3px;margin-top: 10px' class='btn btn-info' href='javascript:reply(" + index + ",\"" + uuid + "\",\"" + email + "\")'>&nbsp;回&nbsp;复&nbsp;</a>";
    item += "</td>";
    item += '</tr>';
    item += '</table>';
    item += '</td>';
    item += '</tr>';
    item += '</table>';
    if (notes != undefined && notes.length > 0) {
      item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding: 0px" id="note' + index + '">';
      for (var j = 0; j < notes.length; j++) {
        var note = notes[j];
        item += parseNote(note.email, note.logo, note.content, note.name, note.createTime, index);
      }
      item += '</div>';
    } else {
      item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding-top: 15px;padding-left:0px" id="note' + index + '"></div>';
    }
    item += '</p>';
    item += '</td>';
    item += '</tr>';
    item += '</table>';
    item == '</div>';
    item == '</div>';
    $('.word img').addClass('img-responsive');
    $('.word img').css('cursor', 'crosshair');
    $('.word img').click(function () {
      var imgUrl = $(this).attr('src');
      if(imgUrl.indexOf('800x800') > 0 || imgUrl.indexOf('750x500') > 0 || imgUrl.indexOf('580x350') > 0){
        imgUrl = imgUrl.substr(0, imgUrl.length - 8);
      }
      $('.imgShow').attr('src', imgUrl);
      $('#imgModal').modal();
    });
    return item;
  }

  function loadGood(pageNo, pageSize) {
    loading();
    var url = "<%=basePath%>/product/mgrList";
    var email = '<%=email%>'
    var param = {pageNo: pageNo, pageSize: pageSize, email: email};
    $.post(url, param, function (data, status) {
      loaded();
      var goods = data;
      $("#goodsArea").hide();
      if (goods.length > 0) {
        for (var i = 0; i < goods.length; i++) {
          $(".good").append(buildGood(goods[i]));
        }
        if (goods.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<h3 style="color: #CCCCCC;padding: 100px 10px "class="text-center">no goods here!</h3>';
          $(".good").empty();
          $(".good").append(noDataShow);
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }

  function cancelReply(index) {
    var clazz = '.replyBox_' + index;
    $(clazz).hide();
    var ID = '#reply' + index;
    $(ID).val('');
  }
  function replayIcon(index, email, name, flag) {
    var clazz = '.replyBox_' + index;
    $(clazz).show();
    var ID = '#reply' + index;
    $(ID).focus();
    if (!flag) {
      $(ID).val('');
      return;
    }
    var alias = name;
    if (name == '' || name == null || name == undefined) {
      alias = email;
    }
    $(ID).val('@' + alias + ' ');
    $("#toEmail").val(email);
  }
  function reply(index, uuid, toEmail) {
    $(".btn").blur();
    var email = '<%=loginEmail%>';
    if (!validateEmail(email)) {
      return;
    }
    var id = '#reply' + index;
    var content = $.trim($(id).val());
    if (content == '') {
      alerErr("输入留言内容");
      return;
    }
    var toEmails = $("#toEmail").val();
    if (toEmails != '') {
      toEmail = toEmails;
    }
    var url = "<%=basePath%>/note/add";
    var param = {content: content, uuid: uuid, toEmail: toEmail, title: ''};
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        $("#toEmail").val('');
        var noteId = '#note' + index;
        $(id).val('');
        var temp = parseNote(data.data.email, data.data.logo, data.data.content, data.data.name, data.data.createTime, index);
        $(noteId).append(temp);
        var clazz = '.replyBox_' + index;
        $(clazz).hide();
        var ID = '#reply' + index;
        $(ID).val('');
      } else {
        aler(data);
      }
    })
  }

  function buildGood(goodExt) {
    var type = goodExt.type;
    var typeDesc = goodExt.remark;
    var list = goodExt.list;
    var item = '';
    var temp = '';
    item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">'
    item += '<h3 style="color: #666666;padding: 0px 10px">' + type + '</h3>'
    item += '<hr>'
    item += '</div>'
    for (var i = 0; i < list.length; i++) {
      var goodUrl = '';
      var url = list[i].url;
      var id = list[i].id;
      var name = list[i].name;
      var deleted = list[i].deleted;
      var sales = list[i].sales;
      var price = list[i].price;
      var unit = list[i].unit;
      var inventory = list[i].inventory;
      var readCount = list[i].readCount;
      var remark = list[i].remark;
      if (url == null || url == '' || url == undefined || url == '<%=basePath%>/img/product.png') {
        goodUrl = '<%=basePath%>/img/product.png';
      } else {
        goodUrl = url + '-360x200';
      }
      item += '<a style="text-decoration: none" href="javascript:void(0);" onclick="goodDetail(' + id + ');">';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 20px;background-color: #ffffff">';
      item += '<div class=" col-xs-3 col-sm-3 col-md-3 col-lg-2 text-center"style="margin: 5px 0px;padding: 0px ">';
      item += '<img class="img-responsive" src="' + goodUrl + '" alt="' + name + '">';
      item += '</div>';
      item += '<div class=" col-xs-9 col-sm-9 col-md-9 col-lg-10">';
      item += '<table style="width: 100%">';
      item += '<tr>';
      item += '<td style="padding: 5px;font-weight: bold"colspan="2">' + name + '</td>';
      item += '</tr>';
      item += '<tr>';
      item += '<td style="padding: 5px;color: red">￥<span style="font-size: larger">' + price.toFixed(2) + '</span>/' + unit + '</td>';
      item += '<td style="padding: 5px" align="right">';
      if (deleted == 1) {
        item += '<span class="label label-default">已下架</span>';
      } else {
        if (inventory == 0) {
          item += '&nbsp;&nbsp;<span class="label label-danger">已卖完</span>';
        } else {
          item += '<span class="label label-success">在售中</span>';
        }
      }
      item += '</td>';
      item += '</tr>';
      item += '<tr>';
      item += '<td colspan="2" style="padding: 5px;color: #999999" align="right"><small>浏览：</small>' + readCount + '&nbsp;|&nbsp;<small>销量：</small>' + sales + '</td>';
      item += '</tr>';
      item += '</table>';
      item += '</div>';
      item += '</div>';
      item += '</a>';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="background-color: #ffffff;"><hr style="margin: 0px"></div>';
      temp += item;
      item = '';
    }
    return temp;
  }

  function initHeight() {
    var height = $(".navbar").height();
    var windowHeight = $(window).height();
    $('.content').height(windowHeight - height);
  }

  function loadMoreWord() {
    $(".loadMoreWord").remove();
    loadWord(wordPageNo++, pageSize);
  }
  function loadMoreStory() {
    $(".loadMoreStory").remove();
    loadStory(storyPageNo++, pageSize);
  }

  $(function () {
    initStyle();
    initHeight();
    // - 动态加载用户角色
    loadRole(${userExtDto.role});

    // - 更新用户角色
    $(".shortselect").change(function () {
      updateUserRole('${userExtDto.email}', $(".shortselect").val());
    })

    // - 交替展示效果
    $('.nav-tabs a').click(function () {
      $('.nav-tabs a').css('border', 'none');
      $(this).css('border-bottom', '2px solid blueviolet');
    })

    // - 初始化加载文章
    loadStory(storyPageNo++, pageSize);

    // - 初始化关注状态
    initAttention();


    $('.logoImg').click(function () {
      var imgUrl = $(this).attr('src');
      imgUrl = imgUrl.substr(0, imgUrl.length - 8);
      $('.imgShow').attr('src', imgUrl);
      $('#imgModal').modal();
    })

    $('.imgShow').click(function () {
      $('#imgModal').modal('hide');
    })
  })
</script>

</html>
