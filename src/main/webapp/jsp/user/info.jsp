<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>个人中心</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <script type="application/javascript" src="<%=basePath%>/plugin/Echarts/echarts.min.js"></script>

  <style type="text/css">

    td {
      padding: 10px;
    }

    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    hr {
      margin-top: 5px;
      margin-bottom: 5px;
    }

    p {
      padding-left: 10px;
    }

  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-2 col-md-3 col-lg-4"></div>
  <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4" style="background-color: #ffffff;margin: 30px 0px;padding: 0px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 20px 0px">
      <a href="<%=basePath%>/set/info">
        <c:choose>
          <c:when test="${userExtDto.logo == null}">
            <img src="<%=basePath%>/img/user.png" id="upload" class="img-responsive img-circle"
                 style="display: block;margin: 0px auto">
          </c:when>
          <c:otherwise>
            <img src="${userExtDto.logo}-120x120" id="upload" class="img-responsive img-circle"
                 style="display: block;margin: 0px auto">
          </c:otherwise>
        </c:choose>
      </a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <%--<p>社交数据</p>--%>
      <hr style="width: 100%;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff">
        <table style="width: 100%">
          <tr>
            <td style="width: 50%;" align="center" valign="middle">
              <h4>
                <small>我的收益</small>
                <span style="color: gray">￥</span>
                <span style="font-size: x-large" class="text-default-color">${userExtDto.inCount}</span>
              </h4>
            </td>
            <td style="width: 50%;" align="center" valign="middle">
              <h4>
                <small>消费额</small>
                <span style="color: gray">￥</span>
                <span style="font-size: x-large" class="text-default-color">${userExtDto.credits}</span>
              </h4>
            </td>
          </tr>
          <tr>
            <td align="center" style="width: 50%;">
              <small><a href="<%=basePath%>/userlist">收益榜</a></small>
            </td>
            <td align="center" style="width: 50%;">
              <small><a href="<%=basePath%>/myorder">历史订单</a></small>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 10px 0px">
      <%--<p>十日消费流水</p>--%>
      <hr style="width: 100%;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff">
        <div id="chart" style="height: 200px;margin: 0px" class="text-center">
          <p style="margin-top: 20px;color: darkgrey"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在计算...
          </p>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 10px 0px;">
      <%--<p>收益统计</p>--%>
      <hr style="width: 100%;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff">
        <table style="width: 100%">
          <tr>
            <td align="center" style="color: darkgrey">累计收益(元)</td>
            <td align="center" style="color: darkgrey">可提现(元)</td>
          </tr>
          <tr>
            <td align="center" id="totalIncome" style="color: #4acaa8">
              <i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在计算...
            </td>
            <td align="center" id="remainIncome" style="color: #4acaa8">
              <i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在计算...
            </td>
          </tr>
          <tr><td><hr></td></tr>
          <tr>
            <td align="center" style="color: grey" colspan="2">
              <a href="<%=basePath%>/income">收益明细</a>&nbsp;｜&nbsp;<a href="<%=basePath%>/cashHistory">提现记录</a>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<script>
  function showChart(xAixs, platWaterYaixs, selfWaterYaixs) {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('chart'));
    var xName = "统计时间：" + getCurDate();
    // 指定图表的配置项和数据
    var option = {
      legend: {
        left: 'center',
        data: ['平台流水', '我的流水']
      },
      xAxis: {
        type: 'category',
        name: xName,
        nameLocation: 'middle',
        nameGap: '30',
        data: xAixs
      },
      yAxis: {
        name: '金额/元',
        type: 'value'
      },
      series: [{
        name: '平台流水',
        type: "line",
        data: platWaterYaixs,
        smooth: true,
        label: {
          normal: {
            show: 'true'
          }
        },
        areaStyle: {normal: {}}
      }, {
        name: '我的流水',
        type: "line",
        data: selfWaterYaixs,
        smooth: true,
        label: {
          normal: {
            show: 'true'
          }
        },
        areaStyle: {normal: {}}
      }]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
  }
  // - 消费流水统计
  function consumpationWaterStatistic(xAixs, playWaterYaixs, selfWaterYaixs) {
    var url = "<%=basePath%>/user/waterStatistic";
    $.post(url, function (data, status) {
      showChart(data.xAixs, data.platWateryAixs, data.selfWateryAixs);
    })
  }
  function getCurDate() {
    var date = new Date();
    var temp = '';
    var month = date.getMonth() + 1;
    temp = date.getFullYear() + "-" + month;
    return temp;
  }
  function loadIncome() {
    var url = '<%=basePath%>/user/incomeStatistic';
    $.post(url, function (data, status) {
      if (data.code == 200) {
        var temp = '<h1 class="text-default-color"><small style="color: gray">￥</small>' + data.data.totalIncome + '</h1>';
        $("#totalIncome").html(temp);
        var temp = '<h1 class="text-default-color"><small style="color: gray">￥</small>' + data.data.remainIncome + '</h1>';
        $("#remainIncome").html(temp);
      } else {
        $("#myIncome").text('加载失败');
      }
    })
  }
  $(function () {
    if (!validateEmail('<%=email%>')) {
      return;
    }
    initStyle();
    // - 流水统计
    consumpationWaterStatistic();
    // - 收益统计
    loadIncome();
    var flag = 0;
    $("#help").click(function () {
      if (flag == 0) {
        flag = 1;
      } else {
        flag = 0;
      }
      if (flag == 1) {
        $(this).text('查看收益计算方式【点击收起】');
      } else {
        $(this).text('查看收益计算方式');
      }
      $("#helpContent").slideToggle();
    })
  })
</script>
</body>
</html>
