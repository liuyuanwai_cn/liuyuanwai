<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>收益流水</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    a:default {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin: 15px 0px;padding: 0px">
    <div id="incomesArea">
      <div style="width: 100%;padding: 100px 0px;background-color: #f4f7ed" class="text-center">
        <p style="color: #d3d3d3;"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......</p>
      </div>
    </div>
    <div id="incomes"></div>
    <div style="display: none;padding: 40px" class="text-center" id="loadMore">
      <p class="load" style="color: darkgrey;">加载更多</p>
    </div>
    <div style="display: none;padding: 40px" id="loadOver" class="text-center">
      <p class="load" style="color: darkgrey;"></p>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
<script language="JavaScript">
  function parseCashStatus(status) {
    var clazz = 'label label-default';
    var temp = '已提现';
    if (status == 0) {
      clazz = 'label label-success';
      temp = '未提现';
    }
    var html = '<span class="' + clazz + '">' + temp + '</span>';
    return html;
  }
  var pageNo = 1;
  function loadIncome(pageNo, pageSize) {
    var url = "<%=basePath%>/user/incomeListGroupByShopId";
    var param = {pageNo: pageNo, pageSize: pageSize};
    $.post(url, param, function (data, status) {
      var incomes = data;
      $("#incomesArea").hide();
      var item;
      if (incomes.length > 0) {
        for (var i = 0; i < incomes.length; i++) {
          item = "";
          item += '<a href="<%=basePath%>/incomeDetail?shopId=' + incomes[i].shopId + '">';
          item += '<table style="width: 100%;margin-top: 5px;margin-bottom: 5px">';
          item += '<tr>';
          item += '<td style="padding: 5px;" colspan="2">' + incomes[i].shopName + '</td>';
          item += '</tr>';
          item += '<tr>';
          item += '<td style="padding: 5px;color: #d3d3d3;border-bottom: 1px solid #f4f7ed">' + incomes[i].addr + '</td>';
          item += '<td style="padding: 5px;border-bottom: 1px solid #f4f7ed;font-size: large;width: 50px" align="center" class="text-default-color" >+' + incomes[i].income + '</td>';
          item += '</tr>';
          item += '</table>';
          item += '</a>';
          $("#incomes").append(item);
        }
        if (incomes.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<div style="padding: 100px;background-color: #f4f7ed"class="text-center">';
          noDataShow += '<p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p><p style="color: #d3d3d3;">没有数据</p>';
          noDataShow += '</div>';
          $("#incomes").empty();
          $("#incomes").append(noDataShow);
        } else if (pageNo > 1) {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  var index = 1;
  $(document).ready(function () {
    if (validateEmail('<%=email%>')) {
      initStyle();
      loadIncome(pageNo++, 10);
      $("#loadMore").click(function () {
        loadIncome(pageNo++, 10);
      })
    }
  })
</script>
</html>
