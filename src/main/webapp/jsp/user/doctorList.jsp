<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object role = request.getParameter("role");
%>
<html>
<head>
  <title>问诊</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-1 col-md-3 col-lg-4"></div>
  <div class="col-xs-12 col-sm-10 col-md-6 col-lg-4" style="background-color: #ffffff;margin: 15px 0px;padding: 0px;">
    <div id="usersArea" style="background-color: #f4f7ed;border: none">
      <table style="width: 100%;">
        <tr>
          <td align="center" style="color: #d3d3d3;padding: 100px 0px;"><i
            class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......
          </td>
        </tr>
      </table>
    </div>
    <div id="users"></div>
    <table style="width: 100%;display: none;" id="loadMore">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px">加载更多</td>
      </tr>
    </table>
    <table style="width: 100%;display: none;" id="loadOver">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
      </tr>
    </table>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
<script language="JavaScript">
  var pageNo = 1;
  function processDoctor(user, isEven) {
    var logo = '';
    if (user.logo == null || user.logo == '' || user.logo == undefined) {
      logo = "<%=basePath%>/img/user.png";
    } else {
      logo = user.logo + "-120x120";
    }
    var temp = '';
    var background = '#ffffff';
    if (isEven) {
      background = "floralwhite";
    }
    var phone = user.phone;
    if(phone == undefined){
      phone = '- -';
    }
    var addr = user.addr;
    if(addr == undefined){
      addr = '- -';
    }
    var name = user.name;
    if(name == undefined || name == ''){
      name = user.email;
    }
    temp += '<table style="width: 100%;background-color: ' + background + '">';
    temp += '<tr>';
    temp += '<td colspan="3" align="center" style="padding: 20px 0px;">';
    temp += '<img src="' + logo + '" class="img-responsive img-circle">';
    temp += '</td>';
    temp += '</tr>';
    temp += '<tr>';
    temp += '<td style="width: 35%;padding: 5px 10px">';
    temp += '<hr>';
    temp += '</td>';
    temp += '<td style="width: 30%" align="center"><span><i class="icon-star" style="color: #CC0099;">&nbsp;&nbsp;</i>' + name + '</td>';
    temp += '<td style="width: 35%;padding: 5px 10px">';
    temp += '<hr>';
    temp += '</td>';
    temp += '</tr>';
    temp += '<tr>';
    temp += '<td colspan="3"style="padding: 0px 15px;">';
    temp += '<blockquote>' + user.remark + '</blockquote>';
    temp += '</td>';
    temp += '</tr>';
    temp += '<tr>';
    temp += '<td colspan="3"style="padding: 10px 15px;">';
    temp += '<table>';
    temp += '<tr>';
    temp += '<td style="color: darkgray;padding: 5px 0px"><i class="icon-phone">&nbsp;' + phone + '</i></td>';
    temp += '<td style="padding: 5px 10px;color: darkgray"><i class="icon-map-marker">&nbsp;' + addr + '</i></td>';
    temp += '</tr>';
    temp += '<tr>';
    temp += '<td style="padding: 5px 0px;padding-top:15px"><a href="<%=basePath%>/user/askDoctor?email=' + user.email + '"style="color: #009900"><i class="icon-leaf">&nbsp;在线咨询</i><a></td>';
    temp += '</tr>';
    temp += '</table>';
    temp += '</td>';
    temp += '</tr>';
    temp += '</table>';
    return temp;
  }
  function loadUser(pageNo, pageSize) {
    var url = "<%=basePath%>/user/list";
    var param = {pageNo: pageNo, pageSize: pageSize, role: '<%=role%>'};
    $.post(url, param, function (data, status) {
      var users = data.paginator.items;
      var user = data.shCompany;
      $("#usersArea").hide();
      var item;
      var subItem;
      var logo;
      var alias;
      var isEven = false;
      if (users != null && users.length > 0) {
        for (var i = 0; i < users.length; i++) {
          if ((i + 1) % 2 == 0) {
            isEven = true;
          } else {
            isEven = false;
          }
          $("#users").append(processDoctor(users[i], isEven));
        }
        if (users.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<div style="padding: 100px 0px;background-color: #f4f7ed" class="text-center">';
          noDataShow += '<p><i class="icon-star icon-4x text-default-color"></i></p><p class="text-default-color">Professor list will be here !</p>';
          noDataShow += '</div>';
          $("#users").empty();
          $("#users").append(noDataShow);
        }
      }
    })
  }
  $(document).ready(function () {
    initStyle();
    loadUser(pageNo++, 10);
    $("#loadMore").click(function () {
      loadUser(pageNo++, 10);
    })
  })
</script>
</html>
