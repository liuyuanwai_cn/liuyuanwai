<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getContextPath();

%>
<!DOCTYPE HTML>
<html>
<head>
    <title>修改密码</title>
    <jsp:include page="../common/common-css.jsp"></jsp:include>
    <jsp:include page="../common/common-js.jsp"></jsp:include>
    <script type="application/javascript"
            src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="application/javascript"
            src="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.zh-CN.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/plugin/datepicker/bootstrap-datetimepicker.min.css">
</head>
<body style="">
<!-- Wrapper -->
<div id="wrapper" style="overflow-y: auto;">
    <div id="main">
        <section>
            <div id="container">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="height: 100%;background-color: #f4f7ed;">

                </div>
            </div>
        </section>
    </div>
</div>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>


<script language="JavaScript">
    $(function () {

    })
</script>
</html>
