<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
  Object role = request.getSession().getAttribute("role");
%>
<html>
<head>
  <title>找小伙伴 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="margin: 15px 0px;padding: 10px;background-color: #ffffff">
    <table style="width: 100%">
      <tr style="border-bottom: 1px solid #d3d3d3">
        <td style="padding: 5px;width: 100%">
          <input type="text" placeholder="姓名/邮箱/手机号" id="queryKey"
                 style="width: 100%;border: none;padding: 5px">
        </td>
        <td align="center" style="padding: 5px;">
          <button class="btn btn-info search" style="padding: 5px 20px">搜&nbsp;&nbsp;索
          </button>
        </td>
      </tr>
    </table>
    <div id="usersArea">
      <table style="width: 100%;margin-top: 20px">
        <tr>
          <td align="center" style="color: #d3d3d3;"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......
          </td>
        </tr>
      </table>
    </div>
    <div id="users"></div>
    <table style="width: 100%;display: none;" id="loadMore">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">加载更多</td>
      </tr>
    </table>
    <table style="width: 100%;display: none;" id="loadOver">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
      </tr>
    </table>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript">
  var pageNo = 1;
  function loadUser(pageNo, pageSize, key) {
    var url = "<%=basePath%>/user/list";
    var myRole = <%=role%>;
    var param = {pageNo: pageNo, pageSize: pageSize, queryKey: key};
    $.post(url, param, function (data, status) {
      var users = data.paginator.items;
      $("#usersArea").hide();
      var item;
      var subItem;
      var logo;
      var alias;
      if (users.length > 0) {
        for (var i = 0; i < users.length; i++) {
          logo = "<%=basePath%>/img/user.png"
          if (users[i].logo != null && users[i].logo != '' && users[i].logo != undefined) {
            logo = users[i].logo + "-120x120";
          }
          alias = users[i].name;
          if (users[i].name == 'Your Name' || users[i].name == '') {
            alias = users[i].email;
          }
          subItem = "";
          item = "";
          item += '<a href="<%=basePath%>/user/queryinfo?email=' + users[i].email + '">';
          item += '<table style="width: 100%;margin-top: 5px;margin-bottom: 5px">';
          item += '<tr>';
          item += '<td align="center" style="width: 30px;">' + (index++) + '</td>';
          item += '<td style="padding: 5px;width: 55px"align="center"><img style="width: 40px;height: 40px" class="img-responsive img-circle" src="' + logo + '"></td>';
          item += '<td style="padding: 5px;border-bottom: 1px solid #f4f7ed;font-size: small">' + alias + '</td>';
          item += '<td style="padding-top: 5px;padding-bottom: 5px;padding-right: 10px;border-bottom: 1px solid #f4f7ed;"class="text-default-color"align="right">' + users[i].role + '</td>';
          item += '</tr>';
          item += '</table>';
          item += '</a>';
          $("#users").append(item);
        }
        if (users.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<table style="width: 100%;margin-top: 20px">';
          noDataShow += '<tr>';
          noDataShow += '<td align="center" style="padding: 100px 0px"><h3 style="color: #cccccc;">All user will be Here</h3></td>';
          noDataShow += '</tr>';
          noDataShow += '</table>';
          $("#users").empty();
          $("#users").append(noDataShow);
          $("#loadOver").hide();
        } else if (pageNo > 1) {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  var index = 1;
  $(document).ready(function () {
    initStyle();
    <%--if (!validateEmail('<%=email%>')) {--%>
    <%--return;--%>
    <%--}--%>
    loadUser(pageNo++, 100, null);
    $("#loadMore").click(function () {
      var key = $.trim($("#queryKey").val());
      loadUser(pageNo++, 100, key);
    })

    $(".search").click(function () {
      var key = $.trim($("#queryKey").val());
      pageNo = 1;
      index = 1;
      $("#users").empty();
      loadUser(pageNo++, 100, key);
    })
  })
</script>
</html>
