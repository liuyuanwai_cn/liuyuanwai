<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>找回密码</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
</head>
<body>
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="col-xs-0 col-sm-3 col-md-4 col-lg-4"></div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="margin-top: 10%">
      <div class="text-center" style="padding: 0px">
        <table style="width: 90%;border-bottom: 1px solid #d3d3d3;margin-left: auto;margin-right: auto">
          <tr><td align="center" colspan="2" style="font-size: x-large;font-weight: lighter;padding-bottom: 40px">Find password</td></tr>
          <tr>
            <td align="center" style="padding: 5px;width: 100%">
              <input type="text" id="email" maxlength="32" style="width: 100%;padding: 8px;border: none"
                     placeholder="example@xx.com">
            </td>
            <td align="right" style="padding: 5px">
              <button id="check" style="padding: 8px;" disabled class="btn btn-disabled-color"
                      type="button" rel="popover">找回密码
              </button>
            </td>
          </tr>
        </table>
      </div>
      <div style="padding: 10px" class="text-center">已有帐号<a href="<%=basePath%>/login">登录</a>或<a
        href="<%=basePath%>/join">加入</a>
      </div>
    </div>
  </div>
</div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
<div class="modal fade bs-example-modal-sm" id="registerSuccessModal" role="dialog" style="margin-top: 100px">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" align="center">
        <b>提示</b>
      </div>
      <div class="modal-body text-center" style="padding: 30px;">
        <p>重置密码已发送至邮箱!</p>
      </div>
      <div class="modal-footer" align="right">
        <button class="btn btn-info" id="IKnow">知道了</button>
      </div>
    </div>
  </div>
</div>
</body>

<script type="application/javascript">


  $(function () {
    initStyle();
    $('#email').bind('input propertychange', function () {
      enable_bt();
      disable_bt();
    });
    $("#IKnow").click(function () {
      $("#registerSuccessModal").modal("hide");
      window.location = "<%=basePath%>/login";
    })
    $("#check").click(function () {
      var email = $.trim($("#email").val());
      if (email == "") {
        alerErr("输入邮箱");
        return;
      }

      var url = "<%=basePath%>/user/findpassword?email=" + email;
      $.post(url, function (data, status) {
        data = JSON.parse(data);
        if (data.code == 200) {
          $("#registerSuccessModal").modal({
            keyboard: false,
            backdrop: 'static'
          });
        } else {
          aler(data);
        }
      })
    })

  });
</script>
</html>
