<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="zh-CN">
<meta name="viewport"
      content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no">
<%
    String basePath = request.getContextPath();
%>

<head>
    <title>登录</title>
    <jsp:include page="../common/common-css.jsp"></jsp:include>
    <jsp:include page="../common/common-js.jsp"></jsp:include>
</head>

<body>
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-xs-0 col-sm-3 col-md-4 col-lg-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="margin-top: 10%;">
            <table style="width: 100%;padding: 0px">
                <tr><td align="center"style="font-size: x-large;font-weight: lighter;padding-bottom: 40px">Login</td></tr>
                <tr style="border-bottom: 1px solid #d3d3d3;">
                    <td style="padding: 5px;">
                        <input type="text" maxlength="32" class=""
                               style="width: 100%;padding: 8px;border: none;" id="email"
                               placeholder="邮箱">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid #d3d3d3;">
                    <td style="padding: 5px;">
                        <input type="password" maxlength="32" class=""
                               style="margin-top: 5px;width: 100%;padding: 8px;border: none;"
                               id="password"
                               placeholder="密码">
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="button" id="login" disabled class="btn btn-disabled-color"
                                style="width: 100%;padding: 15px;border: none;margin-top: 10px">登&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;录
                        </button>
                    </td>
                </tr>
            </table>
            <div style="padding: 10px" class="text-center">
                没有帐号?
                <a href="<%=basePath%>/join">注册</a>
                或
                <a href="<%=basePath%>/findpassword">找回密码</a>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>

<script type="application/javascript">

    function enable_bt() {
        var email = $.trim($("#email").val());
        var password = $.trim($("#password").val());
        $bt = $("#login");
        if (email != "" && password != "") {
            $bt.removeAttr("disabled");
            $bt.removeClass("btn-disabled-color");
            $bt.addClass("btn-info");
        }
    }
    function disable_bt() {
        var email = $.trim($("#email").val());
        var password = $.trim($("#password").val());
        $bt = $("#login");
        if (email == "" || password == "") {
            $bt.attr("disabled", true);
            $bt.removeClass("btn-info");
            $bt.addClass("btn-disabled-color");
        }
    }

    $(function () {

        initStyle();
        $('#password').bind('input propertychange', function () {
            enable_bt();
            disable_bt();
        });
        $('#email').bind('input propertychange', function () {
            enable_bt();
            disable_bt();
        });


        $("#login").click(function () {
            var email = $.trim($("#email").val());
            var password = $.trim($("#password").val());
            if (email == "") {
                alerErr("输入邮箱");
                return;
            }
            if (password == "") {
                alerErr("输入密码");
                return;
            }
            $("#loadModal").modal({
                keyboard: false,
                backdrop: 'static'
            });
            var url = "<%=basePath%>/user/login?email=" + email + "&password=" + password;
            $.post(url, function (data, status) {
                $("#loadModal").modal('hide');
                data = JSON.parse(data);
                if (data.code != 200) {
                    alerErr(data.msg);
                } else {
                    alerOK(data.msg);
                    // - 登录成功将登录信息存入cookie
                    setCookie("email", email);
                    setCookie("login", "1");
                    window.location = "<%=basePath%>/";
                }
            })
        })

    });
</script>
</body>

</html>
