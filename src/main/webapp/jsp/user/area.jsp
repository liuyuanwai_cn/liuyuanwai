<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>地域分布</title>

  <script type="application/javascript" src="<%=basePath%>/plugin/Echarts/echarts.min.js"></script>
  <script type="application/javascript" src="<%=basePath%>/plugin/Echarts/china.js"></script>

  <style type="text/css">
    .panel {
      font-size: 13px
    }
  </style>
</head>
<body style="padding: 0px;margin: 0px">
<div class="container-fluid" style="padding: 0px;margin: 0px;">
  <div class="row-fluid" style="padding: 0px;margin: 0px">
    <div class="col-md-12" style="background-color: white;padding: 20px;height: 100%;">
      <legend style="margin: 5px"><h2>企业用户地域分布图</h2></legend>
      <div class="col-md-9" style="border-right: 1px solid #cccccc;">
        <div id="chinaMap" style="width: 100%;height: 80%"></div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title" id="areaName">-</h3>
          </div>
          <div class="panel-body">
            <ul>
              <li>等待审核:<b style="color: red" id="waidCheck">-</b></li>
              <li>审核通过:<b style="color: red" id="checkPass">-</b></li>
              <li>审核不通过:<b style="color: red" id="checkFail">-</b></li>
              <li>总数量<b style="color: red" id="companyCount">-</b></li>
            </ul>
          </div>
        </div>
        <div class="panel panel-info" id="piePanel" style="">
          <div class="panel-heading">
            <h3 class="panel-title">审核状态占比</h3>
          </div>
          <div class="panel-body" style="padding: 10px;">
            <div id="provincePie" style="height: 300px"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>


<script language="JavaScript">

  // - 动态加载企业审核数据
  function loadProvinceData(name, type, id) {
    $("#areaName").text(name);
    var url = "<%=basePath%>/area/queryStatusCompanyCount?type=" + type + "&id=" + id;
    var waitCount = 0;
    var checkPassCount = 0;
    var checkFailCount = 0;
    var totalCount = 0;
    $.get(url, function (data, status) {
      for (i = 0; i < data.length; i++) {
        if (data[i].name == '等待审核') {
          waitCount = waitCount + data[i].value;
          totalCount = totalCount + waitCount;
        } else if (data[i].name == '审核通过') {
          checkPassCount = checkPassCount + data[i].value;
          totalCount = totalCount + checkPassCount;
        } else if (data[i].name == '审核未通过') {
          checkFailCount = checkFailCount + data[i].value;
          totalCount = totalCount + checkFailCount;
        }
      }
      $("#waidCheck").text(waitCount);
      $("#checkPass").text(checkPassCount);
      $("#checkFail").text(checkFailCount);
      $("#companyCount").text(totalCount);

      // - 加载饼图
      loadProvinceCheckPie(name,data);
    });
  }

  // - 加载中国地图
  function loadAreaMap() {

    // - 得到所有省-ID对应关系
    var url = "<%=basePath%>/area/queryProvince?pid=1";
    $.get(url, function (data, status) {
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.getElementById('chinaMap'));
      // 指定图表的配置项和数据
      var option = {
        tooltip: {
          trigger: 'item'
//          formatter: '{b}'
        },
        visualMap: {
          min: 0,
          max: 100
//          left: 'left',
//          top: 'bottom',
//          text: ['高', '低'],           // 文本，默认为数值文本
//          calculable: true
        },
        series: [
          {
            name: '中国',
            type: 'map',
            mapType: 'china',
            selectedMode: 'single',
            itemStyle: {
              normal: {label: {show: true}},
              emphasis: {label: {show: true}}
            },
            data: data
          }
        ]
      };
      // - 时间
      myChart.on('click', function (obj) {
        // - 加载省级企业用户审核数据
        loadProvinceData(obj.name, "province", obj.value)
      });
      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
    });
  }

  // - 显示省企业数审核数据饼图
  function loadProvinceCheckPie(title,data) {
    var xdata = [];
    for (i = 0; i < data.length; i++) {
      xdata[i] = data[i].name;
    }
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('provincePie'));
    // 指定图表的配置项和数据
    var option = {
      title : {
        text:title,
        x:'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
      },
      legend: {
        x: 'center',
        y: 'bottom',
        data: xdata
      },
      series: [
        {
          name: '审核状态',
          type: 'pie',
          radius: ['30%', '40%'],
          roseType: 'angle',
          itemStyle:{
            normal:{
              label:{
                show: true,
                formatter: '{d}%'
              },
              labelLine :{show:true}
            }
          },
          data: data
        }
      ]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
  }

  $(function () {


    // - 加载中国地图
    loadAreaMap();
    // - 展示所有企业用户数据和对应饼图
    loadProvinceData("中国", "country", 1)


  });

</script>
</html>
