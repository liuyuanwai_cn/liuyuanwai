<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>提现历史</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff;margin: 15px 0px;padding: 0px">
    <div id="cashHistoryArea">
      <div style="width: 100%;padding: 100px 0px;background-color: #f4f7ed" class="text-center">
        <p style="color: #d3d3d3;"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......</p>
      </div>
    </div>
    <div id="items"></div>
    <div style="display: none;padding: 40px" class="text-center" id="loadMore">
      <p class="load" style="color: darkgrey;padding: 40px 0px">加载更多</p>
    </div>
    <div style="display: none;padding: 40px" id="loadOver" class="text-center">
      <p class="load" style="color: darkgrey;padding: 40px 0px"></p>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
<script language="JavaScript">
  // - 解析时间
  function formatDate(strTime) {
    var date = new Date(strTime);
    return date.getFullYear() + "-" + addZero(date.getMonth() + 1) + "-" + addZero(date.getDate()) + " " + addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
  }
  function addZero(val) {
    return val < 10 ? '0' + val : val;
  }
  var pageNo = 1;
  function loadCashHistory(pageNo, pageSize) {
    var url = "<%=basePath%>/cashHistory/list";
    var param = {pageNo: pageNo, pageSize: pageSize};
    $.post(url, param, function (data, status) {
      var items = data.items;
      var user = data.shCompany;
      $("#cashHistoryArea").hide();
      var item;
      if (items.length > 0) {
        for (var i = 0; i < items.length; i++) {
          item = "";
          item += '<table style="width: 100%;margin-top: 5px;margin-bottom: 5px;border-bottom: 1px solid #f4f7ed;">';
          item += '<tr>';
          item += '<td style="padding: 5px;">提现编号：' + items[i].cashCode + '</td>';
          item += '</tr>';
          item += '<tr>';
          item += '<td style="padding: 5px;color: #d3d3d3">' + formatDate(items[i].createTime) + '</td>';
          item += '<td align="center" style="padding: 5px;width: 40px"class="text-default-color">￥' + items[i].cashAmount + '</td>';
          item += '</tr>';
          item += '</table>';
          $("#items").append(item);
        }
        if (items.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<div align="center" style="padding: 100px 0px;background-color: #f4f7ed">';
          noDataShow += '<p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p><p style="color: #d3d3d3;">没有数据</p>';
          noDataShow += '</div>';
          $("#items").empty();
          $("#items").append(noDataShow);
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  $(document).ready(function () {
    if (validateEmail('<%=email%>')) {
      initStyle();
      loadCashHistory(pageNo++, 10);
      $("#loadMore").click(function () {
        loadCashHistory(pageNo++, 10);
      })
    }
  })
</script>
</html>
