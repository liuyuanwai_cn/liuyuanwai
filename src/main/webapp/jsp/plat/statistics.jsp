<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object queryEmail = request.getParameter("email");

%>

<!DOCTYPE HTML>
<html>
<head>
  <title>销量统计</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <script type="application/javascript" src="<%=basePath%>/plugin/Echarts/echarts.min.js"></script>
</head>
<body style="background-color: #f4f7ed;">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-0 col-md-2 col-lg-3"></div>
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6" style="padding: 10px 0px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
      <h4 style="padding-left: 5px;">销售统计</h4>
      <hr style="margin: 15px 0px">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px">
      <div class="panel panel-default"
           style=";border-radius: 0px;-webkit-border-radius: 0px;-moz-border-radius: 0px;border: none">
        <div class="panel-body" style="padding: 10px;border: none">
          <div id="statisticChart" style="margin: 0px" class="text-center">
            <p style="margin-top: 20px;color: darkgrey"><i
              class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在计算...
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<jsp:include page="../common/common-html.jsp"></jsp:include>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
<script>

  function statisticChart(xAixs, yAixs) {
    var myChart = echarts.init(document.getElementById('statisticChart'));
    var xName = '';
    var option = {
      title: {
        text: '商品销量统计',
        subtext: '商品名称'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        type: 'value',
        splitLine: {show: false}
      },
      yAxis: {
        type: 'category',
        data: xAixs,
        splitLine: {show: false}
      },
      series: [{
        type: 'bar',
        name: '销量',
        data: yAixs,
        itemStyle: {
          normal: {
            color: function (value) {
              return getRandomColor()
            },
            label: {show: true, position: 'insideRight'}
          }
        }
      }]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
  }

  var getRandomColor = function () {
    var color = '#' + (function (color) {
        return (color += '0123456789abcdef' [Math.floor(Math.random() * 16)])
        && (color.length == 6) ? color : arguments.callee(color);
      })('');
    return color;
  }
  // - 商品销量统计
  function salesStatistic(queryEmail) {
    var url = '<%=basePath%>/order/statisticSales?email=' + queryEmail;
    $.post(url, function (data, status) {
      var xAixs = data.xAixs;
      var yAixs = data.yAixs;
      if (xAixs == undefined || xAixs.length == 0) {
        $("#statisticChart").html('<div class="text-center"style="padding: 100px 0px;color: #999999">No Goods For ' + queryEmail + '</div>');
      } else {
        var h = xAixs.length * 50;
        h += 'px';
        $("#statisticChart").height(h);
        statisticChart(xAixs, yAixs);
      }
    })
  }
  var queryEmail = '<%=queryEmail%>';
  $(function () {
    initStyle();
    // - 商品销量统计
    salesStatistic(queryEmail);

  })
</script>
</html>
