<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>拍卖</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
    td {
      padding: 5px;
    }

    .auctionList td {
      padding: 0px
    }

    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
    <div class="col-xs-0 col-sm-1 col-md-1 col-lg-2"></div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4" style="margin: 15px 0px;">
      <div class="col-xs-0 col-sm-1 col-md-0 col-lg-0"></div>
      <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12" style="background-color: #ffffff;">
        <table style="width: 100%;">
          <tr>
            <td align="center"><h2>拍卖</h2></td>
          </tr>
          <tr>
            <td style="" align="center">
              <c:choose>
                <c:when test="${advice.img != null && advice.img != ''}">
                  <img src="${advice.img}-750x500" class="img-responsive">
                </c:when>
                <c:otherwise>
                  <hr style="width: 100%;margin: 0px;border:1px solid blueviolet">
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td>${advice.content}</td>
          </tr>
          <tr>
            <td style="padding: 0px">
              <table style="width: 100%">
                <tr>
                  <td align="right" colspan="2">
                    <p style="color: darkgrey">起拍价<span
                      style="color: blueviolet">￥</span><span
                      style="font-size: x-large;color: blueviolet">${advice.fee}</span>
                    </p>
                  </td>
                </tr>
                <c:if test="${advice.isShowMobile == 1}">
                  <tr>
                    <td>
                      <i class="icon-phone" style="color: darkgrey">
                        &nbsp;&nbsp;${advice.phone}</i>
                    </td>
                  </tr>
                </c:if>
                <tr>
                  <td>
                    <i class="icon-map-marker" style="color: darkgrey">
                      &nbsp;&nbsp;${advice.address}</i>
                  </td>
                </tr>
                <tr>
                  <td>
                    <i class="icon-time" style="color: darkgrey">
                      &nbsp;&nbsp;${advice.endTime}&nbsp;结束竞价</i>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 0px">
              <hr style="width: 100%;margin: 0px">
            </td>
          </tr>
        </table>
        <c:if test="${advice.status == 1}">
          <div style="padding: 12px 20px;margin-top: 20px;background-color: #d3d3d3;color: #ffffff">
            <i class="icon-legal"></i>&nbsp;成交金额：￥${advice.dealPrice}
          </div>
        </c:if>
        <c:if test="${advice.status == 2}">
          <div class="text-center" style="padding: 12px 20px;margin-top: 20px;background-color: #d3d3d3;color: #ffffff">
            <i class="icon-exclamation-sign">&nbsp;时间到,无人竞拍</i>
          </div>
        </c:if>
        <c:if test="${advice.status == 0}">
          <div style="padding: 0px;background-color: #ffffff;margin-top: 20px">
            <table style="width: 100%;border-bottom: 1px solid #eaeaea;">
              <tr>
                <td style="padding: 5px">
                  <input type="text" id="price"
                         style="width: 100%;border: none;padding: 8px"
                         onkeyup="this.value=this.value.replace(/\D/g,'')"
                         onafterpaste="this.value=this.value.replace(/\D/g,'')"
                         placeholder="￥输入竞价金额"></td>
                <td style="padding: 3px" align="right">
                  <a href="javascript:auction()" class="btn btn-info">出价&nbsp;<i
                    class="icon-legal"></i></a>
                </td>
              </tr>
            </table>
          </div>
        </c:if>
        <table style="width: 100%;background-color: #ffffff;margin-top: 20px">
          <tr>
            <td align="center"><h3 style="padding: 0px 0px 10px;margin: 0px">竞价榜单</h3></td>
          </tr>
          <tr>
            <td style="padding: 0px">
              <hr style="width: 100%;margin: 0px;">
            </td>
          </tr>
          <tr>
            <td align="center" style="padding: 0px">
              <div id="auctionArea" style="margin-top: 20px;margin-bottom: 100px"
                   class="text-center">
                <table style="width: 100%">
                  <tr>
                    <td align="center"><p style="color: #d3d3d3;"><i
                      class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据...
                    </p>
                    </td>
                  </tr>
                </table>
              </div>
              <div id="auction" class="text-center"></div>
              <table style="width: 100%;display: none;border-top: 1px solid #eaeaea"
                     id="loadMore">
                <tr>
                  <td align="center" class="load" style="color: darkgrey;padding: 10px">
                    加载更多
                  </td>
                </tr>
              </table>
              <table style="width: 100%;display: none;border-top: 1px solid #eaeaea"
                     id="loadOver">
                <tr>
                  <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
                </tr>
              </table>
              <div style="display: none;padding: 100px 0px" id="noAuction"
                   class="text-center">
                <p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p>

                <p style="color: #d3d3d3;">没有数据</p>
              </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4" style="margin: 15px 0px;">
      <div class="col-xs-0 col-sm-1 col-md-0 col-lg-0"></div>
      <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12" style="background-color: #ffffff;">
        <table style="width: 100%;margin-top: 20px">
          <tr style="border-bottom: 1px solid #f4f7ed">
            <td style="width: 100%;padding: 5px">
              <input id="note" type="text" style="width: 100%;padding: 5px;border: none"
                     placeholder="输入留言">
            </td>
            <td align="right" style="padding: 5px">
              <button class="btn btn-info"
                      onclick="javascript:reply('${advice.uuid}');">留言
              </button>
            </td>
          </tr>
        </table>
        <div style="padding: 100px" align="center" class="loadingNotes">
          <i class="icon- icon-cog icon-spin text-default-color"></i>&nbsp;加载留言...
        </div>
        <div style="padding: 100px 0px;display: none" align="center" class="noNotes">
          <p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p>

          <p>没有留言!</p>
        </div>
        <div class="notes" style="padding: 10px"></div>
        <hr style="width: 100%">
        <div class="loadMoreNotes text-center" style="display: none;padding: 40px 0px">加载更多</div>
        <div class="loadNotesOver text-center" style="display: none;padding: 40px 0px"></div>
      </div>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<input type="hidden" id="maxPrice" value="${advice.fee}">
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>


<script language="JavaScript">

  function reply(uuid) {
    $(".btn").blur();
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    var content = $.trim($('#note').val());
    if (content == '') {
      alerErr("输入留言内容");
      return;
    }
    var url = "<%=basePath%>/note/add";
    var param = {content: content, uuid: uuid};
    $.post(url, param, function (data, status) {
      if (status == 'success') {
        $('#note').val('');
        $(".noNotes").hide();
        var temp = parseNote(data.data.email, data.data.logo, data.data.content, data.data.name, data.data.createTime);
        $('.notes').prepend(temp);
      } else {
        aler(data);
      }
    })
  }
  function parseAuction(url, email, name, logo, price, time) {
    if (logo != '' && logo != null && logo != 'null' && logo != undefined) {
      logo += '-120x120';
    }else{
      logo = '<%=basePath%>/img/user.png';
    }
    if(name == '' || name == null || name == undefined){
      name = email;
    }
    var temp = '';
    temp += '<table style="width: 100%;margin: 14px 0px;padding: 0px;"class="auctionList">';
    temp += '<tr>';
    temp += '<td valign="middle"style="width: 50px;height: 50px;padding: 5px"align="center">';
    temp += '<a href="' + url + '">';
    temp += '<img style="" class="img-responsive img-circle" src="' + logo + '">';
    temp += '</a>';
    temp += '</td>';
    temp += '<td style="color: darkgray">';
    temp += '<p style="padding: 0px;margin: 0px">' + name + '</p>';
    temp += '<p style="padding: 0px;margin: 0px">' + time + '</p>';
    temp += '</td>';
    temp += '<td align="right" style="color: blueviolet;padding: 5px 10px">￥<span style="font-size: large;">' + price + '</span></td>';
    temp += '</table>';
    return temp;
  }

  function noAuction() {
    var temp = '';
    temp += '<p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p>';
    temp += '<p style="color: #d3d3d3;">没有数据</p>';
    return temp;
  }

  function parseNote(email, logo, content, name, createTime) {
    if (logo != null && logo != '' && logo != undefined) {
      logo += "-120x120";
    }
    var url = "<%=basePath%>/user/queryinfo?email=" + email;
    var subItem = '';
    subItem += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr>';
    subItem += '<td rowspan="2" style="width: 50px;height: 50px;padding: 5px" align="center" valign="top">';
    subItem += '<a href="' + url + '"><img class="img-responsive img-circle"src="' + logo + '" /></a>';
    subItem += '</td>';
    subItem += '<td style="padding: 5px;">' + content + '</td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style="padding-left: 5px;color: #d3d3d3;padding-bottom: 5px;padding-top: 0px;">' + name + '<br>' + createTime + '</td>';
    subItem += '</tr>';
    subItem += '</table>';
    subItem += '</div>';
    return subItem;
  }
  function auction() {
    $('.btn').blur();
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    var price = $("#price").val();
    if (price == '' || price == null) {
      alerErr('输入竞价金额');
      return;
    }
    var maxPrice = $("#maxPrice").val();
    var uuid = '${advice.uuid}';
    if (Number(price) <= Number(maxPrice)) {
      alerErr('竞价金额不得低于当前最高竞价');
      return;
    }
    var param = {price: price, uuid: uuid, maxPrice: maxPrice};
    var url = '<%=basePath%>/auction/auction';
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        $("#price").val('');
        $("#maxPrice").val(price);
        alerOK("竞价成功");
        $("#noAuction").hide();
        var logo = data.data.logo;
        var url = "<%=basePath%>/user/queryinfo?email=" + data.data.email;
        $("#auction").prepend(parseAuction(url, data.data.email, data.data.name, logo, data.data.price, data.data.time));
      } else {
        aler(data);
      }
    })
  }

  function loadAuction(pageNo, pageSize) {
    var url = '<%=basePath%>/auction/list';
    var param = {uuid: '${advice.uuid}', pageNo: pageNo, pageSize: pageSize};
    $.post(url, param, function (data, status) {
      var len = data.length;
      $("#auctionArea").hide();
      if (len > 0) {
        $("#maxPrice").val(data[0].price);
        for (var i = 0; i < len; i++) {
          var logo = data[i].logo;
          var name = data[i].name;
          var price = data[i].price;
          var email = data[i].email;
          var time = data[i].time;
          var userUrl = "<%=basePath%>/user/queryinfo?email=" + email;
          $("#auction").append(parseAuction(userUrl, email, name, logo, price, time));
        }
        if (len == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          $("#noAuction").show();
        }
      }
    });
  }
  var notePageNo = 1;
  function loadNotes(notePageNo, notePageSize) {
    var uuid = '${advice.uuid}';
    var url = '<%=basePath%>/note/list';
    var param = {uuid: uuid, pageNo: notePageNo, pageSize: notePageSize};
    $.post(url, param, function (data, status) {
      if (data != null && data.length > 0) {
        $(".loadingNotes").hide();
        $(".noNotes").hide();
        var item = '';
        for (var i = 0; i < data.length; i++) {
          item += parseNote(data[i].email, data[i].logo, data[i].content, data[i].name, data[i].createTime);
        }
        $(".notes").prepend(item);
        if (data.length == notePageSize) {
          $(".loadMoreNotes").show();
        } else {
          $(".loadNotesOver").show();
        }
      } else {
        $(".loadingNotes").hide();
        $(".loadMoreNotes").hide();
        if (notePageNo == 1) {
          $(".noNotes").show();
        } else {
          $(".loadNotesOver").show();
        }
      }
    })
  }
  $(".loadMoreNotes").click(function () {
    loadNotes(notePageNo++, 10);
  })
  var pageNo = 1;
  $(function () {
    initStyle();
    loadAuction(pageNo++, 10);
    loadNotes(notePageNo++, 10);
    $("#loadMore").click(function () {
      loadAuction(pageNo++, 10);
    })
  })
</script>
</html>
