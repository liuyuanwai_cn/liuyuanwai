<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
%>

<html>
<head>
  <title>拍卖</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style>
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    a:default {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-0 col-md-1 col-lg-3"></div>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-6" style="padding: 0px;background-color: #ffffff;margin: 15px 0px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 15px 0px">
      <div style="background-color: #f9f9f9;padding: 10px">
        <h3 class="text-default-color">拍卖</h3>
        <small style="color: gray">
          只为懂你的人，让价值最大化
        </small>
        <p style="margin-top: 10px"><a href="<%=basePath%>/advice/advice?type=2"><i
          class="icon-edit text-default-color">
          &nbsp;发布信息</i></a></p>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div id="loadingAdvice" style="margin: 100px;" class="text-center">
        <i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......
      </div>
      <div style="margin: 100px;display: none" id="loadingNoAdvice" class="text-center">
        <p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p>

        <p>没有数据</p>
      </div>
      <div id="advices"></div>
      <div style="display: none;margin: 40px" id="loadMore" class="text-center">加载更多</div>
      <div style="display: none;margin: 40px" id="loadOver" class="text-center"></div>
    </div>
  </div>
</div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>


<script language="JavaScript">
  function noMoreData() {
    // - 没有更多数据
    $("#loadMore").hide();
    $("#loadOver").show();
  }
  function moreData() {
    // - 还有更多数据
    $("#loadOver").hide();
    $("#loadMore").show();
  }
  var pageNo = 1;
  function toAuction(uuid) {
    var url = '<%=basePath%>/advice/toAuction?uuid=' + uuid;
    window.location.href = url;
  }
  function parseAuction(content, uuid, fee, endTime, img,status) {
    if (content.length > 10) {
      content = content.substr(0, 10) + '....';
    }
    var temp = '';
    if(status == 0){
      temp = '<span class="label label-warning">进行中...</span>';
    }else if(status == 1){
      temp = '<span class="label label-success">已完成</span>';
    }else if(status == 2){
      temp = '<span class="label label-default">无人竞价</span>';
    }
    // - 组装拍卖
    var item = '';
    item += '<a href="javascript:toAuction(\'' + uuid + '\')">';
    item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">';
    item += '<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2"style="padding: 0px">';
    item += '<p><img src="' + img + '-360x200" class="img-responsive"/></p>';
    item += '</div>';
    item += '<div class="col-xs-9 col-sm-9 col-md-10 col-lg-10">';
    item += '<p style="margin-bottom: 0px">' + content + '</p>';
    item += '<table style="width: 100%">';
    item += '<tr>';
    item += '<td style="color: darkgrey">';
    item += temp;
    item += '</td>';
    item += '<td align="right" class="text-default-color">￥<span style="font-size:large">' + fee + '</span></td>';
    item += '</tr>';
    item += '</table>';
    item += '</div>';
    item += '<hr style="width: 100% ">';
    item += '</div>';
    item += '</a>';
    return item;
  }
  function loadAdvice(pageNo, pageSize, type, status) {
    var url = "<%=basePath%>/advice/list";
    var param = {pageNo: pageNo, pageSize: pageSize, type: type, status: status};
    $.post(url, param, function (data, status) {
      var advices = data;
      var item;
      if (advices.length == 0) {
        if (pageNo == 1) {
          // - 没有数据
          $("#loadingAdvice").hide();
          $("#loadingNoAdvice").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        $("#loadingAdvice").hide();
        for (var i = 0; i < advices.length; i++) {
          var url = "<%=basePath%>/user/queryinfo?email=" + advices[i].email;
          item = "";
          item += parseAuction(advices[i].content, advices[i].uuid, advices[i].fee, advices[i].endTime, advices[i].img,advices[i].status);
          $("#advices").append(item);
        }
        if (advices.length == pageSize) {
          moreData();
        } else {
          noMoreData();
        }
      }
    })
  }

  $(function () {
    initStyle();
    loadAdvice(pageNo++, 10, ${type}, null);
    $("#loadMore").click(function () {
      loadAdvice(pageNo++, 10, ${type}, null);
    })
  })
</script>
</html>
