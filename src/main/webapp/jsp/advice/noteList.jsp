<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>

<html>
<head>
  <title>龙门阵 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style>
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    a:default {
      text-decoration: none
    }

    .circleBox {
      border-radius: 3px;
      border: none;
    }

    .aliasName {
      position: relative;
    }

    .aliasName:before {
      position: absolute;
      content: "";
      width: 2rem;
      height: 2rem;
      border: none;
      right: -1rem;
      top: 1rem;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      -o-transform: rotate(45deg);
      transform: rotate(45deg);
      background-color: #ffffff;
    }

    pre {
      border: none;
      white-space: pre-wrap;
      word-wrap: break-word;
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 10px 0px;">
  <div class="col-xs-0 col-sm-0 col-md-1 col-lg-2"></div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="padding: 0px;margin: 0px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
      <div class="text-default-color" style="padding-top: 10px">
        <p style="margin-top: 10px;padding: 5px 10px" class="text-center">
          <a href="<%=basePath%>/advice" style="text-decoration: none">
            <i class="icon-edit" style="color: #009999;">&nbsp;摆龙门阵</i>

          </a>
        </p>

        <p class="text-center">&nbsp;『大伙已准备好小板凳~~』</p>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="advices" style="padding: 0px"></div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="loadingNoAdvice" style="display: none">
      <h2 style="color: #cccccc">All note will be Here</h2>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="loadingAdvice" style="padding: 40px 0px;">
      <i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display: none" id="loadMore">
      <a href="javascript:void(0);" class="text-center btn btn-info"
         style="margin: 40px 0px;text-decoration: none">加载更多</a>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3" style="padding: 0px 10px;margin: 0px;">
    <div style="padding: 10px" class="hidden-lg hidden-md">&nbsp;</div>
    <div style="background-color: #ffffff;padding: 10px 10px 100px;">
      <table style="width: 100%">
        <tr>
          <td style="padding: 10px 10px 10px 0px;" colspan="2">阅读最多好文</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid limegreen;width: 50px"></td>
          <td style="border-bottom: 1px solid #f4f7ed"></td>
        </tr>
        <tr>
          <td colspan="2" style="padding: 10px 10px 10px 0px;">
            <p class="text-center storyArea" style="padding: 40px 0px;color: #666666">
              <i class="icon-spinner icon-spin"></i>&nbsp;正在加载数据......
            </p>

            <div class="story">
              <p style="color: #cccccc;display: none" class="NoStory">The latest article 10 will be
                here</p>
            </div>
          </td>
        </tr>
        <tr>
          <td style="padding: 10px 10px 10px 0px;" colspan="2">商品</td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid limegreen;"></td>
          <td style="border-bottom: 1px solid #f4f7ed"></td>
        </tr>
        <tr>
          <td colspan="2" style="padding: 10px 10px 10px 0px;">
            <div id="goods" style="padding: 0px">
              <p class="text-center goodsArea" style="padding: 40px 0px;color: #666666">
                <i class="icon-spinner icon-spin"></i>&nbsp;正在加载数据......
              </p>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" id="imgModal" role="dialog" style="padding: 0px;margin: 0px">
  <div class="modal-dialog modal-lg" style="margin: 0px auto">
    <div class="modal-content"
         style="border: none;box-shadow: none;background-color: transparent;padding: 0px;margin: 0px">
      <table style="width: 100%;height: 100%">
        <tr>
          <td align="center" valign="middle">
            <img src="" class="img-responsive imgShow">
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<input type="hidden" id="toEmail">
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>


<script language="JavaScript">

  var pageNo = 1;
  var pageSize = 20;

  function noMoreData() {
    // - 没有更多数据
    $("#loadMore").hide();
  }
  function moreData() {
    // - 还有更多数据
    $("#loadMore").show();
  }
  // - 日期解析
  function formatDate(strTime) {
    if (strTime == null || strTime == '') {
      return "";
    }
    var date = new Date(strTime);
    return date.getFullYear() + "-" + addZero(date.getMonth() + 1) + "-" + addZero(date.getDate()) + " " + addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
  }
  function addZero(val) {
    return val < 10 ? '0' + val : val;
  }
  function reply(index, uuid, toEmail) {
    $(".btn").blur();
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    var id = '#reply' + index;
    var content = $.trim($(id).val());
    if (content == '') {
      alerErr("输入留言内容");
      return;
    }
    var toEmails = $("#toEmail").val();
    if (toEmails != '') {
      toEmail = toEmails;
    }
    var url = "<%=basePath%>/note/add";
    var param = {content: content, uuid: uuid, toEmail: toEmail, title: ''};
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        $("#toEmail").val('');
        var noteId = '#note' + index;
        $(id).val('');
        var temp = parseNote(data.data.email, data.data.logo, data.data.content, data.data.name, data.data.createTime, index);
        $(noteId).append(temp);
        var clazz = '.replyBox_' + index;
        $(clazz).hide();
        var ID = '#reply' + index;
        $(ID).val('');
      } else {
        aler(data);
      }
    })
  }
  function parseNote(email, logo, content, name, createTime, index) {
    if (logo != null && logo != '' && logo != undefined) {
      logo += "-120x120";
    }
    if (logo == undefined) {
      logo = '<%=basePath%>/img/user.png';
    }
    var url = "<%=basePath%>/user/queryinfo?email=" + email;
    var subItem = '';
    subItem += '<hr style="margin: 5px 0px">';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr><td>';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr>';
    subItem += '<td style="padding-top: 5px;width: 40px;height: 40px;" align="left" valign="top">';
    subItem += '<a href="' + url + '"><img class="img-responsive img-circle" style="width: 30px;height: 30px" src="' + logo + '" /></a>';
    subItem += '</td>';
    subItem += '<td style="padding-top: 10px;padding-bottom: 5px;color: #999999;"valign="middle"><small>' + name + '</small></td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style="padding: 5px"colspan="2">';
    subItem += '<p style="padding: 5px 0px">' + content + '</p>';
    subItem += '<table>';
    subItem += '<tr>';
    subItem += '<td style="color: #999999;">';
    subItem += '<small>' + createTime + '</small>';
    subItem += '</td>';
    subItem += '<td style="padding-left: 10px">';
    subItem += '<a href="javascript:replayIcon(' + index + ',\'' + email + '\',\'' + name + '\',' + true + ')"><i class="icon-comments-alt"></i></a>';
    subItem += '</td>';
    subItem += '</tr>';
    subItem += '</table>';
    subItem += '</td>';
    subItem += '</tr>';
    subItem += '</table>';
    subItem += '</td></tr></table>';
    return subItem;
  }

  function delNote(uuid) {
    var url = '<%=basePath%>/advice/del';
    $.post(url, {uuid: uuid}, function (data, status) {
      if (data.code == 200) {
        var ID = '#content' + uuid;
        $(ID).remove();
      } else {
        aler(data);
      }
    })
  }
  function parseContent(version,url, logo, alias, createtime, content, index, uuid, notes, email, img) {
    var loginEmail = '<%=email%>';
    // - 组装说说
    var item = '';
    item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="content' + uuid + '" style="margin-top: 5px;padding: 0px;">';
    item += '<table style="width: 100%;">';
    item += '<tr>';
    item += '<td class="hidden-xs hidden-sm aliasName" style="width: 70px;height: 70px;padding: 2px 8px;"valign="top">';
    item += '<a href="' + url + '">';
    item += '<img style="width: 40px;height: 40px" class="img-responsive img-circle" src="' + logo + '">';
    item += '</a>';
    item += '</td>';
    item += '<td style="color: darkgrey;">';
    item += '<p style="background-color: #ffffff;padding: 0px;margin: 0px"class=" hidden-xs"></p>'
    item += '<p style="background-color: #ffffff;padding: 10px;" class="circleBox">'
    item += '<table style="width: 100%">';
    item += '<tr>';
    item += '<td>';
    item += '<span>';
    item += '<table>';
    item += '<tr>';
    item += '<td class="visible-xs visible-sm" style="padding-right: 10px">';
    item += '<a href="' + url + '"style="color:#CC9900">';
    item += '<img style="width: 40px;height: 40px;" class="img-responsive img-circle" src="' + logo + '">';
    item += '</a>';
    item += '</td>';
    item += '<td style="padding-left: 5px">';
    item += '<a href="' + url + '"style="color:#CC9900">' + alias + '</a>';
    item += '</td>';
    if (loginEmail != undefined && loginEmail != '' && loginEmail == email) {
      item += '<td>';
      item += '<span style="padding-left: 10px"><a href="javascript:void(0);"onclick="delNote(\'' + uuid + '\')"><small>删除</small></a></span>';
      item += '</td>';
    }
    item += '</tr>';
    item += '</table>';
    item += '</span>';
    item += '</td>';
    item += '</tr>';
    if(version == 0){
      item += '<tr><td style="padding: 10px 5px"class="content">' + HTMLDeCode(content) + '</td></tr>';
    }else if(version == 1){
      item += '<tr><td style="padding: 10px 5px"class="content">' + content + '</td></tr>';
    }
    item += '<tr>';
    item += '<td>';
    item += '<table>';
    item += '<tr>';
    item += '<td align="left" style="padding: 5px;color: #999999;background-color: #ffffff"><small>' + formatDate(createtime) + '</small></td>';
    item += '<td align="right" style="padding: 5px;"><a href="javascript:replayIcon(' + index + ',\'' + alias + '\',\'' + alias + '\',' + false + ')"><i class="icon-comments">&nbsp;评论</i></a></td>';
    item += '</tr>';
    item += '</table>';
    item += '<table style="width: 100%;padding:5px 20px;display:none;margin:10px 0px;"class="replyBox_' + index + '">';
    item += '<tr>';
    item += '<td style="padding: 5px">';
    item += '<table style="width: 100%">';
    item += '<tr>';
    item += '<td style="width: 100%;">';
    item += '<textarea rows="3" id="reply' + index + '" type="text"  maxlength="512" style="resize:none;width: 100%;padding: 5px;border-left: 5px solid #ffffff;border: none;" placeholder="输入内容"/>';
    item += '</td>';
    item += '</tr>';
    item += '<tr>';
    item += "<td align='right'>";
    item += "<a style='padding: 3px;margin-right: 10px;margin-top: 10px' class='btn btn-info' href='javascript:cancelReply(" + index + ");'>&nbsp;取&nbsp;消&nbsp;</a>";
    item += "<a style='padding: 3px;margin-top: 10px' class='btn btn-info' href='javascript:reply(" + index + ",\"" + uuid + "\",\"" + email + "\")'>&nbsp;回&nbsp;复&nbsp;</a>";
    item += "</td>";
    item += '</tr>';
    item += '</table>';
    item += '</td>';
    item += '</tr>';
    item += '</table>';
    if (notes != undefined && notes.length > 0) {
      item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 "style="padding: 0px" id="note' + index + '">';
      for (var j = 0; j < notes.length; j++) {
        var note = notes[j];
        item += parseNote(note.email, note.logo, note.content, note.name, note.createTime, index);
      }
      item += '</div>';
    } else {
      item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding-top: 15px;padding-left:0px" id="note' + index + '"></div>';
    }
    item += '</p>';
    item += '</td>';
    item += '</tr>';
    item += '</table>';
    item == '</div>';
    $('#advices img').addClass('img-responsive');
    return item;
  }

  function cancelReply(index) {
    var clazz = '.replyBox_' + index;
    $(clazz).hide();
    var ID = '#reply' + index;
    $(ID).val('');
  }

  function replayIcon(index, email, name, flag) {
    var clazz = '.replyBox_' + index;
    $(clazz).show();
    var ID = '#reply' + index;
    $(ID).focus();
    if (!flag) {
      $(ID).val('');
      return;
    }
    var alias = name;
    if (name == '' || name == null || name == undefined) {
      alias = email;
    }
    $(ID).val('@' + alias + ' ');
    $("#toEmail").val(email);
  }
  function loadAdvice(pageNo, pageSize, type, status) {
    var index_init = (pageNo - 1) * pageSize;
    var index = 0;
    var url = "<%=basePath%>/advice/list";
    var param = {pageNo: pageNo, pageSize: pageSize, type: type, status: status};
    $.post(url, param, function (data, status) {
      $("#loadingAdvice").hide();
      var advices = data;
      var item;
      var alias;
      var logo = '<%=basePath%>/img/user.png';
      if (advices.length == 0) {
        if (pageNo == 1) {
          // - 没有数据
          $("#loadingNoAdvice").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
        }
      } else {
        $("#advicesArea").hide();
        for (var i = 0; i < advices.length; i++) {
          index = index_init + i;
          logo = '<%=basePath%>/img/user.png';
          var msgType = advices[i].msgType;
          var version = advices[i].dataVersion;
          alias = advices[i].name;
          var dealName = advices[i].dealName;
          var dealTime = advices[i].dealTime;
          var dealPhone = advices[i].dealPhone;
          if (advices[i].name == 'Your Name') {
            alias = advices[i].email;
          }
          if (advices[i].logo != null && advices[i].logo != '' && advices[i].logo != undefined) {
            logo = advices[i].logo + "-120x120";
          }
          var url = "<%=basePath%>/user/queryinfo?email=" + advices[i].email;
          item = "";
          item += parseContent(version,url, logo, alias, advices[i].createtime, advices[i].content, index, advices[i].uuid, advices[i].notes, advices[i].email, advices[i].img);
          $("#advices").append(item);
          $('.content img').addClass('img-responsive');
          $('.content img').css('cursor', 'crosshair');
          $('.content img').click(function () {
            var imgUrl = $(this).attr('src');
            if(imgUrl.indexOf('800x800') > 0 || imgUrl.indexOf('750x500') > 0 || imgUrl.indexOf('580x350') > 0){
              imgUrl = imgUrl.substr(0, imgUrl.length - 8);
            }
            $('.imgShow').attr('src', imgUrl);
            $('#imgModal').modal();
          });
        }
        if (advices.length == pageSize) {
          moreData();
        } else {
          noMoreData();
        }
      }
    })
  }

  function loadStory(pageNo, pageSize) {
    var url = "<%=basePath%>/story/list";
    var param = {pageNo: pageNo, pageSize: pageSize, orderField: "read_count", storyType: 0};
    var item = '';
    var logo = '';
    $.post(url, param, function (data, status) {
      var stories = data;
      $(".storyArea").remove();
      if (stories.length > 0) {
        $(".story").empty();
        for (var i = 0; i < stories.length; i++) {
          logo = '';
          if (stories[i].imgUrl == null || stories[i].imgUrl == '' || stories[i].imgUrl == undefined || stories[i].imgUrl == '<%=basePath%>/img/product.png') {
            logo = '<%=basePath%>/img/product.png';
          } else {
            logo = stories[i].imgUrl + '-360x200';
          }
          item = '';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">';
          item += '<a style="text-decoration: none;" href="<%=basePath%>/story/info?uuid=' + stories[i].uuid + '">';
          item += '<div class=" col-xs-4 col-sm-4 col-md-4 col-lg-3 text-center"style="padding: 0px ;">';
          item += '<img class="img-responsive" src="' + logo + '" alt="' + stories[i].name + '">';
          item += '</div>';
          item += '<div class=" col-xs-8 col-sm-8 col-md-8 col-lg-9">';
          item += '<p>' + stories[i].title + '</p>';
//          item += '<p style="color: #999999;">' + stories[i].subTitle + '</p>';
          item += '<table style="width: 100%">';
          item += '<tr>';
          item += '<td><p><span class="badge" style="background-color: #CC66CC">' + stories[i].tag + '</span></p></td>';
          item += '<td align="right"><p style="color: #999999;" class="text-right"><small>阅读：' + stories[i].readCount + '</small></p></td>';
          item += '</tr>';
          item += '</table>';
          item += '</div>';
          item += '</a>';
          item += '</div>';
          item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px"><hr></div>';
          $(".story").append(item);
        }
      } else {
        $(".NoStory").show();
      }
    })
  }

  function toPage(productId) {
    window.location = "<%=basePath%>/order/preadd?productId=" + productId;
  }

  function buildGood(list) {
    var item = '';
    var temp = '';
    for (var i = 0; i < list.length; i++) {
      var goodUrl = '';
      var url = list[i].url;
      var id = list[i].id;
      var name = list[i].name;
      var deleted = list[i].deleted;
      var sales = list[i].sales;
      var price = list[i].price;
      var unit = list[i].unit;
      var inventory = list[i].inventory;
      if (url == null || url == '' || url == undefined || url == '<%=basePath%>/img/product.png') {
        goodUrl = '<%=basePath%>/img/product.png';
      } else {
        goodUrl = url + '-360x200';
      }
      item += '<a style="text-decoration: none" href="javascript:toPage(' + id + ');">';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 10px 0px;background-color: #ffffff;" >';
      item += '<div class=" col-xs-4 col-sm-4 col-md-4 col-lg-3 text-center"style="margin: 5px 0px;padding: 0px ">';
      item += '<img class="img-responsive" src="' + goodUrl + '" alt="' + name + '">';
      item += '</div>';
      item += '<div class=" col-xs-8 col-sm-8 col-md-8 col-lg-9">';
      item += '<table style="width: 100%">';
      item += '<tr>';
      item += '<td style="padding: 5px;color: #444444">' + name + '</td>';
      item += '<td style="padding: 5px" align="right">';
//      if (inventory == 0) {
//        item += '&nbsp;&nbsp;<span class="label label-default">已卖完</span>';
//      } else {
//        item += '<span class="label label-success">在售中</span>';
//      }
      item += '</td>';
      item += '</tr>';
//      item += '<tr>';
//      item += '<td colspan="2" style="padding: 5px;color: red">￥<span style="font-size: larger">' + price.toFixed(2) + '</span>/' + unit + '</td>';
//      item += '</tr>';
      item += '<tr>';
      item += '<td colspan="2" style="padding: 5px" align="right">';
      item += '<small style="color: #999999;">浏览：<span>' + list[i].readCount + '</span>';
//      item += '&nbsp;&nbsp;|&nbsp;&nbsp;销量：<span>' + list[i].sales + '</span>';
      item += '</small>';
      item += '</td>';
      item += '</tr>';
      item += '</table>';
      item += '</div>';
      item += '</div>';
      item += '</a>';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px"><hr style="margin: 0px"></div>';
      temp += item;
      item = '';
    }
    return temp;
  }

  function HTMLDeCode(str) {
    var s = "";
    if (str.length == 0)    return "";
    s = str.replace(/&lt;/g, "<");
    s = s.replace(/&gt;/g, ">");
    s = s.replace(/&nbsp;/g, "    ");
    s = s.replace(/'/g, "\'");
    s = s.replace(/&quot;/g, "\"");
    s = s.replace(/ <br>/g, "\n");
    return s;
  }

  // - 加载商品列表
  function loadGoods(pageNo, pageSize) {
    var url = "<%=basePath%>/product/list";
    var email = '<%=email%>'
    var param = {pageNo: pageNo, pageSize: pageSize};
    $.post(url, param, function (data, status) {
      var goods = data;
      $(".goodsArea").remove();
      if (goods.length > 0) {
        $("#goods").append(buildGood(goods));
      } else {
        // - 没有数据
        var noDataShow = '';
        noDataShow += '<p style="color: #cccccc;">The latest goods 10 will be here</p>';
        $("#goods").empty();
        $("#goods").append(noDataShow);
      }
    })
  }

  function toProduct(productId) {
    var url = '<%=basePath%>/order/preadd?productId=' + productId;
    window.location.href = url;
  }

  $(function () {
    initStyle();
    loadAdvice(pageNo++, pageSize, 1, null);
    loadStory(0, 10);
    loadGoods(0, 10);
    $("#loadMore").click(function () {
      $("#loadingAdvice").show();
      $("#loadMore").hide();
      loadAdvice(pageNo++, pageSize, 1, null);
    })

    $('.imgShow').click(function () {
      $('#imgModal').modal('hide');
    })

  })
</script>
</html>
