<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>

<html>
<head>
  <title>悬赏</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
  <style>
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    a:default {
      text-decoration: none
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;">
  <div class="col-xs-0 col-sm-0 col-md-1 col-lg-3" style="padding: 0px"></div>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-6" style="padding: 0px;margin: 15px 0px;background-color: #ffffff">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 15px 0px">
      <div style="background-color: #f9f9f9;padding: 10px">
        <h3 class="text-default-color">悬赏</h3>
        <small style="color: gray">
          这，是一个古老的仪式。这，是一个自由的交易
        </small>
        <p style="margin-top: 10px"><a href="<%=basePath%>/advice/advice?type=0"><i
          class="icon-edit text-default-color">
          &nbsp;发布信息</i></a></p>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div id="advicesArea">
        <div id="loadingAdvice" class="text-center" style="padding: 100px 0px">
          <i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载数据......
        </div>
        <div style="margin: 100px;display: none" id="loadingNoAdvice" class="text-center">
          <p><i class="icon-exclamation-sign icon-4x text-default-color"></i></p>

          <p>没有数据</p>
        </div>
      </div>
      <div id="advices"></div>
      <div id="loadMore" class="text-center" style="margin: 40px;display: none">加载更多</div>
      <div style="display: none;margin: 40px;" id="loadOver" class="text-center"></div>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<div class="modal fade bs-example-modal-sm" id="rewardModal" role="dialog" style="margin-top: 100px">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" align="center">
        <b><i class=" icon-bullhorn text-default-color"></i>&nbsp;重要提示</b>
      </div>
      <div class="modal-body" style="padding: 30px;">
        <p>领赏，代表你同意悬赏人提出的诉求。为了保证事件的严肃性，领赏后你的姓名，联系方式将公开并邮件发送至悬赏人!</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-disabled-color" style="color: #ffffff" id="Cancel">取消</button>
        <button class="btn btn-info" id="IKnow">确定领赏</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="rewardIndex">
<input type="hidden" id="rewardUuid">
<input type="hidden" id="rewardEmail">
</body>


<script language="JavaScript">
  function noMoreData() {
    // - 没有更多数据
    $("#loadMore").hide();
    $("#loadOver").show();
  }
  function moreData() {
    // - 还有更多数据
    $("#loadOver").hide();
    $("#loadMore").show();
  }
  // - 日期解析
  function formatDate(strTime) {
    if (strTime == null || strTime == '') {
      return "";
    }
    var date = new Date(strTime);
    return date.getFullYear() + "-" + addZero(date.getMonth() + 1) + "-" + addZero(date.getDate()) + " " + addZero(date.getHours()) + ":" + addZero(date.getMinutes()) + ":" + addZero(date.getSeconds());
  }
  function addZero(val) {
    return val < 10 ? '0' + val : val;
  }
  function reply(index, uuid) {
    $(".btn").blur();
    var email = '<%=email%>';
    if (email == 'null') {
      $("#loginModal").modal();
      return;
    }
    var id = '#reply' + index;
    var content = $.trim($(id).val());
    if (content == '') {
      alerErr("输入留言内容");
      return;
    }
    var url = "<%=basePath%>/note/add";
    var param = {content: content, uuid: uuid};
    $.post(url, param, function (data, status) {
      var clazz = '.replybox_' + index;
      $(clazz).hide();
      if (data.code == 200) {
        var noteId = '#note' + index;
        $(id).val('');
        var temp = parseNote(data.data.email, data.data.logo, data.data.content, data.data.name, data.data.createTime, index);
        $(noteId).prepend(temp);
      } else {
        aler(data);
      }
    })
  }
  var pageNo = 1;
  function parseNote(email, logo, content, name, createTime, index) {
    if (logo != null && logo != '' && logo != undefined) {
      logo += "-120x120";
    }
    var url = "<%=basePath%>/user/queryinfo?email=" + email;
    var subItem = '';
    subItem += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="note' + index + '" style="background-color: #f4f7ed;padding: 0px;margin:0px">';
    subItem += '<table style="width: 100%;">';
    subItem += '<tr>';
    subItem += '<td rowspan="2" style="padding-top: 10px;padding-bottom: 5px;width: 50px;height: 50px" align="center" valign="top">';
    subItem += '<a href="' + url + '"><img class="img-responsive img-circle" style="width: 30px;height: 30px" src="' + logo + '" /></a>';
    subItem += '</td>';
    subItem += '<td style="padding-top: 10px;padding-bottom: 5px">' + content + '</td>';
    subItem += '</tr>';
    subItem += '<tr>';
    subItem += '<td style=";color: #d3d3d3;padding-bottom: 5px;padding-top: 0px;">';
    subItem += '<table style="width: 100%">';
    subItem += '<tr><td style="color: darkgray"><span>' + name + '</span><br/><span>' + createTime + '</span></td>';
    subItem += '<td align="center"style="padding-right: 5px"><a href="javascript:replyIcon(' + index + ',\'' + name + '\',\'' + email + '\',' + true + ')"><i class="icon-comments-alt"></i></a></td></tr>';
    subItem += '</table>';
    subItem += '</td>';
    subItem += '</tr>';
    subItem += '</table>';
    subItem += '</div>';
    return subItem;
  }
  $("#IKnow").click(function () {
    $("#rewardModal").modal("hide");
    var index = $("#rewardIndex").val();
    var uuid = $("#rewardUuid").val();
    var email = $("#rewardEmail").val();
    var url = '<%=basePath%>/advice/reward';
    var param = {uuid: uuid, email: email};
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        var rewardId = '#reward' + index + '';
        $(rewardId).empty();
        $(rewardId).css('background-color', '#d3d3d3');
        $(rewardId).removeClass("text-center");
        var temp = '';
        temp += '<p><i class="icon-user"></i>&nbsp;揭榜人：' + data.data.name + '</p>';
        temp += '<p><i class="icon-phone"></i>&nbsp;联系电话：' + data.data.phone + '</p>';
        temp += '<p><i class="icon-time"></i>&nbsp;领赏时间：' + data.data.time + '</p>';
        $(rewardId).append(temp);
      } else {
        aler(data.data);
      }
    })
  })
  $("#Cancel").click(function () {
    $("#rewardModal").modal("hide");
  })
  function reward(uuid, index, email) {
    var email = '<%=email%>';
    if (email == 'null') {
      $("#loginModal").modal();
      return;
    }
    // - 领赏
    $("#rewardModal").modal({
      keyboard: false
    });
    $("#rewardIndex").val(index);
    $("#rewardUuid").val(uuid);
    $("#rewardEmail").val(email);
  }
  function parseReward(advice, email, url, logo, alias, createtime, content, index, uuid, fee, isShowMobile, mobile, dealName, dealPhone, dealTime, img, address) {
    // - 组装悬赏
    var item = '';
    item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="margin: 0px;padding: 0px;padding-bottom: 20px">';
    item += '<table style="width: 100%;">';
    item += '<tr>';
    item += '<td style="width: 50px;height: 50px;padding: 5px">';
    item += '<a href="' + url + '">';
    item += '<img style="width: 40px;height: 40px" class="img-responsive img-circle" src="' + logo + '">';
    item += '</a>';
    item += '</td>';
    item += '<td style="padding: 5px;color: darkgrey;">';
    item += '<p style="margin: 0px">' + alias + '</p>';
    item += '</td>';
    item += '</tr>';
    item += '</table>';
    item += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"style="margin-bottom: 10px;padding: 0px">';
    item += '<div style="border: 1px solid blueviolet;">';
    item += '<div class="text-center" style="padding: 0px 15px"><h2>悬赏</h2><hr></div>';
    if (img != null && img != '' && img != undefined) {
      item += '<p style="padding: 0px 15px"><table style="width: 100%"><tr><td align="center"><img src="' + img + '-360x200" class="img-responsive"/></td></tr></table></p>';
    } else {
      item += '<p style="padding: 0px 15px"><hr style="border: 1px solid blueviolet;padding: 0px;margin: 0px"/></p>';
    }
    item += '<p style="padding:0px 20px;">' + content + '</p>';
    item += '<table style="width: 100%;">';
    item += '<tr>';
    item += '<td align="right" style="padding-right: 15px;color: blueviolet"><h2 style="margin-top: 0px"><small style="color: darkgrey">￥</small>' + fee + '</h2></td>';
    item += '</tr>';
    item += '<tr>';
    item += '<td valign="bottom" style="color: darkgrey;padding: 5px 5px 5px 10px;">';
    if (isShowMobile == 1) {
      item += '<i class="icon-phone">&nbsp;' + mobile + '</i>';
    } else {
      item += '--';
    }
    item + '</td>';
    item += '</tr>';
    item += '<tr>';
    item += '<td valign="bottom" style="color: darkgrey;padding: 5px 5px 5px 10px">';
    item += '<i class="icon-map-marker">&nbsp;&nbsp;</i>' + address;
    item + '</td>';
    item += '</tr>';
    item += '</table>';
    if (dealName != undefined && dealName != '' && dealName != null) {
      item += '<div style="width: 100%;color: #ffffff;padding: 10px;background-color:#d3d3d3">' +
      '<p><i class="icon-user"></i>&nbsp;揭榜人：' + dealName + '</p>' +
      '<p><i class="icon-phone"></i>&nbsp;电话：' + dealPhone + '</p>' +
      '<p><i class="icon-time"></i>&nbsp;时间：' + dealTime + '</p>' +
      '</div>';
    } else {
      item += '<a href="javascript:reward(\'' + uuid + '\',' + index + ',\'' + email + '\')">';
      item += '<p id="reward' + index + '" class="text-center" style="width: 100%;color: #ffffff;padding: 12px;background-color: blueviolet;margin:0px">领赏</p>';
      item += '</a>';
    }
    item += '</div>';
    item += '</div>';
    item += '<table style="width: 100%;background-color: #f4f7ed;"><tr><td style="padding: 10px 10px;color: darkgray;border-bottom: 1px solid #e8e8e8"><i class="icon-time">&nbsp;</i>' + formatDate(createtime) + '</td><td align="right"style="padding: 10px 10px;border-bottom: 1px solid #e8e8e8"><a href="javascript:replyIcon(' + index + ',\'' + alias + '\',\'' + alias + '\',' + false + ')"><i class="icon-comments">&nbsp;留言</i></a></td></tr></table>';
    item += '<table style="width: 100%;border-bottom: 1px solid #f4f7ed;margin: 5px 0px;display:none;" class="replybox_' + index + '">';
    item += '<tr>';
    item += '<td style="width: 100%">';
    item += '<input id="reply' + index + '" type="text" style="padding:5px;width: 100%;border-left: 5px solid #ffffff;border: none;" placeholder="输入留言"/>';
    item += '</td>';
    item += "<td style='padding: 0px'><a class='btn' href='javascript:reply(" + index + ",\"" + uuid + "\")'><i class='icon-reply'></i></a></td>";
    item += '</tr>';
    item += '</table>';
    var notes = advice.notes;
    if (notes != undefined && notes.length > 0) {
      for (var j = 0; j < notes.length; j++) {
        var note = notes[j];
        var replyId = '#reply' + index;
        item += parseNote(note.email, note.logo, note.content, note.name, note.createTime, index);
      }
    }else{
      item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="note' + index + '" style="padding: 0px;margin:0px"></div>';
    }
    item += '</div>';
    return item;
  }

  function replyIcon(index, name, email, flag) {
    var clazz = '.replybox_' + index;
    $(clazz).show();
    var ID = '#reply' + index;
    $(ID).focus();
    if (!flag) {
      $(ID).val('');
      return;
    }
    var alias = name;
    if (alias == '' || alias == null || alias == undefined) {
      alias = email;
    }
    $(ID).val("@" + alias + " ");
  }
  function loadAdvice(pageNo, pageSize, type, status) {
    var index_init = (pageNo - 1) * pageSize;
    var index = 0;
    var reply_index = 0;
    var url = "<%=basePath%>/advice/list";
    var param = {pageNo: pageNo, pageSize: pageSize, type: type, status: status};
    $.post(url, param, function (data, status) {
      var advices = data;
      var item;
      var alias;
      var logo = '<%=basePath%>/img/user.png';
      if (advices.length == 0) {
        if (pageNo == 1) {
          // - 没有数据
          $("#loadingAdvice").hide();
          $("#loadingNoAdvice").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        $("#advicesArea").hide();
        for (var i = 0; i < advices.length; i++) {
          index = index_init + i;
          logo = '<%=basePath%>/img/user.png';
          var msgType = advices[i].msgType;
          alias = advices[i].name;
          var dealName = advices[i].dealName;
          var dealTime = advices[i].dealTime;
          var dealPhone = advices[i].dealPhone;
          if (advices[i].name == 'Your Name') {
            alias = advices[i].email;
          }
          if (advices[i].logo != null && advices[i].logo != '' && advices[i].logo != undefined) {
            logo = advices[i].logo + "-120x120";
          }
          var url = "<%=basePath%>/user/queryinfo?email=" + advices[i].email;
          item = "";
          // - 悬赏
          item += parseReward(advices[i], advices[i].email, url, logo, alias, advices[i].createtime, advices[i].content, index, advices[i].uuid, advices[i].fee, advices[i].isShowMobile, advices[i].phone, dealName, dealPhone, dealTime, advices[i].img, advices[i].address);
          $("#advices").append(item);
        }
        if (advices.length == pageSize) {
          moreData();
        } else {
          noMoreData();
        }
      }
    })
  }

  $(function () {
    initStyle();
    loadAdvice(pageNo++, 10, ${type}, null);
    $("#loadMore").click(function () {
      loadAdvice(pageNo++, 10, ${type}, null);
    })
  })
</script>
</html>
