<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<!DOCTYPE HTML>
<html>
<head>
  <title>摆龙门</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/simditor.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/module.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/hotkeys.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/uploader.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/simditor.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/extension/simditor-mark.js"></script>


</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-0 col-md-0 col-lg-0"></div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: #ffffff;margin: 10px 0px;padding: 5px">

    <div class="col-xs-12 col-sm-12 col-md-12 cl-lg-12" style="padding: 0px">
      <textarea rows="10" id="content" class="textarea"
                placeholder="在这儿输入内容..."
                style="resize: none;width: 100%;border: none"></textarea>
      <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath" style="display: none">
      <hr style="margin: 0px">
      <button id="save" class="btn btn-info" style="margin: 15px">&nbsp;发&nbsp;布&nbsp;</button>
    </div>
  </div>
</div>
<input type="hidden" id="msgType" value="1">
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript">

  function toInsertImg() {
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    $("#filePath").click();
  }


  // - 上传图片
  function uploadFile(obj) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/img/upload";
    $.ajaxFileUpload({
      url: url,
      fileElementId: "filePath",//file标签的id,
      dateType: "text",
      success: function (data, status) {
        // - ajaxFileUpload这儿必须得转换以下
        data = jQuery.parseJSON(jQuery(data).text());
        if (data.code == 200) {
          var temp = data.data;
          insertImg(temp.downloadUrl + '-800x800');
        } else {
          alerErr(data.msg);
        }
        $("#loadModal").modal('hide');
      },
      error: function (data, status, e) {
        $("#loadModal").modal('hide');
        $("#filePath").replaceWith($("#upload").clone(true));
        alerErr(e);
      }
    });
  }
  $(function () {
    initStyle();
    // - 反馈
    $("#save").click(function () {
      var email = '<%=email%>';
      if (!validateEmail(email)) {
        return;
      }
      var content = $.trim($("#content").val());
      var fee = $("#fee").val();
      var endTime = $.trim($("#endtime").val());
      var msgType = $.trim($("#msgType").val());
      var isShowMobile = $("#isShowMobile").val();
      var img = $("#img").val();
      var address = $.trim($('#address').val());
      if (content == "") {
        alerErr("输入内容");
        return;
      }
      var param = {
        simContent: content,
        msgType: msgType,
        fee: fee,
        isShowMobile: isShowMobile,
        endTime: endTime,
        img: img,
        address: address
      };
      var url = "<%=basePath%>/advice/add";
      $("#loadModal").modal({
        keyboard: false,
        backdrop: 'static'
      });
      $.post(url, param, function (data, status) {
        $("#loadModal").modal('hide');
        if (data.code == 200) {
          $("#content").val('');
          $("#fee").val('');
          $("#endtime").val('');
          $("#msgType").val('');
          $("#address").val('');
          $("#isShowMobile").val(1);
          var backUrl = '';
          backUrl = '<%=basePath%>/note'
          window.location = backUrl;
        } else {
          aler(data);
        }
      })
    })

    $('#content .img').addClass('img-responsive');

    $(function () {
      var editor = new Simditor({
        textarea: $('#content'),
        toolbar: [
          'title',
          'bold',
          'italic',
          'underline',
          'strikethrough',
          'fontScale',
          'color',
          'ol',
          'ul',
          'blockquote',
          'code',
          'link',
          'image',
          'hr',
          'indent',
          'outdent',
          'alignment',
          'mark'
        ],
        upload: {
          url: "<%=basePath%>/img/uploadForSimditor",
          params: {
            fileElementId: "filePath",
            dateType: "text"
          },
          fileKey: "filePath",
          leaveConfirm: '正在上传...'
        },
        pasteImage: true
      });
      editor.focus();
      editor.on('valuechanged', function (e, src) {
        $('img').addClass('img-responsive');
      })
    })
  })
</script>
</html>
