<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
  <title>劉員外</title>
  <jsp:include page="common/common-css.jsp"></jsp:include>
  <jsp:include page="common/common-js.jsp"></jsp:include>

  <script src="<%=basePath%>/plugin/imgshow/jquery.glide.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/imgshow/style.css">

  <style>
    * {
      margin: 0;
      padding: 0;
      border: 0;
    }

    .box {
      width: 100%;
      line-height: 5;
      text-align: center;
      font-size: 5em;
      color: #fff;
      text-transform: capitalize;
      vertical-align: middle;
      padding: 10px;
      background-image: url("../img/bg3.jpg");
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="slider" style="">
    <ul class="slides">
      <li class="slide">
        <div class="box">
          1
        </div>
      </li>
      <li class="slide">
        <div class="box">
          2
        </div>
      </li>
    </ul>
  </div>
</div>
<jsp:include page="../foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
  $(function () {
    initStyle();
  })
  var glide = $('.slider').glide({
    autoplay: false,//是否自动播放 默认值 true如果不需要就设置此值
    animationTime: 500, //动画过度效果，只有当浏览器支持CSS3的时候生效
    arrows: false, //是否显示左右导航器
    arrowRightText: ">",//定义左右导航器文字或者符号也可以是类
    arrowLeftText: "<",
    nav: true, //主导航器也就是本例中显示的小方块
    navCenter: true, //主导航器位置是否居中
    navClass: "slider-nav",//主导航器外部div类名
    navItemClass: "slider-nav__item", //本例中小方块的样式
    navCurrentItemClass: "slider-nav__item--current" //被选中后的样式
  });
</script>
<html>
