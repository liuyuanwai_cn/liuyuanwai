<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
  <title>Loading</title>

  <jsp:include page="common/common-css.jsp"></jsp:include>
</head>

<body style="background-color: #CC9933;height: 100%">
<div class="loadingBox" style="height: 100%;position: absolute;width: 100%">
  <table style="width: 100%;height: 100%"border="1">
    <tr>
      <td align="center">
        <img src="<%=basePath%>/img/loading.gif" class="img-responsive">
      </td>
    </tr>
  </table>
</div>
</body>


</html>
