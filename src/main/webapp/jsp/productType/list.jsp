<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>商品分类</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
  </style>
</head>
<script>
  function toPage(uuid) {
    window.location = "<%=basePath%>/story/info?uuid=" + uuid;
  }
  // - 加载商品类别列表
  function loadProductTypes(pageNo, pageSize) {
    var url = "<%=basePath%>/productType/list";
    var param = {pageNo: pageNo, pageSize: pageSize};
    var item = '';
    $.post(url, param, function (data, status) {
      var productTypes = data;
      $("#productTypesArea").hide();
      if (productTypes.length > 0) {
        for (var i = 0; i < productTypes.length; i++) {
          item = '';
          item += '<table style="width: 100%">';
          item += '<tr style="border-bottom: 1px solid #eaeaea">';
          item += '<td style="padding: 10px;">';
          item += '<a href="<%=basePath%>/productType/info?id=' + productTypes[i].id + '"style="text-decoration: none"><p style="margin: 0px">' + productTypes[i].name + '</p></a>';
          item += '</td>';
          item += '<td style="width: 10px;padding: 10px;" align="center"><i class="icon-angle-right"></i></td>';
          item += '</tr>';
          item += '</table>';
          $("#productTypes").append(item);
        }
        if (productTypes.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        // - 没有数据
        if (pageNo == 1) {
          var noDataShow = '';
          noDataShow += '<table style="width: 100%;background-color: #f9f9f9">';
          noDataShow += '<tr>';
          noDataShow += '<td align="center"style="padding: 100px 0px"><h3 style="color: #cccccc">All Product Type will be here</h3></td>';
          noDataShow += '</tr>';
          noDataShow += '</table>';
          $("#productTypes").empty();
          $("#productTypes").append(noDataShow);
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  var pageNo = 1;
  var pageSize = 10;
  $(function () {
    initStyle();
    if (!validateEmail('<%=email%>')) {
      return;
    }
    $(".load").mousemove(function () {
      $(this).css("cursor", "pointer");
    })
    $(".load").mouseout(function () {
      $(this).css("cursor", "default");
    })
    loadProductTypes(pageNo++, pageSize);
    $("#loadMore").click(function () {
      loadProductTypes(pageNo++, pageSize);
    })
  })
</script>
<body style="padding: 0px;margin: 0px;background-color: #f9f9f9">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;margin-top: 15px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;margin-top: 15px">
    <p style="background-color: #ffffff;padding: 10px;border-bottom: 1px solid #e8e8e8;border-top: 1px solid #e8e8e8">
    <table>
      <tr>
        <td style="font-weight: bold">商品分类</td>
        <td style="padding-left: 15px"><a href="<%=basePath%>/productType/info">新增</a></td>
      </tr>
    </table>
    </p>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;">
      <div id="productTypesArea">
        <table style="width: 100%">
          <tr>
            <td align="center" style="color: #d3d3d3;"><i
              class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载......
            </td>
          </tr>
        </table>
      </div>
      <div id="productTypes" style="background-color: #ffffff;"></div>
      <table style="width: 100%;display: none;" id="loadMore">
        <tr>
          <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">加载更多</td>
        </tr>
      </table>
      <table style="width: 100%;display: none;" id="loadOver">
        <tr>
          <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
        </tr>
      </table>
    </div>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
</html>
