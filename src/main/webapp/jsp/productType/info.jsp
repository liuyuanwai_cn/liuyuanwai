<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
  Object role = request.getSession().getAttribute("role");
  String id = request.getParameter("id");
%>
<!DOCTYPE HTML>
<html>
<head>
  <title>商品分类</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
    td {
      padding: 9px
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin: 10px 0px;">
    <table style="width: 100%;margin-top: 10px">
      <tr style="border-bottom: 1px solid #e8e8e8;">
        <td class="preDesc">
          <input type="text" style="width: 100%;border: none;padding: 5px" id="name"
                 placeholder="名称"
                 maxlength="16"
                 value="">
        </td>
      </tr>
      <tr style="border-bottom: 1px solid #e8e8e8;">
        <td style="padding-top: 10px;padding-bottom: 10px;">
            <textarea style="width: 100%;resize: none;;border: none;color: darkgray;padding: 5px" id="remark"
                      maxlength="512" rows="5" placeholder="描述"></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-info" id="saveOrUpdate">保&nbsp;&nbsp;存</button>
            <button type="button" class="btn btn-default" id="delProductType">删&nbsp;&nbsp;除</button>
            <button type="button" class="btn btn-default" onclick="history.go(-1);">取&nbsp;&nbsp;消</button>
          </div>
        </td>
      </tr>
    </table>
  </div>
</div>
</div>
<input type="hidden" id="id" value="">
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript">


  function saveOrUpdate() {
    $(".btn").blur();
    var url = "<%=basePath%>/productType/saveOrUpdate";
    // - 基础信息
    var email = '<%=email%>';
    var name = $.trim($("#name").val());
    var remark = $.trim($("#remark").val());
    if (name == null || name == '') {
      alerErr("输入名称");
      return;
    }
    var id = $("#id").val();
    var param = {
      id: id,
      name: name,
      remark: remark
    };
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        var backUrl = '<%=basePath%>/productType';
        window.location.href = '<%=basePath%>/success?backUrl=' + backUrl;
      } else {
        alerErr(data.msg);
      }
    })
  }
  function delProductType() {
    $(".btn").blur();
    var url = "<%=basePath%>/productType/del";
    var email = '<%=email%>';
    var id = $("#id").val();
    var param = {
      id: id,
      email: email
    };
    $.post(url, param, function (data, status) {
      if (data.code == 200) {
        var backUrl = '<%=basePath%>/productType';
        window.location.href = '<%=basePath%>/success?backUrl=' + backUrl;
      } else {
        alerErr(data.msg);
      }
    })
  }

  function loadProductType() {
    var id = '<%=id%>';
    if (id == null || id == '') {
      return;
    }
    var url = '<%=basePath%>/productType/queryById?id=' + id;
    $.post(url, function (data, status) {
      $("#id").val(data.id);
      $("#name").val(data.name);
      $("#remark").val(data.remark);
      var email = '<%=email%>';
      var role = '<%=role%>';
      if(email != data.email){
        $(".btn-group").hide();
      }
      if(role == '1' || role == '2'){
        $(".btn-group").show();
      }
    })
  }

  $(function () {
    initStyle();
    if (!validateEmail('<%=email%>')) {
      return;
    }
    loadProductType();

    $("#saveOrUpdate").click(function () {
      saveOrUpdate();
    })
    $("#delProductType").click(function () {
      delProductType();
    })
  })
</script>
</html>
