<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<!DOCTYPE HTML>
<html>
<head>
  <title>个人资料 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
    .preDesc {
      color: darkgray;
    }

    td {
      padding: 10px
    }
  </style>
</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-0 col-sm-0 col-md-3 col-lg-4"></div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" style="background-color: #ffffff;margin: 15px 0px;padding: 0px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="center" style="margin: 20px 0px;padding: 0px">
      <c:choose>
        <c:when test='${not empty userExtDto.logo}'>
          <img src="${userExtDto.logo}-120x120" alt="用户头像" style="width: 100px;height: 100px"
               class="img-responsive img-circle logo">
        </c:when>
        <c:otherwise>
          <img style="width: 100px;height: 100px" src="<%=basePath%>/img/user.png" alt="用户头像"
               class="img-responsive img-circle logo">
        </c:otherwise>
      </c:choose>
      <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath" style="display: none">
      <hr class="hidden-md hidden-lg">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <hr style="width: 100%;margin: 0px">
      <table style="width: 100%;">
        <tr style="border-bottom: 1px solid #ebebeb;">
          <td class="preDesc" style="color: darkgray">
            <input value="${userExtDto.email}" readonly style="width: 100%;border:none">
          </td>
        </tr>
        <tr style="border-bottom: 1px solid #ebebeb;">
          <td class="preDesc">
            <input type="text" style="width: 100%;border: none" id="name"
                   placeholder="姓名"
                   maxlength="16"
                   value="${userExtDto.name}">
          </td>
        </tr>
        <tr style="border-bottom: 1px solid #ebebeb;">
          <td class="preDesc">
            <input type="text" onkeyup="this.value=this.value.replace(/\D/g,'')"
                   onafterpaste="this.value=this.value.replace(/\D/g,'')"
                   style="width: 100%;border: none"
                   id="phone"
                   placeholder="手机号" maxlength="11"
                   value="${userExtDto.phone}">
          </td>
        </tr>
        <tr style="border-bottom: 1px solid #ebebeb;">
          <td class="preDesc">
            <textarea rows="3" style="width: 100%;border: none;resize: none" id="addr"
                      placeholder="联系地址" maxlength="100">${userExtDto.addr}</textarea>
          </td>
        </tr>
        <tr style="border-bottom: 1px solid #ebebeb;">
          <td style="padding-top: 10px;padding-bottom: 10px;" colspan="2">
            <textarea style="width: 100%;resize: none;border: none;color: darkgray" id="remark" placeholder="简介"
                      rows="3">${userExtDto.remark}</textarea>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="right">
            <button id="update" class="btn btn-info" type="button">保存修改</button>
          </td>
        </tr>
      </table>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-bottom: 0px">
      <div class="form-group">
        <table style="width: 100%;border-top: 1px solid #ebebeb;">
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td>
              <input type="password" maxlength="32"
                     style="width: 100%;border: none" id="oldPassword"
                     placeholder="旧密码">
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td>
              <input type="password" maxlength="32"
                     style="width: 100%;border: none" id="password" placeholder="新密码">
            </td>
          </tr>
          <tr style="border: none">
            <td colspan="2" align="right" style="border: none">
              <button id="updatePassword" class="btn btn-info" type="button">保存修改</button>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="form-group">
        <table style="width: 100%;border-top: 1px solid #ebebeb;border-bottom: 1px solid #ebebeb;">
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td style="padding-top: 10px;padding-bottom: 10px;width: 80px">邀请码</td>
            <td class="preDesc" align="right" style="color: darkgray;">${userExtDto.code}</td>
          </tr>
          <tr>
            <td style="border-bottom: 1px solid #ebebeb;border-top: 1px solid #ebebeb;" colspan="2">
              收款码
            </td>
          </tr>
        </table>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;margin-top: 20px;margin-bottom: 20px">
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding: 0px" align="center">
        <c:choose>
          <c:when test="${userExtDto.weixinQrcode == null}">
            <img src="<%=basePath%>/img/qrcode.jpg" class="img-responsive img-rounded img-thumbnail weixinQrcode"
                 style="width: 120px;height: 120px;">
          </c:when>
          <c:otherwise>
            <img src="${userExtDto.weixinQrcode}-120x120" class="img-responsive img-rounded img-thumbnail weixinQrcode"
                 style="width: 120px;height: 120px;padding: 10px">
          </c:otherwise>
        </c:choose>
        <small class="text-center" style="color: darkgray;padding: 10px;display: block">
          微信收款码
        </small>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding: 0px" align="center">
        <c:choose>
          <c:when test="${userExtDto.alipayQrcode == null}">
            <img src="<%=basePath%>/img/qrcode.jpg" class="img-responsive img-rounded img-thumbnail alipayQrcode"
                 style="width: 120px;height: 120px;">
          </c:when>
          <c:otherwise>
            <img src="${userExtDto.alipayQrcode}-120x120" class="img-responsive img-rounded img-thumbnail alipayQrcode"
                 style="width: 120px;height: 120px;padding: 10px">
          </c:otherwise>
        </c:choose>
        <small class="text-center" style="color: darkgray;padding: 10px;display: block">
          支付宝收款码
        </small>
      </div>
    </div>
  </div>
</div>
</div>
<input type="hidden" id="imgType">
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
<script language="JavaScript">
  $("#updatePassword").click(function () {
    $(".btn").blur();
    // - 更新密码
    var email = "<%=email%>";
    var password = $.trim($("#password").val());
    var oldPassword = $.trim($("#oldPassword").val());
    if (oldPassword == "") {
      alerErr("输入旧密码");
      return;
    }
    if (password == "") {
      alerErr("输入新密码");
      return;
    }
    var url = "<%=basePath%>/user/updatepassword?email=" + email + "&password=" + password + "&oldPassword=" + oldPassword;
    $.post(url, function (data, status) {
      data = JSON.parse(data);
      aler(data);
      if (data.code == 200) {
      }
    })
  });

  // - 更新图片
  function updateImg(imgType, imgSrc) {
    var url = '';
    if (imgType == "logo") {
      url = "<%=basePath%>/user/updatelogo?logo=" + imgSrc;
    } else if (imgType == "weixinQrcode") {
      url = "<%=basePath%>/user/updateWeixinQrcode?weixinQrcode=" + imgSrc;
    } else if (imgType == "alipayQrcode") {
      url = "<%=basePath%>/user/updateAlipayQrcode?alipayQrcode=" + imgSrc;
    }
    $.post(url, function (data, status) {
      aler(data);
    })
  }

  function updateImgSrc(imgType, src) {
    if (imgType == "logo") {
      $(".logo").attr("src", src + "-120x120");
    } else if (imgType == "weixinQrcode") {
      $(".weixinQrcode").attr("src", src + "-120x120");
    } else if (imgType == "alipayQrcode") {
      $(".alipayQrcode").attr("src", src + "-120x120");
    }
  }
  // - 上传图片
  function uploadFile(obj) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/img/upload";
    $.ajaxFileUpload({
      url: url,
      fileElementId: "filePath",//file标签的id,
      dateType: "text",
      success: function (data, status) {
        // - ajaxFileUpload这儿必须得转换以下
        data = jQuery.parseJSON(jQuery(data).text());
        if (data.code == 200) {
          var temp = data.data;
          var imgType = $("#imgType").val();
          updateImgSrc(imgType, temp.downloadUrl);
          updateImg(imgType, temp.downloadUrl);
        } else {
          alerErr(data.msg);
        }
        $("#loadModal").modal('hide');
      },
      error: function (data, status, e) {
        $("#loadModal").modal('hide');
        $("#filePath").replaceWith($("#upload").clone(true));
        alerErr(e);
      }
    });
  }
  $(function () {
    if (!validateEmail('<%=email%>')) {
      return;
    }
    initStyle();
    $("img").mousemove(function () {
      $(this).css("cursor", "pointer");
    })
    $("img").mouseout(function () {
      $(this).css("cursor", "default");
    })
    // - 上传LOGO
    $(".logo").click(function () {
      var email = '<%=email%>';
      if (email == 'null') {
        $("#loginModal").modal();
        return;
      }
      $("#imgType").val("logo");
      $("#filePath").click();
    })
    // - 上传微信二维码
    $(".weixinQrcode").click(function () {
      var email = '<%=email%>';
      if (email == 'null') {
        $("#loginModal").modal();
        return;
      }
      $("#imgType").val("weixinQrcode");
      $("#filePath").click();
    })
    // - 上传支付宝二维码
    $(".alipayQrcode").click(function () {
      var email = '<%=email%>';
      if (email == 'null') {
        $("#loginModal").modal();
        return;
      }
      $("#imgType").val("alipayQrcode");
      $("#filePath").click();
    })

    // - 退出
    $("#logout").click(function () {
      logout();
    })
    // - 更新资料
    $.ajaxSetup({
      contentType: 'application/json'
    })
    $("#update").click(function () {
      $(".btn").blur();
      var url = "<%=basePath%>/user/update";
      // - 基础信息
      var email = "${userExtDto.email}";
      var name = $.trim($("#name").val()) == "" ? null : $.trim($("#name").val());
      var phone = $.trim($("#phone").val()) == "" ? null : $.trim($("#phone").val());
      var addr = $.trim($("#addr").val()) == "" ? null : $.trim($("#addr").val());
      var remark = $.trim($("#remark").val());
      if (name == null) {
        alerErr("输入姓名");
        return;
      }
      if (phone == null) {
        alerErr("输入手机号");
        return;
      }
      var param = {
        email: email,
        name: name,
        phone: phone,
        addr: addr,
        remark: remark
      };
      $.post(url, JSON.stringify(param), function (data, status) {
        data = JSON.parse(data);
        if (data.code == 200) {
          alerOK(data.msg);
        } else {
          alerErr(data.msg);
        }
      })
    })
  })
</script>
</html>
