<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>个人中心 - 刘员外</title>

  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">
    td {
      padding: 9px
    }
  </style>
</head>
<body style="background-color: #f4f7ed;">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;margin: 0px;">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin-top: 10px;padding: 0px;border-bottom: 1px solid #eaeaea">
    <table style="width: 100%">
      <tr>
        <td>
          <a href='<%=basePath%>/user/queryinfo?email=<%=email%>' style="text-decoration: none">
            <div><i class="icon-user"style="color: #CC9966;"></i>&nbsp;个人中心</div>
          </a>
        </td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin-top: 10px;padding: 0px;border-bottom: 1px solid #eaeaea">
    <table style="width: 100%">
      <tr>
        <td>
          <a href='<%=basePath%>/set/info' style="text-decoration: none">
            <div><i class="icon-key text-default-color"></i>&nbsp;个人资料</div>
          </a>
        </td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin-top: 10px;padding: 0px;border-bottom: 1px solid #eaeaea">
    <table style="width: 100%">
      <tr>
        <td>
          <a href='<%=basePath%>/myorder' style="text-decoration: none">
            <div><i class="icon-tag"style="color: #CC99CC"></i>&nbsp;订单历史</div>
          </a>
        </td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin-top: 10px;padding: 0px;border-bottom: 1px solid #eaeaea">
    <table style="width: 100%">
      <tr>
        <td>
          <a href='<%=basePath%>/attention' style="text-decoration: none">
            <div><i class="icon-heart"style="color: #FF66CC"></i>&nbsp;关注列表</div>
          </a>
        </td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin-top: 10px;padding: 0px;border-bottom: 1px solid #eaeaea;">
    <table style="width: 100%">
      <tr>
        <td>
          <a href='<%=basePath%>/msgSet' style="text-decoration: none">
            <div><i class="icon-cog"></i>&nbsp;设置</div>
          </a>
        </td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin-top: 10px;padding: 0px;border-bottom: 1px solid #eaeaea;">
    <table style="width: 100%">
      <tr>
        <td>
          <a href='<%=basePath%>/admin' style="text-decoration: none">
            <div><i class="icon-home"style="color: #33CC99"></i>&nbsp;集市</div>
          </a>
        </td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
       style="background-color: #ffffff;margin-top: 10px;padding: 0px;border-bottom: 1px solid #eaeaea;margin-bottom: 100px">
    <table style="width: 100%">
      <tr>
        <td>
          <a href='javascript:void(0);' onclick="logout()" style="text-decoration: none">
            <div><i class="icon-signout"style="color: #999999"></i>&nbsp;退出</div>
          </a>
        </td>
        <td style="width: 10px;color: gray" align="center"><i class="icon-angle-right"></i></td>
      </tr>
    </table>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>


<script language="JavaScript">
  $(function () {
    if (!validateEmail('<%=email%>')) {
      return;
    }
    initStyle();
  })

  function logout() {
    var url = "<%=basePath%>/user/logout";
    $.post(url, function (data, status) {
      if (data.code == 10008) {
        alerOK(data.msg);
        // - 退出成功将登录信息从cookie取消掉
        clearLoginCookie();
        window.top.location = "<%=basePath%>/";
      } else {
        alerErr(data.msg);
      }
    })
  }

  function clearLoginCookie() {
    clearCookie("email");
    clearCookie("login");
    clearCookie("inCount");
  }
</script>
</html>
