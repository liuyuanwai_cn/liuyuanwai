<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<html>
<head>
  <title>消息设置 - 刘员外</title>

  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>


  <style type="text/css">
    td {
      padding: 10px;
    }

  </style>
</head>
<body style="background-color: #f9f9f9;">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;margin: 0px;">
  <div class="row-fluid" style="padding: 0px;margin: 0px">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
         style="background-color: #ffffff;margin-top: 10px;padding: 0px;">
      <table style="width: 100%;">
        <tr style="border-bottom: 1px solid #eaeaea">
          <td style="padding: 10px;"><i class="icon-volume-up" style="color: #0099FF"></i>&nbsp;文章留言提醒</td>
          <td align="right" style="padding: 0px">
            <table style="margin: 5px;">
              <tr>
                <td style="padding: 0px">
                  <a href="javascript:void(0);" style="text-decoration: none">
                    <div class="storyNotify" style="padding: 5px 15px;color: #ffffff;background-color: #0099FF">
                      是
                    </div>
                  </a>
                </td>
                <td style="padding: 0px">
                  <a href="javascript:void(0);" style="text-decoration: none">
                    <div class="storyNotNotify" style="padding: 5px 15px;color: #ffffff;background-color: #CCCCCC">
                      否
                    </div>
                  </a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr style="border-bottom: 1px solid #eaeaea">
          <td style="padding: 10px;"><i class="icon-volume-up" style="color: #0099FF"></i>&nbsp;留言回复提醒</td>
          <td align="right" style="padding: 0px">
            <table style="margin: 5px;">
              <tr>
                <td style="padding: 0px">
                  <a href="javascript:void(0);" style="text-decoration: none">
                    <div class="noteNotify" style="padding: 5px 15px;color: #ffffff;background-color: #0099FF">
                      是
                    </div>
                  </a>
                </td>
                <td style="padding: 0px">
                  <a href="javascript:void(0);" style="text-decoration: none">
                    <div class="noteNotNotify" style="padding: 5px 15px;color: #ffffff;background-color: #CCCCCC">
                      否
                    </div>
                  </a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr style="border-bottom: 1px solid #eaeaea">
          <td style="padding: 10px;"><i class="icon-volume-up" style="color: #0099FF"></i>&nbsp;关注动态提醒</td>
          <td align="right" style="padding: 0px">
            <table style="margin: 5px;">
              <tr>
                <td style="padding: 0px">
                  <a href="javascript:void(0);" style="text-decoration: none">
                    <div class="attentionNotify" style="padding: 5px 15px;color: #ffffff;background-color: #0099FF">
                      是
                    </div>
                  </a>
                </td>
                <td style="padding: 0px">
                  <a href="javascript:void(0);" style="text-decoration: none">
                    <div class="attentionNotNotify" style="padding: 5px 15px;color: #ffffff;background-color: #CCCCCC">
                      否
                    </div>
                  </a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<input type="hidden" id="storyNotify" value="${userExtDto.storyNotify}">
<input type="hidden" id="noteNotify" value="${userExtDto.noteNotify}">
<input type="hidden" id="attentionNotify" value="">
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>


<script language="JavaScript">


  function updateSet() {
    var storyNotify = $("#storyNotify").val();
    var noteNotify = $("#noteNotify").val();
    var attentionNotify = $("#attentionNotify").val();
    var url = '<%=basePath%>/set/saveOrUpdate';
    var param = {storyNotify: storyNotify, noteNotify: noteNotify,attentionNotify:attentionNotify};
    $.post(url, param, function (data, status) {
      aler(data);
    })
  }

  function storyNotify() {
    $(".storyNotify").css("background-color", "#0099FF");
    $(".storyNotNotify").css("background-color", "#CCCCCC");
    $("#storyNotify").val(1);
  }
  function storyNotNotify() {
    $(".storyNotify").css("background-color", "#CCCCCC");
    $(".storyNotNotify").css("background-color", "#0099FF");
    $("#storyNotify").val(0);
  }
  function noteNotify() {
    $(".noteNotify").css("background-color", "#0099FF");
    $(".noteNotNotify").css("background-color", "#CCCCCC");
    $("#noteNotify").val(1);
  }
  function noteNotNotify() {
    $(".noteNotify").css("background-color", "#CCCCCC");
    $(".noteNotNotify").css("background-color", "#0099FF");
    $("#noteNotify").val(0);
  }
  function attentionNotify() {
    $(".attentionNotify").css("background-color", "#0099FF");
    $(".attentionNotNotify").css("background-color", "#CCCCCC");
    $("#attentionNotify").val(1);
  }
  function attentionNotNotify() {
    $(".attentionNotify").css("background-color", "#CCCCCC");
    $(".attentionNotNotify").css("background-color", "#0099FF");
    $("#attentionNotify").val(0);
  }

  $('.attentionNotify').click(function () {
    attentionNotify();
    updateSet();
  })
  $('.attentionNotNotify').click(function () {
    attentionNotNotify();
    updateSet();
  })



  $(".storyNotify").click(function () {
    storyNotify();
    updateSet();
  })
  $(".storyNotNotify").click(function () {
    storyNotNotify();
    updateSet();
  })
  $(".noteNotify").click(function () {
    noteNotify();
    updateSet();
  })
  $(".noteNotNotify").click(function () {
    noteNotNotify();
    updateSet();
  })

  function initSet() {
    var isStoryNotify = ${userExtDto.storyNotify};
    var isNoteNotify = ${userExtDto.noteNotify};
    var isAttentionNotify = ${userExtDto.attentionNotify};
    if (isStoryNotify == 0) {
      storyNotNotify();
    } else {
      storyNotify();
    }
    if (isNoteNotify == 0) {
      noteNotNotify();
    } else {
      noteNotify();
    }
    if (isAttentionNotify == 0) {
      attentionNotNotify();
    } else {
      attentionNotify();
    }
  }


  $(function () {
    if (!validateEmail('<%=email%>')) {
      return;
    }
    initStyle();
    initSet();

    $(".rdolist").labelauty("rdolist", "rdo");
  })
</script>
</html>
