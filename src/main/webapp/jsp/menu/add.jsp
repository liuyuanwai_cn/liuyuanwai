<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
    String basePath = request.getContextPath();
%>
<html>
<head>
    <title>新增菜单</title>


    <style type="text/css">
        .shortselect{
            background:#ffffff;
            height:28px;
            width:180px;
            line-height:28px;
            border:1px solid #CCCCCC;
            -moz-border-radius:4px;
            -webkit-border-radius:4px;
            border-radius:4px;
        }
    </style>
</head>
<body style="padding: 0px;margin: 0px;background-color:#f4f7ed ">
<div class="container-fluid" style="padding: 0px;margin: 0px;">
    <div class="row-fluid" style="padding: 0px;margin: 0px">
        <div class="col-md-6" style="height: 100%;overflow-y: auto">
            <legend><h2>新增菜单</h2></legend>
            <div class="form-group">
                <label>菜单名称</label>
                <input type="text" style="width: 50%" id="name" class="form-control" placeholder="菜单名称">
            </div>
            <div class="form-group">
                <label>菜单地址</label>
                <input type="text" style="width: 50%" id="url" class="form-control" placeholder="菜单地址">
            </div>
            <div class="form-group">
                <label>父级菜单</label>

                <div class="well" style="background-color: #ffffff;">
                    <div class="jumbotron"
                         style="background-color: #ffffff;padding: 20px;margin: 2px;padding-top: 0px">
                        <input type="radio" name="isParent" value="1" checked> 是
                    </div>
                    <div class="jumbotron" style="padding: 20px;margin: 2px">
                        <label class="radio-inline">
                            <input type="radio" name="isParent" value="0"> 否
                        </label>
                        <select id="pId" name="pId" class="shortselect" style="margin-left: 20px;display: none">
                            <option value="0" selected>选择父级菜单</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="button" id="submit" class="btn btn-info">
                <i class="icon-plus"></i>&nbsp;&nbsp;新增
            </button>
        </div>
    </div>
</div>
</body>


<script language="JavaScript">
    $(document).ready(function () {

        var options = "<option value='0' selected>选择父级菜单</option>";
        //加载父级菜单
        $.get("<%=basePath%>/sysmenu/loadParentMenu", function (data, status) {
            if (status == "success") {
                $.each(data, function (index, value) {
                    $("#pId").append("<option value='" + value.id + "'> " + value.name + " </option>");
                });
            } else {
                $("#alertMsg").text(data);
                $("#loadModal").modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }
        })

        $(":radio").click(function () {
            if ($(":radio:checked").val() == 0) {
                $("#pId").fadeToggle();
            } else {
                $("#pId").fadeToggle();
                ;
            }
        })

        $("#submit").click(function () {
            var name = $("#name").val();

            if (name == null || name == "") {
                alerErr("输入菜单名称");
                return;
            }

            var url = $("#url").val();
            var isParent = $(":radio:checked").val();
            var pId = $("#pId option:selected").val();
            if (isParent == 0) {
                if (pId <= 0) {
                    alerErr("选择父级菜单");
                    return;
                }
            } else {
                pId = -1;
            }
            var param = {name: name, url: url, pId: pId};
            $.ajaxSetup({
                contentType: 'application/json'
            });
            url = "<%=basePath%>/sysmenu/add";
            $.post(url, JSON.stringify(param), function (data, status) {
                data = JSON.parse(data);
                if (data.code == 200) {
                    alerOK(data.msg);
                    window.parent.window.reloadTree();
                } else {
                    alerErr(data.msg);
                }
            })
        })
    })
</script>
</html>
