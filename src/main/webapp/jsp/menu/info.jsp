<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>菜单详情</title>


  <style type="text/css">
    .shortselect {
      background: #ffffff;
      height: 28px;
      width: 180px;
      line-height: 28px;
      border: 1px solid #CCCCCC;
      -moz-border-radius: 4px;
      -webkit-border-radius: 4px;
      border-radius: 4px;
    }
  </style>
</head>
<body style="padding: 0px;margin: 0px;background-color: #f4f7ed;">
<div class="container-fluid" style="padding: 0px;margin: 0px;">
  <div class="row-fluid" style="padding: 0px;margin: 0px">
    <div class="col-md-6" style="padding: 30px;height: 100%">
      <legend style="margin: 5px"><h2>菜单详情</h2></legend>
      <div class="form-group" style="margin-top: 20px">
        <label>菜单名称</label>
        <input type="text" id="name" class="form-control" placeholder="菜单名称" value="${shMenu.name}">
      </div>
      <div class="form-group">
        <label>菜单地址</label>
        <input type="text" id="url" class="form-control"
               placeholder="菜单地址" value="${shMenu.url}">
      </div>
      <div class="form-group">
        <label>父级菜单</label>

        <div class="well" style="background-color: #ffffff;">
          <div class="jumbotron"
               style="background-color: #ffffff;padding: 20px;margin: 2px;padding-top: 0px">
            <input type="radio" name="isParent" id="isParent" value="1"> 是
          </div>
          <div class="jumbotron" style="padding: 20px;margin: 2px">
            <label class="radio-inline">
              <input type="radio" name="isParent" id="isNotParent" value="0"> 否
            </label>
            <select class="shortselect" id="pId" name="pId" style="margin-left: 20px;display: none">
              <option value="0" selected>选择父级菜单</option>
            </select>
          </div>
        </div>
      </div>
      <button type="button" id="submit" class="btn btn-info">
        <i class="icon-edit"></i>&nbsp;编辑
      </button>
    </div>
  </div>
</div>
<input type="hidden" id="menuId" value="${shMenu.id}">
</body>


<script language="JavaScript">

  function loadMenu(){
    var pId = ${shMenu.pId};
    // - 加载出菜单列表
    $.get("<%=basePath%>/sysmenu/loadParentMenu", function (data, status) {
      $.each(data, function (index, value) {
        if(pId == value.id){
          $("#pId").append("<option selected value='" + value.id + "'> " + value.name + " </option>");
        }else{
          $("#pId").append("<option value='" + value.id + "'> " + value.name + " </option>");
        }
      })
    })

    // - 加载菜单数据

    if (pId == -1) {
      // - 父菜单
      $("#isNotParent").attr("checked", false);
      $("#isParent").attr("checked", "checked");
      $("#pId").fadeOut();
    } else if (pId > 0) {
      // - 子菜单
      $("#isNotParent").attr("checked", "checked");
      $("#isParent").attr("checked", false);
      $("#pId").val(pId);
      $("#pId").fadeIn();
    }


  }



  $(document).ready(function () {

    loadMenu();

    $(":radio").click(function () {
      if ($(":radio:checked").val() == 0) {
        $("#pId").fadeToggle();
      } else {
        $("#pId").fadeToggle();
        ;
      }
    })

    //菜单编辑
    $("#submit").click(function () {
      var id = $("#menuId").val();
      var name = $("#name").val();
      if (name == "") {
        alerErr("输入菜单名称");
      }
      var url = $("#url").val();
      var isParent = $(":radio:checked").val();
      var pId = $("#pId option:selected").val();
      if (isParent == 0) {
        if (pId <= 0) {
          alerErr("选择父级菜单");
          return;
        }
      } else {
        pId = -1;
      }
      var param = {name: name, url: url, pId: pId, id: id};
      $.ajaxSetup({
        contentType: 'application/json'
      })
      url = "<%=basePath%>/sysmenu/update";
      $.post(url, JSON.stringify(param), function (data, status) {
        data = JSON.parse(data);
        if (data.code == 200) {
          alerOK(data.msg);
        } else {
          alerErr(data.msg);
        }
      })
    })
  })
</script>
</html>
