<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
%>
<!DOCTYPE HTML>
<html>
<head>
  <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmGrid.css">
  <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmGrid-bootstrap.css">
  <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmPaginator.css">
  <link rel="stylesheet" href="<%=basePath%>/plugin/mmgrid/mmPaginator-bootstrap.css">

  <script language="JavaScript" src="<%=basePath%>/plugin/mmgrid/mmGrid.js"></script>
  <script language="JavaScript" src="<%=basePath%>/plugin/mmgrid/mmPaginator.js"></script>

  <style type="text/css">
    table {
      font-size: small
    }
  </style>
</head>
<body style="padding: 0px;margin: 0px">
<div class="container-fluid" style="padding: 0px;margin: 0px">
  <div class="row-fluid" style="padding: 10px;height: 100%;overflow-y: auto">
    <legend style="margin: 0px"><h2>菜单管理</h2></legend>
    <div class="btn-group" role="group" style="margin: 5px; ">
      <a type="button" class="btn btn-info">
        <i class="icon-list"></i>&nbsp;功能
      </a>
      <a type="button" class="btn btn-default" id="del">
        <i class="icon-trash"></i>&nbsp;删除
      </a>
    </div>
    <div class="col-md-12" style="padding: 0px">
      <table id="menu"></table>
      <div id="paginator"></div>
    </div>
  </div>
</div>


</body>


<script language="JavaScript">

  // - 菜单详情链接
  function menuInfo(val, item, rowIndex) {
    return "<a href=<%=basePath%>/sysmenu/info?menuId=" + item.id + ">" + item.name + "</a>";
  }

  $(document).ready(function () {


    var title = [
      {title: '主键', name: 'id',width:'50'},
      {title: '名称', name: 'name', width: '150', renderer: menuInfo},
      {title: '链接', name: 'url', width: '400', sortable: true}
    ];
//        var data = [{
//            index: 1,
//            name: "刘员外"
//        }];

    var mmGrid = $("#menu").mmGrid({
      url: "<%=basePath%>/sysmenu/search",
      method: "post",
      cols: title,
      checkCol: true,
      height: "75%",
      multiSelect: true,
      plugins: [$('#paginator').mmPaginator({})]
    })

    // - 删除选中数据
    $("#del").click(function () {
      var selectedRows = mmGrid.selectedRows();

      if (selectedRows.length == 0) {
        alerErr("选择要删除的行");
        return;
      }

      var temp = "";
      for (var i = 0; i < selectedRows.length; i++) {
        temp += selectedRows[i].id;
        temp += ",";
      }
      var param = {temp: temp};
      $.ajaxSetup({
        contentType: 'application/json'
      });
      var url = "<%=basePath%>/sysmenu/del";
      $.post(url, JSON.stringify(param), function (data, status) {
        data = JSON.parse(data);
        if (data.code == 200) {
          alerOK(data.msg);
          window.parent.window.reloadTree();
        } else {
          alerErr(data.msg);
        }
        mmGrid.load({
          url: "<%=basePath%>/sysmenu/search"
        });
      })
    })
  })
</script>
</html>
