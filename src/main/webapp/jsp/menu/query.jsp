<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>菜单查询</title>


  <style type="text/css">
    .shortselect {
      background: #ffffff;
      height: 28px;
      width: 180px;
      line-height: 28px;
      border: 1px solid #CCCCCC;
      -moz-border-radius: 4px;
      -webkit-border-radius: 4px;
      border-radius: 4px;
    }
  </style>
</head>
<body style="padding: 0px;margin: 0px">
<div class="container-fluid" style="padding: 0px;margin: 0px;">
  <div class="row-fluid" style="padding: 0px;margin: 0px">
    <div class="col-md-6" style="background-color: white;padding: 30px;height: 100%">
      <div class="input-group">
        <input type="text" id="queryName" class="form-control" placeholder="输入菜单名称查询...">
                <span class="input-group-btn">
                    <a class="btn btn-info" type="button" id="search">
                      <i class="icon-search"></i>&nbsp;搜索
                    </a>
                </span>
      </div>
      <div class="form-group" style="margin-top: 20px">
        <label>菜单名称</label>
        <input type="text" id="name" class="form-control" placeholder="菜单名称">
      </div>
      <div class="form-group">
        <label>菜单地址</label>
        <input type="text" id="url" class="form-control"
               placeholder="菜单地址">
      </div>
      <div class="form-group">
        <label>父级菜单</label>

        <div class="well" style="background-color: #ffffff;">
          <div class="jumbotron"
               style="background-color: #ffffff;padding: 20px;margin: 2px;padding-top: 0px">
            <input type="radio" name="isParent" id="isParent" value="1"> 是
          </div>
          <div class="jumbotron" style="padding: 20px;margin: 2px">
            <label class="radio-inline">
              <input type="radio" name="isParent" id="isNotParent" value="0"> 否
            </label>
            <select class="shortselect" id="pId" name="pId" style="margin-left: 20px;display: none">
              <option value="0" selected>选择父级菜单</option>
            </select>
          </div>
        </div>
      </div>
      <button type="button" disabled id="submit" class="btn btn-info">
        <i class="icon-edit"></i>&nbsp;编辑
      </button>
    </div>
  </div>
</div>
<input type="hidden" id="menuId">
</body>


<script language="JavaScript">
  $(document).ready(function () {

    $.get("<%=basePath%>/sysmenu/loadParentMenu", function (data, status) {
      if (status == "success") {
        $.each(data, function (index, value) {
          $("#pId").append("<option value='" + value.id + "'> " + value.name + " </option>");
        });
      } else {
        $("#alertMsg").text(data);
        $("#myModal").modal({
          keyboard: false,
          backdrop: 'static'
        });
      }
    })


    //菜单查询
    $("#search").click(function () {
      var queryName = $("#queryName").val();
      var param = {temp: queryName};
      $.ajaxSetup({
        contentType: 'application/json'
      });
      url = "<%=basePath%>/sysmenu/queryByMenuName";
      $.post(url, JSON.stringify(param), function (data, status) {
        if (data.id == null || data.id == "") {
          alerErr("没有查询到数据");
          return;
        }
        $("#submit").attr("disabled", false);
        $("#menuId").val(data.id);
        $("#name").val(data.name);
        $("#url").val(data.url);
        if (data.pId == -1) {
          // - 父菜单
          $("#isNotParent").attr("checked", false);
          $("#isParent").attr("checked", "checked");
          $("#pId").fadeToggle();
        } else if (data.pId > 0) {
          // - 子菜单
          $("#isNotParent").attr("checked", "checked");
          $("#isParent").attr("checked", false);
          $("#pId").val(data.pId);
          $("#pId").fadeIn();
        }
      })
    })
    //菜单编辑
    $("#submit").click(function () {
      var id = $("#menuId").val();
      var name = $("#name").val();
      if (name == "") {
        alerErr("输入菜单名称");
      }
      var url = $("#url").val();
      var isParent = $(":radio:checked").val();
      var pId = $("#pId option:selected").val();
      if (isParent == 0) {
        if (pId <= 0) {
          alerErr("选择父级菜单");
          return;
        }
      } else {
        pId = -1;
      }
      var param = {name: name, url: url, pId: pId, id: id};
      $.ajaxSetup({
        contentType: 'application/json'
      })
      url = "<%=basePath%>/sysmenu/update";
      $.post(url, JSON.stringify(param), function (data, status) {
        data = JSON.parse(data);
        if (data.code == 200) {
          alerOK(data.msg);
          window.parent.window.reloadTree();
        } else {
          alerErr(data.msg);
        }
      })
    })
  })
</script>
</html>
