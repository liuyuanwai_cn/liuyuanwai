<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>数据采集</title>
</head>
<body style="padding:10px ">
<div class="container-fluid">
  <div class="row-fluid">
    <legend>数据采集</legend>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>姓名</label>
            <input type="text" id="name" class="form-control" placeholder="姓名">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>手机号</label>
            <input type="text" id="mobile" class="form-control" placeholder="手机号">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>来源机构</label>
            <input type="text" id="source" class="form-control" placeholder="来源机构">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>身份证</label>
            <input type="text" id="idcard" class="form-control" placeholder="身份证">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>审核人</label>
            <input type="text" id="checkName" class="form-control" placeholder="审核人">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>是否审核通过</label>
            <input type="text" id="isPass" class="form-control" placeholder="是否审核通过">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>风险等级</label>
            <input type="text" id="level" class="form-control" placeholder="风险等级">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>学历</label>
            <input type="text" id="education" class="form-control" placeholder="学历">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>职务</label>
            <input type="text" id="job" class="form-control" placeholder="职务">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>月租金</label>
            <input type="text" id="rent" class="form-control" placeholder="月租金">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>城市</label>
            <input type="text" id="city" class="form-control" placeholder="城市">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>是否有过逾期</label>
            <input type="text" id="isOver" class="form-control" placeholder="是否有过逾期">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-3">
      <form>
        <fieldset>
          <div class="form-group">
            <label>付款方式</label>
            <input type="text" id="payType" class="form-control" placeholder="付款方式">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-9">
      <form>
        <fieldset>
          <div class="form-group">
            <label>风险评估说明</label>
            <input type="text" id="risk" class="form-control" placeholder="风险评估说明">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-12">
      <form>
        <fieldset>
          <div class="form-group">
            <label>备注</label>
            <input type="text" id="remark" class="form-control" placeholder="备注">
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-md-12">
      <button type="button" id="submit" class="btn btn-info">
        <i class="icon-plus"></i>&nbsp;&nbsp;新增
      </button>
    </div>
  </div>
</div>
</div>
</div>

</body>


<script language="JavaScript">
  $(document).ready(function () {

    //$("#pId").selectpick();
    var options = "<option value='0' selected>选择父级菜单</option>";
    //加载父级菜单
    $.get("<%=basePath%>/sysmenu/loadParentMenu", function (data, status) {
      if (status == "success") {
        $.each(data, function (index, value) {
          $("#pId").append("<option value='" + value.id + "'> " + value.name + " </option>");
        });
      } else {
        $("#alertMsg").text(data);
        $("#myModal").modal({
          keyboard: false,
          backdrop: 'static'
        });
      }
    })

    $(":radio").click(function () {
      if ($(":radio:checked").val() == 0) {
        $("#pId").show();
      } else {
        $("#pId").hide();
      }
    })

    $("#submit").click(function () {
      var name = $("#name").val();
      var url = $("#url").val();
      var isParent = $(":radio:checked").val();
      var pId = $("#pId option:selected").val();
      if (isParent == 0) {
        if (pId <= 0) {
          alert("选择父级菜单");
          return;
        }
      } else {
        pId = -1;
      }
      var param = {name: name, url: url, pId: pId};
      $.ajaxSetup({
        contentType: 'application/json'
      });
      url = "<%=basePath%>/sysmenu/addMenu";
      $.post(url, JSON.stringify(param), function (data, status) {
        data = JSON.parse(data);
        aler(data);
      })
    })
  })
</script>
</html>
