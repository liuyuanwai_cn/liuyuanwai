<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
    String basePath = request.getContextPath();
%>

<html>
<head>
    <title>统计报表</title>

    <script type="application/javascript" src="<%=basePath%>/plugin/Echarts/echarts.min.js"></script>

</head>
<body style="padding:5px;">
<div class="container-fluid" style="padding: 0px;">
    <div class="row-fluid" style="padding: 0px">
        <div class="col-md-12" style="padding: 0px">
            <legend style="margin:5px;">统计报表</legend>
        </div>
        <div class="col-md-12" style="padding: 10px">
            <div class="panel panel-info">
                <div class="panel-heading" style="padding: 2px">
                    <div class="btn-group" role="group" style="margin: 5px;float: right">
                        <a type="button" class="btn btn-default" id="year">年</a>
                        <a type="button" class="btn btn-default" id="month">月</a>
                        <a type="button" class="btn btn-default" id="day">日</a>
                        <a type="button" class="btn btn-default" id="data">数据</a>
                        <a type="button" class="btn btn-default" id="trend">走势</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="chart" style="width: 100%;height: 400px"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>


<script language="JavaScript">
    var url = "<%=basePath%>/risk/statistic";
    var date = new Date();
    function showChart(type,title,subTitle,categories,series,name){

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('chart'));
        // 指定图表的配置项和数据
        var option = {
            title: {
                text: title
            },
            tooltip: {},
            legend: {
                data:[name]
            },
            xAxis: {
                data: categories
            },
            yAxis: {},
            series: [{
                name: name,
                type: type,
                data: series,
                barWidth: 30,
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            textStyle: {
                                color: '#ffffff'
                            }
                        }
                    }
                }
            }]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }


    // - 年统计
    function yearStatistic(){
        $.post(url+"?type=year",function(data,status){
            showChart("bar","年报表","年销量统计",data.xAixs,data.series,"年销量");
        })
    }
    // - 月统计
    function monthStatistic(){
        $.post(url+"?type=month",function(data,status){
            showChart("bar","月报表","月销量统计",data.xAixs,data.series,date.getFullYear()+"年"+(date.getMonth()+1)+"月"+"  日销量");
        })
    }
    // - 日统计
    function dayStatistic(){
        $.post(url+"?type=day",function(data,status){
            showChart("bar","日报表","日销量统计",data.xAixs,data.series,date.getFullYear()+"年"+(date.getMonth()+1)+"月"+date.getDay()+"日  时销量");
        })
    }



    $(function () {
        yearStatistic();

        // - 年
        $("#year").click(function(){
            yearStatistic();
        })
        // - 月
        $("#month").click(function(){
            monthStatistic();
        })
        // - 日
        $("#day").click(function(){
            dayStatistic();
        })
        // - 数据
        $("#day").click(function(){

        })
        // - 走势
        $("#trend").click(function(){

        })


    })

</script>
</html>
