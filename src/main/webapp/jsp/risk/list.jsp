<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/common.jsp"></jsp:include>
<%
  String basePath = request.getContextPath();
%>

<html>
<head>
  <title>客户列表</title>
  <link rel="stylesheet" href="../../plugin/mmgrid/mmGrid.css">
  <link rel="stylesheet" href="../../plugin/mmgrid/mmGrid-bootstrap.css">
  <link rel="stylesheet" href="../../plugin/mmgrid/mmPaginator.css">
  <%--<link rel="stylesheet" href="../../plugin/mmgrid/mmGrid-bootstrap.css">--%>
  <link rel="stylesheet" href="../../plugin/mmgrid/mmPaginator-bootstrap.css">
  <link rel="stylesheet" href="../../plugin/Font-Awesome-3.2.1/css/font-awesome.min.css">

  <script language="JavaScript" src="../../plugin/mmgrid/mmGrid.js"></script>
  <script language="JavaScript" src="../../plugin/mmgrid/mmPaginator.js"></script>

  <script language="JavaScript" src="../../plugin/ajaxfileupload/ajaxfileupload.js"></script>

  <style type="text/css">
    table {
      font-size: 14px
    }
  </style>


</head>
<body style="padding:5px;">
<div class="container-fluid" style="padding: 0px;">
  <div class="row-fluid" style="padding: 0px">
    <div class="col-md-12" style="padding: 0px">
      <legend style="margin:5px;">客户列表</legend>
    </div>
    <div class="col-md-12" style="padding: 0px">
      <div style="display: none" id="fileDiv">
        <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath">
      </div>
      <div class="input-group col-md-3" style="float: right">
        <input type="text" id="query" class="form-control" placeholder="姓名/手机号...">
                <span class="input-group-btn">
                    <a class="btn btn-info" type="button" id="search">
                      <i class="icon-search"></i>&nbsp;&nbsp;搜索
                    </a>
                </span>
      </div>
      <div class="btn-group  col-md-7" role="group" style="margin: 5px;float: left">
        <a type="button" class="btn btn-default">
          <i class="icon-list"></i>&nbsp;&nbsp;功能
        </a>
        <a type="button" class="btn btn-default" id="import">
          <i class="icon-download-alt"></i>&nbsp;&nbsp;导入
        </a>
        <a type="button" class="btn btn-default" id="export">
          <i class="icon-upload-alt"></i>&nbsp;&nbsp;导出
        </a>
        <a type="button" class="btn btn-default" id="templatedownload">
          <i class="icon-file"></i>&nbsp;&nbsp;模版下载
        </a>
        <a href="chart.jsp" target="_self" type="button" class="btn btn-default" id="chart">
          <i class="icon-bar-chart"></i>&nbsp;&nbsp;统计报表
        </a>
        <a type="button" class="btn btn-danger" id="del">
          <i class="icon-trash"></i>&nbsp;&nbsp;删除
        </a>
      </div>
    </div>
    <div class="col-md-12" style="padding: 0px">
      <table id="menu"></table>
      <div id="pg"></div>
    </div>
  </div>
</div>

</body>


<script language="JavaScript">

  //配置当前grid默认的分页大小以及总记录数的名称
  var paginator = $('#pg').mmPaginator({});

  var mmGrid = "";

  function uploadFile(obj) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });

    var fileUpload = $("#fileDiv").html();
    var url = "<%=basePath%>/risk/importExcel";
    $.ajaxFileUpload({
      url: url,
      fileElementId: "filePath",//file标签的id,
      dateType: "text",
      success: function (data, status) {
        // - ajaxFileUpload这儿必须得转换以下
        data = jQuery.parseJSON(jQuery(data).text());
        if (data.code == 200) {
          mmGrid.load();
          alerOK(data.msg);
        } else {
          alerErr(data.msg);
        }
        $("#loadModal").modal('hide');
      },
      error: function (data, status, e) {
        $("#filePath").replaceWith($("#upload").clone(true));
        alerErr(e);
      }
    });
  }


  $(document).ready(function () {

    $.ajaxSetup({
      contentType: 'application/json'
    });

    var title = [
      {
        title: "个人信息", titleHtml: "<p style=''>个人信息</p>", align: "center", cols: [
        {title: "序号",hidden:true, titleHtml: "<p style=''>ID</p>", name: "id"},
        {title: "日期", titleHtml: "<p style=''>日期</p>", name: "joinTime"},
        {title: "姓名", titleHtml: "<p style=''>姓名</p>", name: "name"},
        {title: "手机号", titleHtml: "<p style=''>手机号</p>", name: "mobile"}
      ]
      },
      {
        title: "租房信息", titleHtml: "<p style=''>租房信息</p>", align: "center", cols: [
        {title: "月租金", titleHtml: "<p style=''>月租金</p>", name: "rent"},
        {title: "交租方式", titleHtml: "<p style=''>交租方式</p>", name: "payType"},
        {title: "城市", titleHtml: "<p style=''>城市</p>", name: "city"}
      ]
      },
      {
        title: "学历信息", titleHtml: "<p style=''>学历信息</p>", align: "center", cols: [
        {title: "是否毕业", titleHtml: "<p style=''>是否毕业</p>", name: "isGraduated"},
        {title: "毕业院校", titleHtml: "<p style=''>毕业院校</p>", name: "school"},
        {title: "毕业时间", titleHtml: "<p style=''>毕业时间</p>", name: "graduationDate"},
        {title: "学历", titleHtml: "<p style=''>学历</p>", name: "education"}
      ]
      },
      {
        title: "工作信息", titleHtml: "<p style=''>工作信息</p>", align: "center", cols: [
        {title: "公司", titleHtml: "<p style=''>公司</p>", name: "company"},
        {title: "入职时间", titleHtml: "<p style=''>入职时间</p>", name: "entryTime"},
        {title: "职务", titleHtml: "<p style=''>职务</p>", name: "job"},
        {title: "月薪", titleHtml: "<p style=''>月薪</p>", name: "income"},
        {title: "月支出", titleHtml: "<p style=''>月支出</p>", name: "expense"}
      ]
      },
      {
        title: "备注信息", titleHtml: "<p style=''>备注信息</p>", align: "center", cols: [
        {title: "QQ", titleHtml: "<p style=''>QQ</p>", name: "qq"},
        {title: "是否授权芝麻", titleHtml: "<p style=''>是否授权芝麻</p>", name: "authSesame"}
      ]
      }
    ];
//    var data = [{
//      name: "刘员外",
//      mobile: 1
//    }, {
//      name: "刘员外",
//      mobile: 2
//    }];

    mmGrid = $("#menu").mmGrid({
      url: "<%=basePath%>/risk/search",
      method: "post",
      cols: title,
      checkCol: true,
      multiSelect: true,
      width: "auto",
      height: "75%",
      nowrap: false,
      fullWidthRows: true,
      plugins: [paginator]
    })


//  导入excel
    $("#import").click(function () {
      $("#filePath").click();
    })

//    模版下载
    $("#templatedownload").click(function (data, status) {
      var url = "<%=basePath%>/risk/downTemplate ";
      window.location = url;
    })

//    导出数据
    $("#export").click(function () {
      var url = "<%=basePath%>/risk/exportExcel";
      window.location = url;
    })

    // - 批量删除
    $("#del").click(function () {
      var rows = mmGrid.selectedRows();
      if (rows.length <= 0) {
        alerErr("选择要删除的行");
      }
      var idArr = new Array();
      for (i = 0; i < rows.length; i++) {
        idArr[i] = rows[i].id;
      }
      var url = "<%=basePath%>/risk/batchDel?idArr=" + idArr;
      $.post(url, function (data, status) {
        alerOK(data.msg);
        mmGrid.load();
      })
    })


    // - 模糊查询
    $("#search").click(function () {
      var key = $("#query").val();
      mmGrid.load({key: key});
    })
  })

</script>
</html>
