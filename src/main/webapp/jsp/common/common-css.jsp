<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
      content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no">
<%
    String basePath = request.getContextPath();
%>
<link rel="Shortcut Icon" href="<%=basePath%>/favicon.ico">
<link href="<%=basePath%>/jsp/common/common.css" rel="stylesheet">
<link href="<%=basePath%>/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<%=basePath%>/plugin/Font-Awesome-3.2.1/css/font-awesome.min.css">
