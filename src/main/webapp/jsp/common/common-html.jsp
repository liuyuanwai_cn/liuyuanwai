<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
%>
<div id="msgDIV" class="col-xs-12 text-center"
     style="display: none;position: fixed;top: 0px;padding: 10px;z-index: 10003;">
  提示
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="loadModal" role="dialog" style="margin-top: 15%;z-index: 10003;">
  <div class="modal-dialog modal-sm" style="">
    <div class="modal-content" style="">
      <div class="modal-body text-center">
        <img src="<%=basePath%>/img/loading.gif">
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="loginModal" role="dialog" style="margin-top: 10%">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" align="center">
        <p>您还没有登录!</p>
        <small style="color: darkgray">注册只需1个邮箱,花1秒钟</small>
      </div>
      <div class="modal-footer" style="border: none">
        <table style="width: 100%;">
          <tr>
            <td style="width: 50%" align="center">
              <a class="btn btn-default"href="javascript:void(0);"class="close" data-dismiss="modal">取&nbsp;&nbsp;消</a>
            </td>
            <td style="width: 50%" align="center">
              <a href="<%=basePath%>/login" class="btn btn-info">登&nbsp;&nbsp;录</a>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>