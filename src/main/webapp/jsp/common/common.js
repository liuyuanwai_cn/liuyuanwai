function loadNav(url) {
    $.get(url,
        function (html) {
            $("#nav").html(html)
        })
}
function enable_bt() {
    var email = $.trim($("#email").val());
    $bt = $("#check");
    if (email != "") {
        $bt.removeAttr("disabled");
        $bt.removeClass("btn-disabled-color");
        $bt.addClass("btn-info")
    }
}
function disable_bt() {
    var email = $.trim($("#email").val());
    $bt = $("#check");
    if (email == "") {
        $bt.attr("disabled", true);
        $bt.removeClass("btn-info");
        $bt.addClass("btn-disabled-color");
        $("#email").val("")
    }
}
function alerOK(msg) {
    $("#msgDIV").removeClass("alert-danger");
    $("#msgDIV").addClass("alert-info");
    $("#msgDIV").html("<i class='icon-comments-alt'></i>&nbsp;&nbsp;" + msg);
    $("#msgDIV").slideToggle("slow");
    $("#msgDIV").fadeOut(5000)
}
function alerErr(msg) {
    $("#msgDIV").removeClass("alert-info");
    $("#msgDIV").addClass("alert-danger");
    $("#msgDIV").html("<i class='icon-comments-alt'></i>&nbsp;&nbsp;" + msg);
    $("#msgDIV").slideToggle("slow");
    $("#msgDIV").fadeOut(5000)
}
function aler(data) {
    if (data.code == undefined) {
        data = JSON.parse(data)
    }
    if (data.code == 200) {
        alerOK(data.msg)
    } else {
        alerErr(data.msg)
    }
}
$(".NumDecText").keyup(function () {
    $(this).val($(this).val().replace(/[^\d.]/g, ""));
    $(this).val($(this).val().replace(/^\./g, ""));
    $(this).val($(this).val().replace(/\.{2,}/g, "."));
    $(this).val($(this).val().replace(".", "$#$").replace(/\./g, "").replace("$#$", "."))
}).bind("paste",
    function () {
        $(this).val($(this).val().replace(/[^\d.]/g, ""));
        $(this).val($(this).val().replace(/^\./g, ""));
        $(this).val($(this).val().replace(/\.{2,}/g, "."));
        $(this).val($(this).val().replace(".", "$#$").replace(/\./g, "").replace("$#$", "."))
    });
function setCookie(c_name, value, expiredays) {
    var exdate = new Date();
    expiredays = 365 * 10;
    exdate.setDate(exdate.getDate() + expiredays);
    var cookie = document.cookie;
    document.cookie = c_name + "=" + value + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString()) + ";path=/";
    cookie = document.cookie;
}
function getCookie(c_name) {
    var cookie = document.cookie;
    if (cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1
            c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1) c_end = document.cookie.length
            return document.cookie.substring(c_start, c_end);
        }
    }
    return "";
}
function clearCookie(name) {
    setCookie(name, "", -1);
}
function initStyle() {
    $('pre').addClass('preNoBorder');
}
function validateEmail(email) {
    if (email == undefined || email == 'null' || email == '') {
        $("#loginModal").modal({
            backdrop: 'static',
            keyboard: false
        });
        return false;
    }
    return true;
}
//function validate(phone) {
//    if (email == 'null') {
//        $("#loginModal").modal();
//        return false;
//    }
//    return true;
//}

function getRandom(n) {
    return Math.floor(Math.random() * n + 1)
}


function validateNum(val) {
    var reg = /^0*[0-9]{1,6}([.]\d{0,2})?$/;
    if (reg.test(val)) {
        return true;
    }
    return false;
}

function isNull(val){
    if(val == undefined || val == '' || val == 'null'){
        return true;
    }
    return false;
}