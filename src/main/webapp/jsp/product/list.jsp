<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
%>
<html>
<head>
  <title>集市 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <style type="text/css">

    .badge{
      background-color: #ffffff;
      color: #000000;
      border: 1px solid #CCCCCC;
    }
  </style>
</head>
<script>
  function toPage(productId) {
    window.location = "<%=basePath%>/order/preadd?productId=" + productId;
  }

  function buildGood(list) {
    var item = '';
    var temp = '';
    for (var i = 0; i < list.length; i++) {
      var goodUrl = '';
      var url = list[i].url;
      var id = list[i].id;
      var name = list[i].name;
      var deleted = list[i].deleted;
      var sales = list[i].sales;
      var price = list[i].price;
      var unit = list[i].unit;
      var inventory = list[i].inventory;
      if (url == null || url == '' || url == undefined || url == '<%=basePath%>/img/product.png') {
        goodUrl = '<%=basePath%>/img/product.png';
      } else {
        goodUrl = url + '-360x200';
      }
      if (name.length > 12) {
        name = name.substr(0, 12) + '...';
      }
      item += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" style="padding-bottom: 7px;padding-left: 5px;padding-right: 5px">';
      item += '<div class="thumbnail"style="border: none;border-radius: 0px;-webkit-border-radius: 0px;margin:0px;padding: 5px 0px" id="good' + id + '"onmouseover="focusGood(' + id + ')"onmouseout="blurGood(' + id + ')" >';
      item += '<p style="margin: 8px">' + name + '</p>';
      item += '<p class="" style="color: red;margin: 5px">￥<span style="font-size: larger">' + price.toFixed(2) + '</span>/' + unit + '</p>';
      item += '<a style="text-decoration: none" href="javascript:void(0);" onclick="javascript:toPage(' + id + ');">';
      item += '<img class="img-responsive" style="height:200px" src="' + goodUrl + '" alt="' + name + '">';
      item += '<div class="caption"style="margin-top: 10px">';
      if (inventory == 0) {
        item += '<p><span class="label label-default">已卖完</span></p>';
      } else {
        item += '<p><span class="label label-success">在售中</span></p>';
      }
      item += '<p style="padding-top: 10px"><small style="color: #999999;">浏览：<span>' + list[i].readCount + '</span>&nbsp;|&nbsp;销量：<span>' + list[i].sales + '</span></small></p>';
      item += '</div>';
      item += '</a>';
      item += '</div>';
      item += '</div>';

      temp += item;
      item = '';
    }
    return temp;
  }

  function focusGood(id) {
    var clazz = '#good' + id;
    $(clazz).css('box-shadow', '2px 4px 4px #ccc');
  }
  function blurGood(id) {
    var clazz = '#good' + id;
    $(clazz).css('box-shadow', '0px 0px 0px #fff');
  }

  // - 加载商品列表
  function loadGoods(pageNo, pageSize, productType) {
    $("#goodsArea").show();
    if (productType == 0) {
      productType = null;
    }
    var url = "<%=basePath%>/product/list";
    var param = {pageNo: pageNo, pageSize: pageSize, productType: productType};
    $.post(url, param, function (data, status) {
      var goods = data;
      $("#goodsArea").hide();
      if (goods.length > 0) {
        $(".noGoods").hide();
        $("#goods").append(buildGood(goods));
        if (goods.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<div class="text-center noGoods"style="padding: 100px 0px; ">';
          noDataShow += '<h3 style="color: #999999">No Goods Here!</h3>';
          noDataShow += '</div>';
          $("#goods").empty();
          $("#goods").append(noDataShow);
          $("#loadMore").hide();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }

  function chooseType(tag) {
    pageNo = 1;
    $(".tag").removeClass('badge');
    var id = '#type' + tag;
    $(id).addClass('badge');
    $("#tag").val(tag);
    $("#goods").empty();
    if (tag == 0) {
      tag = null;
    }
    loadGoods(pageNo++, pageSize, tag);
  }

  var getRandomColor = function () {
    return '#' + (function (color) {
        return (color += '0123456789abcdef' [Math.floor(Math.random() * 16)])
        && (color.length == 6) ? color : arguments.callee(color);
      })('');
  }
  var getRandomSize = function () {
    var randomSize = parseInt((12 + Math.random() * 8));
    return randomSize;
  }

  function loadProductType() {
    var url = '<%=basePath%>/productType/list';
    $.post(url, function (data, status) {
      for (var i = 0; i < data.length; i++) {
        var item = '';
        item += '<a href="javascript:chooseType(' + data[i].id + ')" style="text-decoration: none">';
        item += '<span id="type' + data[i].id + '" class="tag" style="margin:7px;color:' + getRandomColor() + ';font-size:' + getRandomSize() + '">' + data[i].name + '</span>';
        item += '</a>';
        $(".storyTags").append(item);
      }
    })
  }
  var pageNo = 1;
  var pageSize = 100;
  $(function () {
    initStyle();
    loadProductType();
    loadGoods(pageNo++, pageSize, null);
    $("#loadMore").click(function () {
      $("#loadMore").hide();
      loadGoods(pageNo++, pageSize, $("#tag").val());
    })

    $('.market').mouseover(function () {
      $(this).css('background-color', '#cccccc');
    })
    $('.market').mouseout(function () {
      $(this).css('background-color', '#ffffff');
    })
  })
</script>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class=" col-xs-12 col-sm-12 col-md-1 col-lg-2"></div>
  <div class=" col-xs-12 col-sm-12 col-md-10 col-lg-8" style="padding: 0px;margin: 5px 0px;">
    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 5px">
      <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
        <div class="panel panel-default" style="border-radius: 0px;border: none;margin: 0px;box-shadow: none">
          <div class="panel-body" style="border: none;border-radius: 0px">
            <div class="storyTags">
              <a href="javascript:chooseType(0)" style="text-decoration: none">
                <span id="type0" class="tag badge">所有</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 5px 0px;">
      <div id="goods"></div>
      <div id="goodsArea">
        <table style="width: 100%">
          <tr>
            <td align="center" style="color: #d3d3d3;"><i
              class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载商品......
            </td>
          </tr>
        </table>
      </div>
      <table style="width: 100%;display: none;" id="loadMore">
        <tr>
          <td align="center" style="color: darkgrey;padding: 40px 0px">
            <a class="btn btn-info" style="text-decoration: none;padding: 7px 40px"
               href="javascript:void(0);">加载更多</a>
          </td>
        </tr>
      </table>
      <table style="width: 100%;display: none;" id="loadOver">
        <tr>
          <td align="center" style="color: darkgrey;padding: 40px 0px"></td>
        </tr>
      </table>
    </div>
  </div>
</div>
<input type="hidden" id="tag">
<jsp:include page="../../foot.jsp"></jsp:include>
</body>
</html>
