<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
  Object queryEmail = request.getParameter("email");
%>
<html>
<head>
  <title>货品管理 - 刘员外</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>
</head>
<script>
  function toPage(productId) {
    var queryEmail = '<%=queryEmail%>';
    if (queryEmail != undefined && queryEmail != null && queryEmail != '' && queryEmail != 'null') {
      // - 跳转到用户商品页面
      window.location = "<%=basePath%>/order/preadd?productId=" + productId;
    } else {
      // - 跳转到商品管理页面
      window.location = "<%=basePath%>/product/info?productId=" + productId;
    }
  }

  function buildGood(goodExt) {
    var type = goodExt.type;
    var typeDesc = goodExt.remark;
    var list = goodExt.list;
    var item = '';
    var temp = '';
    item += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 0px">'
    item += '<h3 style="color: #444444;padding: 10px">' + type + '</h3>'
    item += '<hr>'
    item += '</div>'
    for (var i = 0; i < list.length; i++) {
      var goodUrl = '';
      var url = list[i].url;
      var id = list[i].id;
      var name = list[i].name;
      var deleted = list[i].deleted;
      var sales = list[i].sales;
      var price = list[i].price;
      var unit = list[i].unit;
      var inventory = list[i].inventory;
      var remark = list[i].remark;
      if (url == null || url == '' || url == undefined || url == '<%=basePath%>/img/product.png') {
        goodUrl = '<%=basePath%>/img/product.png';
      } else {
        goodUrl = url + '-360x200';
      }
      item += '<a style="text-decoration: none" href="javascript:toPage(' + id + ');">';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="margin: 10px 0px;padding: 20px;background-color: #ffffff">';
      item += '<div class=" col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"style="margin: 5px 0px;padding: 0px ">';
      item += '<img class="img-responsive" src="' + goodUrl + '" alt="' + name + '">';
      item += '</div>';
      item += '<div class=" col-xs-9 col-sm-9 col-md-9 col-lg-9">';
      item += '<table style="width: 100%">';
      item += '<tr>';
      item += '<td style="padding: 5px;font-weight: bold">' + name + '</td>';
      item += '<td style="padding: 5px" align="right">';
      if (deleted == 1) {
        item += '<span class="label label-default">已下架</span>';
      } else {
        item += '<span class="label label-success">在售中</span>';
      }
      if (inventory == 0) {
        item += '&nbsp;&nbsp;<span class="label label-danger">已卖完</span>';
      }
      item += '</td>';
      item += '</tr>';
      item += '<tr>';
      item += '<td style="padding: 5px;color: red" colspan="2">￥<span style="font-size: large">' + price.toFixed(2) + '</span>/' + unit + '</td>';
      item += '</tr>';
      item += '<tr>';
      item += '<td colspan="2" style="padding: 5px;color: #999999" align="right"><small>库存：</small>' + inventory + '&nbsp;|&nbsp;<small>销量：</small>' + sales + '</td>';
      item += '</tr>';
      item += '</table>';
      item += '</div>';
      item += '</div>';
      item += '</a>';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">';
      item += '</div>';
      temp += item;
      item = '';
    }
    return temp;
  }

  // - 加载商品列表
  function loadGoods(pageNo, pageSize) {
    var url = "<%=basePath%>/product/mgrList";
    var email = '<%=email%>'
    if (queryEmail != undefined && queryEmail != null && queryEmail != '' && queryEmail != 'null') {
      email = queryEmail;
    }
    var param = {pageNo: pageNo, pageSize: pageSize, email: email};
    $.post(url, param, function (data, status) {
      var goods = data;
      $("#goodsArea").hide();
      if (goods.length > 0) {
        for (var i = 0; i < goods.length; i++) {
          $("#goods").append(buildGood(goods[i]));
        }
        if (goods.length == pageSize) {
          // - 还有更多数据
          $("#loadOver").hide();
          $("#loadMore").show();
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      } else {
        if (pageNo == 1) {
          // - 没有数据
          var noDataShow = '';
          noDataShow += '<div style="padding: 100px 0px;color: #cccccc" class="text-center"><h3>No goods Here!</h3></div>';
          $("#goods").empty();
          $("#goods").append(noDataShow);
        } else {
          // - 没有更多数据
          $("#loadMore").hide();
          $("#loadOver").show();
        }
      }
    })
  }
  var pageNo = 1;
  var queryEmail = '<%=queryEmail%>';
  $(function () {
    initStyle();
    if (queryEmail == undefined || queryEmail == null || queryEmail == '' || queryEmail == 'null') {
      if (!validateEmail('<%=email%>')) {
        return;
      }
    }
    $(".load").mousemove(function () {
      $(this).css("cursor", "pointer");
    })
    $(".load").mouseout(function () {
      $(this).css("cursor", "default");
    })

    loadGoods(pageNo++, 10);
    $("#loadMore").click(function () {
      loadGoods(pageNo++, 10);
    })
  })
</script>
<body style="background-color: #f9f9f9">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-2"></div>
  <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-8" style="padding: 0px;margin-top: 15px;">
    <div id="goodsArea">
      <table style="width: 100%">
        <tr>
          <td align="center" style="color: #d3d3d3;"><i class="icon-spinner icon-spin text-default-color"></i>&nbsp;正在加载商品......
          </td>
        </tr>
      </table>
    </div>
    <div id="goods"></div>
    <table style="width: 100%;display: none;" id="loadMore">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px">加载更多</td>
      </tr>
    </table>
    <table style="width: 100%;display: none;" id="loadOver">
      <tr>
        <td align="center" class="load" style="color: darkgrey;padding: 40px 0px"></td>
      </tr>
    </table>
  </div>
</div>
<jsp:include page="../../foot.jsp"></jsp:include>
<jsp:include page="../common/common-html.jsp"></jsp:include>
</body>
</html>
