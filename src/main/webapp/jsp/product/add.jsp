<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<!DOCTYPE HTML>
<html>
<head>
  <title>新增商品</title>
  <jsp:include page="../common/common-css.jsp"></jsp:include>
  <jsp:include page="../common/common-js.jsp"></jsp:include>

  <link rel="stylesheet" type="text/css" href="<%=basePath%>/plugin/simditor/simditor.css"/>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/module.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/hotkeys.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/uploader.min.js"></script>
  <script type="text/javascript" src="<%=basePath%>/plugin/simditor/simditor.min.js"></script>

  <style type="text/css">
    .preDesc {
      color: darkgray;
      width: 120px;
    }

    td {
      padding: 8px
    }

    input {
      padding: 5px;
    }
  </style>


</head>
<body style="background-color: #f4f7ed">
<jsp:include page="../../head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px">
    <div class="col-xs-0 col-sm-0 col-md-3 col-lg-4"></div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4"
         style="margin: 15px 0px;padding: 0px">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px" align="center">
        <input type="file" onchange="uploadFile(this);" name="filePath" id="filePath"
               style="display: none">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 15px;">
        <h4 style="color: #666666;font-weight: bold">新增商品</h4>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
           style="margin: 0px;padding: 10px;background-color: #ffffff;margin-top: 10px">
        <table style="width: 100%;border-bottom: 1px solid #ebebeb;">
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td colspan="2">
              <input type="text" id="name" style="width: 100%;border: none"
                     placeholder="商品名称" maxlength="15">
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td colspan="2">
              <input type="text" class="NumDecText"
                     style="width: 100%;border: none" id="originalPrice" placeholder="商品成本(元)"
                     maxlength="8">
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td colspan="2">
              <input type="text" class="NumDecText"
                     style="width: 100%;border: none" id="price" placeholder="商品售价(元)"
                     maxlength="8">
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td colspan="2">
              <input type="text" style="width: 100%;border: none"
                     onkeyup="this.value=this.value.replace(/\D/g,'')"
                     onafterpaste="this.value=this.value.replace(/\D/g,'')" id="inventory"
                     value="${shProduct.inventory}" maxlength="8" placeholder="商品库存">
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td class="preDesc" style="padding: 13px">商品单位</td>
            <td align="right">
              <select id="unit" style="border: none">
                <option value="1">斤</option>
                <option value="2" selected>份</option>
                <option value="3" selected>台</option>
                <option value="4" selected>件</option>
                <option value="5" selected>套</option>
              </select>
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td class="preDesc" style="padding: 13px">商品分类</td>
            <td align="right">
              <select id="productType" style="border: none;"></select>
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td style="padding: 13px;color: darkgrey">
              <a href="javascript:toUploadFile()" style="text-decoration: none" class="preDesc">
                <i class="icon-camera icon-small productImg">&nbsp;&nbsp;插入封面</i>
              </a>
            </td>
            <td align="right" style="padding-right: 20px;width: 20px">
              <i class="icon-angle-right"></i>
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #ebebeb;">
            <td colspan="2"style="padding: 0px">
              <textarea class="textarea" style="resize: none;width: 100%;border: none;padding: 8px"
                        rows="20" id="remark"
                        placeholder="输入商品描述"></textarea>
            </td>
          </tr>
        </table>
        <div class="btn-group" role="group" aria-label="..." style="margin: 20px">
          <button onclick="save()" class="btn btn-info">保&nbsp;存</button>
          <button type="button" class="btn btn-default" onclick="history.go(-1)">取&nbsp;消</button>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" class="isHomeImg" value="1">
<input type="hidden" id="productImg">
<jsp:include page="../common/common-html.jsp"></jsp:include>
<jsp:include page="../../foot.jsp"></jsp:include>
</body>


<script language="JavaScript">
  // - 新增商品
  function save() {
    $(".btn").blur();
    var name = $.trim($("#name").val());
    var price = $("#price").val();
    var originalPrice = $("#originalPrice").val();
    var unit = $.trim($("#unit").val());
    var remark = $.trim($("#remark").val());
    var inventory = $.trim($("#inventory").val());
    var productImg = $("#productImg").val();
    var productType = $("#productType").val();
    if (name == "") {
      alerErr("输入商品名称!");
      return;
    }
    if (originalPrice == null || originalPrice == '') {
      alerErr("输入商品成本!");
      return;
    }
    if(!validateNum(originalPrice)){
      alerErr('输入成本价不合法');
      return;
    }
    if (price == null || price == '') {
      alerErr("输入商品售价!");
      return;
    }
    if(!validateNum(price)){
      alerErr('输入单价不合法');
      return;
    }
    if (inventory == "") {
      alerErr("输入商品库存!");
      return;
    }
    if (productType == "") {
      alerErr("选择商品类别!");
      return;
    }
    if (Number(originalPrice) > Number(price)) {
      alerErr("成本价格不得高于售价!");
      return;
    }
    if (productImg == '') {
      alerErr("上传商品封面!");
      return;
    }
    $.ajaxSetup({
      contentType: 'application/json'
    });
    var param = {
      name: name,
      price: price,
      originalPrice: originalPrice,
      unit: unit,
      productType: productType,
      remark: remark,
      inventory: inventory,
      url: productImg
    };
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/product/add";
    $.post(url, JSON.stringify(param), function (data, status) {
      $("#loadModal").modal('hide');
      aler(data);
      if (data.code == 200) {
        window.location = "<%=basePath%>/success?backUrl=<%=basePath%>/goodMgr";
      }
    })
  }
  // - 上传图片
  function uploadFile(obj) {
    $("#loadModal").modal({
      keyboard: false,
      backdrop: 'static'
    });
    var url = "<%=basePath%>/img/upload";
    var isHomeImage = $(".isHomeImg").val();
    $.ajaxFileUpload({
      url: url,
      fileElementId: "filePath",//file标签的id,
      dateType: "text",
      success: function (data, status) {
        // - ajaxFileUpload这儿必须得转换以下
        data = jQuery.parseJSON(jQuery(data).text());
        if (data.code == 200) {
          var temp = data.data;
          if (isHomeImage == 0) {
            // - 不是上传封面
            insertImg(temp.downloadUrl + '-750x500');
          } else if (isHomeImage == 1) {
            // - 上传封面
            $("#productImg").val(temp.downloadUrl);
            $(".productImg").text('  ' + temp.downloadUrl);
          }
        } else {
          alerErr(data.msg);
        }
        $("#loadModal").modal('hide');
      },
      error: function (data, status, e) {
        $("#loadModal").modal('hide');
        $("#filePath").replaceWith($("#upload").clone(true));
        alerErr(e);
      }
    });
  }

  function loadProductType() {
    var url = '<%=basePath%>/productType/list';
    $.post(url, function (data, status) {
      var item = '';
      for (var i = 0; i < data.length; i++) {
        item = '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        $("#productType").append(item);
      }
    })
  }

  function toUploadFile() {
    $(".isHomeImg").val(1);
    $("#filePath").click();
  }

  function toInsertImg() {
    var email = '<%=email%>';
    if (!validateEmail(email)) {
      return;
    }
    $(".isHomeImg").val(0);
    $("#filePath").click();
  }

  $(function () {
    initStyle();
    var editor = new Simditor({
      textarea: $('#remark'),
      toolbar: [
        'title',
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'color',
        'ol',
        'ul',
        'link',
        'image',
        'hr',
        'indent',
        'outdent',
        'alignment'
      ],
      upload: {
        url: "<%=basePath%>/img/uploadForSimditor",
        params: {
          fileElementId: "filePath",
          dateType: "text"
        },
        fileKey: "filePath",
        leaveConfirm: '正在上传...'
      },
      pasteImage: true
    });
    editor.focus();
    editor.on('valuechanged', function (e, src) {
      $('img').addClass('img-responsive');
    })

    loadProductType();
    if (!validateEmail('<%=email%>')) {
      return;
    }
    $("button").click(function () {
      $(this).blur()
    });
  })
</script>
</html>
