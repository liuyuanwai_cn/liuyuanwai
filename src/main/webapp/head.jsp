<%@ page contentType="text/html;charset=UTF-8" %>
<%
  String basePath = request.getContextPath();
  String logo = "/img/user.png";
  Object temp = request.getSession().getAttribute("logo");
  if (temp != null) {
    logo = temp.toString() + "-120x120";
  }
  Object role = request.getSession().getAttribute("role");
%>
<head>
  <meta name="keywords" content="传播优质内容，分享美好'商品'"/>
  <meta name="description" content="传播优质内容，分享美好'商品'"/>
  <script>
    var _hmt = _hmt || [];
    (function () {
      var hm = document.createElement("script");
      hm.src = "//hm.baidu.com/hm.js?f098efe8dac25b31f3a8ef410f5cacd1";
      var s = document.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(hm, s);
    })();
  </script>

  <style type="text/css">
    a {
      color: #000000;
    }
  </style>
</head>
<nav class="navbar" style="background-color: #ffffff;border-bottom: 1px solid #eaeaea;">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
            aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar" style="background-color: #000000"></span>
      <span class="icon-bar" style="background-color: #000000"></span>
      <span class="icon-bar" style="background-color: #000000"></span>
    </button>
    <a class="navbar-brand" href="<%=basePath%>/" style="padding: 10px">
      <img src="<%=basePath%>/img/logo.png" class="img-responsive" style="width: 70px;">
    </a>
  </div>
  <div id="navbar" class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="<%=basePath%>/story">资讯</a></li>
      <li><a href="<%=basePath%>/goods">集市</a></li>
      <li><a href="<%=basePath%>/note">龙门阵</a></li>
      <li><a href="<%=basePath%>/links">关于</a></li>
      <li><a href="<%=basePath%>/search"><i class="icon-search">&nbsp;</i></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li style="margin-right: 20px">
        <a href="<%=basePath%>/set" style="padding: 10px 15px">
          <img src="<%=basePath%><%=logo%>" class="img-responsive img-circle" style="width: 30px;height: 30px">
        </a>
      </li>
    </ul>
  </div>
</nav>
