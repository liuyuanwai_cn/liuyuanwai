<%@ page contentType="text/html;charset=UTF-8" %>
<% String basePath = request.getContextPath(); %>
<div class="col-xs-12 col-sm-12 col-md-12 col-md-12 " align="center" style="margin: 40px 0px;color: darkgray">
  <table>
    <tr style="border: none">
      <td style="padding: 5px 0px;width: 70px">
        <a href="<%=basePath%>/"><img src="<%=basePath%>/img/logo.png" class="img-responsive" style="width: 60px"></a>
      </td>
      <td style="padding: 5px 0px;font-size: smaller;color: #444444">
        专注互联网生活体验
      </td>
    </tr>
    <tr style="border: none">
      <td align="center" style="padding: 5px 0px;font-size: smaller;color: #444444" colspan="2">
        &copy;2016&nbsp;liuyuanwai.cn.&nbsp;All&nbsp;Rights&nbsp;Reserved.&nbsp;&nbsp;<br class="visible-xs"><br
        class="visible-xs"> 蜀ICP备16019297号-1
      </td>
    </tr>
  </table>
</div>
