<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getContextPath();
  Object email = request.getSession().getAttribute("email");
%>
<!DOCTYPE html>
<html>
<head>
  <title>刘员外 - 传播优质内容，分享美好'商品'</title>

  <jsp:include page="jsp/common/common-css.jsp"></jsp:include>
  <jsp:include page="jsp/common/common-js.jsp"></jsp:include>

  <style type="text/css">
    a {
      text-decoration: none
    }

    a:hover {
      text-decoration: none
    }

    a:link {
      text-decoration: none
    }

    a:visited {
      text-decoration: none
    }

    * {
      margin: 0;
      padding: 0;
      border: 0;
    }

    .slide {
      padding: 40px 0px;
    }

  </style>
</head>

<body style="background-color: #f4f7ed;">
<jsp:include page="head.jsp"></jsp:include>
<div class="container-fluid" style="padding: 0px;">
  <div class="row-fluid" style="padding: 0px;">
    <!-- 代码 开始 -->
    <%--<div class="slider">--%>
    <%--<ul class="slides">--%>
    <%--<li class="slide" style="background-color: #663366;padding: 0px 0px 30px;">--%>
    <%--<table style="width: 100%;">--%>
    <%--<tr>--%>
    <%--<td align="center" valign="middle" style="padding: 0px 20px">--%>
    <%--<img src="" class="img-responsive">--%>
    <%--</td>--%>
    <%--</tr>--%>
    <%--</table>--%>
    <%--</li>--%>
    <%--</ul>--%>
    <%--</div>--%>
    <!-- 代码 结束 -->
    <%--商品--%>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;">
      <div class="col-xs-0 col-sm-0 col-md-1 col-lg-2 "></div>
      <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 story" style="padding: 5px 5px 0px;">
        <p class="text-center storyArea" style="padding: 100px 0px;color: #666666">
          <i class="icon-spinner icon-spin"></i>&nbsp;正在加载数据......
        </p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 product" style="padding: 5px;">
        <p class="text-center productArea" style="padding: 100px 0px;color: #666666">
          <i class="icon-spinner icon-spin"></i>&nbsp;正在加载数据......
        </p>
      </div>
    </div>
  </div>
</div>
<jsp:include page="foot.jsp"></jsp:include>
</body>
<script>

  function toPage(uuid) {
    window.location = "<%=basePath%>/story/info?uuid=" + uuid;
  }


  function toProduct(productId) {
    var url = '<%=basePath%>/order/preadd?productId=' + productId;
    window.location.href = url;
  }
  function syncLogin() {
    var email = "<%=email%>";
    if (email == null || email == "null" || email == "") {
      email = getCookie("email");
      if (email == null || email == "null" || email == "") {
        return;
      }
      var login = getCookie("login");
      if (login == null || login == "null" || login == "") {
        return;
      }
      var url = "<%=basePath%>/user/fakeLogin";
      var param = {email: email, login: login};
      $.post(url, param, function (data, status) {
        data = JSON.parse(data);
        if (data.code == 200) {
          // - 异步登录成功设置email隐藏域的值
          $("#email").val(email);
          // - 同步更新inCount
          setCookie("inCount", data.data.inCount);
        }
      })
    }
  }

  function buildProduct(list) {
    var item = '';
    var temp = '';
    for (var i = 0; i < list.length; i++) {
      var goodUrl = '';
      var url = list[i].url;
      var id = list[i].id;
      var name = list[i].name;
      var deleted = list[i].deleted;
      var sales = list[i].sales;
      var price = list[i].price;
      var unit = list[i].unit;
      var inventory = list[i].inventory;
      if (url == null || url == '' || url == undefined || url == '<%=basePath%>/img/product.png') {
        goodUrl = '<%=basePath%>/img/product.png';
      } else {
        goodUrl = url + '-360x200';
      }
      item += '<a style="text-decoration: none" href="javascript:void(0);" onclick="javascript:toProduct(' + id + ');">';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="margin: 5px 0px;padding: 20px 10px 20px 20px;background-color: #ffffff" >';
      item += '<div class=" col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center"style="margin: 5px 0px;padding: 0px ">';
      item += '<img class="img-responsive" src="' + goodUrl + '" alt="' + name + '">';
      item += '</div>';
      item += '<div class=" col-xs-8 col-sm-8 col-md-8 col-lg-8">';
      item += '<table style="width: 100%">';
      item += '<tr>';
      item += '<td style="padding: 5px;font-weight: bold;color: #444444">' + name + '</td>';
      item += '<td style="padding: 5px" align="right">';
//      if (inventory == 0) {
//        item += '&nbsp;&nbsp;<span class="label label-default">已卖完</span>';
//      } else {
//        item += '<span class="label label-success">在售中</span>';
//      }
      item += '</td>';
      item += '</tr>';
//      item += '<tr>';
//      item += '<td colspan="2" style="padding: 5px;color: red">￥<span style="font-size: larger">' + price.toFixed(2) + '</span>/' + unit + '</td>';
//      item += '</tr>';
      item += '<tr>';
      item += '<td colspan="2" style="padding: 5px" align="right">';
      item += '<small style="color: #999999;">浏览：<span>' + list[i].readCount + '</span>';
//      item += '&nbsp;&nbsp;|&nbsp;&nbsp;销量：<span>' + list[i].sales + '</span>';
      item += '</small>';
      item += '</td>';
      item += '</tr>';
      item += '</table>';
      item += '</div>';
      item += '</div>';
      item += '</a>';
      item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">';
      item += '</div>';
      temp += item;
      item = '';
    }
    return temp;
  }
  function calIncome(productId) {
    var url = '<%=basePath%>/product/calIncome?productId=' + productId;
    $.post(url, function (data, status) {
      if (status == 'success') {
        $("#part1").text(data.part1);
        $("#part2").text(data.part2);
        $("#incomeModal").modal();
      } else {
        alerErr('计算失败');
      }
    })
  }
  function buildStory(title, subTitle, readCount, imgUrl, uuid, tag, logo, name, email) {
    imgUrl += '-580x350';
    if (logo == '' || logo == undefined) {
      logo = '<%=basePath%>/img/user.png';
    } else {
      logo += '-120x120';
    }
    var alias = name;
    if (alias == '' || alias == undefined) {
      alias = email;
    }
    var item = '';
    item += '<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12"style="padding: 30px 5px 30px 20px;margin: 5px 0px;background-color: #ffffff">';
    item += '<a style="text-decoration: none;" href="javascript:void(0);" onclick="javascript:toPage(\'' + uuid + '\');">';
    item += '<div class=" col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center"style="padding: 0px ;">';
    item += '<img class="img-responsive" src="' + imgUrl + '">';
    item += '</div>';
    item += '<div class=" col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    item += '<table style="width: 100%">';
    item += '<tr>';
    item += '<td><h4 style="margin-top: 2px;font-weight: bold;">' + title + '</h4></td>';
    item += '</tr>';
    item += '<tr>';
    item += '<td><p class="hidden-xs"style="color: #999999">' + subTitle + '</p></td>';
    item += '</tr>';
    item += '<tr>';
    item += '<td>';
    item += '<small style="color: #999999;padding: 0px"align="right">作者：' + alias + '&nbsp;|&nbsp;阅读：<span style="">' + readCount + '</span></small>';
    item += '</td>';
    item += '</tr>';
    item += '<tr><td style="padding-top: 10px"><span class="badge" style="background-color: #CC66CC">' + tag + '</span></td></tr>';
    item += '</table>';
    item += '</div>';
    item += '</a>';
    item += '</div>';
    return item;
  }


  function loadProduct() {
    var url = "<%=basePath%>/product/list";
    var param = {pageNo: 1, pageSize: 10};
    $.post(url, param, function (data, status) {
      $(".productArea").remove();
      if (data.length == 0) {
        var temp = '<div style="padding: 100px 0px;color: #d3d3d3;"class="text-center">No Goods Here</div>';
        $('.product').append(temp);
      } else {
        var clazz = '.product';
        $(clazz).append(buildProduct(data));
      }
    })
  }
  function loadStory() {
    var url = "<%=basePath%>/story/list";
    var param = {pageNo: 1, pageSize: 10, storyType: 0};
    $.post(url, param, function (data, status) {
      $(".storyArea").remove();
      if (data.length == 0) {
        var temp = '<div style="padding: 100px 0px;color: #d3d3d3;"class="text-center">All goods will be Here!</div>';
        $('.story').append(temp);
      } else {
        for (var i = 0; i < data.length; i++) {
          var clazz = '.story';
          $(clazz).append(buildStory(data[i].title, data[i].subTitle, data[i].readCount, data[i].imgUrl, data[i].uuid, data[i].tag, data[i].logo, data[i].name, data[i].email));
        }
      }
    })
  }

  $(function () {
    initStyle();
    syncLogin();
    loadProduct();
    loadStory();
//    $('.slider').glide({
////      autoplay: true,//是否自动播放 默认值 true如果不需要就设置此值
//      animationTime: 500, //动画过度效果，只有当浏览器支持CSS3的时候生效
//      arrows: false, //是否显示左右导航器
//      arrowRightText: ">",//定义左右导航器文字或者符号也可以是类
//      arrowLeftText: "<",
//      nav: true, //主导航器也就是本例中显示的小方块
//      navCenter: true, //主导航器位置是否居中
//      navClass: "slider-nav",//主导航器外部div类名
//      navItemClass: "slider-nav__item", //本例中小方块的样式
//      navCurrentItemClass: "slider-nav__item--current" //被选中后的样式
//    });
  })
</script>
</html>
