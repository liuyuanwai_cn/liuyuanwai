package com.pojo;

import java.util.Date;

public class ShAttention {
    private Integer id;

    private String email;

    private String attentionEmail;

    private Date createTime;

    private Integer deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getAttentionEmail() {
        return attentionEmail;
    }

    public void setAttentionEmail(String attentionEmail) {
        this.attentionEmail = attentionEmail == null ? null : attentionEmail.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}