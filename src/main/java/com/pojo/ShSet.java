package com.pojo;

import java.util.Date;

public class ShSet {
    private Integer id;

    private String email;

    private Integer storyNotify;

    private Integer noteNotify;

    private Date createTime;

    private Date updateTime;

    private Integer attentionNotify;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Integer getStoryNotify() {
        return storyNotify;
    }

    public void setStoryNotify(Integer storyNotify) {
        this.storyNotify = storyNotify;
    }

    public Integer getNoteNotify() {
        return noteNotify;
    }

    public void setNoteNotify(Integer noteNotify) {
        this.noteNotify = noteNotify;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getAttentionNotify() {
        return attentionNotify;
    }

    public void setAttentionNotify(Integer attentionNotify) {
        this.attentionNotify = attentionNotify;
    }
}