package com.dto;

import com.domain.page.Paginator;
import com.pojo.ShCompany;

/**
 * Created by Administrator on 2016/4/17 0017.
 */
public class UserSortDto {
    private ShCompany shCompany;
    private Paginator paginator;

    public ShCompany getShCompany() {
        return shCompany;
    }

    public void setShCompany(ShCompany shCompany) {
        this.shCompany = shCompany;
    }

    public Paginator getPaginator() {
        return paginator;
    }

    public void setPaginator(Paginator paginator) {
        this.paginator = paginator;
    }
}
