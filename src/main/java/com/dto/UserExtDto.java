package com.dto;

import com.dto.statistic.SortDto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2016/5/2 0002.
 */
public class UserExtDto {
  private Integer id;

  private String name;

  private Integer inCountSort;

  private String cityId;

  private String districtId;

  private String communityId;

  private String addr;

  private String companyAddr;

  private String email;

  private String phone;

  private String iiuv;

  private String remark;

  private Integer deleted;

  private Integer checkStatus;

  private String logo;

  private BigDecimal credits;

  private Date createtime;

  private String workontime;

  private String workofftime;

  private BigDecimal inCount;

  private String password;

  private Date updatetime;

  private String role;

  private Integer age;

  private String code;

  private String referralCode;
  private BigDecimal income;
  private String weixinQrcode;

  private String alipayQrcode;
  private Integer storyNotify;
  private Integer noteNotify;
  private Integer attentionNotify;
  private SortDto sortDto;

  public Integer getAttentionNotify() {
    return attentionNotify;
  }

  public void setAttentionNotify(Integer attentionNotify) {
    this.attentionNotify = attentionNotify;
  }

  public SortDto getSortDto() {
    return sortDto;
  }

  public void setSortDto(SortDto sortDto) {
    this.sortDto = sortDto;
  }

  public Integer getStoryNotify() {
    return storyNotify;
  }

  public void setStoryNotify(Integer storyNotify) {
    this.storyNotify = storyNotify;
  }

  public Integer getNoteNotify() {
    return noteNotify;
  }

  public void setNoteNotify(Integer noteNotify) {
    this.noteNotify = noteNotify;
  }

  public String getWeixinQrcode() {
    return weixinQrcode;
  }

  public void setWeixinQrcode(String weixinQrcode) {
    this.weixinQrcode = weixinQrcode;
  }

  public String getAlipayQrcode() {
    return alipayQrcode;
  }

  public void setAlipayQrcode(String alipayQrcode) {
    this.alipayQrcode = alipayQrcode;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getInCountSort() {
    return inCountSort;
  }

  public void setInCountSort(Integer inCountSort) {
    this.inCountSort = inCountSort;
  }

  public String getCityId() {
    return cityId;
  }

  public void setCityId(String cityId) {
    this.cityId = cityId;
  }

  public String getDistrictId() {
    return districtId;
  }

  public void setDistrictId(String districtId) {
    this.districtId = districtId;
  }

  public String getCommunityId() {
    return communityId;
  }

  public void setCommunityId(String communityId) {
    this.communityId = communityId;
  }

  public String getAddr() {
    return addr;
  }

  public void setAddr(String addr) {
    this.addr = addr;
  }

  public String getCompanyAddr() {
    return companyAddr;
  }

  public void setCompanyAddr(String companyAddr) {
    this.companyAddr = companyAddr;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getIiuv() {
    return iiuv;
  }

  public void setIiuv(String iiuv) {
    this.iiuv = iiuv;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Integer getDeleted() {
    return deleted;
  }

  public void setDeleted(Integer deleted) {
    this.deleted = deleted;
  }

  public Integer getCheckStatus() {
    return checkStatus;
  }

  public void setCheckStatus(Integer checkStatus) {
    this.checkStatus = checkStatus;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public BigDecimal getCredits() {
    return credits;
  }

  public void setCredits(BigDecimal credits) {
    this.credits = credits;
  }

  public Date getCreatetime() {
    return createtime;
  }

  public void setCreatetime(Date createtime) {
    this.createtime = createtime;
  }

  public String getWorkontime() {
    return workontime;
  }

  public void setWorkontime(String workontime) {
    this.workontime = workontime;
  }

  public String getWorkofftime() {
    return workofftime;
  }

  public void setWorkofftime(String workofftime) {
    this.workofftime = workofftime;
  }

  public BigDecimal getInCount() {
    return inCount;
  }

  public void setInCount(BigDecimal inCount) {
    this.inCount = inCount;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Date getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(Date updatetime) {
    this.updatetime = updatetime;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getReferralCode() {
    return referralCode;
  }

  public void setReferralCode(String referralCode) {
    this.referralCode = referralCode;
  }

  public BigDecimal getIncome() {
    return income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }
}
