package com.dto;

import com.pojo.ShRole;
import com.pojo.ShRoleMenuRel;

import java.util.List;

/**
 * Created by Administrator on 2016/2/18 0018.
 * 角色菜单DTO
 */
public class RoleMenuDto {
    /// - 角色对象
    private ShRole shRole;
    // - 角色拥有的菜单ID，逗号分割
    private String menuIds;

    public ShRole getShRole() {
        return shRole;
    }

    public void setShRole(ShRole shRole) {
        this.shRole = shRole;
    }

    public String getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(String menuIds) {
        this.menuIds = menuIds;
    }
}
