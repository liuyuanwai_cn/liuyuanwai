package com.dto;

import java.util.List;

/**
 * Created by Huoyunren on 2016-07-22.
 */
public class BulletinDto {
  private String date;
  private List<StoryExtDto> list;

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public List<StoryExtDto> getList() {
    return list;
  }

  public void setList(List<StoryExtDto> list) {
    this.list = list;
  }
}
