package com.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Huoyunren on 2016/5/25.
 */
public class AdviceDto {
  private Integer id;
  private String email;
  private String content;
  private String name;
  private String createtime;
  private Integer status;
  private String img;
  private BigDecimal fee;
  private String address;
  private String dealEmail;
  private String dealTime;
  private String uuid;
  private Integer msgType;
  private Integer isShowMobile;
  private String endTime;
  private String phone;
  private BigDecimal dealPrice;

  public BigDecimal getDealPrice() {
    return dealPrice;
  }

  public void setDealPrice(BigDecimal dealPrice) {
    this.dealPrice = dealPrice;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getDealEmail() {
    return dealEmail;
  }

  public void setDealEmail(String dealEmail) {
    this.dealEmail = dealEmail;
  }
  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public Integer getMsgType() {
    return msgType;
  }

  public void setMsgType(Integer msgType) {
    this.msgType = msgType;
  }

  public Integer getIsShowMobile() {
    return isShowMobile;
  }

  public void setIsShowMobile(Integer isShowMobile) {
    this.isShowMobile = isShowMobile;
  }

  public String getCreatetime() {
    return createtime;
  }

  public void setCreatetime(String createtime) {
    this.createtime = createtime;
  }

  public String getDealTime() {
    return dealTime;
  }

  public void setDealTime(String dealTime) {
    this.dealTime = dealTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
}
