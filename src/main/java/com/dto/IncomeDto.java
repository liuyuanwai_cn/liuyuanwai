package com.dto;

import java.math.BigDecimal;

/**
 * Created by admin on 2016/6/10.
 */
public class IncomeDto {
    // - 派息
    private BigDecimal part1;
    // - 提成
    private BigDecimal part2;

    public BigDecimal getPart1() {
        return part1;
    }

    public void setPart1(BigDecimal part1) {
        this.part1 = part1;
    }

    public BigDecimal getPart2() {
        return part2;
    }

    public void setPart2(BigDecimal part2) {
        this.part2 = part2;
    }
}
