package com.dto;

/**
 * Created by Huoyunren on 2016-09-07.
 */
public class AttentionDto {
  private String email;
  private String name;
  private String logo;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }
}
