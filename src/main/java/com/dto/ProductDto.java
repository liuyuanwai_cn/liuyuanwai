package com.dto;

import com.pojo.ShProduct;

import java.util.List;

/**
 * Created by Huoyunren on 2016-07-22.
 */
public class ProductDto {
  private String type;
  private String remark;
  private List<ShProduct> list;

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<ShProduct> getList() {
    return list;
  }

  public void setList(List<ShProduct> list) {
    this.list = list;
  }
}
