package com.dto.simditor;

/**
 * Created by Huoyunren on 2016-09-26.
 */
public class SimditorDto {

  private boolean success;
  private String msg;
  private String file_path;


  public SimditorDto() {
    msg = "OK";
    success = Boolean.TRUE;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getFile_path() {
    return file_path;
  }

  public void setFile_path(String file_path) {
    this.file_path = file_path;
  }
}
