package com.dto;

/**
 * Created by Administrator on 2016/5/1 0001.
 */
public class CompanyExtDto {

    // - 我的邮箱
    private String email;
    // - 我的推荐人邮箱
    private String referralEmail;
    // - 推荐人姓名
    private String referralName;

    public String getReferralName() {
        return referralName;
    }

    public void setReferralName(String referralName) {
        this.referralName = referralName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReferralEmail() {
        return referralEmail;
    }

    public void setReferralEmail(String referralEmail) {
        this.referralEmail = referralEmail;
    }
}
