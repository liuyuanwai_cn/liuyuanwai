package com.dto;

import com.pojo.ShCompany;
import com.pojo.ShProduct;
import com.pojo.ShStory;

import java.util.List;

/**
 * Created by Huoyunren on 2016-08-24.
 */
public class SearchDto {

  private List<ShStory> stories;
  private List<ShProduct> products;
  private List<ShCompany> companies;

  public List<ShStory> getStories() {
    return stories;
  }

  public void setStories(List<ShStory> stories) {
    this.stories = stories;
  }

  public List<ShProduct> getProducts() {
    return products;
  }

  public void setProducts(List<ShProduct> products) {
    this.products = products;
  }

  public List<ShCompany> getCompanies() {
    return companies;
  }

  public void setCompanies(List<ShCompany> companies) {
    this.companies = companies;
  }
}
