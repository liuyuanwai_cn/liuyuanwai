package com.dto.statistic;

/**
 * Created by Administrator on 2016/2/1 0001.
 * 区域员工数统计
 *
 */
public class NameValueDto {

    private Integer value;
    private String name;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
