package com.dto.statistic;

import java.util.List;

/**
 * Created by Huoyunren on 2016-08-18.
 */
public class SortUserDto {
  private SortDto sortDto;
  private List<SortDto> list;

  public SortDto getSortDto() {
    return sortDto;
  }

  public void setSortDto(SortDto sortDto) {
    this.sortDto = sortDto;
  }

  public List<SortDto> getList() {
    return list;
  }

  public void setList(List<SortDto> list) {
    this.list = list;
  }
}
