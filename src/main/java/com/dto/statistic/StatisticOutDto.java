package com.dto.statistic;

import java.util.List;

/**
 * Created by Huoyunren on 2016/5/12.
 */
public class StatisticOutDto {
  // - y轴
  private List<Integer> yAixs;
  // - X轴
  private List<String> xAixs;

  public List<Integer> getyAixs() {
    return yAixs;
  }

  public void setyAixs(List<Integer> yAixs) {
    this.yAixs = yAixs;
  }

  public List<String> getxAixs() {
    return xAixs;
  }

  public void setxAixs(List<String> xAixs) {
    this.xAixs = xAixs;
  }
}
