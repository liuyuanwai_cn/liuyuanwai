package com.dto.statistic;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2016/1/23 0023.
 * 统计数据输出
 */
public class StatisticOutputDto {

    // - x轴坐标名称
    private String name;
    // - x轴对应数据
    private BigDecimal data;
    // - x轴对应数据
    private BigDecimal data1;

    public BigDecimal getData1() {
        return data1;
    }

    public void setData1(BigDecimal data1) {
        this.data1 = data1;
    }

    public BigDecimal getData() {
        return data;
    }

    public void setData(BigDecimal data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
