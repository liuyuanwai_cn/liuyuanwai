package com.dto.statistic;

/**
 * Created by Administrator on 2016/2/1 0001.
 * 区域员工数统计
 *
 */
public class IdCountDto {

    private Integer id;
    private Integer count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
