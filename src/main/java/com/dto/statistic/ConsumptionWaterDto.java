package com.dto.statistic;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Huoyunren on 2016/4/15.
 * 消费流水
 */
public class ConsumptionWaterDto {
  // - 平台流水
  private List<BigDecimal> platWateryAixs;
  // - 平台成本
  private List<BigDecimal> platCostyAixs;
  // - 平台收益
  private List<BigDecimal> platIncomeyAixs;
  // - 我的流水
  private List<BigDecimal> selfWateryAixs;
  // - 用户数
  private List<Integer> yAixs;
  // - X日期轴
  private List<String> xAixs;
  // - 总流水
  private BigDecimal platWater;
  // - 总成本
  private BigDecimal platCost;
  // - 总收益
  private BigDecimal platIncome;

  public List<Integer> getyAixs() {
    return yAixs;
  }

  public void setyAixs(List<Integer> yAixs) {
    this.yAixs = yAixs;
  }

  public BigDecimal getPlatWater() {
    return platWater;
  }

  public void setPlatWater(BigDecimal platWater) {
    this.platWater = platWater;
  }

  public BigDecimal getPlatCost() {
    return platCost;
  }

  public void setPlatCost(BigDecimal platCost) {
    this.platCost = platCost;
  }

  public BigDecimal getPlatIncome() {
    return platIncome;
  }

  public void setPlatIncome(BigDecimal platIncome) {
    this.platIncome = platIncome;
  }

  public List<BigDecimal> getPlatCostyAixs() {
    return platCostyAixs;
  }

  public void setPlatCostyAixs(List<BigDecimal> platCostyAixs) {
    this.platCostyAixs = platCostyAixs;
  }

  public List<BigDecimal> getPlatIncomeyAixs() {
    return platIncomeyAixs;
  }

  public void setPlatIncomeyAixs(List<BigDecimal> platIncomeyAixs) {
    this.platIncomeyAixs = platIncomeyAixs;
  }

  public List<BigDecimal> getPlatWateryAixs() {
    return platWateryAixs;
  }

  public void setPlatWateryAixs(List<BigDecimal> platWateryAixs) {
    this.platWateryAixs = platWateryAixs;
  }

  public List<BigDecimal> getSelfWateryAixs() {
    return selfWateryAixs;
  }

  public void setSelfWateryAixs(List<BigDecimal> selfWateryAixs) {
    this.selfWateryAixs = selfWateryAixs;
  }

  public List<String> getxAixs() {
    return xAixs;
  }

  public void setxAixs(List<String> xAixs) {
    this.xAixs = xAixs;
  }
}
