package com.dto.statistic;

/**
 * Created by Huoyunren on 2016/5/6.
 */
public class ShopExtDto {
  private Integer id;

  private String name;

  private String url;

  private String addr;

  private String mobile;

  private String phone;

  private String remark;

  private String ad;

  private Integer status;

  private String email;
}
