package com.dto.statistic;

import java.math.BigDecimal;

/**
 * Created by Huoyunren on 2016/5/10.
 */
public class IncomeGroupByShopDto {
  private Integer shopId;
  private String shopName;
  private BigDecimal income;
  private String addr;

  public String getAddr() {
    return addr;
  }

  public void setAddr(String addr) {
    this.addr = addr;
  }

  public Integer getShopId() {
    return shopId;
  }

  public void setShopId(Integer shopId) {
    this.shopId = shopId;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public BigDecimal getIncome() {
    return income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }
}
