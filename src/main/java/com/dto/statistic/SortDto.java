package com.dto.statistic;

import java.math.BigDecimal;

/**
 * Created by Huoyunren on 2016-08-17.
 */
public class SortDto {
  private String email;
  private String name;
  private String logo;
  private BigDecimal income;
  private BigDecimal adviceCount;
  private BigDecimal storyCount;
  private BigDecimal storyReadCount;
  private BigDecimal productCount;
  private BigDecimal productReadCount;
  private BigDecimal productSales;
  private BigDecimal inviteCount;
  private BigDecimal totalScore;
  private Integer fans;
  private Integer attentionCount;

  public Integer getFans() {
    return fans;
  }

  public void setFans(Integer fans) {
    this.fans = fans;
  }

  public Integer getAttentionCount() {
    return attentionCount;
  }

  public void setAttentionCount(Integer attentionCount) {
    this.attentionCount = attentionCount;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public BigDecimal getIncome() {
    return income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }

  public BigDecimal getAdviceCount() {
    return adviceCount;
  }

  public void setAdviceCount(BigDecimal adviceCount) {
    this.adviceCount = adviceCount;
  }

  public BigDecimal getStoryCount() {
    return storyCount;
  }

  public void setStoryCount(BigDecimal storyCount) {
    this.storyCount = storyCount;
  }

  public BigDecimal getStoryReadCount() {
    return storyReadCount;
  }

  public void setStoryReadCount(BigDecimal storyReadCount) {
    this.storyReadCount = storyReadCount;
  }

  public BigDecimal getProductCount() {
    return productCount;
  }

  public void setProductCount(BigDecimal productCount) {
    this.productCount = productCount;
  }

  public BigDecimal getProductReadCount() {
    return productReadCount;
  }

  public void setProductReadCount(BigDecimal productReadCount) {
    this.productReadCount = productReadCount;
  }

  public BigDecimal getProductSales() {
    return productSales;
  }

  public void setProductSales(BigDecimal productSales) {
    this.productSales = productSales;
  }

  public BigDecimal getInviteCount() {
    return inviteCount;
  }

  public void setInviteCount(BigDecimal inviteCount) {
    this.inviteCount = inviteCount;
  }

  public BigDecimal getTotalScore() {
    return totalScore;
  }

  public void setTotalScore(BigDecimal totalScore) {
    this.totalScore = totalScore;
  }
}
