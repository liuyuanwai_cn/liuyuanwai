package com.dto;

import java.util.Date;
import java.util.List;

/**
 * Created by Huoyunren on 2016-06-24.
 */
public class WordsDto {
  private String email;
  private String logo;
  private String name;
  private Date createTime;
  private String content;
  private String uuid;
  private List<NoteDto> notes;

  public List<NoteDto> getNotes() {
    return notes;
  }

  public void setNotes(List<NoteDto> notes) {
    this.notes = notes;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
}
