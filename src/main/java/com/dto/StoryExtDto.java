package com.dto;


/**
 * Created by Huoyunren on 2016-07-01.
 */
public class StoryExtDto {
  private Integer id;
  private String title;
  private String subTitle;
  private Integer readCount;
  private String imgUrl;
  private String email;
  private String uuid;
  private String createTime;
  private String content;
  private String name;
  private String logo;
  private String remark;
  private String tag;
  private String weixinQrcode;
  private String alipayQrcode;
  private Integer dataVersion;

  public Integer getDataVersion() {
    return dataVersion;
  }

  public void setDataVersion(Integer dataVersion) {
    this.dataVersion = dataVersion;
  }

  public String getWeixinQrcode() {
    return weixinQrcode;
  }

  public void setWeixinQrcode(String weixinQrcode) {
    this.weixinQrcode = weixinQrcode;
  }

  public String getAlipayQrcode() {
    return alipayQrcode;
  }

  public void setAlipayQrcode(String alipayQrcode) {
    this.alipayQrcode = alipayQrcode;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubTitle() {
    return subTitle;
  }

  public void setSubTitle(String subTitle) {
    this.subTitle = subTitle;
  }

  public Integer getReadCount() {
    return readCount;
  }

  public void setReadCount(Integer readCount) {
    this.readCount = readCount;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }


  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }
}
