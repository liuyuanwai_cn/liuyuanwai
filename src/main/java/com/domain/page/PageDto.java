package com.domain.page;

/**
 * Created by Huoyunren on 2016/1/19.
 * 分页入参
 */
public class PageDto {

  private int limit;
  private int page;

  // - 查询关键字
  private String key;

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
