package com.domain.page;

/**
 * Created by Huoyunren on 2016/1/21.
 */
public class Paginator {

  // - 当前页数
  private int page;
  // - 每页条数
  private int limit;
  // - 总页数
  private int totalCount;
  // - 数据
  private Object items;

  // - 查询关键字
  private String queryKey;

  private Paginator(int totalCount, Object items) {
    this.totalCount = totalCount;
    this.items = items;
  }

  public static Paginator instance(int totalCount,Object items){
    return new Paginator(totalCount,items);
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(int totalCount) {
    this.totalCount = totalCount;
  }

  public Object getItems() {
    return items;
  }

  public void setItems(Object items) {
    this.items = items;
  }

  public String getQueryKey() {
    return queryKey;
  }

  public void setQueryKey(String queryKey) {
    this.queryKey = queryKey;
  }
}
