package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/28 0028.
 * 提现码使用状态
 */
public enum CashCodeEnum {
    UnUsed(0,"未使用"),
    Used(1,"已使用"),
    TimeOut(2,"超时失效"),
    ;

    private Integer code;
    private String msg;

    CashCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public static String queryStatusVal(int code){
        for (CashCodeEnum roleEnum : CashCodeEnum.values()){
            if (roleEnum.getCode() == code){
                return roleEnum.getMsg();
            }
        }
        return "-";
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
