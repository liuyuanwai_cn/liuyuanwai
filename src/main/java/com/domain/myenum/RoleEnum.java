package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/28 0028.
 */
public enum RoleEnum {
    SuperAdmin(1,"超级管理员"),
    Admin(2,"管理员"),
    Customer(3,"普通用户"),
    ;

    private Integer code;
    private String msg;

    RoleEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public static String queryStatusVal(int code){
        for (RoleEnum roleEnum : RoleEnum.values()){
            if (roleEnum.getCode() == code){
                return roleEnum.getMsg();
            }
        }
        return "-";
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
