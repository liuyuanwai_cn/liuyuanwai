package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/20 0020.
 * 订单状态
 */
public enum OrderStatusEnum {

    // - 卖家处理
    Wait(1,"待确认"),
    Accepted(2,"已接单"),
    Sended(3,"派送中"),
    Finished(4,"完成"),
    Refused(5,"拒绝"),
    // - 买家处理
    Canceled(6,"撤单"),
    TimeOut(7,"超时"),


    ;

    private int code;
    private String value;

    OrderStatusEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public static String queryStatusVal(int code){
        for (OrderStatusEnum orderStatusEnum : OrderStatusEnum.values()){
            if (orderStatusEnum.getCode() == code){
                return orderStatusEnum.getValue();
            }
        }
        return "-";
    }
}
