package com.domain.myenum;

/**
 * Created by Huoyunren on 2016/2/2.
 * 审核状态枚举
 */
public enum CheckStatusEnum {

  WaitCheck(0,"等待审核"),
  CheckPass(1,"审核通过"),
  CheckFailure(2,"审核未通过")
  ;


  private int code;
  private String value;

  CheckStatusEnum(int code, String value) {
    this.code = code;
    this.value = value;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  /**
   * 根据CODE得到VALUE
   * @param code
   * @return
   */
  public static final String queryValueByCode(int code){
    String temp = "";
    for (CheckStatusEnum checkStatusEnum:CheckStatusEnum.values()){
      if (checkStatusEnum.getCode() == code){
        temp = checkStatusEnum.getValue();
        break;
      }
    }
    return temp;
  }
}
