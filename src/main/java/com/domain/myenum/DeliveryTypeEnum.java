package com.domain.myenum;

/**
 * Created by Administrator on 2016/4/26 0026.
 */
public enum DeliveryTypeEnum {
    GetBySelf(1,"预约到店"),
    SendHome(2,"送货上门")


    ;

    private int code;
    private String value;

    DeliveryTypeEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String queryStatusVal(int code){
        for (DeliveryTypeEnum deliveryTypeEnum : DeliveryTypeEnum.values()){
            if (deliveryTypeEnum.getCode() == code){
                return deliveryTypeEnum.getValue();
            }
        }
        return "-";
    }
}
