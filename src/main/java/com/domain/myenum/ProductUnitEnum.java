package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/20 0020.
 * 商品单位
 */
public enum ProductUnitEnum {

    Catty(1,"斤"),
    Piece(2,"份"),
    plat(3,"台"),
    unit(4,"件"),
    cover(5,"套"),


    ;

    private int code;
    private String value;

    ProductUnitEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public static String queryStatusVal(int code){
        for (ProductUnitEnum productUnitEnum : ProductUnitEnum.values()){
            if (productUnitEnum.getCode() == code){
                return productUnitEnum.getValue();
            }
        }
        return "-";
    }
}
