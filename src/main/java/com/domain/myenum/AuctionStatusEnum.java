package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/20 0020.
 * 拍卖状态
 */
public enum AuctionStatusEnum {

    On(0,"进行中"),
    Over(1,"结束"),
    TimeOut(2,"超时过期"),
    ;

    private int code;
    private String value;

    AuctionStatusEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public static String queryStatusVal(int code){
        for (AuctionStatusEnum productUnitEnum : AuctionStatusEnum.values()){
            if (productUnitEnum.getCode() == code){
                return productUnitEnum.getValue();
            }
        }
        return "-";
    }
}
