package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/20 0020.
 * 支付状态
 */
public enum PayStatusEnum {

    NotPay(0,"未支付"),
    PayOK(1,"已支付")


    ;

    private int code;
    private String value;

    PayStatusEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String queryStatusVal(int code){
        for (PayStatusEnum payStatusEnum : PayStatusEnum.values()){
            if (payStatusEnum.getCode() == code){
                return payStatusEnum.getValue();
            }
        }
        return "-";
    }
}
