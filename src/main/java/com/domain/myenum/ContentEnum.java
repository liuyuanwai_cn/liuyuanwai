package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/28 0028.
 */
public enum ContentEnum {
    Content(0,"文章"),
    Bulletin(1,"公告"),
    ;

    private Integer code;
    private String msg;

    ContentEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public static String queryStatusVal(int code){
        for (ContentEnum roleEnum : ContentEnum.values()){
            if (roleEnum.getCode() == code){
                return roleEnum.getMsg();
            }
        }
        return "-";
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
