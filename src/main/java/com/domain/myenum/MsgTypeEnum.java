package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/20 0020.
 * 支付方式
 */
public enum MsgTypeEnum {

    Reward(0,"悬赏"),
    Say(1,"说说"),
    Auction(2,"拍卖"),
    DoctorInfo(3,"问诊")
    ;

    private int code;
    private String value;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    MsgTypeEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public String queryStatusVal(int code){
        for (MsgTypeEnum payTypeEnum : MsgTypeEnum.values()){
            if (payTypeEnum.getCode() == code){
                return payTypeEnum.getValue();
            }
        }
        return "-";
    }
}
