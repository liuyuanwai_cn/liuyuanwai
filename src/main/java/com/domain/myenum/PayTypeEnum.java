package com.domain.myenum;

/**
 * Created by Administrator on 2016/2/20 0020.
 * 支付方式
 */
public enum PayTypeEnum {

    OffLine(0,"现金"),
    AliPay(1,"支付宝"),
    WeixinPay(2,"微信支付")
    ;

    private int code;
    private String value;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    PayTypeEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public String queryStatusVal(int code){
        for (PayTypeEnum payTypeEnum : PayTypeEnum.values()){
            if (payTypeEnum.getCode() == code){
                return payTypeEnum.getValue();
            }
        }
        return "-";
    }
}
