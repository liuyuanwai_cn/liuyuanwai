package com.domain.ex;

/**
 * Created by Administrator on 2015/12/27 0027.
 */
public enum Codes {

    OK("200","操作成功"),
    SystemError("10001","系统异常"),
    ParamError("10002","系统提示:"),
    DbError("10003","数据库异常"),
    ExcelParseError("10004","Excel文件解析错误"),
    ExcelTypeError("10005","Excel文件格式错误"),
    TemplateExportError("10006","Excel模版导出错误"),
    LoginError("10007","登录失败"),
    LogoutOk("10008","退出成功"),
    SystemDeclare("10009","系统提示:"),
    FileNotFound("10010","没有检测到文件"),
    NotImg("10011","图片格式错误"),
    FileUploadError("10012","文件上传异常"),
    ImgSizeError("10013","图片大小不得超过5M"),
    NotLogin("10014","请先登录!"),
    AuthError("10015","权限不足")

    ;
    private String code;
    private String msg;


    Codes(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
