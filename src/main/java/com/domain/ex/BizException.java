package com.domain.ex;

/**
 * Created by Administrator on 2015/12/27 0027.
 * final类，不可被继承
 */
public final class BizException extends RuntimeException {

    /**
     * 业务异常码
     */
    private Codes codes;

    private String bizMsg;


    /**
     * 私有构造函数
     *
     * @param codes
     * @param bizMessage
     */
    private BizException(Codes codes, String bizMessage) {
        super(codes.getCode()+":"+codes.getMsg()+"『" + bizMessage + "』");
        this.codes = codes;
        this.bizMsg = bizMessage;
    }


    /**
     * 静态工厂方法
     *
     * @return
     */
    public static BizException create(Codes code) {
        return new BizException(code, null);
    }

    public static BizException create(Codes code, String msg) {
        return new BizException(code, msg);
    }


    public Codes getCodes() {
        return codes;
    }

    public void setCodes(Codes codes) {
        this.codes = codes;
    }

    public String getBizMsg() {
        return bizMsg;
    }

    public void setBizMsg(String bizMsg) {
        this.bizMsg = bizMsg;
    }
}
