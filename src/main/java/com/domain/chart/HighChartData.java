package com.domain.chart;

/**
 * Created by Administrator on 2016/1/23 0023.
 * highChart封装
 */
public class HighChartData {

    private Object xAixs;
    private Object series;

    public Object getxAixs() {
        return xAixs;
    }

    public void setxAixs(Object xAixs) {
        this.xAixs = xAixs;
    }

    public Object getSeries() {
        return series;
    }

    public void setSeries(Object series) {
        this.series = series;
    }
}
