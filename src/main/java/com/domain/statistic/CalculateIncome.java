package com.domain.statistic;

import java.math.BigDecimal;

/**
 * Created by Huoyunren on 2016/4/20.
 * 收益统计
 */
public class CalculateIncome {
  // - 平台流水
  private BigDecimal platWater;
  // - 我的流水
  private BigDecimal selfWater;
  // - 我的收益
  private BigDecimal income;
  // - 平台成本
  private BigDecimal platCost;


  public CalculateIncome(BigDecimal platWater, BigDecimal selfWater, BigDecimal platCost) {
    this.platWater = platWater;
    this.selfWater = selfWater;
    this.platCost = platCost;
    ensureParamsNotNull();
    calculateIncome();
  }

  // - 计算我的收益
  // - 我的收益 = (平台流水-成本)*10%*(消费流水/平台流水)
  // - 最后结果四舍五入保留2位小数
  public void calculateIncome(){
    if (platWater.compareTo(BigDecimal.ZERO) == 0){
      income = BigDecimal.ZERO;
    }else{
      income = (platWater.subtract(platCost)).multiply(new BigDecimal("0.1")).multiply((selfWater.divide(platWater,2,BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
  }

  private void ensureParamsNotNull(){
    if (platWater == null){
      platWater = BigDecimal.ZERO;
    }
    if (selfWater == null){
      selfWater = BigDecimal.ZERO;
    }
    if (income == null){
      income = BigDecimal.ZERO;
    }
    if (platCost == null){
      platCost = BigDecimal.ZERO;
    }
  }
  public BigDecimal getPlatWater() {
    return platWater;
  }

  public void setPlatWater(BigDecimal platWater) {
    this.platWater = platWater;
  }

  public BigDecimal getSelfWater() {
    return selfWater;
  }

  public void setSelfWater(BigDecimal selfWater) {
    this.selfWater = selfWater;
  }

  public BigDecimal getIncome() {
    return income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }
}
