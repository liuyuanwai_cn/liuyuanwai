package com.domain;

import com.domain.ex.Codes;
import org.apache.commons.lang.StringUtils;

/**
 * Created by Administrator on 2015/12/27 0027.
 */
public class HttpResponse {

    private String code;
    private String msg;
    private Object data;

    private HttpResponse(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static HttpResponse create(Codes codes){

        return new HttpResponse(codes.getCode(),codes.getMsg(),null);
    }

    public static HttpResponse create(Codes codes,String msg,Object data){
        if (StringUtils.isNotBlank(msg)){
            msg = "『"+msg+"』";
        }
        return new HttpResponse(codes.getCode(),codes.getMsg() + msg,data);
    }

    public static HttpResponse create(Codes codes,String msg){
        if (StringUtils.isNotBlank(msg)){
            msg = "『"+msg+"』";
        }
        return new HttpResponse(codes.getCode(),codes.getMsg() + msg,null);
    }


    public static HttpResponse OK(){
        return new HttpResponse(Codes.OK.getCode(),Codes.OK.getMsg(),null);
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
