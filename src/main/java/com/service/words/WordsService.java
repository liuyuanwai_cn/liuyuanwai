package com.service.words;

/**
 * Created by Huoyunren on 2016-06-24.
 */
public interface WordsService {

  public Object add(String from,String to,String content);

  public Object list(Integer pageNo,Integer pageSize,String to);
}
