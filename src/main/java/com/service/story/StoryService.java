package com.service.story;

import com.dto.BulletinDto;
import com.dto.StoryExtDto;

import java.util.List;

/**
 * Created by Huoyunren on 2016-07-01.
 */
public interface StoryService {

    public List<StoryExtDto> list(Integer pageNo,Integer pageSize,String email,String orderField,Integer storyType,Integer tag);
    public List<BulletinDto> bulletin(Integer pageNo,Integer pageSize,String email,String orderField,Integer storyType);

    public Object save(String email,String title,String subTitle,String content,String img,Integer storyType,Integer chooseTag);

    public StoryExtDto queryByUuid(String uuid);

    public void incrReadCount(String uuid);

    public void del(int storyId,String authEmail);
}
