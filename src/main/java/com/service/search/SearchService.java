package com.service.search;

/**
 * Created by Huoyunren on 2016-08-24.
 */
public interface SearchService  {


  public Object search(String keyword);
}
