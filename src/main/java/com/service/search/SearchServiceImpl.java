package com.service.search;

import com.dto.SearchDto;
import com.mapper.self.CompanyMapper;
import com.mapper.self.ProductMapper;
import com.mapper.self.StoryMapper;
import com.pojo.ShCompany;
import com.pojo.ShProduct;
import com.pojo.ShStory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Huoyunren on 2016-08-24.
 */
@Service
public class SearchServiceImpl implements SearchService {

  @Autowired
  private StoryMapper storyMapper;
  @Autowired
  private ProductMapper productMapper;
  @Autowired
  private CompanyMapper companyMapper;

  @Override
  public Object search(String keyword) {
    List<ShStory> stories = storyMapper.queryByKeyWord(keyword);
    List<ShProduct> products = productMapper.queryByKeyWord(keyword);
    List<ShCompany> companies = companyMapper.queryByKeyWord(keyword);
    SearchDto searchResult = new SearchDto();
    searchResult.setStories(stories);
    searchResult.setCompanies(companies);
    searchResult.setProducts(products);
    return searchResult;
  }
}
