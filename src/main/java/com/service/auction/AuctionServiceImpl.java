package com.service.auction;

import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.dto.AdviceDto;
import com.dto.AuctionDto;
import com.mapper.generator.ShAuctionMapper;
import com.mapper.self.AdviceMapper;
import com.mapper.self.AuctionMapper;
import com.mapper.self.CompanyMapper;
import com.pojo.ShAuction;
import com.pojo.ShCompany;
import com.service.user.UserService;
import com.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Huoyunren on 2016/5/24.
 */
@Service
public class AuctionServiceImpl implements AuctionService {
    @Autowired
    private ShAuctionMapper shAuctionMapper;
    @Autowired
    private AuctionMapper auctionMapper;
    @Autowired
    private AdviceMapper adviceMapper;
    @Autowired
    private UserService userService;

    @Override
    public void auction(BigDecimal price, String uuid, String email, BigDecimal maxPrice) {
        userService.validateUserInfoCompleted(email);
        AdviceDto dto = adviceMapper.queryByUuid(uuid);
        if (dto.getStatus() != 0) {
            throw BizException.create(Codes.SystemDeclare, "拍卖已经结束");
        }
        if (price.compareTo(maxPrice) <= 0) {
            throw BizException.create(Codes.SystemDeclare, "竞价金额不得低于当前最高竞价");
        }
        ShAuction auction = new ShAuction();
        auction.setUuid(uuid);
        auction.setEmail(email);
        auction.setPrice(price);
        auction.setCreateTime(DateUtils.currentDate());
        shAuctionMapper.insertSelective(auction);
    }

    @Override
    public AuctionDto queryLatestByEmailAndUuid(String email, String uuid) {

        return auctionMapper.queryLatestByEmailAndUuid(email, uuid);
    }

    @Override
    public List<AuctionDto> list(String uuid, Integer pageNo, Integer pageSize) {
        if (pageNo > 0) {
            pageNo = pageNo - 1;
        }
        List<AuctionDto> list = auctionMapper.list(uuid, pageNo * pageSize, pageSize);
        for (AuctionDto auctionDto : list) {
            auctionDto.setTime(auctionDto.getTime().substring(0, auctionDto.getTime().length() - 2));
        }
        return list;
    }
}
