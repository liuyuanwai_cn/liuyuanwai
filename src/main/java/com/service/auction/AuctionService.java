package com.service.auction;

import com.dto.AuctionDto;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Huoyunren on 2016/5/24.
 */
public interface AuctionService {

  public void auction(BigDecimal price,String uuid,String email,BigDecimal maxPrice);

  public AuctionDto queryLatestByEmailAndUuid(String email,String uuid);

  public List<AuctionDto> list(String uuid,Integer pageNo,Integer pageSize);
}
