package com.service.img;

import com.domain.HttpResponse;
import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.dto.simditor.SimditorDto;
import com.dto.simditor.UploadResultDto;
import com.google.gson.Gson;
import com.utils.ConfigUtils;
import com.utils.ImgUploadUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.IOException;

/**
 * Created by Administrator on 2016/1/10 0010.
 */
@Service
public class ImgServiceImpl implements ImgService {

    @Autowired
    private Gson gson;

    @Override
    public Object uploadImg(CommonsMultipartFile file) {
        if (file.getSize() > Long.valueOf(ConfigUtils.getString("ImgMaxSize"))){
            return HttpResponse.create(Codes.ImgSizeError);
        }
        if (file.isEmpty()) {
            return HttpResponse.create(Codes.FileNotFound);
        }
        String OriginalFilename = file.getOriginalFilename();
        if (StringUtils.isBlank(OriginalFilename)) {
            throw BizException.create(Codes.FileNotFound);
        }
        Object obj = null;
        FileItem fileItem = file.getFileItem();
        validateImgType(OriginalFilename);
        try {
            obj = ImgUploadUtils.uploadImgByQclound(fileItem.getInputStream());
        } catch (IOException e) {
            throw BizException.create(Codes.FileUploadError,e.getMessage());
        }
        Object object = HttpResponse.create(Codes.OK,"",obj);
        return object;
    }


    @Override
    public Object uploadImgForSimditor(CommonsMultipartFile file) {
        if (file.getSize() > Long.valueOf(ConfigUtils.getString("ImgMaxSize"))){
            return HttpResponse.create(Codes.ImgSizeError);
        }
        if (file.isEmpty()) {
            return HttpResponse.create(Codes.FileNotFound);
        }
        String OriginalFilename = file.getOriginalFilename();
        if (StringUtils.isBlank(OriginalFilename)) {
            throw BizException.create(Codes.FileNotFound);
        }
        Object obj = null;
        FileItem fileItem = file.getFileItem();
        validateImgType(OriginalFilename);
        try {
            obj = ImgUploadUtils.uploadImgByQclound(fileItem.getInputStream());
        } catch (IOException e) {
            throw BizException.create(Codes.FileUploadError,e.getMessage());
        }
        UploadResultDto uploadResultDto = gson.fromJson(gson.toJson(obj),UploadResultDto.class);
        SimditorDto simditorDto = new SimditorDto();
        simditorDto.setFile_path(uploadResultDto.getDownloadUrl());
        return simditorDto;
    }

    private void validateImgType(String originalFilename) {
        if (!"jpg".equalsIgnoreCase(originalFilename.substring(originalFilename.lastIndexOf(".") + 1))
          && !"jpeg".equalsIgnoreCase(originalFilename.substring(originalFilename.lastIndexOf(".") + 1))
          && !"png".equalsIgnoreCase(originalFilename.substring(originalFilename.lastIndexOf(".") + 1))
          && !"gif".equalsIgnoreCase(originalFilename.substring(originalFilename.lastIndexOf(".") + 1))
          && !"bmp".equalsIgnoreCase(originalFilename.substring(originalFilename.lastIndexOf(".") + 1))
          ) {
            throw BizException.create(Codes.NotImg, "JPG/JPEG/PNG/BMP/GIF");
        }
    }
}
