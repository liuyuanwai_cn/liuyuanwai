package com.service.img;

import com.domain.chart.HighChartData;
import com.domain.page.PageDto;
import com.pojo.ShRiskUser;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by Administrator on 2016/1/10 0010.
 */
public interface ImgService {


    public Object uploadImg(CommonsMultipartFile file);

    public Object uploadImgForSimditor(CommonsMultipartFile file);



}
