package com.service.tag;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.mapper.generator.ShTagMapper;
import com.mapper.self.TagMapper;
import com.pojo.ShTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Huoyunren on 2016-07-28.
 */
@Service
public class TagServiceImpl implements TagService {
  @Autowired
  private TagMapper tagMapper;
  @Autowired
  private ShTagMapper shTagMapper;

  @Override
  public Object list() {

    List<ShTag> list = tagMapper.list();
    return list;
  }

  @Override
  public Object save(String name) {
    ShTag tag = new ShTag();
    tag.setName(name);
    tag.setCreateTime(new Date());
    tag.setDeleted(0);
    shTagMapper.insertSelective(tag);
    return HttpResponse.create(Codes.OK,"",tag);
  }
}
