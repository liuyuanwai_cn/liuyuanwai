package com.service.tag;

import com.pojo.ShTag;

import java.util.List;

/**
 * Created by Huoyunren on 2016-07-28.
 */
public interface TagService {

  public Object list();

  public Object save(String name);
}
