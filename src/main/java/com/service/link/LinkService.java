package com.service.link;

import com.pojo.ShLink;

import java.util.List;

/**
 * Created by Huoyunren on 2016-08-09.
 */
public interface LinkService {

  public Object saveOrUpdate(Integer linkId, String linkName, String linkUrl, String linkDesc);

  public List<ShLink> list();

  public Object del(Integer linkId);

  public Object incrClickCount(Integer linkId);
}
