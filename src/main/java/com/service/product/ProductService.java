package com.service.product;

import com.dto.IncomeDto;
import com.dto.RoleMenuDto;
import com.pojo.ShProduct;
import com.pojo.ShRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Administrator on 2016/2/18 0018.
 */

public interface ProductService {
    public Object save(ShProduct shProduct);

    public Object list(int page, int limit, Integer deleted, String email,Integer soldOut,Integer productType);

    public Object update(ShProduct shProduct);

    public Object batchDel(Integer[] productIds);

    public Object updateProductImg(Integer productId, String url);

    public Object updateProductStatus(Integer productId, Integer deleted);

    public IncomeDto calIncome(Integer productId);

    public Object incrReadCount(Integer productId);

    public Object mgrList(int page, int limit, Integer deleted, String email,Integer soldOut,Integer pType);

}
