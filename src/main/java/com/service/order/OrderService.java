package com.service.order;

import com.dto.OrderExtDto;
import com.dto.statistic.StatisticOutDto;
import com.dto.statistic.StatisticOutputDto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2016/2/18 0018.
 */

public interface OrderService {


    public Object add(int productId,String email,int count,String sendtime,BigDecimal amount,BigDecimal originalCost,Integer deliveryType,String sendAddr,String remark);


    public Object list(int page,int limit,String email);

    public Object dealOrder(String orderNo,Integer status,String reason);

    public Object orderManage(int page,int limit,String email);

    public StatisticOutDto statisticSales(String email);

    public void notifyReferralIncomeChangeByEmail(String email, BigDecimal income, String referralName, String productName,String shopName,Date time);

    public void orderNotifyEmail(OrderExtDto orderExtDto, String subject, Set<String> emailSet);
}
