package com.service.advice;

import com.dto.AdviceDto;
import com.dto.AuctionDto;
import com.dto.NoteDto;
import com.pojo.ShAdvice;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Huoyunren on 2016/2/27.
 */
public interface AdviceService {

    public NoteDto add(String email, String content, Integer msgType, BigDecimal fee, Integer isShowMobile,String endTime,String img,String address);

    public Object list(Integer page, Integer limit,Integer type,Integer status,String email);

    public Object reward(String uuid,String dealEmail,String rewardEmail);

    public AdviceDto queryByUuid(String uuid);

    public void rewardNotify(String rewardEmail, String dealName, String dealPhone, String dealTime,String dealEmail);

    public void del(final String uuid);

}
