package com.service.cover;

import com.pojo.ShAdvice;
import com.pojo.ShCover;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by Huoyunren on 2016/2/27.
 */
public interface CoverService {

  public int add(String email,String show,String name,String remark,String logo,String startDate,String endDate);


  public List<ShCover> list(int page, int limit, String email);


  public Object check(String ids,String status,String reason);

}
