package com.service.cover;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.domain.page.Paginator;
import com.mapper.generator.ShCoverMapper;
import com.mapper.self.CoverMapper;
import com.pojo.ShCover;
import com.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Huoyunren on 2016/2/27.
 */
@Service
public class CoverServiceImpl implements CoverService {

  @Autowired
  private ShCoverMapper shCoverMapper;
  @Autowired
  private CoverMapper coverMapper;

  @Override
  public int add(String email,String show,String name,String remark,String logo,String startDate,String endDate) {

    ShCover shCover = new ShCover();
    shCover.setEmail(email);
    shCover.setName(name);
    shCover.setDeleted(0);
    if (StringUtils.isNotBlank(startDate)){
      shCover.setStartdate(DateUtils.parseDate("yyyy-MM-dd", startDate));
    }
    if (StringUtils.isNotBlank(endDate)){
      shCover.setEnddate(DateUtils.parseDate("yyyy-MM-dd", endDate));
    }
    shCover.setLogo(logo);
    shCover.setUrl(show);
    shCover.setRemark(remark);
    shCover.setFavourCount(0);
    shCover.setStatus(0);


    return coverMapper.saveOrUpdate(shCover);
  }

  // - 查询申请列表列表
  @Override
  public List<ShCover> list(int page,int limit,String email) {
    if (page > 0){
      page = page - 1;
    }
    return coverMapper.list(page,limit,null);
  }

  @Override
  public Object check(String ids, String status,String reason) {

    if (StringUtils.isBlank(ids)){
      return HttpResponse.create(Codes.SystemDeclare,"选择数据");
    }

    String[] idArr = ids.split(",");
    coverMapper.check(idArr,status,reason);

    return HttpResponse.OK();
  }
}
