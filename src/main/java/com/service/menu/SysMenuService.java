package com.service.menu;

import com.pojo.ShMenu;


/**
 * Created by Administrator on 2015/11/29 0029.
 */
public interface SysMenuService {

    /**
     * 查询系统菜单树列表
     * @return
     */
    public Object search(int page,int limit);

    /**
     * 加载菜单树
     * @return
     */
    public Object loadMenuTree(Integer roleId);


    /**
     * 加载父级菜单
     * @return
     */
    public Object loadParentMenu();

    /**
     * 根据菜单名称查询
     * @return
     */
    public Object queryByMenuName(String menuName);


    /**
     * 新增菜单
     * @param shMenu
     * @return
     */
    public Object addMenu(ShMenu shMenu);

    /**
     * 菜单名称重复性检测
     * @param shMenu
     * @return
     */
    public boolean isMenuNameExist(ShMenu shMenu);


    /**
     * 更新菜单
     * @param shMenu
     * @return
     */
    public Object saveOrUpdate(ShMenu shMenu);

    /**
     * 批量删除菜单
     * @param ids
     */
    public void del(String ids);
}
