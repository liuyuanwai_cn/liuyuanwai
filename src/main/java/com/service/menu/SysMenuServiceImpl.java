package com.service.menu;

import com.domain.HttpResponse;
import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.domain.page.Paginator;
import com.google.gson.Gson;
import com.mapper.generator.ShMenuMapper;
import com.mapper.self.MenuMapper;
import com.pojo.ShMenu;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2015/11/29 0029.
 */
@Service
public class SysMenuServiceImpl implements SysMenuService{

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private ShMenuMapper shMenuMapper;
    @Autowired
    private Gson gson;

    /**
     * 查询系统菜单列表
     * @return
     */
    @Override
    public Object search(int page,int limit) {

        if (page > 0){
            page = page - 1;
        }

        List<ShMenu> list = menuMapper.search(page*limit,limit);
        Paginator paginator = Paginator.instance(menuMapper.count(),list);
        paginator.setPage(page+1);
        paginator.setLimit(limit);
        return paginator;
    }

    /**
     * 加载父级菜单
     * @return
     */
    @Override
    public Object loadParentMenu() {


        return menuMapper.loadParentMenu();
    }

    /**
     * 新增菜单
     * @param shMenu
     * @return
     */
    @Override
    public Object addMenu(ShMenu shMenu) {

        if (shMenu == null){
            throw BizException.create(Codes.ParamError);
        }
        if (isMenuNameExist(shMenu)){
            throw BizException.create(Codes.ParamError,"菜单名称重复");
        }
        if (shMenu.getpId() != -1){
            shMenu.setTarget("subFrame");
        }
        if (shMenu.getpId() == 1){
            shMenu.setOpen(1);
        }
        int ret = shMenuMapper.insertSelective(shMenu);
        if (ret < 0){
            throw BizException.create(Codes.DbError);
        }
        return gson.toJson(HttpResponse.OK());
    }


    @Override
    public boolean isMenuNameExist(ShMenu shMenu) {
        int ret = -1;
        ret = menuMapper.isMenuNameExist(shMenu.getName(),shMenu.getpId());
        if (ret > 0){
            return true;
        }

        return false;
    }
    /**
     * 根据菜单名称查询
     * @return
     */
    @Override
    public Object queryByMenuName(String menuName) {
        return menuMapper.queryByMenuName(menuName);
    }


    /**
     * 更新菜单
     * @param shMenu
     * @return
     */
    @Override
    public Object saveOrUpdate(ShMenu shMenu) {

        menuMapper.saveOrUpdate(shMenu);

        return gson.toJson(HttpResponse.OK());
    }

    /**
     * 批量删除菜单
     * @param ids
     */
    @Override
    public void del(String ids) {
        if (StringUtils.isBlank(ids)){
            return;
        }
        String [] idArr = ids.split(",");
        menuMapper.batchDel(idArr);
    }


    /**
     * 加载菜单树
     * @return
     */
    @Override
    public Object loadMenuTree(Integer roleId){
        List<ShMenu> shMenus = null;
        if (roleId != null){
            shMenus = menuMapper.loadMenuTreeByRoleId(roleId);
        }else {
            shMenus = menuMapper.loadMenuTree();
        }
        for (ShMenu shMenu:shMenus){
            if (shMenu.getpId() == -1){
                // - 父节点
                shMenu.setName("<b>"+shMenu.getName()+"</b>");
            }else {
                // - 子节点
            }
        }
        return shMenus;
    };
}
