package com.service.attention;

/**
 * Created by Huoyunren on 2016-09-06.
 */
public interface AttentionService {

  public int saveOrUpdate(String attentionEmail,Integer isAttention);

  public Object list(String email,Integer pageNo,Integer pageSize);
}
