package com.service.risk;

import com.domain.chart.HighChartData;
import com.domain.page.PageDto;
import com.dto.statistic.StatisticOutputDto;
import com.pojo.ShRiskUser;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by Administrator on 2016/1/10 0010.
 */
public interface RiskService {


    public Object importExcel(CommonsMultipartFile file);


    public Object downTemplate();


    /**
     * 批量插入/更新
     *
     * @param list
     * @return
     */
    public int batchSaveOrUpdate(List<ShRiskUser> list);


    public Object exportExcel();


    public Object query(PageDto pageDto);


    public Object batchDel(Integer[] idArr);


    /**
     * 时域统计
     *
     * @param type year,month,day
     * @return
     */
    public HighChartData timeStatistic(String type);



}
