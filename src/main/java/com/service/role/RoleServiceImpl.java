package com.service.role;

import com.domain.HttpResponse;
import com.domain.page.Paginator;
import com.dto.RoleMenuDto;
import com.google.common.collect.Lists;
import com.mapper.generator.ShRoleMapper;
import com.mapper.self.RoleMapper;
import com.mapper.self.RoleMenuRelMapper;
import com.pojo.ShRole;
import com.pojo.ShRoleMenuRel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2016/2/18 0018.
 */
@Service
public class RoleServiceImpl implements RoleService{

    @Autowired
    private ShRoleMapper shRoleMapper;
    @Autowired
    private RoleMenuRelMapper roleMenuRelMapper;
    @Autowired
    private RoleMapper roleMapper;


    /**
     * 批量新增角色的菜单列表
     * @param roleName
     * @param menuIds
     * @return
     */
    @Override
    @Transactional
    public Object add(String roleName, String menuIds) {

        // - 新增菜单
        ShRole shRole = new ShRole();
        shRole.setName(roleName);
        shRole.setDeleted(0);
        shRoleMapper.insertSelective(shRole);
        // - 新增角色菜单关系
        List<ShRoleMenuRel> shRoleMenuRels = Lists.newArrayList();

        String[] menuIdArr = menuIds.split(",");
        Integer[] menuIdsArr = new Integer[menuIdArr.length];
        for (int i=0;i<menuIdArr.length;i++){
            menuIdsArr[i] = Integer.parseInt(menuIdArr[i]);
        }

        for (Integer menuId:menuIdsArr){
            ShRoleMenuRel shRoleMenuRel = new ShRoleMenuRel();
            shRoleMenuRel.setRoleId(shRole.getId());
            shRoleMenuRel.setMenuId(menuId);
            shRoleMenuRel.setDeleted(0);
            shRoleMenuRels.add(shRoleMenuRel);
        }
        roleMenuRelMapper.batchInsert(shRoleMenuRels);
        return HttpResponse.OK();
    }

    /**
     * 根据角色名称查询
     * @param roleName
     * @return
     */
    @Override
    public ShRole searchByRoleName(String roleName) {



        return null;
    }

    /**
     * 查询角色列表
     * @return
     */
    @Override
    public Object list(int page,int limit ) {
        if (page > 0){
            page = page - 1;
        }
        return Paginator.instance(roleMapper.count(),roleMapper.list(page,limit));
    }

    @Override
    public RoleMenuDto info(int roleId) {

        ShRole shRole = shRoleMapper.selectByPrimaryKey(roleId);
        List<ShRoleMenuRel> shRoleMenuRels = roleMenuRelMapper.listByRoleId(roleId);

        RoleMenuDto roleMenuDto = new RoleMenuDto();
        roleMenuDto.setShRole(shRole);

        StringBuffer sb = new StringBuffer();
        for(ShRoleMenuRel shRoleMenuRel:shRoleMenuRels){
            sb.append(shRoleMenuRel.getMenuId());
            sb.append(",");
        }
        roleMenuDto.setMenuIds(sb.toString());
        return roleMenuDto;
    }


    /**
     * 删除角色，对应的用户角色关系数据也同时删除
     * @param roleIds
     * @return
     */
    @Override
    public Object del(Integer[] roleIds) {

        // - 删除角色
        roleMapper.batchDelByRoleIds(roleIds);
        // - 删除角色-用户对应关系数据
        roleMenuRelMapper.batchDelByRoleIds(roleIds);


        return HttpResponse.OK();
    }


    /**
     * 更新
     * @param roleName
     * @param menuIds
     * @param roleId
     * @return
     */
    @Override
    public Object update(String roleName, String menuIds, Integer roleId) {

        // - 更新角色名称
        roleMapper.updateRoleNameByRoleId(roleId,roleName);

        Integer[] roleIds = new Integer[1];
        roleIds[0] = roleId;
        // - 删除已有角色菜单关系数据
        roleMenuRelMapper.batchDelByRoleIds(roleIds);

        // - 再新增关系数据
        // - 新增角色菜单关系
        List<ShRoleMenuRel> shRoleMenuRels = Lists.newArrayList();

        Integer[] menuIdArr = new Integer[menuIds.split(",").length];
        for (int i=0;i<menuIds.split(",").length;i++){
            menuIdArr[i] = Integer.parseInt(menuIds.split(",")[i]);
        }

        for (Integer menuId:menuIdArr){
            ShRoleMenuRel shRoleMenuRel = new ShRoleMenuRel();
            shRoleMenuRel.setRoleId(roleId);
            shRoleMenuRel.setMenuId(menuId);
            shRoleMenuRel.setDeleted(0);
            shRoleMenuRels.add(shRoleMenuRel);
        }
        roleMenuRelMapper.batchInsert(shRoleMenuRels);
        return HttpResponse.OK();
    }




}
