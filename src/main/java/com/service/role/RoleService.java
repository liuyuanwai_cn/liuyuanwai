package com.service.role;

import com.dto.RoleMenuDto;
import com.pojo.ShRole;

import java.util.List;

/**
 * Created by Administrator on 2016/2/18 0018.
 */

public interface RoleService {

    public Object add(String roleName,String menuIds);

    public ShRole searchByRoleName(String roleName);

    public Object list(int page,int limit );

    /**
     * 角色详情查询
     * @param roleId
     * @return
     */
    public RoleMenuDto info(int roleId);


    public Object del(Integer[] roleIds);


    public Object update(String roleName,String menuIds,Integer roleId);

}
