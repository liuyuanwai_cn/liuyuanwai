package com.service.code;

import com.pojo.ShCashHistory;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Huoyunren on 2016/5/5.
 */
public interface CashHistoryService {

  // - 发送验证码
  public Object sendCashCode(String buyerEmail,String shopEmail, BigDecimal cashAmount);

  // - 提现
  public Object cashAmount(String buyerEmail,String shopEmail, BigDecimal cashAmount, Integer cashCode);

  // - 提现记录
  public Object list(Integer pageNo,Integer pageSize,String email);
}
