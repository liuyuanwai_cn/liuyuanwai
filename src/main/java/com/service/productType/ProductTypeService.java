package com.service.productType;

import com.pojo.ShProductType;

import java.util.List;

/**
 * Created by admin on 2016/7/20.
 */
public interface ProductTypeService {

    public Object saveOrUpdate(Integer id,String name,String remark);

    public Object del(Integer id);

    public List<ShProductType> list(Integer pageNo,Integer pageSize);
}
