package com.service.user;

import com.domain.statistic.CalculateIncome;
import com.dto.UserExtDto;
import com.pojo.ShCompany;
import com.pojo.ShIncomeWater;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Huoyunren on 2016/1/27.
 */
public interface UserService {

    public Object register(String email, String referralCode);

    public Object login(String email, String password);

    public Object fakeLogin(String email, String login);


    public UserExtDto info(String email);

    public UserExtDto queryInfo(String email);

    public Object updateInfo(ShCompany shCompany);

    public Object updatePassword(String password, String email, String oldPassword);

    public Object list(int page, int limit, String queryKey, String role);
    public Object sortList();

    public Object check(String ids, Integer status);

    public void validateUserInfoCompleted(String email);

    public void validateUserInfoCompleted(ShCompany company);

    public Object findPassword(String email);

    public Object updateUserRole(String email, Integer roleId);

    public Object updateLogo(String logo, String email);

    public Object updateWeixinQrcode(String weixinQrcode, String email);

    public Object updateAlipayQrcode(String alipayQrcode, String email);

    public void updateInCount();

    public Object warterStatistic();

    public Object platStatistic(String shopEmail);

    // - 计算我的收益
    public BigDecimal incomeStatistic(String email, Integer deleted);

    // - 计算我在某个门店收益
    public BigDecimal incomeSingleStatistic(String buyerEmail, String shopEmail);

    // - 收益产生流水
    public Object incomeList(int pageNo, int pageSize, Integer shopId);

    // - 收益产生流水按门店分类
    public Object incomeListGroupByShop(int pageNo, int pageSize);

    public Object userStatistic();
}
