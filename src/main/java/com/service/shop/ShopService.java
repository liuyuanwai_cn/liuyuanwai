package com.service.shop;

import com.pojo.ShShop;

import java.util.List;

/**
 * Created by Huoyunren on 2016/5/6.
 */
public interface ShopService {

  public ShShop queryByEmail(String email);

  public void updateShopInfo(ShShop shop);

  public List<ShShop> list(int page,int limit);

  public void updateLogo(String logo,String email);
}
