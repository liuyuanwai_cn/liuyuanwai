package com.service.shop;

import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.domain.myenum.ShopStatusEnum;
import com.mapper.self.ShopMapper;
import com.pojo.ShShop;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Huoyunren on 2016/5/6.
 */
@Service
public class ShopServiceImpl implements ShopService {
  @Autowired
  private ShopMapper shopMapper;
  @Override
  public ShShop queryByEmail(String email) {
    ShShop shop = shopMapper.queryByEmail(email);
    if (shop == null){
      shop = new ShShop();
    }
    return shop;
  }

  @Override
  public void updateShopInfo(ShShop shop) {
    ensureShopLegal(shop);
    shop.setStatus(ShopStatusEnum.Open.getCode());
    shopMapper.saveOrUpdate(shop);
  }


  public void ensureShopLegal(ShShop shop){
    if (StringUtils.isBlank(shop.getUrl())){
      throw BizException.create(Codes.SystemDeclare,"选择门店图片");
    }
    if (StringUtils.isBlank(shop.getName())){
      throw BizException.create(Codes.SystemDeclare,"输入门店名称");
    }
    if (StringUtils.isBlank(shop.getAd())){
      throw BizException.create(Codes.SystemDeclare,"输入门店宣传语");
    }
    if (StringUtils.isBlank(shop.getAddr())){
      throw BizException.create(Codes.SystemDeclare,"输入门店地址");
    }
    if (StringUtils.isBlank(shop.getRemark())){
      throw BizException.create(Codes.SystemDeclare,"输入门店介绍");
    }
    if (StringUtils.isBlank(shop.getMobile()) && StringUtils.isBlank(shop.getPhone())){
      throw BizException.create(Codes.SystemDeclare,"手机号和座机至少填一个");
    }
  }

  @Override
  public List<ShShop> list(int page,int limit) {
    if (page > 0){
      page = page - 1;
    }
    List<ShShop> shops = shopMapper.list(page*limit,limit,ShopStatusEnum.Open.getCode());
    return shops;
  }

  @Override
  public void updateLogo(String logo,String email) {
    shopMapper.updateLogo(logo,email);
  }
}
