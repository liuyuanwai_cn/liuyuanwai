package com.service.note;

import com.dto.NoteDto;

import java.util.List;

/**
 * Created by Huoyunren on 2016/5/18.
 */
public interface NoteService {

  public NoteDto add(String uuid,String content,String toEmail,String title);

  public NoteDto queryByNoteId(Integer noteId);

  public List<NoteDto> list(String uuid,Integer pageNo,Integer pageSize);
}
