package com.service.note;

import com.dto.NoteDto;
import com.handler.email.Email;
import com.handler.email.EmailHandler;
import com.mapper.generator.ShNoteMapper;
import com.mapper.self.NoteMapper;
import com.mapper.self.SetMapper;
import com.pojo.ShNote;
import com.pojo.ShSet;
import com.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Huoyunren on 2016/5/18.
 */
@Service
public class NoteServiceImpl implements NoteService {
  @Autowired
  private ShNoteMapper shNoteMapper;
  @Autowired
  private NoteMapper noteMapper;
  @Autowired
  private EmailHandler emailHandler;
  @Autowired
  private SetMapper setMapper;

  @Override
  public NoteDto add(String uuid, String content, String toEmail, String title) {
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
      .getRequestAttributes()).getRequest();
    Object name = request.getSession().getAttribute("name");
    String email = request.getSession().getAttribute("email").toString();
    ShNote note = new ShNote();
    note.setContent(content);
    note.setUuid(uuid);
    note.setEmail(email);
    note.setCreateTime(DateUtils.currentDate());
    shNoteMapper.insertSelective(note);
    NoteDto noteDto = queryByNoteId(note.getId());
    noteDto.setCreateTime(noteDto.getCreateTime().substring(0, noteDto.getCreateTime().length() - 2));

    mailNotify(content, toEmail, title, name, email);
    return noteDto;
  }

  private void mailNotify(String content, String toEmail, String title, Object name, String email) {
    if (email.equals(toEmail)) {
      // - 自己回复自己的留言无需提醒
      return;
    }
    ShSet set = setMapper.queryByEmail(toEmail);
    if (set != null) {
      if (StringUtils.isBlank(title)){
        // - 龙门阵留言
        if (set.getNoteNotify().intValue() == 0){
          // - 龙门阵留言不提醒
          return;
        }
      }else{
        // - 文章留言
        if (set.getStoryNotify().intValue() == 0){
          // - 文章留言不提醒
          return;
        }
      }
    }
    String alias = email;
    if (name != null) {
      alias = name.toString();
    }
    Email e = new Email();
    String[] to = {toEmail};
    e.setTo(to);
    e.setSubject(alias + " 给你留言：" + title);
    e.setEnableHtml(Boolean.FALSE);
    StringBuffer buffer = new StringBuffer();
    buffer.append("留言内容：");
    buffer.append(content);
    buffer.append("\n\n\n");
    buffer.append("温馨提示：如果你不想收到此类消息,可以在个人设置关掉消息提醒。谢谢！\n");
    buffer.append("更多详情：http://liuyuanwai.cn");
    e.setContent(buffer.toString());
    emailHandler.asyncSendMail(e);
  }

  @Override
  public NoteDto queryByNoteId(Integer noteId) {
    return noteMapper.queryByNoteId(noteId);
  }

  @Override
  public List<NoteDto> list(String uuid, Integer pageNo, Integer pageSize) {
    if (pageNo == null) {
      pageNo = 0;
    }
    if (pageSize == null) {
      pageSize = 10;
    }
    if (pageNo > 0) {
      pageNo -= 1;
    }
    List<NoteDto> list = noteMapper.list(uuid, pageNo * pageSize, pageSize);
    for (NoteDto noteDto : list) {
      noteDto.setCreateTime(noteDto.getCreateTime().substring(0, noteDto.getCreateTime().length() - 2));
    }
    return list;
  }
}
