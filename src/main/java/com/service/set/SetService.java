package com.service.set;

/**
 * Created by Huoyunren on 2016-08-04.
 */
public interface SetService {

  public int saveOrUpdate(Integer storyNotify,Integer noteNotify,Integer attentionNotify);
}
