package com.service.set;

import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.mapper.self.SetMapper;
import com.pojo.ShSet;
import com.pojo.ShShop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by Huoyunren on 2016-08-04.
 */
@Service
public class SetServiceImpl implements SetService {
  @Autowired
  private SetMapper setMapper;


  @Override
  public int saveOrUpdate(Integer storyNotify, Integer noteNotify,Integer attentionNotify) {

    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    Object obj = request.getSession().getAttribute("email");
    if (obj == null) {
      throw BizException.create(Codes.SystemDeclare, "登录超时");
    }
    ShSet set = new ShSet();
    Date now = new Date();
    set.setEmail(obj.toString());
    set.setNoteNotify(noteNotify);
    set.setStoryNotify(storyNotify);
    set.setAttentionNotify(attentionNotify);
    set.setCreateTime(now);
    set.setUpdateTime(now);
    setMapper.saveOrUpdate(set);
    return 0;
  }
}
