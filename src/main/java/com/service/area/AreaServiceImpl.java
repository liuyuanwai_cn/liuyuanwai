package com.service.area;

import com.domain.myenum.CheckStatusEnum;
import com.dto.statistic.IdCountDto;
import com.dto.statistic.NameValueDto;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mapper.self.SelfProvinceMapper;
import com.pojo.Province;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Huoyunren on 2016/2/2.
 */
@Service
public class AreaServiceImpl implements AreaService {
  @Autowired
  private SelfProvinceMapper selfProvinceMapper;


  /**
   * 按审核状态统计员工数量
   * @param type      "country":国家,"province":省，"":市
   * @param id        国家，省，市ID
   * @return
   */
  @Override
  public Object queryStatusCompanyCount(String type, Integer id) {

    // - 获取状态分类及对应数据
    List<IdCountDto> list = selfProvinceMapper.queryStatusCompanyCount(type, id);
    Map<Integer,Integer> map = Maps.newHashMap();
    for (IdCountDto idCountDto:list){
      map.put(idCountDto.getId(),idCountDto.getCount());
    }

    // - 初始化所有审核状态分类
    List<NameValueDto> nameValueDtos = Lists.newArrayList();
    for (CheckStatusEnum checkStatusEnum:CheckStatusEnum.values()){
      NameValueDto nameValueDto = new NameValueDto();
      nameValueDto.setName(String.valueOf(checkStatusEnum.getCode()));
      nameValueDtos.add(nameValueDto);
    }
    // - 将实际数据填充到所有分类模型
    for (NameValueDto nameValueDto:nameValueDtos){
      if (map.get(Integer.parseInt(nameValueDto.getName())) != null){
        nameValueDto.setValue(map.get(Integer.parseInt(nameValueDto.getName())));
        nameValueDto.setName(CheckStatusEnum.queryValueByCode(Integer.parseInt(nameValueDto.getName())));
      }else {
        nameValueDto.setValue(0);
        nameValueDto.setName(CheckStatusEnum.queryValueByCode(Integer.parseInt(nameValueDto.getName())));
      }
    }

    return nameValueDtos;
  }

  /**
   * 查省
   * @param pid 国家ID，固定值1为中国
   * @return
   */
  @Override
  public Object queryProvince(Integer pid) {
    List<Province> list = selfProvinceMapper.queryByPid(pid);
    List<NameValueDto> nameValueDtos = Lists.newArrayList();
    for (Province province:list){
      NameValueDto nameValueDto = new NameValueDto();
      nameValueDto.setName(province.getName());
      nameValueDto.setValue(province.getId());
      nameValueDtos.add(nameValueDto);
    }
    return nameValueDtos;
  }
}
