package com.utils;

import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/3/6 0006.
 * HttpClient模拟调用工具
 */
public class HttpRequestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(HttpRequestUtils.class);


    /**
     * post请求
     * @param url   请求地址
     * @param paramMap  请求参数，表单
     * @return
     */
    public static Object httpPostByFormData(String url,Map<String,String> paramMap){
        if (StringUtils.isBlank(url)){
            throw BizException.create(Codes.SystemError,"请求地址为空");
        }

        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        // - 搜集表单参数
        List<NameValuePair> nameValuePairs = Lists.newArrayList();
        if (paramMap != null){
            for (String key:paramMap.keySet()){
                nameValuePairs.add(new BasicNameValuePair(key,paramMap.get(key)));
            }
        }
        try {
            UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
            httpPost.setEntity(uefEntity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
                throw BizException.create(Codes.SystemError,"HttpPost请求失败");
            }
            String temp = EntityUtils.toString(httpResponse.getEntity());
            return JsonUtils.JSON().fromJson(temp, com.domain.HttpResponse.class);
        } catch (UnsupportedEncodingException e) {
            throw BizException.create(Codes.SystemError,"HttpPost请求失败："+e.getMessage());
        } catch (ClientProtocolException e) {
            throw BizException.create(Codes.SystemError,"HttpPost请求失败："+e.getMessage());
        } catch (IOException e) {
            throw BizException.create(Codes.SystemError,"HttpPost请求失败："+e.getMessage());
        }
    }




    // - Get请求
    public static Object httpGet(String url){

        if (StringUtils.isBlank(url)){
            throw BizException.create(Codes.SystemError,"请求地址为空");
        }
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
                throw BizException.create(Codes.SystemError,"HttpGet请求失败");
            }
            String temp = EntityUtils.toString(httpResponse.getEntity());
            return JsonUtils.JSON().fromJson(temp, com.domain.HttpResponse.class);
        } catch (IOException e) {
            throw BizException.create(Codes.SystemError,"HttpGet请求失败："+e.getMessage());
        }
    }

}



