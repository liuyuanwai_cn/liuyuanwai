package com.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2015/11/29 0029.
 */
public class DateUtils {
    private static final Logger LOG = LoggerFactory.getLogger(DateUtils.class);

    /**
     * 当前日期
     *
     * @return
     */
    public static String now() {

        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /**
     * 当前时间
     * @return
     */
    public static String currentTime() {

        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }


    public static Date currentDate(){
        return new Date();
    }


    /**
     * 根据时间格式字符串得到时间对象
     * @param pattern       时间格式
     * @param tempDate      时间字符串
     * @return
     */
    public static Date parseDate(String pattern, String tempDate) {

        Date date = null;
        if (StringUtils.isBlank(pattern)) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            date = dateFormat.parse(tempDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date parseDate(String tempDate){

        return parseDate(null, tempDate);
    }


    // - 得到当前年
    public static Integer getYear(){
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }
    // - 得到今天几号
    public static Integer getDay(){
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    // - 得到格式化时间
    public static String getFormatDate(String pattern){
        if (StringUtils.isBlank(pattern)){
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(new Date());
    }
    // - 得到格式化时间
    public static String getFormatDate(String pattern,Date date){
        if (StringUtils.isBlank(pattern)){
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }

    public static Integer getLastDayOfLastMonth(){
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH, month-1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date date = calendar.getTime();
        return Integer.valueOf(getFormatDate("dd",date));
    }

    public static void main(String[] args) {
        LOG.info("{}",DateUtils.getLastDayOfLastMonth());
    }



    public static boolean isToday(String datetime){
        Calendar cur = Calendar.getInstance();
        cur.setTime(parseDate(datetime));
        Calendar today = Calendar.getInstance();	//今天
        today.set(Calendar.YEAR, today.get(Calendar.YEAR));
        today.set(Calendar.MONTH, today.get(Calendar.MONTH));
        today.set(Calendar.DAY_OF_MONTH,today.get(Calendar.DAY_OF_MONTH));
        //  Calendar.HOUR——12小时制的小时数 Calendar.HOUR_OF_DAY——24小时制的小时数
        today.set( Calendar.HOUR_OF_DAY, 0);
        today.set( Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        if (cur.after(today)){
            return true;
        }
        return false;
    }
}
