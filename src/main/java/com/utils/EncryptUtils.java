package com.utils;

import org.apache.commons.lang.StringUtils;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Huoyunren on 2016/1/26.
 *
 * 加密工具
 *
 */
public class EncryptUtils {

  private static final String MAC_NAME = "HmacSHA1";
  private static final String ENCODING = "UTF-8";

  /**
   * MD5加密成32位密文
   * @param temp
   * @return
   */
  public final static String md5(String temp){
    if (StringUtils.isBlank(temp)){
      return null;
    }
    try {
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      md5.update(temp.getBytes());
      StringBuilder sb = new StringBuilder();
      byte [] bt = md5.digest();
      for (byte b:bt) {
        sb.append(Integer.toHexString(b&0xff));
      }
      return sb.toString();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return null;
  }



  /**
   * 使用 HMAC-SHA1 签名方法对对encryptText进行签名
   * @param encryptText 被签名的字符串
   * @param encryptKey  密钥
   * @return
   * @throws Exception
   */
  public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey) throws Exception
  {
    byte[] data=encryptKey.getBytes(ENCODING);
    //根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
    SecretKey secretKey = new SecretKeySpec(data, MAC_NAME);
    //生成一个指定 Mac 算法 的 Mac 对象
    Mac mac = Mac.getInstance(MAC_NAME);
    //用给定密钥初始化 Mac 对象
    mac.init(secretKey);
    byte[] text = encryptText.getBytes(ENCODING);
    //完成 Mac 操作
    return mac.doFinal(text);
  }


  public static void main(String[] args) {
    System.out.print(EncryptUtils.md5(null));
  }
}
