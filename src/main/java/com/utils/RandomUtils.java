package com.utils;

import org.slf4j.Logger;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Huoyunren on 2016/2/3.
 */
public class RandomUtils {


  /**
   * 生成随机四位数
   *
   * @return
   */
  public static String random4() {
    Double d = Math.random() * 9000 + 1000;
    return String.valueOf(d.intValue());
  }

  /**
   * 生成随机六位数
   *
   * @return
   */
  public static String random6() {
    Double d = Math.random() * 900000 + 100000;
    return String.valueOf(d.intValue());
  }


  // - 获取订单编号
  public static String getOrderCode() {
    String temp = "NO.";
    temp += DateUtils.getFormatDate("yyyyMMddHHmm");
    temp += RandomUtils.random4();
    return temp;
  }

  public static String getUuid() {
    UUID uuid = UUID.randomUUID();
    StringBuffer sbuffer = new StringBuffer();
    String temp = uuid.toString();
    sbuffer.append(temp.substring(0, 8));
    sbuffer.append(temp.substring(9, 13));
    sbuffer.append(temp.substring(14, 18));
    sbuffer.append(temp.substring(19, 23));
    sbuffer.append(temp.substring(24));
    return sbuffer.toString();
  }


  public static void main(String[] args) {

//    Random random = new Random();
//    System.out.println(random.nextInt(2));
//    System.out.println(random.nextInt(3));
//    System.out.println(random.nextInt(10));
//    System.out.println(random.nextInt());

    for (int i = 0; i < 1000; i++) {
      System.out.println(RandomUtils.random6());
    }

//    System.out.print(RandomUtils.random6());


  }

}
