package com.utils;

import com.domain.HttpResponse;
import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.qcloud.PicCloud;
import com.qcloud.UploadResult;
import org.apache.commons.codec.binary.Base64;

import java.io.InputStream;
import java.util.Date;

/**
 * Created by Administrator on 2016/3/6 0006.
 *
 * 文件上传工具 - 接入万象优图服务 - 免费版可以存储10GB图片
 *
 */
public class ImgUploadUtils {
    private static final int APP_ID = Integer.parseInt(ConfigUtils.getString("AppId"));
    private static final String SECRET_ID = ConfigUtils.getString("SecretID");
    private static final String SECRET_KEY = ConfigUtils.getString("SecretKey");
    private static final String Bucket = ConfigUtils.getString("Bucket");
    // - 使用qcloud-java-sdk则无需自己处理签名
    /**
     * 上传图片 - 流式上传
     * @param inputStream
     */
    public static Object uploadImgByQclound(InputStream inputStream){
        PicCloud picCloud = new PicCloud(APP_ID, SECRET_ID, SECRET_KEY, Bucket);
        //UploadResult是上传的返回结果
        UploadResult result = new UploadResult();
        result = picCloud.upload(inputStream);
        return result;
    }
    /**
     * 通过文件完整路径上次文件
     * @param filePath
     * @return
     */
    public static Object uploadImg(String filePath){
        PicCloud picCloud = new PicCloud(APP_ID, SECRET_ID, SECRET_KEY, Bucket);
        //UploadResult是上传的返回结果
        UploadResult result = new UploadResult();
        result = picCloud.upload(filePath);
        return result;
    }


}
