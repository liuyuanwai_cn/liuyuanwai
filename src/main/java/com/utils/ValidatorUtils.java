package com.utils;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Huoyunren on 2016/1/27.
 */
public class ValidatorUtils {

  // - 验证邮箱
  public final static boolean validatorEmail(String email) {
    boolean flag = false;
    if (StringUtils.isBlank(email)) {
      return false;
    }
    Pattern pattern = Pattern.compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
    Matcher matcher = pattern.matcher(email);
    flag = matcher.matches();
    return flag;
  }


  /**
   * 手机号验证
   *
   * @param str
   * @return 验证通过返回true
   */
  public static boolean validatorMobile(String str) {
    if (StringUtils.isBlank(str)) {
      return false;
    }
    Pattern p = null;
    Matcher m = null;
    boolean b = false;
    p = Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$");
    m = p.matcher(str);
    b = m.matches();
    return b;
  }

  /**
   * 校验URL
   * 必须是http://或https://开头
   *
   * @param url
   * @return
   */
  public static boolean validatorUrl(String url) {
    if (StringUtils.isBlank(url)) {
      return false;
    }
    String regex = "^((https|http)://)[a-zA-Z0-9+.]*[a-zA-Z]";
    Pattern patt = Pattern.compile(regex);
    Matcher matcher = patt.matcher(url);
    boolean isMatch = matcher.matches();
    return isMatch;
  }

  public static void main(String[] args) {
    String url = "http://liuyuanwai.com.cn";
    System.out.print(ValidatorUtils.validatorUrl(url));
  }
}
