package com.utils;

import com.domain.ex.BizException;
import com.domain.ex.Codes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by Administrator on 2015/12/13 0013.
 * 读取属性文件信息
 */
public class ConfigUtils {
  private static final Logger LOG = LoggerFactory.getLogger(ConfigUtils.class);
  private static Properties properties = new Properties();
  private static final String configName = "config/properties/config.properties";

  static {
    try {
      properties.load(new InputStreamReader(ConfigUtils.class.getClassLoader().getResourceAsStream(configName), "UTF-8"));
    } catch (Exception e) {
      e.printStackTrace();
      throw BizException.create(Codes.SystemError, "读属性文件错误");
    }
  }

  public static String getString(String key) {

    return properties.getProperty(key);
  }

  public static Double getDouble(String key) {
    // - 字符串转换为double
    return Double.parseDouble(properties.getProperty(key));
  }
}
