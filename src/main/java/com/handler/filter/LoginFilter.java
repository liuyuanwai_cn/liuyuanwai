package com.handler.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Huoyunren on 2016/2/16.
 */
public class LoginFilter implements Filter {


  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse rep = (HttpServletResponse) response;
    String url = req.getRequestURI();
//    if (!url.contains("login")
//      && !url.contains("findpassword")
//      && !url.contains("timeout")
//      && !url.contains("nav")
//      && !url.contains("home")
//      && !url.contains("index")
//      && !url.contains("shop")
//      && !url.contains("join")
//      && !url.contains(".")
//      && !url.equals("/")
//      && !url.contains("register")) {
//      if (req.getSession() == null || req.getSession().getAttribute("email") == null) {
//        // - session超时，跳转到首页通过cookie跳转
//        rep.sendRedirect("/timeout");
//      }
//    }
    chain.doFilter(request, response);
  }

  @Override
  public void destroy() {

  }
}
