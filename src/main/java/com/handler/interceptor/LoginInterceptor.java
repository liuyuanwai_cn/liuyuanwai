package com.handler.interceptor;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Huoyunren on 2016/2/16.
 * 登录拦截，未登录请求跳转到登录页面
 */
public class LoginInterceptor extends HandlerInterceptorAdapter{

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


    String url = request.getRequestURI();
    if (!url.contains("login")
      && !url.contains("findpassword")
      && !url.contains("product")
      && !url.contains("preadd")
      && !url.contains("join")
      && !url.contains("timeout")
      && !url.contains("index")
      && !url.contains("register")){
      if (request.getSession().getAttribute("email") == null){
        // - 未登录
        response.sendRedirect("/timeout.jsp");
      }
    }
    return super.preHandle(request, response, handler);
  }
}
