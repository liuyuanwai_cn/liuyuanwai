package com.handler.email;

import java.util.Map;

/**
 * Created by Huoyunren on 2016/2/15.
 */
public class Email {
  // - 收件人，多个邮箱逗号分割
  private String[] to;
  private String subject;
  private String content;
  private Boolean enableHtml;
  // - 目标名称
  private String template;
  // - 模版参数
  private Map<String, Object> map;

  public Boolean getEnableHtml() {
    return enableHtml;
  }

  public void setEnableHtml(Boolean enableHtml) {
    this.enableHtml = enableHtml;
  }

  public String getTemplate() {
    return template;
  }

  public void setTemplate(String template) {
    this.template = template;
  }

  public Map<String, Object> getMap() {
    return map;
  }

  public void setMap(Map<String, Object> map) {
    this.map = map;
  }

  public String[] getTo() {
    return to;
  }

  public void setTo(String[] to) {
    this.to = to;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
