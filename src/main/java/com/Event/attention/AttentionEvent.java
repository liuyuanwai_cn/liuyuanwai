package com.event.attention;

/**
 * Created by Huoyunren on 2016-09-07.
 * 关注消息体
 */
public class AttentionEvent {
  private String email;
  private String attentionEmail;
  private Integer isAttention;
  public Integer getIsAttention() {
    return isAttention;
  }
  public void setIsAttention(Integer isAttention) {
    this.isAttention = isAttention;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getAttentionEmail() {
    return attentionEmail;
  }
  public void setAttentionEmail(String attentionEmail) {
    this.attentionEmail = attentionEmail;
  }
}
