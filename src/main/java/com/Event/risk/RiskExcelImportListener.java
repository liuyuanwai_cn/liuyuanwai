package com.event.risk;

import com.task.risk.RiskExcelImportTask;
import com.google.common.eventbus.Subscribe;
import com.google.gson.Gson;
import com.pojo.ShRiskUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.List;

/**
 * Created by Huoyunren on 2016/1/16.
 *
 *  - 风控用户导入消息监听
 */
public class RiskExcelImportListener {

  private static final Logger LOG = LoggerFactory.getLogger(RiskExcelImportListener.class);
  @Autowired
  private Gson gson;
  @Autowired
  private ThreadPoolTaskExecutor threadPoolTaskExecutor;

  @Subscribe
  public void invok(List<ShRiskUser> list){


    RiskExcelImportTask riskExcelImportTask = new RiskExcelImportTask();
    riskExcelImportTask.setList(list);
    threadPoolTaskExecutor.execute(riskExcelImportTask);
    LOG.info("obj = {0}",gson.toJson(list));

  }

}
