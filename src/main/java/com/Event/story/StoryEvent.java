package com.event.story;

/**
 * Created by Huoyunren on 2016-09-07.
 */
public class StoryEvent {
  private String email;
  private String title;
  private String subTitle;
  private String articleUuid;

  public StoryEvent(String email, String title, String subTitle, String articleUuid) {
    this.email = email;
    this.title = title;
    this.subTitle = subTitle;
    this.articleUuid = articleUuid;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubTitle() {
    return subTitle;
  }

  public void setSubTitle(String subTitle) {
    this.subTitle = subTitle;
  }

  public String getArticleUuid() {
    return articleUuid;
  }

  public void setArticleUuid(String articleUuid) {
    this.articleUuid = articleUuid;
  }
}
