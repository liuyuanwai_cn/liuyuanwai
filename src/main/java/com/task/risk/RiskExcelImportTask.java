package com.task.risk;

import com.pojo.ShRiskUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Huoyunren on 2016/1/17.
 *
 */

public class RiskExcelImportTask implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(RiskExcelImportTask.class);

  private List<ShRiskUser> list;

  public List<ShRiskUser> getList() {
    return list;
  }

  public void setList(List<ShRiskUser> list) {
    this.list = list;
  }

  /**
   * 风控用户数据导入异步任务将导入数据入库
   */
  @Override
  public void run() {

//    RiskService riskService = (RiskService)ApplicationContextHandler.getSpringBean("riskService");
//    int ret = riskService.batchSaveOrUpdate(list);
//    LOG.info("进入到job任务：{}",ret);


  }
}
