package com.mapper.generator;

import com.pojo.ShProduct;

public interface ShProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShProduct record);

    int insertSelective(ShProduct record);

    ShProduct selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShProduct record);

    int updateByPrimaryKeyWithBLOBs(ShProduct record);

    int updateByPrimaryKey(ShProduct record);
}