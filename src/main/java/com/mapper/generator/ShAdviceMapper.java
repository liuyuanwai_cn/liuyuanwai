package com.mapper.generator;

import com.pojo.ShAdvice;

public interface ShAdviceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShAdvice record);

    int insertSelective(ShAdvice record);

    ShAdvice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShAdvice record);

    int updateByPrimaryKeyWithBLOBs(ShAdvice record);

    int updateByPrimaryKey(ShAdvice record);
}