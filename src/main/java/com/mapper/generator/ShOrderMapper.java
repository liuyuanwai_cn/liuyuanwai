package com.mapper.generator;

import com.pojo.ShOrder;

public interface ShOrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShOrder record);

    int insertSelective(ShOrder record);

    ShOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShOrder record);

    int updateByPrimaryKey(ShOrder record);
}