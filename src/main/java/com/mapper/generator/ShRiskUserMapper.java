package com.mapper.generator;

import com.pojo.ShRiskUser;

public interface ShRiskUserMapper {
    int deleteByPrimaryKey(Integer[] idArr);

    int insert(ShRiskUser record);

    int insertSelective(ShRiskUser record);

    ShRiskUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShRiskUser record);

    int updateByPrimaryKey(ShRiskUser record);
}