package com.mapper.generator;

import com.pojo.ShAuction;

public interface ShAuctionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShAuction record);

    int insertSelective(ShAuction record);

    ShAuction selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShAuction record);

    int updateByPrimaryKey(ShAuction record);
}