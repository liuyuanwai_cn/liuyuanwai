package com.mapper.generator;

import com.pojo.ShWords;

public interface ShWordsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShWords record);

    int insertSelective(ShWords record);

    ShWords selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShWords record);

    int updateByPrimaryKeyWithBLOBs(ShWords record);

    int updateByPrimaryKey(ShWords record);
}