package com.mapper.generator;

import com.pojo.ShSet;

public interface ShSetMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShSet record);

    int insertSelective(ShSet record);

    ShSet selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShSet record);

    int updateByPrimaryKey(ShSet record);
}