package com.mapper.generator;

import com.pojo.ShShop;

public interface ShShopMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShShop record);

    int insertSelective(ShShop record);

    ShShop selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShShop record);

    int updateByPrimaryKey(ShShop record);
}