package com.mapper.generator;

import com.pojo.ShLink;

public interface ShLinkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShLink record);

    int insertSelective(ShLink record);

    ShLink selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShLink record);

    int updateByPrimaryKey(ShLink record);
}