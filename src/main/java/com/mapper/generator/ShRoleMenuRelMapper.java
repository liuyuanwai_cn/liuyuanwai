package com.mapper.generator;

import com.pojo.ShRoleMenuRel;

public interface ShRoleMenuRelMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShRoleMenuRel record);

    int insertSelective(ShRoleMenuRel record);

    ShRoleMenuRel selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShRoleMenuRel record);

    int updateByPrimaryKey(ShRoleMenuRel record);
}