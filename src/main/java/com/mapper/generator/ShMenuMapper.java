package com.mapper.generator;

import com.pojo.ShMenu;

public interface ShMenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShMenu record);

    int insertSelective(ShMenu record);

    ShMenu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShMenu record);

    int updateByPrimaryKey(ShMenu record);
}