package com.mapper.generator;

import com.pojo.ShRole;

public interface ShRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShRole record);

    int insertSelective(ShRole record);

    ShRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShRole record);

    int updateByPrimaryKey(ShRole record);
}