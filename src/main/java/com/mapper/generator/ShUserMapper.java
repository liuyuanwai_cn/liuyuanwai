package com.mapper.generator;

import com.pojo.ShUser;

public interface ShUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShUser record);

    int insertSelective(ShUser record);

    ShUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShUser record);

    int updateByPrimaryKey(ShUser record);
}