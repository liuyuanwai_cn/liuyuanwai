package com.mapper.generator;

import com.pojo.ShInventory;

public interface ShInventoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShInventory record);

    int insertSelective(ShInventory record);

    ShInventory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShInventory record);

    int updateByPrimaryKey(ShInventory record);
}