package com.mapper.generator;

import com.pojo.ShStory;

public interface ShStoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShStory record);

    int insertSelective(ShStory record);

    ShStory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShStory record);

    int updateByPrimaryKeyWithBLOBs(ShStory record);

    int updateByPrimaryKey(ShStory record);
}