package com.mapper.generator;

import com.pojo.ShAttention;

public interface ShAttentionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShAttention record);

    int insertSelective(ShAttention record);

    ShAttention selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShAttention record);

    int updateByPrimaryKey(ShAttention record);
}