package com.mapper.generator;

import com.pojo.ShCover;

public interface ShCoverMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShCover record);

    int insertSelective(ShCover record);

    ShCover selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShCover record);

    int updateByPrimaryKey(ShCover record);
}