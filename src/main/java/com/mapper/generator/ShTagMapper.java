package com.mapper.generator;

import com.pojo.ShTag;

public interface ShTagMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShTag record);

    int insertSelective(ShTag record);

    ShTag selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShTag record);

    int updateByPrimaryKey(ShTag record);
}