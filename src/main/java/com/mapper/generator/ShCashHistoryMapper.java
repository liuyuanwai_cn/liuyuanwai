package com.mapper.generator;

import com.pojo.ShCashHistory;

public interface ShCashHistoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShCashHistory record);

    int insertSelective(ShCashHistory record);

    ShCashHistory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShCashHistory record);

    int updateByPrimaryKey(ShCashHistory record);
}