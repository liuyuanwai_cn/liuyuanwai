package com.mapper.generator;

import com.pojo.ShNote;

public interface ShNoteMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShNote record);

    int insertSelective(ShNote record);

    ShNote selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShNote record);

    int updateByPrimaryKey(ShNote record);
}