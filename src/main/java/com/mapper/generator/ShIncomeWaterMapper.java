package com.mapper.generator;

import com.pojo.ShIncomeWater;

public interface ShIncomeWaterMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShIncomeWater record);

    int insertSelective(ShIncomeWater record);

    ShIncomeWater selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShIncomeWater record);

    int updateByPrimaryKey(ShIncomeWater record);
}