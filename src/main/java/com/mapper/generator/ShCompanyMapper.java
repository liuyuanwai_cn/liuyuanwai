package com.mapper.generator;

import com.pojo.ShCompany;

public interface ShCompanyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShCompany record);

    int insertSelective(ShCompany record);

    ShCompany selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShCompany record);

    int updateByPrimaryKey(ShCompany record);
}