package com.mapper.generator;

import com.pojo.ShProductType;

public interface ShProductTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShProductType record);

    int insertSelective(ShProductType record);

    ShProductType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShProductType record);

    int updateByPrimaryKey(ShProductType record);
}