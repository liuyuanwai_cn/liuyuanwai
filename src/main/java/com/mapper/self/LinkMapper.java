package com.mapper.self;

import com.pojo.ShLink;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LinkMapper {


  public int saveOrUpdate(ShLink link);

  public List<ShLink> list();

  public int del(@Param("linkId") Integer linkId);

  public int incrClickCount(@Param("linkId") Integer linkId);

}