package com.mapper.self;

import com.dto.AttentionDto;
import com.pojo.ShAttention;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AttentionMapper {


    public int saveOrUpdate(ShAttention attention);

    public ShAttention queryAttention(@Param("attentionEmail")String attentionEmail,@Param("email")String email);

    // - 查我的关注
    public List<AttentionDto> list(@Param("email")String email,@Param("start")Integer start,@Param("offset")Integer offset);

    // - 查粉丝
    public List<AttentionDto> queryFans(@Param("email")String email);
}