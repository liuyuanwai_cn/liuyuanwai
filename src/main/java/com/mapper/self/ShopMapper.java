package com.mapper.self;

import com.pojo.ShShop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShopMapper {
  public ShShop queryByEmail(@Param("email") String email);

  public void saveOrUpdate(ShShop shop);

  public int count(@Param("shopStatus") Integer shopStatus);

  public List<ShShop> list(@Param("start") int start, @Param("limit") int limit, @Param("shopStatus") Integer shopStatus);

  public void updateLogo(@Param("logo")String logo,@Param("email")String email);
}