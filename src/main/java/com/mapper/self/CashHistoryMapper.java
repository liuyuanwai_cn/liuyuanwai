package com.mapper.self;

import com.pojo.ShCashHistory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CashHistoryMapper {

  public void updateCodeStatus(@Param("codeStatus") int codeStatus, @Param("email") String email, @Param("cashCode") Integer cashCode);

  public ShCashHistory queryCashHistory(@Param("email") String email, @Param("cashCode") Integer cashCode);

  public void dealTimeOutCashCode();

  public List<ShCashHistory> list(@Param("start")Integer start,@Param("limit")Integer limit,@Param("email")String email);

  public int count(@Param("email")String email);

  public ShCashHistory queryCashCode(@Param("email")String email);
}