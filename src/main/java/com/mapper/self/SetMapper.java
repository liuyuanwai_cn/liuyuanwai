package com.mapper.self;

import com.pojo.ShSet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SetMapper {

  public int saveOrUpdate(ShSet set);

  public ShSet queryByEmail(@Param("email") String email);

  public List<ShSet> list(@Param("list") List<String> emailList);
}