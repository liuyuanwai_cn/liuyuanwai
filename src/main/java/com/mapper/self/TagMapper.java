package com.mapper.self;

import com.pojo.ShTag;

import java.util.List;

public interface TagMapper {

    public List<ShTag> list();

}