package com.mapper.self;

import com.dto.StoryExtDto;
import com.pojo.ShStory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StoryMapper {

  public List<StoryExtDto> list(@Param("start")Integer start,@Param("offset")Integer offset,@Param("email")String email,@Param("orderField")String orderField,@Param("storyType")Integer storyType,@Param("tag")Integer tag);

  public StoryExtDto queryByUuid(@Param("uuid")String uuid) ;

  public void incrReadCount(@Param("uuid")String uuid);

  public List<ShStory> queryByKeyWord(@Param("title")String title);
}