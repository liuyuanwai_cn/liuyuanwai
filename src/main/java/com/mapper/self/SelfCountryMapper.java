package com.mapper.self;

import com.pojo.Country;

import java.util.List;

public interface SelfCountryMapper {

    public List<Country> queryAll();
}