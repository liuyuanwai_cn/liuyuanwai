package com.mapper.self;

import com.dto.OrderExtDto;
import com.dto.statistic.StatisticOutputDto;
import com.pojo.ShOrder;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface OrderMapper {
    public List<OrderExtDto> list(@Param("page") int page, @Param("limit") int limit, @Param("email") String email);

    public List<OrderExtDto> orderManage(@Param("page") int page, @Param("limit") int limit, @Param("shopEmail") String shopEmail);

    public int orderMgrCount();

    public int count(@Param("email") String email);

    public OrderExtDto queryByOrderNo(@Param("orderNo") String orderNo);

    public ShOrder selectByOrderNo(@Param("orderNo") String orderNo);

    /**
     * 批量更新订单状态
     *
     * @param list        订单编号列表
     * @param orderStatus 订单新状态
     * @param reason      备注
     * @return
     */
    public int updateOrderStatus(@Param("list") List<String> list, @Param("orderStatus") Integer orderStatus, @Param("reason") String reason);

    public List<ShOrder> queryByOrderNos(@Param("list") List<String> list);

    public BigDecimal waterStatistic();

    public BigDecimal shopWaterStatistic(@Param("shopEmail") String shopEmail);

    // - 成本统计
    public BigDecimal costStatistic();

    public BigDecimal shopCostStatistic(@Param("shopEmail") String shopEmail);

    public List<String> queryTimeOutOrders();

    public void dealTimeOutOrder();



}