package com.mapper.self;


import com.pojo.ShProductType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductTypeMapper {

    public int saveOrUpdate(ShProductType productType);

    public List<ShProductType> list(@Param("offset")Integer offset,@Param("start")Integer start);

    public int del(@Param("id")Integer id,@Param("email")String email);

    public int isTypeInUsing(@Param("productTypeId")Integer productTypeId);

    public int queryByName(@Param("productTypeName")String productTypeName);
}