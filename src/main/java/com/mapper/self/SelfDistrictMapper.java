package com.mapper.self;

import com.pojo.District;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SelfDistrictMapper {

    public List<District> queryByPid(@Param("pid")Integer pid);
}