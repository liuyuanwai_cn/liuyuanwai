package com.mapper.self;

import com.pojo.ShMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuMapper {

    /**
     * 分页查询
     * @param start 开始索引
     * @param limit 分页条数
     * @return
     */
    public List<ShMenu> search(@Param("start")int start,@Param("limit")int limit);


    public List<ShMenu> loadMenuTreeByRoleId(@Param("roleId")Integer roleId);
    public List<ShMenu> loadMenuTree();


    public int count();
    public List<ShMenu> loadParentMenu();


    public int isMenuNameExist(@Param("menuName") String menuName,@Param("pid")Integer pid);

    public ShMenu queryByMenuName(@Param("menuName") String menuName);

    public int saveOrUpdate(ShMenu shMenu);


    public void batchDel(@Param("idArr") String[]idArr);


}