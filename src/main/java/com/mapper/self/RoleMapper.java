package com.mapper.self;

import com.pojo.ShRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {

    public List<ShRole> list(@Param("page")int page,@Param("limit")int limit);

    public int count();


    public int batchDelByRoleIds(@Param("roleIds")Integer[] roleIds);

    public int updateRoleNameByRoleId(@Param("roleId")int roleId,@Param("roleName")String roleName);

}