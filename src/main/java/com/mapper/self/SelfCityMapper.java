package com.mapper.self;

import com.pojo.City;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SelfCityMapper {
    public List<City> queryByPid(@Param("pid")Integer pid);
}