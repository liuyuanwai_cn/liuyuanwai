package com.mapper.self;

import com.dto.statistic.StatisticOutputDto;
import com.pojo.ShRiskUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RiskUserMapper {


    /**
     * 查询用户列表
     * @return
     */
    public List<ShRiskUser> query(@Param("key")String key,@Param("offset")Integer offset,@Param("limit") Integer limit);


    /**
     * 批量插入/更新
     * @param list
     * @return
     */
    public int batchSaveOrUpdate(@Param("list")List<ShRiskUser> list);


    // - 条件查询总页数
    public int queryTotalCount(@Param("key")String key);


    // - 批量删除
    public int batchDel(@Param("arr")Integer[] arr);


    // - 年统计
    public List<StatisticOutputDto> yearStatistic();
    // - 月统计
    public List<StatisticOutputDto> monthStatistic();
    // - 日统计
    public List<StatisticOutputDto> dayStatistic();

}