package com.mapper.self;

import com.pojo.ShCover;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CoverMapper {
    public List<ShCover> list(@Param("page")int page,@Param("limit")int limit,@Param("email")String email);


    public int count(@Param("email")String email);


    public ShCover queryByEmail(@Param("email")String email);


    public int check(@Param("ids")String[]ids,@Param("status")String status,@Param("reason")String reason);


    public int saveOrUpdate(ShCover shCover);
}