package com.mapper.self;

import com.dto.AdviceDto;
import com.dto.RewardDto;
import com.dto.UserAdviceDto;
import com.pojo.ShAdvice;
import org.apache.ibatis.annotations.Param;
import org.quartz.Scheduler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface AdviceMapper {
  public List<UserAdviceDto> list(@Param("page") int page, @Param("limit") int limit, @Param("email") String email,@Param("type")Integer type,@Param("status")Integer status);

  public int count(@Param("email") String email);

  public void reward(@Param("uuid") String uuid, @Param("dealEmail") String dealEmail, @Param("dealTime") Date dealTime);

  public RewardDto queryRewardInfo(@Param("uuid") String uuid, @Param("dealEmail") String dealEmail);

  public AdviceDto queryByUuid(@Param("uuid") String uuid);

  public void changeAuctionStatus(@Param("uuid") String uuid, @Param("status") Integer status,@Param("dealEmail")String dealEmail,@Param("dealPrice")BigDecimal dealPrice);

  public void del(@Param("uuid")String uuid);
}