package com.mapper.self;

import com.dto.AuctionDto;
import com.pojo.ShAuction;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuctionMapper {

  public ShAuction queryByUuidAndEmail(@Param("uuid")String uuid,@Param("email")String email);

  public AuctionDto queryLatestByEmailAndUuid(@Param("email") String email,@Param("uuid")String uuid);

  public List<AuctionDto> list(@Param("uuid")String uuid,@Param("start")Integer start,@Param("offset")Integer offset);

  public ShAuction queryLatestByUuid(@Param("uuid")String uuid);
}