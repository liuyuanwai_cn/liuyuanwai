package com.mapper.self;

import com.pojo.ShRoleMenuRel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMenuRelMapper {


    /**
     * 批量新增
     * @param list
     * @return
     */
    public int batchInsert(@Param("list") List<ShRoleMenuRel> list);


    public List<ShRoleMenuRel> listByRoleId(@Param("roleId")int roleId);

    public int batchDelByRoleIds(@Param("roleIds")Integer[] roleIds);
}