package com.mapper.self;

import com.dto.statistic.NameValueDto;
import com.pojo.ShProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper {
    public List<ShProduct> list(@Param("page") int page, @Param("limit") int limit, @Param("deleted") Integer deleted, @Param("email") String email,@Param("soldOut")Integer soldOut,@Param("productType")Integer productType);

    public int count(@Param("deleted") Integer deleted, @Param("email") String email);

    public ShProduct queryByName(@Param("name") String name);

    public int batchDel(@Param("productIds") Integer[] productIds);

    public int updateProductImg(@Param("productId") Integer productId, @Param("url") String url);

    public int update(ShProduct shProduct);

    public void updateProductSalesAndInventory(@Param("count") Integer count, @Param("productId") Integer productId);

    public void updateProductStatus(@Param("productId") Integer productId, @Param("deleted") Integer deleted);

    public List<NameValueDto> queryProductSalesByShopEmail(@Param("email")String email);

    public int countProductByShopEmail(@Param("email")String email);

    public int incrReadCount(@Param("productId")Integer productId);

    public List<ShProduct> queryByKeyWord(@Param("name")String name);
}