package com.mapper.self;

import com.dto.statistic.IdCountDto;
import com.pojo.Province;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SelfProvinceMapper {

    public List<Province> queryByPid(@Param("pid")Integer pid);


    /**
     * 按区域统计员工数量
     * @param type  "country":国家,"province":省，"":市
     * @return
     */
    public List<IdCountDto> queryAreaCompanyCount(@Param("type")String type);


    /**
     * 按审核状态统计员工数量
     * @param type      "country":国家,"province":省，"":市
     * @param id        国家，省，市ID
     * @return
     */
    public List<IdCountDto> queryStatusCompanyCount(@Param("type")String type,@Param("id")Integer id);

}