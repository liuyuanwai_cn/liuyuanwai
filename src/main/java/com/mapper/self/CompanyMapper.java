package com.mapper.self;

import com.dto.CompanyExtDto;
import com.dto.UserExtDto;
import com.dto.statistic.SortDto;
import com.dto.statistic.StatisticOutputDto;
import com.pojo.ShCompany;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface CompanyMapper {
  int saveOrUpdate(ShCompany shCompany);

  public ShCompany login(@Param("email") String email, @Param("password") String password);

  public ShCompany queryByEmail(@Param("email") String email);

  public ShCompany queryUserExtByEmail(@Param("email") String email);

  public int updatePassword(@Param("password") String password, @Param("email") String email, @Param("oldPassword") String oldPassword, @Param("updatetime") Date updatetime);

  public int register(@Param("email") String email, @Param("password") String password, @Param("createtime") Date createtime, @Param("code") String code, @Param("referralCode") String referralCode);

  public List<ShCompany> search(@Param("start") int start, @Param("limit") int limit, @Param("queryKey") String queryKey);

  public int count(@Param("queryKey") String key, @Param("role") String role);

  public int check(@Param("ids") String[] ids, @Param("status") Integer status);

  public int updateUserRole(@Param("email") String email, @Param("roleId") Integer roleId);

  public void updateLogo(@Param("logo") String logo, @Param("email") String email);

  public void updateWeixinQrcode(@Param("weixinQrcode") String weixinQrcode, @Param("email") String email);

  public void updateAlipayQrcode(@Param("alipayQrcode") String alipayQrcode, @Param("email") String email);

  public void incrInCount(@Param("email") String email);

  public List<ShCompany> list(@Param("start") int start, @Param("limit") int limit, @Param("queryKey") String key, @Param("role") String role);

  public int incrUserCredits(@Param("email") String email, @Param("amount") BigDecimal amount);

  // - 流水统计
  public List<StatisticOutputDto> searchConsumptionWarter(@Param("email") String email);

  // - 流水统计
  public List<StatisticOutputDto> searchShopWarter(@Param("shopEmail") String shopEmail);

  public List<String> querySalesman();

  public List<String> queryCodes();

  public int isReferralCodeExists(@Param("referralCode") String referralCode);

  public CompanyExtDto queryCompanyExtByEmail(@Param("email") String email);

  public List<StatisticOutputDto> userStatistic();

  public void addIncome(@Param("email") String email, @Param("income") BigDecimal income);

  public List<SortDto> sortList();

  public SortDto sortData(@Param("email") String email);

  public List<ShCompany> queryByKeyWord(@Param("name")String name);
}