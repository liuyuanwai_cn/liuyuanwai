package com.mapper.self;

import com.pojo.ShIncomeWater;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface IncomeWaterMapper {
  public void batchInsert(List<ShIncomeWater> incomeWaters);

  // - 统计我的总收益
  public BigDecimal calculateMyIncome(@Param("email") String email, @Param("deleted") Integer deleted);

  // - 查询在某个门店收益
  public BigDecimal calculateSingleIncome(@Param("buyerEmail") String buyerEmail, @Param("shopEmail") String shopEmail);

  public List<ShIncomeWater> list(@Param("email") String email, @Param("start") int start, @Param("limit") int limit,@Param("shopId")Integer shopId);

  public List<ShIncomeWater> listIncomeGroupByShop(@Param("email") String email, @Param("start") int start, @Param("limit") int limit);

  public int count(@Param("email") String email);

  public void updateIncomeStatus(@Param("buyerEmail") String buyerEmail, @Param("shopEmail") String shopEmail);
}