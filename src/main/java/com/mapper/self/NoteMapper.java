package com.mapper.self;

import com.dto.NoteDto;
import com.pojo.ShNote;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface NoteMapper {

  public List<NoteDto> queryByUuid(@Param("list") Set<String> uuid);

  public NoteDto queryByNoteId(@Param("noteId") Integer noteId);

  public List<NoteDto> list(@Param("uuid") String uuid, @Param("beginIndex") Integer beginIndex, @Param("offset") Integer offset);

  public void del(@Param("uuid")String uuid);

}