package com.mapper.self;

import com.dto.WordsDto;
import com.pojo.ShWords;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WordsMapper {

  public List<WordsDto> list(@Param("start") Integer start, @Param("offset") Integer offset, @Param("to") String to);

}