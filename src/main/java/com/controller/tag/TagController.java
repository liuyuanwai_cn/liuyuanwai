package com.controller.tag;

import com.domain.HttpResponse;
import com.service.tag.TagService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Huoyunren on 2016-07-01.
 */
@Controller
@RequestMapping("tag")
public class TagController {
  @Autowired
  private TagService tagService;

  @ResponseBody
  @RequestMapping("add")
  public Object add(@Param("name") String name) {

    return tagService.save(name);
  }

  @ResponseBody
  @RequestMapping("list")
  public Object list() {

    return tagService.list();
  }
}
