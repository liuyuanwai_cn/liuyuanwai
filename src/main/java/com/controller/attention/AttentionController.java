package com.controller.attention;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.mapper.self.AttentionMapper;
import com.pojo.ShAttention;
import com.service.attention.AttentionService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Huoyunren on 2016-09-06.
 */
@Controller
@RequestMapping("attention")
public class AttentionController {

  @Autowired
  private AttentionService attentionService;
  @Autowired
  private AttentionMapper attentionMapper;

  // - 添加关注
  @RequestMapping("add")
  @ResponseBody
  public Object add(@Param("attentionEmail") String attentionEmail) {

    attentionService.saveOrUpdate(attentionEmail, 1);
    return HttpResponse.OK();
  }

  // - 取消关注
  @RequestMapping("cancel")
  @ResponseBody
  public Object cancelAttention(@Param("attentionEmail") String attentionEmail) {

    attentionService.saveOrUpdate(attentionEmail, 0);
    return HttpResponse.OK();
  }

  // - 查询关注
  @RequestMapping("queryAttention")
  @ResponseBody
  public Object queryAttention(@Param("attentionEmail") String attentionEmail, @Param("email") String email) {

    ShAttention data = attentionMapper.queryAttention(attentionEmail, email);
    return HttpResponse.create(Codes.OK, "", data);
  }

  // - 关注列表
  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("email") String email,@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize) {


    return attentionService.list(email,pageNo,pageSize);
  }

}
