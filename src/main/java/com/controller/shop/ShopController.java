package com.controller.shop;

import com.domain.HttpResponse;
import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.pojo.ShShop;
import com.service.shop.ShopService;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.formula.functions.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping(value = "shop")
public class ShopController {

  @Autowired
  private ShopService shopService;

  @RequestMapping("info")
  public ModelAndView info(HttpServletRequest request){
    Object obj = request.getSession().getAttribute("email");
    String email = "";
    if (obj != null){
      email = obj.toString();
    }
    ShShop shop  = shopService.queryByEmail(email);
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject(shop);
    modelAndView.setViewName("/shop/info");
    return modelAndView;
  }
  @RequestMapping("queryInfo")
  @ResponseBody
  public Object queryInfo(HttpServletRequest request){
    Object obj = request.getSession().getAttribute("email");
    String email = null;
    if (obj != null){
      email = obj.toString();
    }
    ShShop shop  = shopService.queryByEmail(email);
    return HttpResponse.create(Codes.OK,"",shop);
  }

  @RequestMapping("shopInfo")
  public ModelAndView shopInfo(@Param("email")String email){
    ShShop shop  = shopService.queryByEmail(email);
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject(shop);
    return modelAndView;
  }

  @RequestMapping("mgr")
  @ResponseBody
  public Object mgr(@RequestBody ShShop shop,HttpServletRequest request){
    shop.setEmail(request.getSession().getAttribute("email").toString());
    shopService.updateShopInfo(shop);
    return HttpResponse.OK();
  }

  @RequestMapping("updateLogo")
  @ResponseBody
  public Object updateLogo(@Param("logo")String logo,HttpServletRequest request){
    Object obj = request.getSession().getAttribute("email");
    if (obj == null){
      throw BizException.create(Codes.SystemDeclare,"登录超时");
    }
    String email = obj.toString();
    shopService.updateLogo(logo,email);
    return HttpResponse.OK();
  }

  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize){

    return shopService.list(pageNo,pageSize);
  }



}
