package com.controller.area;

import com.dto.statistic.IdCountDto;
import com.mapper.generator.CityMapper;
import com.mapper.generator.CountryMapper;
import com.mapper.generator.ProvinceMapper;
import com.mapper.self.SelfCityMapper;
import com.mapper.self.SelfCountryMapper;
import com.mapper.self.SelfProvinceMapper;
import com.service.area.AreaService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Huoyunren on 2016/1/27.
 */
@RequestMapping(value = "area")
@Controller
public class AreaController {
  @Autowired
  private SelfCountryMapper selfCountryMapper;
  @Autowired
  private SelfProvinceMapper selfProvinceMapper;
  @Autowired
  private SelfCityMapper selfCityMapper;
  @Autowired
  private AreaService areaService;


  /**
   * 查国家
   * @return
   */
  @RequestMapping(value = "queryCountry")
  @ResponseBody
  public Object queryCountry(){

    return selfCountryMapper.queryAll();
  }


  /**
   * 查省
   * @param pid 国家ID，固定值1为中国
   * @return
   */
  @RequestMapping(value = "queryProvince")
  @ResponseBody
  public Object queryProvince(@Param("pid")Integer pid){

    return areaService.queryProvince(pid);
  }


  /**
   * 查城市
   * @param pid 省ID
   * @return
   */
  @RequestMapping(value = "queryCity")
  @ResponseBody
  public Object queryCity(@Param("pid")Integer pid){

    return selfCityMapper.queryByPid(pid);
  }

  /**
   * 按区域统计员工数量
   * @param type  "country":国家,"province":省，"":市
   * @return
   */
  @RequestMapping(value = "queryAreaCompanyCount")
  @ResponseBody
  public Object queryAreaCompanyCount(@Param("type")String type){

    List<IdCountDto> list = selfProvinceMapper.queryAreaCompanyCount(type);
    return list;
  }

  /**
   * 按审核状态统计员工数量
   * @param type      "country":国家,"province":省，"":市
   * @param id        国家，省，市ID
   * @return
   */
  @RequestMapping(value = "queryStatusCompanyCount")
  @ResponseBody
  public Object queryStatusCompanyCount(@Param("type")String type,@Param("id")Integer id){



    return areaService.queryStatusCompanyCount(type,id);
  }

}
