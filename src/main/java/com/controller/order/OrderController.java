package com.controller.order;

import com.domain.myenum.ProductUnitEnum;
import com.dto.OrderExtDto;
import com.dto.UserExtDto;
import com.mapper.generator.ShProductMapper;
import com.mapper.generator.ShShopMapper;
import com.mapper.self.OrderMapper;
import com.mapper.self.ShopMapper;
import com.pojo.ShCompany;
import com.pojo.ShProduct;
import com.pojo.ShShop;
import com.service.order.OrderService;
import com.service.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2016/2/18 0018.
 */
@Controller
@RequestMapping("order")
public class OrderController {

  private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);
  @Autowired
  private ShProductMapper shProductMapper;
  @Autowired
  private UserService userService;
  @Autowired
  private OrderService orderService;
  @Autowired
  private OrderMapper orderMapper;
  @Autowired
  private ShShopMapper shopMapper;


  @RequestMapping("add")
  @ResponseBody
  public Object add(@Param("productId") int productId, @Param("email") String email, @Param("count") int count, @Param("sendtime") String sendtime, @Param("amount") BigDecimal amount, @Param("originalCost") BigDecimal originalCost, @Param("deliveryType") Integer deliveryType, @Param("sendAddr") String sendAddr, @Param("remark") String remark) {

    return orderService.add(productId, email, count, sendtime, amount, originalCost, deliveryType, sendAddr, remark);
  }

  // - 下单页面
  @RequestMapping("preadd")
  @ResponseBody
  public ModelAndView preAdd(@Param("productId") int productId) {

    OrderExtDto orderExtDto = new OrderExtDto();
    // - 商品信息
    ShProduct shProduct = shProductMapper.selectByPrimaryKey(productId);
    BeanUtils.copyProperties(shProduct, orderExtDto);
    orderExtDto.setProductId(shProduct.getId());
    orderExtDto.setProductName(shProduct.getName());
    orderExtDto.setUnit(ProductUnitEnum.queryStatusVal(Integer.valueOf(shProduct.getUnit())));
    orderExtDto.setRemark(shProduct.getRemark());
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    Object obj = request.getSession().getAttribute("email");
    if (obj != null) {
      UserExtDto user = userService.queryInfo(obj.toString());
      if (user != null) {
        orderExtDto.setAddr(user.getAddr());
      }
    }
    UserExtDto shopper = userService.queryInfo(shProduct.getCompanyEmail());
    orderExtDto.setShopEmail(shopper.getEmail());
    orderExtDto.setShopName(shopper.getName());
    orderExtDto.setShopAddr(shopper.getAddr());
    orderExtDto.setShopPhone(shopper.getPhone());
    orderExtDto.setShopUrl(shopper.getLogo());
    orderExtDto.setWeixinQrcode(shopper.getWeixinQrcode());
    orderExtDto.setAlipayQrcode(shopper.getAlipayQrcode());
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject(orderExtDto);
    modelAndView.setViewName("order/preadd");
    return modelAndView;
  }


  // - 订单管理 - 我卖的单
  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize, HttpServletRequest request) {
    String email = request.getSession().getAttribute("email")==null?"":request.getSession().getAttribute("email").toString();
    Object obj = orderService.orderManage(pageNo, pageSize, email);
    return obj;
  }


  // - 我的订单
  @RequestMapping("myorder")
  @ResponseBody
  public Object myorder(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize, HttpServletRequest request) {
    String email = request.getSession().getAttribute("email").toString();
    Object obj = orderService.list(pageNo, pageSize, email);
    return obj;
  }

  @RequestMapping("infoMgr")
  @ResponseBody
  public ModelAndView infoMgr(@Param("orderNo") String orderNo,HttpServletRequest request) {
    ModelAndView modelAndView = new ModelAndView();
    if (request.getSession().getAttribute("email") == null){
      modelAndView.setViewName("user/login");
      return modelAndView;
    }
    OrderExtDto orderExtDto = orderMapper.queryByOrderNo(orderNo);
    if (StringUtils.isBlank(orderExtDto.getOrderRemark())) {
      orderExtDto.setOrderRemark("无");
    }
    orderExtDto.setUnit(ProductUnitEnum.queryStatusVal(Integer.valueOf(orderExtDto.getUnit())));
    modelAndView.addObject(orderExtDto);
    modelAndView.setViewName("/order/infoMgr");
    return modelAndView;
  }
  @RequestMapping("info")
  @ResponseBody
  public ModelAndView info(@Param("orderNo") String orderNo,HttpServletRequest request) {
    ModelAndView modelAndView = new ModelAndView();
    if (request.getSession().getAttribute("email") == null){
      modelAndView.setViewName("user/login");
      return modelAndView;
    }
    OrderExtDto orderExtDto = orderMapper.queryByOrderNo(orderNo);
    if (StringUtils.isBlank(orderExtDto.getOrderRemark())) {
      orderExtDto.setOrderRemark("无");
    }
    orderExtDto.setUnit(ProductUnitEnum.queryStatusVal(Integer.parseInt(orderExtDto.getUnit())));
    modelAndView.addObject(orderExtDto);
    modelAndView.setViewName("/order/info");
    return modelAndView;
  }


  // - 处理订单：更新订单状态
  @RequestMapping("dealOrder")
  @ResponseBody
  public Object dealOrder(@Param("orderNo") String orderNo, @Param("orderStatus") Integer orderStatus, @Param("reason") String reason) {


    return orderService.dealOrder(orderNo, orderStatus, reason);
  }

  // - 商品销量统计
  @RequestMapping("statisticSales")
  @ResponseBody
  public Object statisticSales(@Param("email")String email) {

    return orderService.statisticSales(email);
  }


}
