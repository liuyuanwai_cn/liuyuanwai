package com.controller.search;

import com.service.search.SearchService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Huoyunren on 2016-08-24.
 */

@Controller
@RequestMapping("search")
public class SearchController {

  @Autowired
  private SearchService searchService;


  @RequestMapping("search")
  @ResponseBody
  public Object search(@Param("keyword")String keyword){

    return searchService.search(keyword);
  }
}
