package com.controller.menu;

import com.domain.HttpResponse;
import com.domain.page.Paginator;
import com.dto.TempDto;
import com.google.gson.Gson;
import com.mapper.generator.ShMenuMapper;
import com.pojo.ShMenu;
import com.service.menu.SysMenuService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2015/11/29 0029.
 * 系统菜单【树形】
 */
@RequestMapping(value = "sysmenu")
@Controller
public class SysMenuController {
    private static final Logger LOG = LoggerFactory.getLogger(SysMenuController.class);

    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private Gson gson;
    @Autowired
    private ShMenuMapper shMenuMapper;

    /**
     * 查询菜单列表
     * @return
     */
    @RequestMapping(value = "search")
    @ResponseBody
    public Object search(@RequestBody Paginator paginator){

        Object obj = sysMenuService.search(paginator.getPage(),paginator.getLimit());
        return obj;
    }

    /**
     * 根据角色加载菜单树
     * @param roleId 角色ID
     * @return
     */
    @RequestMapping(value = "loadMenuTree")
    @ResponseBody
    public Object loadMenuTree(@Param("roleId")Integer roleId,HttpServletRequest request){


        if (roleId == null){
            if(request.getSession().getAttribute("roleId") == null){
                return null;
            }
            roleId = Integer.valueOf(request.getSession().getAttribute("roleId").toString());
        }

        Object obj = sysMenuService.loadMenuTree(roleId);
//        LOG.info("search.param {}",gson.toJson(obj));
        return obj;
    }


    /**
     * 加载父级菜单
     * @return
     */
    @RequestMapping(value = "loadParentMenu")
    @ResponseBody
    public Object loadParentMenu(){

        Object obj = sysMenuService.loadParentMenu();
//        LOG.info("loadParentMenu.param {}",obj);
        return obj;
    }

    /**
     * 加载子菜单
     * @return
     */
    @RequestMapping(value = "queryByMenuName",method = RequestMethod.POST)
    @ResponseBody
    public Object queryByMenuName(@RequestBody TempDto tempDto){

        Object obj = sysMenuService.queryByMenuName(tempDto.getTemp());
//        LOG.info("queryByMenuName.param {}",obj);
        return obj;
    }


    /**
     * 新增菜单
     * @return
     */
    @RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    public Object addMenu(@RequestBody ShMenu shMenu){

//        LOG.info("add.param {}",gson.toJson(shMenu));
        return sysMenuService.addMenu(shMenu);
    }


    /**
     * 编辑菜单
     * @return
     */
    @RequestMapping(value = "update",method = RequestMethod.POST)
    @ResponseBody
    public Object updateMenu(@RequestBody ShMenu shMenu){

//        LOG.info("update.param {}",gson.toJson(shMenu));
        return sysMenuService.saveOrUpdate(shMenu);
    }


    /**
     * 【批量】删除菜单
     * @return
     */
    @RequestMapping(value = "del")
    @ResponseBody
    public Object delMenu(@RequestBody TempDto tempDto){

//        LOG.info("del.param {}",gson.toJson(tempDto));
        sysMenuService.del(tempDto.getTemp());
        return gson.toJson(HttpResponse.OK());
    }


    @RequestMapping("info")
    @ResponseBody
    public ModelAndView info(@Param("menuId")int menuId){
        ModelAndView modelAndView = new ModelAndView();
        ShMenu shMenu = shMenuMapper.selectByPrimaryKey(menuId);
        modelAndView.addObject(shMenu);
        modelAndView.setViewName("/menu/info");
        return modelAndView;
    }

}
