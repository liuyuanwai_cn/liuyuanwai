package com.controller.productType;

import com.mapper.generator.ShProductMapper;
import com.mapper.generator.ShProductTypeMapper;
import com.pojo.ShProductType;
import com.service.productType.ProductTypeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Huoyunren on 2016-07-01.
 */
@Controller
@RequestMapping("productType")
public class ProductTypeController {
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private ShProductTypeMapper productTypeMapper;

    @RequestMapping("list")
    @ResponseBody
    public Object list(@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize) {
        return productTypeService.list(pageNo,pageSize);
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public Object saveOrUpdate(@Param("id") Integer id,@Param("name") String name,@Param("remark") String remark) {

        return productTypeService.saveOrUpdate(id,name,remark);
    }
    @RequestMapping("del")
    @ResponseBody
    public Object del(@Param("id") Integer id) {

        return productTypeService.del(id);
    }



    @RequestMapping("info")
    public ModelAndView info(@Param("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/productType/info");
        modelAndView.addObject("id",id);
        return modelAndView;
    }

    @RequestMapping("queryById")
    @ResponseBody
    public Object queryById(@Param("id")Integer id) {

        return productTypeMapper.selectByPrimaryKey(id);
    }
}
