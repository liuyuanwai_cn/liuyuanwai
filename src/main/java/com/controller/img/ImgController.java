package com.controller.img;

import com.service.img.ImgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping("img")
public class ImgController {
    private static final Logger LOG = LoggerFactory.getLogger(ImgController.class);

    @Autowired
    private ImgService imgService;

    @RequestMapping(value = "upload")
    @ResponseBody
    public Object upload(@RequestParam("filePath") CommonsMultipartFile file){

        return imgService.uploadImg(file);

    }
    @RequestMapping(value = "uploadForSimditor")
    @ResponseBody
    public Object uploadForSimditor(@RequestParam("filePath") CommonsMultipartFile file){

        return imgService.uploadImgForSimditor(file);

    }
    @RequestMapping(value = "upload2")
    @ResponseBody
    public Object upload2(@RequestParam("filePath_logo") CommonsMultipartFile file){

        return imgService.uploadImg(file);

    }

}
