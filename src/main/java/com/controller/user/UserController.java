package com.controller.user;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.domain.page.Paginator;
import com.google.common.collect.Maps;
import com.pojo.ShCompany;
import com.service.user.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by Huoyunren on 2016/1/27.
 */
@Controller
@RequestMapping("user")
public class UserController {
  @Autowired
  private UserService userService;

  // - 登录
  @RequestMapping(value = "login")
  @ResponseBody
  public Object login(@Param("email") String email, @Param("password") String password) {
    Object obj = userService.login(email, password);
    return obj;
  }

  // - 伪登录
  @RequestMapping(value = "fakeLogin")
  @ResponseBody
  public Object fakeLogin(@Param("email") String email, @Param("login") String login) {
    Object obj = userService.fakeLogin(email, login);
    return obj;
  }

  /**
   * 退出登录
   */
  @RequestMapping("logout")
  @ResponseBody
  public Object logout(HttpServletRequest request) {
    // - 退出登录清空session
    request.getSession().setAttribute("email", null);
    request.getSession().setAttribute("role", null);
    request.getSession().setAttribute("name", null);
    request.getSession().setAttribute("logo", null);
    return HttpResponse.create(Codes.LogoutOk);
  }

  // - 注册
  @RequestMapping("register")
  @ResponseBody
  public Object register(@Param("email") String email, @Param("referralCode") String referralCode, HttpServletRequest request) {
    Object obj = userService.register(email, referralCode);
    return obj;
  }

  // - 企业信息
  @RequestMapping("info")
  public Object info(HttpServletRequest request) {
    Object obj = request.getSession().getAttribute("email");
    ModelAndView modelAndView = new ModelAndView();
    if (obj == null){
      modelAndView.setViewName("/user/login");
      return modelAndView;
    }
    String email = null;
    if (obj != null) {
      email = obj.toString();
    }
    modelAndView.addObject(userService.info(email));
    return modelAndView;
  }

  // - 登录信息
  @RequestMapping("changepassword")
  public Object changepassword() {
    ModelAndView modelAndView = new ModelAndView();
    return modelAndView;
  }

  // - 更新资料
  @RequestMapping("update")
  @ResponseBody
  public Object update(@RequestBody ShCompany shCompany) {
    return userService.updateInfo(shCompany);
  }

  // - 修改密码
  @RequestMapping("updatepassword")
  @ResponseBody
  public Object updatePassword(@Param("email") String email, @Param("password") String password, @Param("oldPassword") String oldPassword) {
    return userService.updatePassword(password, email, oldPassword);
  }

  // - 查询所有用户
  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize, @Param("role") String role, @Param("queryKey") String queryKey) {
    Object obj = userService.list(pageNo, pageSize, queryKey, role);
    return obj;
  }
  // - 查询排名用户
  @RequestMapping("sortList")
  @ResponseBody
  public Object sortList(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize) {
    Object obj = userService.sortList();
    return obj;
  }

  // - 企业信息.只读
  @RequestMapping("queryinfo")
  public Object queryInfo(@Param("email") String email) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject(userService.queryInfo(email));
    modelAndView.setViewName("user/queryinfo");
    return modelAndView;
  }

  /**
   * 审核
   *
   * @param ids    审核用户ID集合，逗号分割
   * @param status 审核结果【1：审核通过，2：审核失败】
   * @return
   */
  @RequestMapping("check")
  @ResponseBody
  public Object check(@Param("ids") String ids, @Param("status") Integer status) {
    return userService.check(ids, status);
  }

  /**
   * 找回密码
   *
   * @return
   */
  @RequestMapping("findpassword")
  @ResponseBody
  public Object findPassword(@Param("email") String email) {
    return userService.findPassword(email);
  }

  // - 更新用户角色
  @RequestMapping("updateuserrole")
  @ResponseBody
  public Object updateUserRole(@Param("roleId") Integer roleId, @Param("email") String email) {
    return userService.updateUserRole(email, roleId);
  }

  // - 更新用户头像
  @RequestMapping("updatelogo")
  @ResponseBody
  public Object updateLogo(@Param("logo") String logo, HttpServletRequest request) {
    return userService.updateLogo(logo, request.getSession().getAttribute("email").toString());
  }
  // - 更新微信二维码
  @RequestMapping("updateWeixinQrcode")
  @ResponseBody
  public Object updateWeixinQrcode(@Param("weixinQrcode") String weixinQrcode, HttpServletRequest request) {
    return userService.updateWeixinQrcode(weixinQrcode, request.getSession().getAttribute("email").toString());
  }
  // - 更新支付宝二维码
  @RequestMapping("updateAlipayQrcode")
  @ResponseBody
  public Object updateAlipayQrcode(@Param("alipayQrcode") String alipayQrcode, HttpServletRequest request) {
    return userService.updateAlipayQrcode(alipayQrcode, request.getSession().getAttribute("email").toString());
  }

  // - 硬度加1
  @RequestMapping("incrincount")
  @ResponseBody
  public void incrInCount() {
    userService.updateInCount();
  }

  // - 流水统计
  @RequestMapping("waterStatistic")
  @ResponseBody
  public Object waterStatistic() {
    return userService.warterStatistic();
  }

  // - 平台数据统计
  @RequestMapping("platStatistic")
  @ResponseBody
  public Object platStatistic() {

    return userService.platStatistic(null);
  }

  // - 商户消费流水统计
  @RequestMapping("shopStatistic")
  @ResponseBody
  public Object shopStatistic(HttpServletRequest request) {

    return userService.platStatistic(request.getSession().getAttribute("email").toString());
  }

  // - 用户统计
  @RequestMapping("userStatistic")
  @ResponseBody
  public Object userStatistic() {

    return userService.userStatistic();
  }

  // - 总收益计算
  @RequestMapping("incomeStatistic")
  @ResponseBody
  public Object incomeStatistic() {
    Map<String, Object> map = Maps.newHashMap();
    map.put("totalIncome", userService.incomeStatistic(null, null));
    map.put("remainIncome", userService.incomeStatistic(null, 0));
    return HttpResponse.create(Codes.OK, "", map);
  }

  // - 门店收益计算
  @RequestMapping("incomeSingleStatistic")
  @ResponseBody
  public Object incomeSingleStatistic(@Param("buyerEmail") String buyerEmail, @Param("shopEmail") String shopEmail) {
    return HttpResponse.create(Codes.OK, "", userService.incomeSingleStatistic(buyerEmail, shopEmail));
  }

  // - 收益产生历史数据
  @RequestMapping("incomeList")
  @ResponseBody
  public Object incomeList(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize, @Param("shopId") Integer shopId) {
    return userService.incomeList(pageNo, pageSize, shopId);
  }

  // - 收益产生历史数据
  @RequestMapping("incomeListGroupByShopId")
  @ResponseBody
  public Object incomeListGroupByShopId(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize) {
    return userService.incomeListGroupByShop(pageNo, pageSize);
  }

  @RequestMapping("askDoctor")
  public ModelAndView askDoctor(@Param("email")String email) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("doctor",userService.info(email));
    modelAndView.setViewName("/user/askDoctor");
    return modelAndView;
  }


}
