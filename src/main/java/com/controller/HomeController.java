package com.controller;

import com.dto.UserExtDto;
import com.google.common.collect.Maps;
import com.mapper.generator.ShTagMapper;
import com.mapper.self.CompanyMapper;
import com.pojo.ShTag;
import com.service.user.UserService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
public class HomeController {
  private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

  @Autowired
  private ShTagMapper tagMapper;
  @Autowired
  private UserService userService;

  @RequestMapping(value = "nav")
  public Object nav() {
    ModelAndView modelAndView = new ModelAndView();
    return modelAndView;
  }

  @RequestMapping("login")
  public ModelAndView login() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/user/login");
    return modelAndView;
  }

  @RequestMapping("join")
  public ModelAndView join() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/user/join");
    return modelAndView;
  }

  @RequestMapping("auction")
  public ModelAndView auction(@Param("type") Integer type, @Param("status") Integer status) {
    ModelAndView modelAndView = new ModelAndView();
    Map<String, Object> map = Maps.newHashMap();
    map.put("type", type);
    map.put("status", status);
    modelAndView.addAllObjects(map);
    modelAndView.setViewName("advice/auctionList");
    return modelAndView;
  }

  @RequestMapping("reward")
  public ModelAndView reward(@Param("type") Integer type, @Param("status") Integer status) {
    ModelAndView modelAndView = new ModelAndView();
    Map<String, Object> map = Maps.newHashMap();
    map.put("type", type);
    map.put("status", status);
    modelAndView.addAllObjects(map);
    modelAndView.setViewName("advice/rewardList");
    return modelAndView;
  }

  @RequestMapping("note")
  public ModelAndView note(@Param("type") Integer type, @Param("status") Integer status) {
    ModelAndView modelAndView = new ModelAndView();
    Map<String, Object> map = Maps.newHashMap();
    map.put("type", type);
    map.put("status", status);
    modelAndView.addAllObjects(map);
    modelAndView.setViewName("advice/noteList");
    return modelAndView;
  }

  @RequestMapping("findpassword")
  public ModelAndView findpassword() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/user/findpassword");
    return modelAndView;
  }

  @RequestMapping("home")
  public ModelAndView home() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/home");
    return modelAndView;
  }

  @RequestMapping("success")
  public ModelAndView success(@Param("backUrl") String backUrl) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/success");
    modelAndView.addObject("backUrl", backUrl);
    return modelAndView;
  }

  @RequestMapping("timeout")
  public ModelAndView timeout() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/timeout");
    return modelAndView;
  }

  // - 设置
  @RequestMapping("set")
  public ModelAndView info(HttpServletRequest request) {
    Object obj = request.getSession().getAttribute("email");
    ModelAndView modelAndView = new ModelAndView();
    if (obj == null) {
      modelAndView.setViewName("/user/login");
      return modelAndView;
    }
    String email = obj.toString();
    modelAndView.addObject(userService.info(email));
    modelAndView.setViewName("/set/list");
    return modelAndView;
  }

  // - 设置
  @RequestMapping("msgSet")
  public ModelAndView msgSet(HttpServletRequest request) {
    Object obj = request.getSession().getAttribute("email");
    ModelAndView modelAndView = new ModelAndView();
    if (obj == null) {
      modelAndView.setViewName("/user/login");
      return modelAndView;
    }
    String email = obj.toString();
    modelAndView.addObject(userService.info(email));
    modelAndView.setViewName("/set/msg");
    return modelAndView;
  }

  @RequestMapping("myorder")
  public ModelAndView myOrder() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/order/myorder");
    return modelAndView;
  }
  @RequestMapping("attention")
  public ModelAndView attention() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/attention/list");
    return modelAndView;
  }

  @RequestMapping("user")
  public ModelAndView userList() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/user/list");
    return modelAndView;
  }

  @RequestMapping("doctorList")
  public ModelAndView doctorList(@Param("role") String role) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("role", role);
    modelAndView.setViewName("/user/doctorList");
    return modelAndView;
  }


  @RequestMapping("addGood")
  public ModelAndView addGood() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/product/add");
    return modelAndView;
  }

  @RequestMapping("orderMgr")
  public ModelAndView orderMgr() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/order/list");
    return modelAndView;
  }

  @RequestMapping("goodMgr")
  public ModelAndView goodMgr(@Param("email")String email) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/product/mgrList");
    modelAndView.addObject("email",email);
    return modelAndView;
  }

  @RequestMapping("goods")
  public ModelAndView goods(HttpServletRequest request) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/product/list");
    return modelAndView;
  }

  @RequestMapping("userMgr")
  public ModelAndView userMgr(@Param("role") String role) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("role", role);
    modelAndView.setViewName("/user/userMgr");
    return modelAndView;
  }

  @RequestMapping("income")
  public ModelAndView income() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/user/income");
    return modelAndView;
  }

  @RequestMapping("incomeDetail")
  public ModelAndView incomeDetail(@Param("shopId") Integer shopId) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/user/incomeDetail");
    modelAndView.addObject("shopId", shopId);
    return modelAndView;
  }

  @RequestMapping("admin")
  public ModelAndView admin() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/admin");
    return modelAndView;
  }

  @RequestMapping("platDataStatistic")
  public ModelAndView platDataStatistic(@Param("email")String email) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/plat/statistics");
    modelAndView.addObject("email",email);
    return modelAndView;
  }

  @RequestMapping("cashHistory")
  public ModelAndView cashHistory() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/cashHistory/list");
    return modelAndView;
  }

  @RequestMapping("shop")
  public ModelAndView shop() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/shop/list");
    return modelAndView;
  }

  @RequestMapping("about")
  public ModelAndView about() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/about");
    return modelAndView;
  }

  @RequestMapping("shopShow")
  public ModelAndView shopShow() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/shopShow");
    return modelAndView;
  }

  @RequestMapping("story")
  public ModelAndView story() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/story/list");
    return modelAndView;
  }

  @RequestMapping("bulletin")
  public ModelAndView bulletin() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/story/bulletinList");
    return modelAndView;
  }

  @RequestMapping("myStory")
  public ModelAndView myStory(@Param("email") String email) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("story/myList");
    UserExtDto userExtDto = userService.queryInfo(email);
    modelAndView.addObject("user", userExtDto);
    return modelAndView;
  }

  @RequestMapping("writeStory")
  public ModelAndView writeStory() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/story/story");
    return modelAndView;
  }

  @RequestMapping("writeBulletin")
  public ModelAndView writeBulletin() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/story/bulletin");
    return modelAndView;
  }

  @RequestMapping("previewStory")
  public ModelAndView previewStory(@Param("name") String name, @Param("title") String title, @Param("subTitle") String subTitle, @Param("content") String content, @Param("logo") String logo, @Param("chooseTag") Integer chooseTag) {
    ModelAndView modelAndView = new ModelAndView();
    Map<String, String> map = Maps.newHashMap();
    map.put("name", name);
    map.put("title", title);
    map.put("subTitle", subTitle);
    map.put("logo", logo);
    map.put("content", content);
    ShTag tag = tagMapper.selectByPrimaryKey(chooseTag);
    if (tag != null) {
      map.put("chooseTag", tag.getName());
    } else {
      map.put("chooseTag", "未分类");
    }
    modelAndView.setViewName("/story/preview");
    modelAndView.addAllObjects(map);
    return modelAndView;
  }

  @RequestMapping("productType")
  public ModelAndView productType(@Param("id") Integer id) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/productType/list");
    modelAndView.addObject("id", id);
    return modelAndView;
  }

  @RequestMapping("links")
  public ModelAndView links(@Param("id") Integer id) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/links");
    return modelAndView;
  }

  // - 跳转到吐槽页面
  @RequestMapping("advice")
  @ResponseBody
  public ModelAndView advice() {

    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/advice/advice");
    return modelAndView;
  }

  // - 搜索
  @RequestMapping("search")
  @ResponseBody
  public ModelAndView search() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/search");
    return modelAndView;
  }
}
