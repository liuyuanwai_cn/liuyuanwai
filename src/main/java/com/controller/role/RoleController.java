package com.controller.role;

import com.domain.HttpResponse;
import com.domain.page.Paginator;
import com.dto.RoleMenuDto;
import com.mapper.self.RoleMapper;
import com.service.role.RoleService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Administrator on 2016/2/18 0018.
 */
@Controller
@RequestMapping("role")
public class RoleController {

    private static final Logger LOG = LoggerFactory.getLogger(RoleController.class);
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleMapper roleMapper;


    @RequestMapping("add")
    @ResponseBody
    public Object add(@Param("roleName")String roleName,@Param("menuIds")String menuIds){

        return roleService.add(roleName, menuIds);
    }

    // - 更新角色权限
    @RequestMapping("update")
    @ResponseBody
    public Object update(@Param("roleName")String roleName,@Param("menuIds")String menuIds,@Param("roleId")Integer roleId){

        return roleService.update(roleName, menuIds,roleId);
    }

    @RequestMapping("list")
    @ResponseBody
    public Object list(@RequestBody Paginator paginator){

        return roleService.list(paginator.getPage(), paginator.getLimit());
    }
    // - 单纯查询角色列表
    @RequestMapping("querylist")
    @ResponseBody
    public Object querylist(){

        return roleMapper.list(0,roleMapper.count());
    }

    @RequestMapping("info")
    @ResponseBody
    public ModelAndView info(@Param("roleId")int roleId){
        ModelAndView modelAndView = new ModelAndView();
        RoleMenuDto roleMenuDto = roleService.info(roleId);
        modelAndView.addObject(roleMenuDto);
        modelAndView.setViewName("/role/info");
        return modelAndView;
    }

    @RequestMapping("del")
    @ResponseBody
    public Object del(@Param("roleIds")Integer[] roleIds){

        return roleService.del(roleIds);
    }



}
