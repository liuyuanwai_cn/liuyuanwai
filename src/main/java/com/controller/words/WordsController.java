package com.controller.words;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.google.common.collect.Maps;
import com.mapper.self.CompanyMapper;
import com.service.user.UserService;
import com.service.words.WordsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping("words")
public class WordsController {
  private static final Logger LOG = LoggerFactory.getLogger(WordsController.class);
  @Autowired
  private WordsService wordsService;


  @RequestMapping("add")
  @ResponseBody
  public Object add(@Param("from") String from, @Param("to") String to, @Param("content") String content) {
    return HttpResponse.create(Codes.OK,null,wordsService.add(from, to, content));
  }

  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize,@Param("to")String to){
    return wordsService.list(pageNo,pageSize,to);
  }
}
