package com.controller.advice;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.dto.AdviceDto;
import com.dto.NoteDto;
import com.service.advice.AdviceService;
import com.service.user.UserService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping("advice")
public class AdviceController {
  private static final Logger LOG = LoggerFactory.getLogger(AdviceController.class);

  @Autowired
  private AdviceService adviceService;
  @Autowired
  private UserService userService;

  // - 发表留言
  @RequestMapping(value = "add")
  @ResponseBody
  public Object add(@Param("simContent") String simContent, @Param("fee") BigDecimal fee, @Param("msgType") Integer msgType, @Param("isShowMobile") Integer isShowMobile, @Param("endTime") String endTime,@Param("img")String img,@Param("address")String address, HttpServletRequest request) {
    NoteDto note = adviceService.add(request.getSession().getAttribute("email").toString(), simContent, msgType, fee, isShowMobile, endTime, img, address);

    return HttpResponse.create(Codes.OK, "", note);
  }


  // - 消息列表
  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize,@Param("type")Integer type,@Param("status")Integer status,@Param("email")String email) {

    return adviceService.list(pageNo, pageSize,type,status,email);
  }


  // - 领赏
  @RequestMapping("reward")
  @ResponseBody
  public Object reward(HttpServletRequest request, @Param("uuid") String uuid, @Param("email") String email) {
    if (request.getSession().getAttribute("email") == null) {
      return HttpResponse.create(Codes.SystemDeclare, "请先登录");
    }
    String dealEmail = request.getSession().getAttribute("email").toString();
    return HttpResponse.create(Codes.OK, "", adviceService.reward(uuid, dealEmail, email));
  }

  // - 跳转到竞价页面
  @RequestMapping("toAuction")
  @ResponseBody
  public ModelAndView toAuction(@Param("uuid") String uuid) {

    ModelAndView modelAndView = new ModelAndView();
    AdviceDto adviceDto = adviceService.queryByUuid(uuid);
    adviceDto.setEndTime(adviceDto.getEndTime().substring(0, adviceDto.getEndTime().length() - 2));
    modelAndView.addObject("advice", adviceDto);
    modelAndView.setViewName("/advice/auction");
    return modelAndView;
  }


  // - 删除
  @RequestMapping("del")
  @ResponseBody
  public Object del(@Param("uuid") String uuid) {
    adviceService.del(uuid);
    return HttpResponse.OK();
  }

}
