package com.controller.link;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.service.link.LinkService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2016/2/18 0018.
 */
@Controller
@RequestMapping("link")
public class LinkController {

  private static final Logger LOG = LoggerFactory.getLogger(LinkController.class);

  @Autowired
  private LinkService linkService;


  @RequestMapping("saveOrUpdate")
  @ResponseBody
  public Object saveOrUpdate(@Param("linkId") Integer linkId, @Param("linkName") String linkName, @Param("linkUrl") String linkUrl, @Param("linkDesc") String linkDesc) {

    return linkService.saveOrUpdate(linkId, linkName, linkUrl, linkDesc);
  }

  @RequestMapping("list")
  @ResponseBody
  public Object list() {

    Object list = linkService.list();
    return HttpResponse.create(Codes.OK, "", list);
  }

  @RequestMapping("del")
  @ResponseBody
  public Object del(@Param("linkId") Integer linkId) {

    return linkService.del(linkId);
  }

  @RequestMapping("incrClickCount")
  @ResponseBody
  public Object incrClickCount(@Param("linkId") Integer linkId) {

    return linkService.incrClickCount(linkId);
  }


}
