package com.controller.code;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.domain.myenum.CashCodeEnum;
import com.pojo.ShCashHistory;
import com.service.code.CashHistoryService;
import com.service.user.UserService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;


/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping("cashHistory")
public class CashHistoryController {
  private static final Logger LOG = LoggerFactory.getLogger(CashHistoryController.class);

  @Autowired
  private CashHistoryService cashHistoryService;
  @Autowired
  private UserService userService;


  // - 发送验证码
  @RequestMapping("sendCashCode")
  @ResponseBody
  public Object sendCashCode(@Param("buyerEmail") String buyerEmail,@Param("shopEmail")String shopEmail, @Param("cashAmount") BigDecimal cashAmount) {
    return cashHistoryService.sendCashCode(buyerEmail,shopEmail, cashAmount);
  }

  ;

  // - 提现
  @RequestMapping("cashAmount")
  @ResponseBody
  public Object cashAmount(@Param("buyerEmail") String buyerEmail,@Param("shopEmail") String shopEmail, @Param("cashAmount") BigDecimal cashAmount, @Param("cashCode") Integer cashCode) {
    return cashHistoryService.cashAmount(buyerEmail,shopEmail, cashAmount, cashCode);
  }

  ;

  // - 提现记录
  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize ,HttpServletRequest request) {

    return cashHistoryService.list(pageNo,pageSize,request.getSession().getAttribute("email").toString());
  }


}
