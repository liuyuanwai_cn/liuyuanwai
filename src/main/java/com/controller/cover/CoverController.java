package com.controller.cover;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.domain.myenum.CheckStatusEnum;
import com.domain.page.Paginator;
import com.dto.CoverDto;
import com.mapper.generator.ShCoverMapper;
import com.mapper.self.CoverMapper;
import com.pojo.ShCover;
import com.service.cover.CoverService;
import com.utils.DateUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping("cover")
public class CoverController {
    private static final Logger LOG = LoggerFactory.getLogger(CoverController.class);

    @Autowired
    private CoverService coverService;
    @Autowired
    private CoverMapper coverMapper;
    @Autowired
    private ShCoverMapper shCoverMapper;


    @RequestMapping(value = "save")
    @ResponseBody
    public Object save(HttpServletRequest request,@Param("show")String show,@Param("name")String name,@Param("remark")String remark,@Param("logo")String logo,@Param("startDate")String startDate,@Param("endDate")String endDate){
        coverService.add(request.getSession().getAttribute("email").toString(),show,name,remark,logo,startDate,endDate);
        return HttpResponse.create(Codes.OK);

    }
    // - 审核
    @RequestMapping(value = "check")
    @ResponseBody
    public Object check(@Param("ids")String ids,@Param("status")String status,@Param("reason")String reason){

        return coverService.check(ids, status,reason);

    }

    @RequestMapping(value = "list")
    @ResponseBody
    public Object list(@RequestBody Paginator paginator,HttpServletRequest request){
        List<ShCover> list = coverService.list(paginator.getPage(),paginator.getLimit(),request.getSession().getAttribute("email").toString());
        paginator.setItems(list);
        paginator.setTotalCount(coverMapper.count(null));
        return paginator;
    }


    @RequestMapping(value = "info")
    public ModelAndView info(@Param("id")Integer id){
        ShCover shCover = shCoverMapper.selectByPrimaryKey(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(shCover);
        modelAndView.setViewName("/cover/info");
        return modelAndView;

    }


    // - 申请加入
    @RequestMapping(value = "join")
    public ModelAndView join(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        ShCover shCover = coverMapper.queryByEmail(request.getSession().getAttribute("email").toString());
        if (shCover == null){
            shCover = new ShCover();
            shCover.setUrl(request.getContextPath()+"");
            shCover.setLogo(request.getContextPath()+"");
        }
        CoverDto coverDto = new CoverDto();
        BeanUtils.copyProperties(shCover,coverDto);
        if (shCover.getStartdate()!=null){
            coverDto.setStartDate(DateUtils.getFormatDate("yyyy-MM-dd", shCover.getStartdate()));
        }
        if (shCover.getEnddate()!=null){
            coverDto.setEndDate(DateUtils.getFormatDate("yyyy-MM-dd",shCover.getEnddate()));
        }
        coverDto.setStatus(CheckStatusEnum.WaitCheck.getCode());
        modelAndView.setViewName("/cover/join");
        modelAndView.addObject(coverDto);
        return modelAndView;
    }





}
