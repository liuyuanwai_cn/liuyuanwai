package com.controller.product;

import com.domain.myenum.ProductUnitEnum;
import com.mapper.generator.ShProductMapper;
import com.pojo.ShProduct;
import com.service.product.ProductService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2016/2/18 0018.
 */
@Controller
@RequestMapping("product")
public class ProductController {

    private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);
    @Autowired
    private ProductService productService;
    @Autowired
    private ShProductMapper shProductMapper;


    @RequestMapping("add")
    @ResponseBody
    public Object add(@RequestBody ShProduct shProduct, HttpServletRequest httpServletRequest) {

        shProduct.setCompanyEmail(httpServletRequest.getSession().getAttribute("email").toString());
        return productService.save(shProduct);
    }

    // - 更新商品
    @RequestMapping("update")
    @ResponseBody
    public Object update(@RequestBody ShProduct shProduct) {

        return productService.update(shProduct);
    }

    // - 更新商品图片
    @RequestMapping("updateimg")
    @ResponseBody
    public Object updateProductImg(@Param("productId") Integer productId, @Param("url") String url) {

        return productService.updateProductImg(productId, url);
    }

    // - 上/下架商品
    @RequestMapping("updateProductStatus")
    @ResponseBody
    public Object updateProductStatus(@Param("productId") Integer productId, @Param("productStatus") Integer productStatus) {

        return productService.updateProductStatus(productId, productStatus);
    }

    @RequestMapping("list")
    @ResponseBody
    public Object list(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize, @Param("email") String email,@Param("soldOut")Integer soldOut,@Param("productType")Integer productType) {
        return productService.list(pageNo, pageSize, 0, email,soldOut,productType);
    }

    @RequestMapping("mgrList")
    @ResponseBody
    public Object mgrList(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize, @Param("email") String email,@Param("productType")Integer productType) {
        return productService.mgrList(pageNo, pageSize, null, email,null,productType);
    }


    @RequestMapping("info")
    @ResponseBody
    public ModelAndView info(@Param("productId") int productId) {
        ModelAndView modelAndView = new ModelAndView();
        ShProduct shProduct = shProductMapper.selectByPrimaryKey(productId);
        modelAndView.addObject(shProduct);
        modelAndView.setViewName("/product/info");
        return modelAndView;
    }

    @RequestMapping("del")
    @ResponseBody
    public Object del(@Param("productIds") Integer[] productIds) {

        return productService.batchDel(productIds);
    }

    @RequestMapping("calIncome")
    @ResponseBody
    public Object calIncome(@Param("productId") Integer productId) {

        return productService.calIncome(productId);
    }
    @RequestMapping("incrReadCount")
    @ResponseBody
    public Object incrReadCount(@Param("productId") Integer productId) {

        return productService.incrReadCount(productId);
    }


}
