package com.controller.risk;

import com.domain.page.PageDto;
import com.service.risk.RiskService;
import com.google.gson.Gson;
import com.mapper.self.RiskUserMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping(value = "risk")
public class RiskController {
  @Autowired
  private RiskUserMapper riskUserMapper;
  @Autowired
  private RiskService riskService;
  @Autowired
  private Gson gson;


  @RequestMapping(value = "search")
  @ResponseBody
  public Object search(@RequestBody PageDto pageDto) {


    return riskService.query(pageDto);
  }

  @RequestMapping(value = "batchDel")
  @ResponseBody
  public Object batchDel(@Param("idArr") Integer[] idArr) {

    return riskService.batchDel(idArr);
  }



  @RequestMapping(value = "importExcel")
  @ResponseBody
  public Object importExcel(@RequestParam("filePath") CommonsMultipartFile file) {

    return riskService.importExcel(file);
  }


  /**
   * 下载模版
   *
   * @return
   */
  @RequestMapping(value = "downTemplate")
  public Object downTemplate() {

    return riskService.downTemplate();
  }


  /**
   * 导出
   *
   * @return
   */
  @RequestMapping(value = "exportExcel")
  public Object exportExcel() {

    return riskService.exportExcel();
  }


  /**
   * 时域统计
   * @param type year,month,day
   * @return
   */
  @RequestMapping(value = "statistic")
  @ResponseBody
  public Object statistic(@Param("type")String type) {

    return riskService.timeStatistic(type);
  }


}
