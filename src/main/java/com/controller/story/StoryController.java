package com.controller.story;

import com.domain.HttpResponse;
import com.dto.StoryExtDto;
import com.mapper.generator.ShStoryMapper;
import com.pojo.ShTag;
import com.service.story.StoryService;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.List;

/**
 * Created by Huoyunren on 2016-07-01.
 */
@Controller
@RequestMapping("story")
public class StoryController {
  @Autowired
  private StoryService storyService;
  @Autowired
  private ShStoryMapper storyMapper;

  @RequestMapping("list")
  @ResponseBody
  @ApiOperation(value = "条件查询文章列表", httpMethod = "GET",response = Collection.class, notes = "条件查询文章列表")
  private Object list(@ApiParam(required = true) @Param("pageNo") Integer pageNo,
                      @ApiParam(required = true) @Param("pageSize") Integer pageSize,
                      @ApiParam(required = false) @Param("email") String email,
                      @ApiParam(required = false) @Param("orderField") String orderField,
                      @ApiParam(required = false) @Param("storyType") Integer storyType,
                      @ApiParam(required = false) @Param("tag") Integer tag) {
    return storyService.list(pageNo, pageSize, email, orderField, storyType, tag);
  }

  @RequestMapping("bulletin")
  @ResponseBody
  public Object bulletin(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize, @Param("email") String email, @Param("orderField") String orderField, @Param("storyType") Integer storyType) {
    return storyService.bulletin(pageNo, pageSize, email, orderField, storyType);
  }

  @RequestMapping("save")
  @ResponseBody
  public Object save(@Param("title") String title, @Param("subTitle") String subTitle, @Param("simContent") String simContent, @Param("email") String email, @Param("img") String img, @Param("storyType") Integer storyType, @Param("chooseTag") Integer chooseTag) {
    return storyService.save(email, title, subTitle, simContent, img, storyType, chooseTag);
  }

  @RequestMapping("info")
  public ModelAndView info(@Param("uuid") String uuid) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/story/info");
    StoryExtDto storyExtDto = storyService.queryByUuid(uuid);
    modelAndView.addObject("story", storyExtDto);
    return modelAndView;
  }

  @RequestMapping("incrReadCount")
  @ResponseBody
  public Object incrReadCount(@Param("uuid") String uuid) {
    storyService.incrReadCount(uuid);
    return HttpResponse.OK();
  }

  @RequestMapping("del")
  @ResponseBody
  public Object del(@Param("storyId") Integer storyId, @Param("authEmail") String authEmail) {
    storyService.del(storyId, authEmail);
    return HttpResponse.OK();
  }
}
