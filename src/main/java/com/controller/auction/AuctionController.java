package com.controller.auction;

import com.domain.HttpResponse;
import com.domain.ex.BizException;
import com.domain.ex.Codes;
import com.service.auction.AuctionService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * Created by Huoyunren on 2016/5/24.
 */
@Controller
@RequestMapping("auction")
public class AuctionController {

  @Autowired
  private AuctionService auctionService;

  @RequestMapping("auction")
  @ResponseBody
  public Object auction(@Param("price")BigDecimal price,@Param("uuid")String uuid,@Param("maxPrice")BigDecimal maxPrice,HttpServletRequest request){
    Object obj = request.getSession().getAttribute("email");
    String email = "";
    if (obj != null){
      email = obj.toString();
    }else {
      throw BizException.create(Codes.SystemDeclare,"登录超时");
    }
    auctionService.auction(price,uuid,email,maxPrice);
    return HttpResponse.create(Codes.OK,"",auctionService.queryLatestByEmailAndUuid(email,uuid));
  }

  @RequestMapping("list")
  @ResponseBody
  public Object list(@Param("uuid")String uuid,@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize){

    return auctionService.list(uuid,pageNo,pageSize);
  }

}
