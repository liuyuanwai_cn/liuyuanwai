package com.controller.set;

import com.domain.HttpResponse;
import com.service.set.SetService;
import com.service.user.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Huoyunren on 2016-08-04.
 */
@Controller
@RequestMapping("set")
public class SetController {
  @Autowired
  private SetService setService;
  @Autowired
  private UserService userService;


  @RequestMapping("saveOrUpdate")
  @ResponseBody
  public Object saveOrUpdate(@Param("storyNotify") Integer storyNotify, @Param("noteNotify") Integer noteNotify, @Param("attentionNotify") Integer attentionNotify) {

    setService.saveOrUpdate(storyNotify, noteNotify,attentionNotify);
    return HttpResponse.OK();
  }

  // - 设置
  @RequestMapping("info")
  public ModelAndView info(HttpServletRequest request) {
    Object obj = request.getSession().getAttribute("email");
    ModelAndView modelAndView = new ModelAndView();
    if (obj == null) {
      modelAndView.setViewName("/user/login");
      return modelAndView;
    }
    String email = obj.toString();
    modelAndView.addObject(userService.info(email));
    modelAndView.setViewName("/set/info");
    return modelAndView;
  }
}
