package com.controller.note;

import com.domain.HttpResponse;
import com.domain.ex.Codes;
import com.service.advice.AdviceService;
import com.service.note.NoteService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2015/11/22 0022.
 */
@Controller
@RequestMapping("note")
public class NoteController {
  private static final Logger LOG = LoggerFactory.getLogger(NoteController.class);
  @Autowired
  private NoteService noteService;

  // - 发表留言
  @RequestMapping(value = "add")
  @ResponseBody
  public Object add(@Param("content") String content, @Param("uuid") String uuid,@Param("toEmail")String toEmail,@Param("title")String title) {

    return HttpResponse.create(Codes.OK,"",noteService.add(uuid, content,toEmail,title));
  }
  @RequestMapping(value = "list")
  @ResponseBody
  public Object list(@Param("pageNo") Integer pageNo, @Param("uuid") String uuid,@Param("pageSize") Integer pageSize) {

    return noteService.list(uuid,pageNo,pageSize);
  }

}
