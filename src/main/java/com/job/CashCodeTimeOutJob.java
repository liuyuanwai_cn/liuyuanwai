package com.job;

import com.mapper.self.CashHistoryMapper;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by Huoyunren on 2016/4/28.
 *
 */
public class CashCodeTimeOutJob implements Job,InitializingBean {
  private static final Logger LOG = LoggerFactory.getLogger(CashCodeTimeOutJob.class);
  @Autowired
  private CashHistoryMapper cashHistoryMapper;
  @Autowired
  private Scheduler scheduler;


  @Override
  public void afterPropertiesSet() throws Exception {
    if (scheduler.checkExists(JobKey.jobKey("CashCodeTimeOutJob"))){
      return;
    }
    JobDetail job = JobBuilder
      .newJob(CashCodeTimeOutJob.class)
      .withIdentity("CashCodeTimeOutJob")
      .build();
    Trigger trigger = TriggerBuilder
      .newTrigger()
      .withIdentity("CashCodeTimeOutJob")
      .withSchedule(CronScheduleBuilder.cronSchedule("0 0/10 * * * ?"))
      .build();
    this.scheduler.scheduleJob(job,trigger);
  }

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    LOG.info("提现码超时处理任务【每10分钟执行一次】");
    cashHistoryMapper.dealTimeOutCashCode();
  }
}
