package com.job;

import com.mapper.self.OrderMapper;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by Huoyunren on 2016/4/28.
 *
 */
public class OrderTimeOutJob implements Job,InitializingBean{
  private static final Logger LOG = LoggerFactory.getLogger(OrderTimeOutJob.class);
  @Autowired
  private OrderMapper orderMapper;
  @Autowired
  private Scheduler scheduler;

  @Override
  public void afterPropertiesSet() throws Exception {
    if (scheduler.checkExists(JobKey.jobKey("OrderTimeOutJob"))){
      return;
    }
    JobDetail job = JobBuilder
      .newJob(OrderTimeOutJob.class)
      .withIdentity("OrderTimeOutJob")
      .build();
    Trigger trigger = TriggerBuilder
      .newTrigger()
      .withIdentity("OrderTimeOutJob")
      .withSchedule(CronScheduleBuilder.cronSchedule("0 0/30 * * * ?"))
      .build();
    this.scheduler.scheduleJob(job,trigger);
  }

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    LOG.info("订单超时处理任务【每30分钟执行一次】");
    orderMapper.dealTimeOutOrder();
  }
}
