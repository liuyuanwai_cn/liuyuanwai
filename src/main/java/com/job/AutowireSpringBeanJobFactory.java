package com.job;

import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

/**
 * Created by Huoyunren on 2016/5/26.
 */
public class AutowireSpringBeanJobFactory  extends SpringBeanJobFactory {
  @Autowired
  private AutowireCapableBeanFactory factory;

  /**
   * {@inheritDoc}
   */
  @Override
  protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
    Object bean = super.createJobInstance(bundle);
    factory.autowireBean(bean);
    return bean;
  }
}
