### 关于劉員外

> 寻梦？撑一支长篙，向青草更青处漫溯


### 一张图了解网站功能
![输入图片说明](http://git.oschina.net/uploads/images/2016/0723/165656_f08cfb34_858177.jpeg "在这里输入图片标题")
### 演示

> 地址：[http://liuyuanwai.cn](http://liuyuanwai.cn)

### 开源 

> 网站使用开源技术编码，开发工具也是免费。网站集B端和C端一体，多终端自动适配，兼容性还不错。业务上满足商家基本销售，统计，营销功能，并且提供用户和商家互动社区。网站所有资料开源，商家可以免费商用。如果您觉得有可取之处，欢迎后期功能定制和二次开发

### 联系


> QQ群号：221052890
> 公众号：劉員外

### 商户演示帐号



> 834989221@qq.com/1234
> 备注：该帐号为本人线上用帐号。还望大家只做功能浏览，不要编辑内容。谢谢大家
 
### 资讯

- [劉員外网站功能详解](http://www.liuyuanwai.cn/story/info?uuid=b3dc23069f2d48a5954d167461864881)

- [商户后台功能截图](http://liuyuanwai.cn/story/info?uuid=29685bb02b564e5795579324f7f4e81d)

- [阅读最多好文：文中自有颜如玉](http://liuyuanwai.cn/story/info?uuid=16eb1a9a68e7448fa80d3b8b36b481aa)

- [刘员外部署运行手册](http://liuyuanwai.cn/story/info?uuid=a96f561677734d5e960c30d8a4ecb78d)

- [Java代码托管到gitosc，绑定Mopaas部署运行步骤](http://liuyuanwai.cn/story/info?uuid=f0c9a69ff75a4a569bb4bd647e815022)